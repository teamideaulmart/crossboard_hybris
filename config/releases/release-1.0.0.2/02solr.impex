#
# Import the Solr configuration
$searchIndexNamePrefix=crossboard
$solrIndexedType=crossboardProductType

# Non-facet properties
INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);sortableType(code);currency[default=false];localized[default=false];multiValue[default=false];useForSpellchecking[default=false];useForAutocomplete[default=false];fieldValueProvider;valueProviderParameter;ftsPhraseQuery[default=false];ftsPhraseQueryBoost;ftsQuery[default=false];ftsQueryBoost;ftsFuzzyQuery[default=false];ftsFuzzyQueryBoost;ftsWildcardQuery[default=false];ftsWildcardQueryType(code)[default=POSTFIX];ftsWildcardQueryBoost;ftsWildcardQueryMinTermLength
;$solrIndexedType; merchantUrl            ;string ;			   ;    ;	 ;    ;    ;    ;productMerchantUrlProvider                  ;merchantUrl	; ; ; ; ; ; ;    ;   ;  ;

INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);sortableType(code);currency[default=false];localized[default=false];multiValue[default=false];useForSpellchecking[default=false];useForAutocomplete[default=false];fieldValueProvider;valueProviderParameter;ftsPhraseQuery[default=false];ftsPhraseQueryBoost;ftsQuery[default=false];ftsQueryBoost;ftsFuzzyQuery[default=false];ftsFuzzyQueryBoost;ftsWildcardQuery[default=false];ftsWildcardQueryType(code)[default=POSTFIX];ftsWildcardQueryBoost;ftsWildcardQueryMinTermLength
;$solrIndexedType; priceFrom             ;double ;            ;true;    ;    ;    ;    ;productFromPriceProvider         ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;

INSERT_UPDATE SolrIndexerQuery;solrIndexedType(identifier)[unique=true];identifier[unique=true];type(code);injectCurrentDate[default=true];injectCurrentTime[default=true];injectLastIndexTime[default=true];query;user(uid)
;$solrIndexedType;$searchIndexNamePrefix-fullQuery;full;;;false;"
SELECT tbl.pk FROM (
{{
SELECT {bp.PK} as PK FROM {Product! as bp}
WHERE {bp:merchant} IS NOT NULL AND {bp.variantType} IS NULL
AND ({bp.deleted} = 0 OR {bp.deleted} IS NULL)
AND EXISTS (
{{
  SELECT {mcj:category} FROM {MerchantConfig AS mcj}
    WHERE {bp:merchant} = {mcj:merchant}
    AND {mcj:category} IN (  {{ SELECT {cat:source} FROM {CategoryProductRelation as cat} WHERE {cat:target}={bp:pk} }}  )
    AND {mcj:displayAllowed}=1 AND {mcj.category} NOT IN ( {{ SELECT {vc.PK} FROM {VariantCategory! as vc} }})
}} )
AND EXISTS (
    {{
	    SELECT {PK} FROM {StockLevel as s}
		WHERE {s.productCode} = {bp.code}
		AND ({s.available} > 0
			OR {s.inStockStatus} IN ({{
						SELECT {iss.pk} FROM {InStockStatus as iss} WHERE {iss.code}='forceInStock'
					}})
      		)
}} )
AND EXISTS (
{{
	SELECT {PK} FROM {PriceRow as pl} WHERE ({pl.productId} = {bp.code} OR {pl.product} = {bp.pk})
	AND (
		({pl.startTime} IS NULL AND {pl.endTime} IS NULL) OR
		(sysdatetime() BETWEEN {pl.startTime} AND {pl.endTime})
            )
}}
)
}}

UNION ALL
{{
SELECT max(vvv.PK) as PK FROM (
{{
SELECT {vp.PK} as PK
, max(CASE WHEN {vc.code} = 'var_Color' THEN {vvc.code} END) as vc1
, max(CASE WHEN {vc.code} = 'var_Size' THEN {vvc.code} END) as vc2
, max(CASE WHEN {vc.code} = 'var_Material' THEN {vvc.code} END) as vc3
, max(CASE WHEN {vc.code} = 'var_BedSize' THEN {vvc.code} END) as vc4
, max(CASE WHEN {vc.code} = 'var_BeltsLength' THEN {vvc.code} END) as vc5
, max(CASE WHEN {vc.code} = 'var_CapSize' THEN {vvc.code} END) as vc6
, max(CASE WHEN {vc.code} = 'var_CapBase' THEN {vvc.code} END) as vc7
, max(CASE WHEN {vc.code} = 'var_Capacity' THEN {vvc.code} END) as vc8
, max(CASE WHEN {vc.code} = 'var_Gender' THEN {vvc.code} END) as vc9
, max(CASE WHEN {vc.code} = 'var_Power' THEN {vvc.code} END) as vc10
, max(CASE WHEN {vc.code} = 'var_MemoryCapacity' THEN {vvc.code} END) as vc11
, max(CASE WHEN {vc.code} = 'var_Width' THEN {vvc.code} END) as vc12
, max(CASE WHEN {vc.code} = 'var_Length' THEN {vvc.code} END) as vc13
, max(CASE WHEN {vc.code} = 'var_Strength' THEN {vvc.code} END) as vc14
, max(CASE WHEN {vc.code} = 'var_Diameter' THEN {vvc.code} END) as vc15
, max(CASE WHEN {vc.code} = 'var_Radius' THEN {vvc.code} END) as vc16
, max(CASE WHEN {vc.code} = 'var_Platform' THEN {vvc.code} END) as vc17
, max(CASE WHEN {vc.code} = 'var_Model' THEN {vvc.code} END) as vc18
, max(CASE WHEN {vc.code} = 'var_ScreenSize' THEN {vvc.code} END) as vc19
, max(CASE WHEN {vc.code} = 'var_ColorNumber' THEN {vvc.code} END) as vc20
FROM {GenericVariantProduct! as vp}, {Product! as bp JOIN  CategoryProductRelation as cpr ON {cpr.target} = {bp.pk}
	JOIN Category2VariantCategory as cvc ON {cvc.source} = {cpr.source}
	JOIN VariantCategory as vc ON {vc.PK} = {cvc.target}
	JOIN CategoryCategoryRelation as ccr ON {ccr.source} = {cvc.target}
	JOIN CategoryProductRelation as vpr ON {vpr.source} = {ccr.target}
    	JOIN VariantValueCategory as vvc ON {vvc.pk} = {vpr.source}
	}
WHERE {vp:merchant} IS NOT NULL
AND ({bp.deleted} = 0 OR {bp.deleted} IS NULL)
AND {vp.baseProduct} = {bp.pk} AND {vpr.target} = {vp.pk}
AND EXISTS (
{{
	SELECT {mcj:category} FROM {MerchantConfig AS mcj}
	WHERE {bp:merchant} = {mcj:merchant}
	AND {mcj:category} IN (  {{ SELECT {cat:source} FROM {CategoryProductRelation as cat} WHERE {cat:target}={bp:pk} }}  )
	AND {mcj:displayAllowed}=1 AND {mcj.category} NOT IN ( {{ SELECT {vc.PK} FROM {VariantCategory! as vc} }})
}} )
AND EXISTS (
{{
	SELECT {PK} FROM {StockLevel as s}
	WHERE {s.productCode} = {vp.code}
	AND ( ({s.available} > 0)
	OR {s.inStockStatus} IN ({{ SELECT {iss.pk} FROM {InStockStatus as iss} WHERE {iss.code}='forceInStock' }}))
}} )
AND EXISTS (
{{
	SELECT {PK} FROM {PriceRow as pl} WHERE ({pl.productId} = {vp.code} OR {pl.product} = {vp.PK} )
	AND (({pl.startTime} IS NULL AND {pl.endTime} IS NULL) OR (sysdatetime() BETWEEN {pl.startTime} AND {pl.endTime}))
}}
)
GROUP BY {vp.PK}
}}) as vvv
GROUP BY vvv.vc1, vvv.vc2, vvv.vc3, vvv.vc4, vvv.vc5, vvv.vc6, vvv.vc7, vvv.vc8, vvv.vc9,
vvv.vc10, vvv.vc11, vvv.vc12, vvv.vc13, vvv.vc14, vvv.vc15, vvv.vc16, vvv.vc17, vvv.vc18, vvv.vc19, vvv.vc20
}}

UNION ALL

{{
SELECT {bp.PK} as PK FROM {Product! as bp}
WHERE {bp:merchant} IS NOT NULL
AND ({bp.deleted} = 0 OR {bp.deleted} IS NULL)
AND {bp.variantType} IS NOT NULL
AND EXISTS (
{{
	SELECT {c.PK} FROM {Category! as c JOIN CategoryProductRelation as cpr ON {c.pk} = {cpr.source}}
	WHERE {cpr.target} = {bp.pk} AND NOT EXISTS ({{ SELECT {PK} FROM {Category2VariantCategory as cvc} WHERE {cvc.source} = {c.pk}  }})
}} )
AND EXISTS (
{{
  SELECT {mcj:category} FROM {MerchantConfig AS mcj}
    WHERE {bp:merchant} = {mcj:merchant}
    AND {mcj:category} IN (  {{ SELECT {cat:source} FROM {CategoryProductRelation as cat} WHERE {cat:target}={bp:pk} }}  )
    AND {mcj:displayAllowed}=1 AND {mcj.category} NOT IN ( {{ SELECT {vc.PK} FROM {VariantCategory! as vc} }})
}} )
AND EXISTS (
    {{
	    SELECT {s.PK} FROM {GenericVariantProduct! as vp}, {StockLevel as s}
		WHERE {vp.baseProduct} = {bp.PK}
		AND {s.productCode} = {vp.code}
		AND ({s.available} > 0
			OR {s.inStockStatus} IN ({{
						SELECT {iss.pk} FROM {InStockStatus as iss} WHERE {iss.code}='forceInStock'
					}})
      		)
}} )
AND EXISTS (
{{
	SELECT {vp.PK} FROM {GenericVariantProduct! as vp}, {PriceRow as pl}
	WHERE {vp.baseProduct} = {bp.PK} AND ({pl.productId} = {vp.code} OR {pl.product} = {vp.PK} )
	AND (
		({pl.startTime} IS NULL AND {pl.endTime} IS NULL) OR
		(sysdatetime() BETWEEN {pl.startTime} AND {pl.endTime})
            )
}}
)
}}
) tbl
";anonymous



