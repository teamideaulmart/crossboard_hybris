
<!-- START $contextName -->
#if($contextDescription && $contextDescription != '')
                        <!-- $contextDescription   $test -->
#end
                                                                                                                  
<Context someattr="$contextDescription" path="$contextPath" docBase="$contextDocBase" $!contextAdditionalAttributes>                                                                                                                          
#if($contextLoader && $contextLoader != '')                                                                       
                                $contextLoader            
#end                                                                                                           
#if($contextAdditionalElements && $contextAdditionalElements != '')                                                 
                                $contextAdditionalElements
#end


<Valve className="com.orangefunction.tomcat.redissessions.RedisSessionHandlerValve" />
<Manager className="com.orangefunction.tomcat.redissessions.RedisSessionManager"
         host="$redisHost"
         port="$redisPort"
         database="$redisDatabase"
         maxInactiveInterval="$redisMaxInactiveInterval"
         sessionPersistPolicies="$redisSessionPersistPolicies"
         
           />


</Context>

<!-- END $contextName -->
