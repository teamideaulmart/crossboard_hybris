#!/usr/bin/env bash

###############################
# This script is needed in order to prevent Spring Integration from freezing
# when NFS fails and shared folder's descriptor changes.
# The script continuously copies files from shared folder to local folder,
# so descriptor stays the same.
###############################

NFS_PROCESSING_FOLDER='/opt/data/int/processing/*.xml'

LOCAL_PROCESSING_FOLDER='/opt/hybris/hybris/data/processing/'

TMP_NAME=$LOCAL_PROCESSING_FOLDER'__TMPFILE'

for file in $NFS_PROCESSING_FOLDER
do
	cp -u $file $TMP_NAME
	ORIG_MD5=$(md5sum $file | awk '{print $1}')
	TMP_MD5=$(md5sum $TMP_NAME | awk '{print $1}')
	filename=$(basename $file)
	if [ "$ORIG_MD5" == "$TMP_MD5" ]; then
		rm -f $file && mv $TMP_NAME ${LOCAL_PROCESSING_FOLDER}${filename} || rm -f $TMP_NAME 
	else
		rm -f $TMP_NAME
	fi
done
