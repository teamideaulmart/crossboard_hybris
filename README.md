# Установка
В корне репозиторя выполнить ant без параметров (или ant install)


Система скачает hybris, создаст внутри симлинк на папку config

Нужно выполнить ant customize


После этого нужно внести локальные настройки в файл config/developer.properties 



После добавления настроек выполнить ant clean all внутри платформы, запустить сервер, инициализировать систему.



Система сборки при каждом запуске будет собирать файл local.properties из следующих файлов: 
  * config//common.properties
  * config//developer.properties
  * config//instances//HOSTNAME//ВСЕ_ФАЙЛЫ.properties

Где HOSTNAME - реальное имя машины, на которой производится сборка.

Так же система скопирует из той же папки HOSTNAME файлы localextensions.xml , папку с конфигом tomcat и папку с конфигом solr




