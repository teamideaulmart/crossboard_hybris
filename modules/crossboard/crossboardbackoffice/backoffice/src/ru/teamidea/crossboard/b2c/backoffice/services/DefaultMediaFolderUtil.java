package ru.teamidea.crossboard.b2c.backoffice.services;

import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.servicelayer.internal.service.ServicelayerUtils;
import de.hybris.platform.servicelayer.media.MediaService;

/**
 * Created by vitalii on 26.10.16.
 */
@SuppressWarnings("unused")
public class DefaultMediaFolderUtil {

    public static final String IMAGES_FOLDER_NAME = "images";
    public static final MediaFolderModel FOLDER = getMediaFolder(IMAGES_FOLDER_NAME);

    public static MediaFolderModel getMediaFolder(String name) {
        MediaService mediaService = ServicelayerUtils.getApplicationContext().getBean(MediaService.class);
        if (mediaService != null) {
            return mediaService.getFolder(name);
        }
        return null;
   }
}

