package ru.teamidea.crossboard.b2c.backoffice.renderers;

import com.hybris.cockpitng.core.config.impl.jaxb.listview.ListColumn;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.widgets.common.WidgetComponentRenderer;
import de.hybris.platform.core.model.product.ProductModel;
import org.zkoss.zul.Listcell;

/**
 * Created by Timofey Klyubin on 16.09.16.
 */
public class ProductDeletedFieldRenderer implements WidgetComponentRenderer<Listcell, ListColumn, ProductModel> {

    @Override
    public void render(Listcell listcell, ListColumn listColumn, ProductModel s, DataType dataType, WidgetInstanceManager widgetInstanceManager) {
        listcell.setLabel((s.getDeleted() != null && s.getDeleted()) ? "✔" : "✘");
        listcell.setStyle("text-align: center;");
    }
}
