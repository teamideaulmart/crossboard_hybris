package ru.teamidea.crossboard.b2c.backoffice.actions;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.log4j.Logger;
import org.zkoss.zul.Messagebox;
import ru.teamidea.solrsearch.events.SolrProductIndexingAfterSaveListener;

import javax.annotation.Resource;
import java.util.Collections;

/**
 * Created by Alexey on 11.10.2016.
 */
public class SolrIndexProductAction implements CockpitAction<ProductModel, String> {
    private static final Logger LOG = Logger.getLogger(SolrIndexProductAction.class);

    @Resource(name = "teamideaSolrProductReindexAfterSaveListener")
    private SolrProductIndexingAfterSaveListener afterSaveListener;

    @Override
    public ActionResult<String> perform(final ActionContext<ProductModel> ctx)
    {
        LOG.info("Force product solr indexing action called");
        ActionResult<String> result = null;
        final ProductModel product = ctx.getData();
        if (product != null) {
            LOG.info(String.format("Send product Product code is: %s",product.getCode()));

            final ProductModel productToIndex = product instanceof VariantProductModel ? ((VariantProductModel) product).getBaseProduct() : product;
            afterSaveListener.afterSave(Collections.singleton(new AfterSaveEvent(productToIndex.getPk(), AfterSaveEvent.UPDATE)));

            result = new ActionResult<>(ActionResult.SUCCESS, ctx.getLabel("solr.sent.success"));
        } else {
            result = new ActionResult<>(ActionResult.ERROR);
        }

        Messagebox.show(result.getData() + " (" + result.getResultCode() + ")");
        return result;
    }
}
