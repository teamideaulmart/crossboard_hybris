package ru.teamidea.crossboard.b2c.backoffice.actions;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zul.Messagebox;
import ru.ulmart.crossboard.core.enums.ProductDescriptionStatus;
import ru.ulmart.crossboard.core.model.CrossboardProductAttributeModel;
import ru.ulmart.crossboard.core.services.translate.DescriptionTranslateService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by max on 03.10.16.
 */
public class TranslateProductAction  implements CockpitAction<ProductModel, String> {

    private static final Logger LOG = Logger.getLogger(TranslateProductAction.class.getName());

    private final Locale TARGET_LANG = Locale.forLanguageTag("RU");

    @Resource(name="descriptionTranslateService")
    private DescriptionTranslateService descriptionTranslateService;

    public void setDescriptionTranslateService(DescriptionTranslateService service){
        this.descriptionTranslateService = service;
    }

    @Resource(name="modelService")
    private ModelService modelService;


    public void setModelService(ModelService service){
        this.modelService = service;
    }

    @Resource(name="catalogVersionService")
    private CatalogVersionService catalogVersionService;

    public void setCatalogVersionService(CatalogVersionService service){
        this.catalogVersionService = service;
    }

    @Override
    public ActionResult<String> perform(final ActionContext<ProductModel> ctx)
    {
        LOG.info("Translate product action called");
        ActionResult<String> result = null;
        final ProductModel product = (ProductModel) ctx.getData();
        if (product != null)
        {
            LOG.info(String.format("Product code is: %s",product.getCode()));

            clearTranslatedStringsForProduct(product);

            List<String> codes = new ArrayList<>();
            codes.add((product.getCode()));



            descriptionTranslateService.translateProductDescriptions(codes,getCatalogVersion());

            result = new ActionResult<String>(ActionResult.SUCCESS, ctx.getLabel("translation.success")/*ctx.getLabel("message", new Object[] { product })*/);
        }
        else
        {
            result = new ActionResult<String>(ActionResult.ERROR);
        }
        Messagebox.show(result.getData() + " (" + result.getResultCode() + ")");

        return result;
    }

    private CatalogVersionModel getCatalogVersion(){
        return catalogVersionService.getCatalogVersion("crossboardProductCatalog", "Online");
    }


    private void clearTranslatedStringsForProduct(ProductModel product) {
        product.setName("", TARGET_LANG);
        product.setShortName("", TARGET_LANG);
        product.setDescription("", TARGET_LANG);
        product.setShortDescription("", TARGET_LANG);
        product.setBrand("", TARGET_LANG);
        product.setDescriptionStatus(ProductDescriptionStatus.NOT_PROCESSED);

        List<CrossboardProductAttributeModel> productAttributes = product.getAttributes();
          for (CrossboardProductAttributeModel attr : productAttributes) {
              attr.setName("",TARGET_LANG);
              attr.setValue("",TARGET_LANG);
              attr.setUnit("",TARGET_LANG);
              modelService.save(attr);
          }
          modelService.saveAll(product);
    }

    @Override
    public boolean canPerform(final ActionContext<ProductModel> ctx)
    {
        return true;
//        final Object data = ctx.getData();
//        return (data instanceof String) && (!((String) data).isEmpty());
    }

    @Override
    public boolean needsConfirmation(final ActionContext<ProductModel> ctx)
    {
        return true;
    }

    @Override
    public String getConfirmationMessage(final ActionContext<ProductModel> ctx)
    {
        return ctx.getLabel("confirmation.message");
    }
}
