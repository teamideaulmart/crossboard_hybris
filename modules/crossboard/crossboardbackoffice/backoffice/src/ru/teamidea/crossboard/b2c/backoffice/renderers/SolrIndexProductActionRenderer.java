package ru.teamidea.crossboard.b2c.backoffice.renderers;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.impl.DefaultActionRenderer;

/**
 * Created by Alexey on 11.10.2016.
 */
public class SolrIndexProductActionRenderer extends DefaultActionRenderer<String, Object> {
    protected static final String ACTION_NAME = "solrindexproduct.action.name";

    protected String getLocalizedName(ActionContext<?> context) {
        return context.getLabel(ACTION_NAME);
    }
}
