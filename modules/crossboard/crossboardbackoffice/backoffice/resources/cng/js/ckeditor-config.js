/**
 * Created by Timofey Klyubin on 27.09.16.
 */

(function($editorConfig){

    $editorConfig.config = function(config) {

        config.toolbar_Full = null; // new toolbar with all buttons

        config.toolbar_DefaultToolbar = [
            [ 'Format' ], [ 'Bold', 'Italic', '-', 'Link', 'BulletedList', '-', 'Source','-','Undo', 'Redo']
        ];
        config.toolbar_Minimal = [
            [ 'Bold', 'Italic', '-', 'Link' ]
        ];

        config.toolbar = 'Full';
    };

})(CKEDITOR.editorConfig);
