package ru.ulmart.crossboard.storefront.breadcrumb.impl;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Oksana Kurushina on 08.11.2016.
 */
public class CrossboardProductBreadcrumbBuilder extends ProductBreadcrumbBuilder {
    private static final String LAST_LINK_CLASS = "active";

    public List<Breadcrumb> getBreadcrumbs(final String productCode)
    {
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();

        final Collection<CategoryModel> categoryModels = new ArrayList<>();
        final Breadcrumb last;

        final ProductModel baseProductModel = getBaseProduct(productModel);
        last = getProductBreadcrumb(baseProductModel);
        if (last.getName() == null ) {
            last.setName(productModel.getName());
        }
        categoryModels.addAll(baseProductModel.getSupercategories());
        last.setLinkClass(LAST_LINK_CLASS);

        breadcrumbs.add(last);

        while (!categoryModels.isEmpty())
        {
            CategoryModel toDisplay = null;
            toDisplay = processCategoryModels(categoryModels, toDisplay);
            categoryModels.clear();
            if (toDisplay != null)
            {
                breadcrumbs.add(getCategoryBreadcrumb(toDisplay));
                categoryModels.addAll(toDisplay.getSupercategories());
            }
        }
        Collections.reverse(breadcrumbs);
        return breadcrumbs;
    }

    protected CategoryModel processCategoryModels(final Collection<CategoryModel> categoryModels,
                                                  final CategoryModel toDisplay)
    {
        CategoryModel categoryToDisplay = toDisplay;

        for (final CategoryModel categoryModel : categoryModels)
        {
            if (!(categoryModel instanceof ClassificationClassModel)
                    && !(categoryModel instanceof VariantCategoryModel)
                    && !(categoryModel instanceof VariantValueCategoryModel)
                    )
            {
                if (categoryToDisplay == null)
                {
                    categoryToDisplay = categoryModel;
                }
                if (getBrowseHistory().findEntryMatchUrlEndsWith(categoryModel.getCode()) != null)
                {
                    break;
                }
            }
        }
        return categoryToDisplay;
    }
}
