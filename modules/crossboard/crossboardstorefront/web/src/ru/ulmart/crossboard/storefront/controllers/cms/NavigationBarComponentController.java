/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package ru.ulmart.crossboard.storefront.controllers.cms;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ulmart.crossboard.core.model.MerchantConfigModel;
import ru.ulmart.crossboard.facades.populators.CrossboardCategoryPopulator;
import ru.ulmart.crossboard.storefront.controllers.ControllerConstants;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
@Controller("NavigationBarComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.NavigationBarComponent)
public class NavigationBarComponentController extends AbstractAcceleratorCMSComponentController<NavigationBarComponentModel> {

    @Resource
    private CategoryService categoryService;

    @Autowired
    CrossboardCategoryPopulator crossboardCategoryPopulator;

    private final static String CROSSBOARD_BRANDS_CODE="brands";


    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final NavigationBarComponentModel component) {
        ListMultimap<String,CategoryData> categoryBrandsMap= LinkedListMultimap.create();
        CategoryModel brandCategory=categoryService.getCategoryForCode(CROSSBOARD_BRANDS_CODE);
        CategoryModel categoriesRoot = component.getLink().getCategory();

        int lastElementIndex = 0;
        for (CategoryModel categoryModel : categoriesRoot.getCategories()) {
            if (categoryModel.getOrder() != null) {
                lastElementIndex = Math.max(lastElementIndex, categoryModel.getOrder());
            }
        }

        List<CategoryData> outerDataList = new ArrayList<>(categoriesRoot.getCategories().size());
        for (CategoryModel outerModel : categoriesRoot.getCategories()) {
            if (outerModel.getPk().equals(brandCategory.getPk())){
                continue;
            }
            CategoryData outerData = new CategoryData();
            crossboardCategoryPopulator.populate(outerModel, outerData);
            List<CategoryData> middleDataList = new ArrayList<>();
            for (CategoryModel middleModel : outerModel.getCategories()) {
                CategoryData middleData = new CategoryData();
                crossboardCategoryPopulator.populate(middleModel, middleData);
                if (middleModel.getPicture()!=null && middleModel.getSupercategories().stream().anyMatch(x->x.getPk().equals(brandCategory.getPk()))){
                    categoryBrandsMap.put(outerData.getCode(),middleData);
                }
                List<CategoryData> innerDataList = new ArrayList<>();

                for (CategoryModel innerModel : middleModel.getCategories()) {
                    if (innerModel.getProducts().isEmpty()) {
                        continue;
                    }
                    boolean hideCategory = false;
                    for (MerchantConfigModel merchantConfig : innerModel.getMerchantConfig()) {
                        if (BooleanUtils.isTrue(merchantConfig.getDisplayAllowed())) {
                            if (innerModel.getProducts()
                                    .stream()
                                    .filter(p -> p.getMerchant() != null && merchantConfig.getMerchant() != null && p.getMerchant().getCode().equals(merchantConfig.getMerchant().getCode()))
                                    .count() > 0) {
                                hideCategory = false;
                            }
                        } else {
                            hideCategory = true;
                        }
                    }

                    if (hideCategory) {
                        continue;
                    }

                    CategoryData innerData = new CategoryData();
                    crossboardCategoryPopulator.populate(innerModel, innerData);
                    innerDataList.add(innerData);
                }
                if (!innerDataList.isEmpty() || CollectionUtils.isNotEmpty(middleModel.getProducts())) {
                    middleData.setSubCategories(innerDataList);
                    middleDataList.add(middleData);
                }
            }

            if (!middleDataList.isEmpty()) {
                outerData.setSubCategories(middleDataList);

                int index = outerModel.getOrder() == null ? ++lastElementIndex : outerModel.getOrder();
                outerData.setSequence(index);
                outerDataList.add(outerData);
            }
        }

        Collections.sort(outerDataList, (CategoryData first, CategoryData second) ->
                ((Integer.valueOf(first.getSequence()))).compareTo(second.getSequence()));

        CategoryData root = new CategoryData();
        root.setSubCategories(outerDataList);

        model.addAttribute("component", root);
        model.addAttribute("categoryBrandsMap", categoryBrandsMap);
    }

}
