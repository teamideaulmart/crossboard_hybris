/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package ru.ulmart.crossboard.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductReportService;
import ru.ulmart.crossboard.storefront.controllers.ControllerConstants;
import ru.ulmart.crossboard.storefront.seo.provider.SEOMetadataProvider;

import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * Controller for home page
 */
@Controller
@RequestMapping("/")
public class HomePageController extends AbstractPageController {

	@Autowired
	private SEOMetadataProvider seoMetadataProvider;


	public CrossboardProductReportService getProductReportService() {
		return productReportService;
	}
    @Autowired
	public void setProductReportService(CrossboardProductReportService productReportService) {
		this.productReportService = productReportService;
	}

	private CrossboardProductReportService productReportService;


	@RequestMapping(method = RequestMethod.GET)
	public String home(@RequestParam(value = "logout", defaultValue = "false") final boolean logout, final Model model,
					   final RedirectAttributes redirectModel) throws CMSItemNotFoundException {

		if (logout) {
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER,
					"account.confirmation.signout.title");
			return REDIRECT_PREFIX + ROOT;
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		updatePageTitle(model);

		return getViewForPage(model);
	}

	@RequestMapping(value="/csv/", method = RequestMethod.GET)
	public String csv(final Model model, final HttpServletResponse response) throws CMSItemNotFoundException {

		productReportService.getCSVData();
		prepareNotFoundPage(model, response);
		return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
	}

	@Override
	protected void setUpMetaDataForContentPage(Model model, ContentPageModel contentPage) {
		String keywords = seoMetadataProvider.getMetaDataForUrl(SEOMetadataProvider.Page.HOMEPAGE, SEOMetadataProvider.META_KEYWORDS);
		String description = seoMetadataProvider.getMetaDataForUrl(SEOMetadataProvider.Page.HOMEPAGE, SEOMetadataProvider.META_DESCRIPTION);
		setUpMetaData(model, getLocalizedMessage(keywords), getLocalizedMessage(description));
	}

	protected void updatePageTitle(final Model model) {
		String title = seoMetadataProvider.getMetaDataForUrl(SEOMetadataProvider.Page.HOMEPAGE, SEOMetadataProvider.META_TITLE);
		storeContentPageTitleInModel(model, getLocalizedMessage(title));
	}

	private String getLocalizedMessage(String key) {
		return getMessageSource().getMessage(key, null, Locale.forLanguageTag(getCurrentLanguage().getIsocode()));
	}
}
