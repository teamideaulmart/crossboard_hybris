package ru.ulmart.crossboard.storefront.controllers.cms;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ulmart.crossboard.core.model.components.CrossboardMerchantInfoComponentModel;
import ru.ulmart.crossboard.storefront.controllers.ControllerConstants;

import javax.servlet.http.HttpServletRequest;

@Controller("CrossboardMerchantInfoComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CrossboardMerchantInfoComponentController)
public class CrossboardMerchantInfoComponentController extends AbstractAcceleratorCMSComponentController<CrossboardMerchantInfoComponentModel>{

    @Override
    protected void fillModel(HttpServletRequest httpServletRequest, Model model, CrossboardMerchantInfoComponentModel component) {
        model.addAttribute("merchant", component.getMerchant());
    }
}