package ru.ulmart.crossboard.storefront.controllers.integration;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.util.Config;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.teamidea.redisclient.service.RedisclientService;
import ru.ulmart.crossboard.facades.search.solrfacetsearch.CrossboardProductSearchFacade;
import ru.ulmart.crossboard.storefront.controllers.ControllerConstants;

import javax.annotation.Resource;
import javax.net.ssl.SSLContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * Created by Oksana Kurushina on 08.09.2016.
 */

@Controller
@RequestMapping("/ulmart")
public class UlmartSiteController {
    private static final Logger LOG = Logger.getLogger(UlmartSiteController.class);
    private static final String ULMART_GETHEADER_URL = "ulmart.getheader.url";
    private static final String ULMART_COOKIE_DOMAIN = "ulmart.cookie.domain";
    private static final String ULMART_GETHEADER_LOGIN = "ulmart.getheader.login";
    private static final String ULMART_GETHEADER_PASSWORD = "ulmart.getheader.password";

    @Resource(name = "redisclientService")
    private RedisclientService redisclientService;

    @Resource(name = "crossboardProductSearchFacade")
    private CrossboardProductSearchFacade<ProductData> productSearchFacade;

    @RequestMapping(value = "/getdata", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getUlmartData(HttpServletRequest request, HttpServletResponse servletResponse) {
        Cookie[] cookies = request.getCookies();
        BasicCookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie clientCookie;
        for (Cookie cookie : cookies) {
            clientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
            clientCookie.setPath("/");
            clientCookie.setDomain(Config.getParameter(ULMART_COOKIE_DOMAIN));
            cookieStore.addCookie(clientCookie);
        }

        LOG.info(String.format("Header/footer request cookies: [%1$s]",
                Arrays.asList(cookies).stream().map(cookie -> cookie.getName() + ":" + cookie.getValue()).collect(Collectors.joining(", "))));

        HttpResponse response;
        try {
            response = getHttpResponseConnectMethod(cookieStore);
        }catch (IOException e){
            LOG.error(e.getMessage());
            return "";
        }

        LOG.info("Request from /getHeader has been received");
        Header[] responseHeaders = response.getAllHeaders();
        for (Header header : responseHeaders) {
            if (header.getName().equals("Set-Cookie")) {
                HeaderElement headerEl = header.getElements()[0];
                Cookie ulmartCookie= new Cookie(headerEl.getName(), headerEl.getValue());
                ulmartCookie.setPath("/");
                ulmartCookie.setDomain(".ulmart.ru");
                servletResponse.addCookie(ulmartCookie);
            }
        }
        BufferedReader rd;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException ex) {
            LOG.error(ex);
            return "";
        }
        StringBuffer result = new StringBuffer();
        String line = "";
        try {
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException ex) {
            LOG.error(ex);
            return "";
        }
        return result.toString()
                .replace("Разработка — <a href=\\\"http://www.dz.ru/\\\" class=\\\"underline text-muted\\\" target=\\\"_blank\\\">Digital Zone</a>,\\n                            поставщик ERP — <a href=\\\"http://www.ultimaerp.com/\\\" class=\\\"underline text-muted\\\" target=\\\"_blank\\\">Ultima</a>",
                        "Разработка — <a href='http://www.teamidea.ru/' class='underline text-muted' target='_blank'>TeamIdea</a>");
    }


    private HttpResponse getHttpResponseConnectMethod(BasicCookieStore cookieStore)  throws IOException{
        SSLConnectionSocketFactory sslsf = getSslConnectionSocketFactory();

        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
        clientBuilder.setDefaultCookieStore(cookieStore);
        clientBuilder.useSystemProperties();
        clientBuilder.setSSLSocketFactory(sslsf).setHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        final String login = Config.getParameter(ULMART_GETHEADER_LOGIN);
        final String password = Config.getParameter(ULMART_GETHEADER_PASSWORD);

        HttpClient client;
        if(!StringUtils.isEmpty(login) && !StringUtils.isEmpty(password)){
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(org.apache.http.auth.AuthScope.ANY,
                    new UsernamePasswordCredentials(login, password));
            client =
                    clientBuilder.setDefaultCredentialsProvider(credentialsProvider).build();
        }
        else{
            client = clientBuilder.build();
        }

        HttpGet get = new HttpGet(Config.getParameter(ULMART_GETHEADER_URL));

        HttpResponse response;
        try {
            LOG.info("Send /getHeader response to " + ULMART_GETHEADER_URL);
            response = client.execute(get);
        } catch (Exception ex) {
            LOG.error(ex);
            throw new IOException("Request failed");
        }
        return response;
    }

    private SSLConnectionSocketFactory getSslConnectionSocketFactory() throws IOException {
        SSLContext sslContext;
        try {
            sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustStrategy() {
                        @Override
                        public boolean isTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
                            return true;
                        }
                    })
                    .useTLS()
                    .build();
        } catch (Exception ex) {
            LOG.error(ex);
           throw  new IOException("Creating SSLContext failed");
        }
        return new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    }

    @RequestMapping(value = "/getuserdata", method = RequestMethod.GET)
    public String getUlmartUserData(HttpServletRequest request, final Model model) {
        Cookie[] cookies = request.getCookies();
        String jSessionId = "", userId = "-1", city = "18414";
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("JSESSIONID")) jSessionId = cookie.getValue();
            if (cookie.getName().equals("User-ID")) userId = cookie.getValue();
            if (cookie.getName().equals("city")) city = cookie.getValue();
        }
        /*HashMap redisData = redisclientService.getRedisSession(jSessionId);
        model.addAttribute("userData", redisData);*/
        model.addAttribute("userId", userId);
        model.addAttribute("city", city);
        return ControllerConstants.Views.Fragments.BackupHeaderFooter.HeaderFooterHtml;
    }

    @RequestMapping(value = "/getulmartsearchresults", method = RequestMethod.GET, produces = "text/html")
    public String getUlmartSearchResults(@RequestParam(value = "searchText", defaultValue = "") final String searchText,
                                         HttpServletRequest request, HttpServletResponse servletResponse, final Model model) {

        model.addAttribute("searchText", searchText);
        model.addAttribute("goodsAmountUlmart", productSearchFacade.getAutocompleteGoodsAmountUlmart(searchText, request, servletResponse));
        model.addAttribute("goodsAmountUlmartDiscount", productSearchFacade.getAutocompleteGoodsAmountDiscount(searchText, request, servletResponse));

        return ControllerConstants.Views.Fragments.ElasticSearch.ElasticSearchResults;
    }

    @RequestMapping(value = "/putcity", method = RequestMethod.GET)
    public void putCity(@RequestParam(value = "city", required = true) final String city,
                                         HttpServletRequest request, HttpServletResponse servletResponse) {
        productSearchFacade.putCity(city, request, servletResponse);
    }

}
