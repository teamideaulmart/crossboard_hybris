/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package ru.ulmart.crossboard.storefront.controllers.pages;

import com.sap.security.core.server.csi.XSSEncoder;
import de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.AutocompleteResultData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.*;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ulmart.crossboard.core.services.catalog.CrossboardViewTypeService;
import ru.ulmart.crossboard.core.services.search.UlmartElasticSearchService;
import ru.ulmart.crossboard.facades.search.solrfacetsearch.CrossboardProductSearchFacade;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static ru.ulmart.crossboard.storefront.controllers.pages.CategoryPageController.DEFAULT_MIN_PRICE;
import static ru.ulmart.crossboard.storefront.controllers.pages.CategoryPageController.LAST_FULL_TEXT_COOKIE;


@Controller
@RequestMapping("/search")
public class SearchPageController extends AbstractSearchPageController
{
	private static final String SEARCH_META_DESCRIPTION_ON = "search.meta.description.on";
	private static final String SEARCH_META_DESCRIPTION_RESULTS = "search.meta.description.results";

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(SearchPageController.class);

	private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";
	private static final String FACET_SEPARATOR = ":";

	private static final String SEARCH_CMS_LIST_PAGE_ID = "searchList";
	private static final String SEARCH_CMS_GRID_PAGE_ID = "searchGrid";
	private static final String NO_RESULTS_CMS_PAGE_ID = "searchEmpty";

	private static final String PRICE_DESC_CODE = "price-desc";


	@Resource(name = "crossboardProductSearchFacade")
	private CrossboardProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "searchBreadcrumbBuilder")
	private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "crossboardViewTypeService")
	private CrossboardViewTypeService viewTypeService;

    @Autowired
    private UlmartElasticSearchService ulmartElasticSearchService;

	@RequestMapping(method = RequestMethod.GET, params = "!filters")
	public String textSearch(@RequestParam(value = "text", defaultValue = "") final String searchText,
							 @RequestParam(value = "viewType", required = false) final Integer viewTypeCode,
							 @CookieValue(value = LAST_FULL_TEXT_COOKIE, defaultValue = "") String lastFulltextCookie,
							 final HttpServletRequest request, final HttpServletResponse servletResponse, final Model model)
			throws CMSItemNotFoundException, UnsupportedEncodingException {
		Cookie viewTypeCookie = viewTypeService.getViewTypeCookie(request, viewTypeCode);
		servletResponse.addCookie(viewTypeCookie);

		if (StringUtils.isNotBlank(searchText)) {
			final PageableData pageableData = createPageableData(0, getSearchPageSize(), null, ShowMode.Page);
			final String encodedSearchText = XSSFilterUtil.filter(searchText);

			final SearchStateData searchState = new SearchStateData();
			final SearchQueryData searchQueryData = new SearchQueryData();
			searchQueryData.setValue(encodedSearchText);
			searchState.setQuery(searchQueryData);

			ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
			try
			{
				searchPageData = encodeSearchPageData(productSearchFacade.textSearch(searchState, pageableData));
			}
			catch (final ConversionException e) // NOSONAR
			{
				// nothing to do - the exception is logged in SearchSolrQueryPopulator
			}

			correctPriceBreadcrumbs(searchPageData);
			
			if (searchPageData == null)
			{
				storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
			}
			else if (searchPageData.getKeywordRedirectUrl() != null)
			{
				// if the search engine returns a redirect, just
				return "redirect:" + searchPageData.getKeywordRedirectUrl();
			}
			else if (searchPageData.getPagination().getTotalNumberOfResults() == 0)
			{
				model.addAttribute("searchPageData", searchPageData);
				storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
				updatePageTitle(encodedSearchText, model);
			}
			else {
				storeContinueUrl(request);
				populateModel(model, searchPageData, ShowMode.Page);
				int viewTypeValue = Integer.valueOf(viewTypeCookie.getValue());
				String label = viewTypeValue == 0 ? SEARCH_CMS_GRID_PAGE_ID : SEARCH_CMS_LIST_PAGE_ID;
				storeCmsPageInModel(model, getContentPageForLabelOrId(label));
				updatePageTitle(encodedSearchText, model);
			}

			final PageableData maxPricePageableData = createPageableData(0, 1, PRICE_DESC_CODE, ShowMode.Page);

			final SearchStateData maxPriceSearchState = new SearchStateData();
			final SearchQueryData maxPriceSearchQueryData = new SearchQueryData();
			maxPriceSearchQueryData.setValue(encodedSearchText);
			maxPriceSearchState.setQuery(maxPriceSearchQueryData);

			ProductSearchPageData<SearchStateData, ProductData> maxPriceSearchPageData = null;
			try {
				maxPriceSearchPageData = encodeSearchPageData(productSearchFacade.textSearch(maxPriceSearchState, maxPricePageableData));
				fillMinAndMaxPrices(model, searchText, maxPriceSearchPageData.getResults(), lastFulltextCookie, servletResponse);
			} catch (final ConversionException e) { // NOSONAR
				// {// nothing to do - the exception is logged in SearchSolrQueryPopulator
			}


			model.addAttribute("userLocation", customerLocationService.getUserLocation());
			getRequestContextData(request).setSearch(searchPageData);
			if (searchPageData != null)
			{
				model.addAttribute(
						WebConstants.BREADCRUMBS_KEY,
						searchBreadcrumbBuilder.getBreadcrumbs(null, encodedSearchText,
								CollectionUtils.isEmpty(searchPageData.getBreadcrumbs())));
			}
		} else {
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
		}
		model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);

		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(
				SEARCH_META_DESCRIPTION_RESULTS, null, SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale())
				+ " "
				+ searchText
				+ " "
				+ getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON,
						getI18nService().getCurrentLocale()) + " " + getSiteName());
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);


		return getViewForPage(model);
	}

	@RequestMapping(method = RequestMethod.GET, params = "filters")
	public String refineSearch(@RequestParam("filters") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "text", required = false) final String searchText,
			@RequestParam(value = "viewType", required = false) final Integer viewTypeCode,
			@CookieValue(value = LAST_FULL_TEXT_COOKIE, defaultValue = "") String lastFulltextCookie,
			final HttpServletRequest request, final HttpServletResponse response, final Model model) throws CMSItemNotFoundException, UnsupportedEncodingException {
		Cookie viewTypeCookie = viewTypeService.getViewTypeCookie(request, viewTypeCode);
		response.addCookie(viewTypeCookie);

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, getSearchPageSize());

		final ProductSearchPageData<SearchStateData, ProductData> maxPriceData = performSearch(searchQuery, 0, showMode,
				PRICE_DESC_CODE, 1);

        String finalSearchText = searchText;
        if (finalSearchText == null) {
            String[] filterItems = searchQuery.split(":");
            if (filterItems.length > 0) finalSearchText = searchQuery.split(":")[0];
        }

		fillMinAndMaxPrices(model, finalSearchText, maxPriceData.getResults(), lastFulltextCookie, response);
		correctPriceBreadcrumbs(searchPageData);
		populateModel(model, searchPageData, showMode);
		model.addAttribute("userLocation", customerLocationService.getUserLocation());

		if (searchPageData.getPagination().getTotalNumberOfResults() == 0)
		{
			updatePageTitle(searchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
		}
		else
		{
			storeContinueUrl(request);
			updatePageTitle(searchPageData.getFreeTextSearch(), model);
			int viewTypeValue = Integer.valueOf(viewTypeCookie.getValue());
			String label = viewTypeValue == 0 ? SEARCH_CMS_GRID_PAGE_ID : SEARCH_CMS_LIST_PAGE_ID;
			storeCmsPageInModel(model, getContentPageForLabelOrId(label));
		}
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, searchBreadcrumbBuilder.getBreadcrumbs(null, searchPageData));

		model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());

		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(
				SEARCH_META_DESCRIPTION_RESULTS, null, SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale())
				+ " "
				+ searchText
				+ " "
				+ getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON,
						getI18nService().getCurrentLocale()) + " " + getSiteName());

		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewForPage(model);
	}

	private void fillMinAndMaxPrices(final Model model, final String fulltextQuery, final List<ProductData> productData,
									 String lastFulltextQuery, HttpServletResponse response) throws UnsupportedEncodingException {

		final CategoryPageController.MinMaxPriceHolder minMaxPriceHolder =
				new CategoryPageController.MinMaxPriceHolder(fulltextQuery, productData, response).invoke();
		BigDecimal min = minMaxPriceHolder.getMin();
		BigDecimal max = minMaxPriceHolder.getMax();

		if (lastFulltextQuery.equals("")) {
			response.addCookie(new Cookie(LAST_FULL_TEXT_COOKIE, URLEncoder.encode(fulltextQuery, "UTF-8").replace("%25", "%2525")
					.concat(":")
					.concat(min.toString())
					.concat(":")
					.concat(max.toString())));
		} else {
			final String[] cookieValues = lastFulltextQuery.split(":");
			cookieValues[0] = URLDecoder.decode(cookieValues[0], "UTF-8");
			if (cookieValues[0].equals(fulltextQuery)) {
				min = DEFAULT_MIN_PRICE; //new BigDecimal(cookieValues[1]);
				BigDecimal cookieMax = new BigDecimal(cookieValues[2]);
				if (cookieMax.compareTo(max) < 0) {
					response.addCookie(new Cookie(LAST_FULL_TEXT_COOKIE, URLEncoder.encode(fulltextQuery, "UTF-8").replace("%25", "%2525")
							.concat(":")
							.concat(min.toString())
							.concat(":")
							.concat(max.toString())));
				} else {
					max = cookieMax;
				}
			} else {
				min = minMaxPriceHolder.getMin();
				max = minMaxPriceHolder.getMax();
				response.addCookie(new Cookie(LAST_FULL_TEXT_COOKIE, URLEncoder.encode(fulltextQuery, "UTF-8").replace("%25", "%2525")
						.concat(":")
						.concat(min.toString())
						.concat(":")
						.concat(max.toString())));
			}
		}

		model.addAttribute("minPrice", min);
		model.addAttribute("maxPrice", max);
	}

	private void correctPriceBreadcrumbs(ProductSearchPageData<SearchStateData, ProductData> searchPageData){
		if(searchPageData != null && searchPageData.getBreadcrumbs() != null) {
			for (BreadcrumbData<SearchStateData> breadcrumbData : searchPageData.getBreadcrumbs()) {
				if (breadcrumbData.getFacetCode().equals("priceValue")) {
					Locale currentLocale = getI18nService().getCurrentLocale();
					String facetValue = breadcrumbData.getFacetValueCode();
					facetValue = facetValue
							.replace("[", getMessageSource().getMessage("search.nav.from_price", new Object[]{}, currentLocale).concat(" "))
							.replace("TO", getMessageSource().getMessage("search.nav.to_price", new Object[]{}, currentLocale))
							.replace("]", "");
					breadcrumbData.setFacetValueName(facetValue);
				}
			}
		}
	}

	protected ProductSearchPageData<SearchStateData, ProductData> performSearch(final String searchQuery, final int page,
			final ShowMode showMode, final String sortCode, final int pageSize)
	{
		final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);

		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);

		return encodeSearchPageData(productSearchFacade.textSearch(searchState, pageableData));
	}

	@ResponseBody
	@RequestMapping(value = "/results", method = RequestMethod.GET)
	public SearchResultsData<ProductData> jsonSearchResults(@RequestParam("filters") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException
	{
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, getSearchPageSize());
		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		searchResultsData.setResults(searchPageData.getResults());
		searchResultsData.setPagination(searchPageData.getPagination());
		return searchResultsData;
	}

	@ResponseBody
	@RequestMapping(value = "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException
	{
		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(searchState,
				createPageableData(page, getSearchPageSize(), sortCode, showMode));
		final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
				convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
		final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
		refinement.setFacets(facets);
		refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
		refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());
		return refinement;
	}

	@ResponseBody
	@RequestMapping(value = "/autocomplete/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public AutocompleteResultData getAutocompleteSuggestions(@PathVariable final String componentUid,
			@RequestParam("term") final String term, HttpServletRequest request, HttpServletResponse servletResponse) throws CMSItemNotFoundException
	{
		final AutocompleteResultData resultData = new AutocompleteResultData();

		final SearchBoxComponentModel component = (SearchBoxComponentModel) cmsComponentService.getSimpleCMSComponent(componentUid);

		if(component.isDisplayCategories()){
			resultData.setCategories(productSearchFacade.categoryTextSearch(term, component.getMaxCategories()));
		}

		if (component.isDisplaySuggestions())
		{
			resultData.setSuggestions(subList(productSearchFacade.getAutocompleteSuggestions(term), component.getMaxSuggestions()));
		}

		if (component.isDisplayProducts())
		{
			resultData.setProducts(subList(productSearchFacade.textSearch(term).getResults(), component.getMaxProducts()));
		}

        resultData.setGoodsAmountUlmart(productSearchFacade.getAutocompleteGoodsAmountUlmart(term, request, servletResponse));
        resultData.setGoodsAmountDiscount(productSearchFacade.getAutocompleteGoodsAmountDiscount(term, request, servletResponse));

		return resultData;
	}

	protected <E> List<E> subList(final List<E> list, final int maxElements)
	{
		if (CollectionUtils.isEmpty(list))
		{
			return Collections.emptyList();
		}

		if (list.size() > maxElements)
		{
			return list.subList(0, maxElements);
		}

		return list;
	}

	protected void updatePageTitle(final String searchText, final Model model)
	{
		storeContentPageTitleInModel(
				model,
				getPageTitleResolver().resolveContentPageTitle(
						getMessageSource().getMessage("search.meta.title", null, "search.meta.title",
								getI18nService().getCurrentLocale())
								+ " " + searchText));
	}


	protected ProductSearchPageData<SearchStateData, ProductData> encodeSearchPageData(
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
	{
		final SearchStateData currentQuery = searchPageData.getCurrentQuery();

		if (currentQuery != null)
		{
			try
			{
				final SearchQueryData query = currentQuery.getQuery();
				final String encodedQueryValue = XSSEncoder.encodeHTML(query.getValue());
				query.setValue(encodedQueryValue);
				currentQuery.setQuery(query);
				searchPageData.setCurrentQuery(currentQuery);
				final String unencodedFreeTextSearch = searchPageData.getFreeTextSearch();
				searchPageData.setFreeTextSearch(XSSEncoder.encodeHTML(searchPageData.getFreeTextSearch()));

				if (CollectionUtils.isNotEmpty(searchPageData.getResults())) {

					final ProductData singleProduct = searchPageData.getResults()
							.stream()
							.filter(pd -> pd.getCode().equalsIgnoreCase(StringUtils.trimToEmpty(unencodedFreeTextSearch)))
							.findAny().orElse(null);

					if (singleProduct != null) {
						searchPageData.setKeywordRedirectUrl(singleProduct.getUrl());
					}
				}

				final List<FacetData<SearchStateData>> facets = searchPageData.getFacets();
				if (CollectionUtils.isNotEmpty(facets))
				{
					processFacetData(facets);
				}
			}
			catch (final UnsupportedEncodingException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Error occured during Encoding the Search Page data values", e);
				}
			}
		}
		return searchPageData;
	}

	protected void processFacetData(final List<FacetData<SearchStateData>> facets) throws UnsupportedEncodingException
	{
		for (final FacetData<SearchStateData> facetData : facets)
		{
			final List<FacetValueData<SearchStateData>> topFacetValueDatas = facetData.getTopValues();
			if (CollectionUtils.isNotEmpty(topFacetValueDatas))
			{
				processFacetDatas(topFacetValueDatas);
			}
			final List<FacetValueData<SearchStateData>> facetValueDatas = facetData.getValues();
			if (CollectionUtils.isNotEmpty(facetValueDatas))
			{
				processFacetDatas(facetValueDatas);
			}
		}
	}

	protected void processFacetDatas(final List<FacetValueData<SearchStateData>> facetValueDatas)
			throws UnsupportedEncodingException
	{
		for (final FacetValueData<SearchStateData> facetValueData : facetValueDatas)
		{
			final SearchStateData facetQuery = facetValueData.getQuery();
			final SearchQueryData queryData = facetQuery.getQuery();
			final String queryValue = queryData.getValue();
			if (StringUtils.isNotBlank(queryValue))
			{
				final String[] queryValues = queryValue.split(FACET_SEPARATOR);
				final StringBuilder queryValueBuilder = new StringBuilder();
				queryValueBuilder.append(XSSEncoder.encodeHTML(queryValues[0]));
				for (int i = 1; i < queryValues.length; i++)
				{
					queryValueBuilder.append(FACET_SEPARATOR).append(queryValues[i]);
				}
				queryData.setValue(queryValueBuilder.toString());
			}
		}
	}

}
