package ru.ulmart.crossboard.storefront.security.cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.util.WebUtils;
import ru.ulmart.crossboard.core.constants.CrossboardCoreConstants;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Created by vitalii on 28.10.16.
 */
public class RequestThrottler {

    private static final Logger LOGGER = Logger.getLogger(RequestThrottler.class);


    public boolean isTimeLimitExpired(HttpServletRequest request, HttpServletResponse response, long timeLimitMillis) {
        Cookie lastMessageTimeCookie = WebUtils.getCookie(request, CrossboardCoreConstants.LAST_ERROR_MESSAGE_TIME);
        if (lastMessageTimeCookie == null || StringUtils.isEmpty(lastMessageTimeCookie.getValue())) {
            lastMessageTimeCookie = new Cookie(CrossboardCoreConstants.LAST_ERROR_MESSAGE_TIME, LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            response.addCookie(lastMessageTimeCookie);
            return true;
        }

        try {
            LocalDateTime lastMessageTime = LocalDateTime.parse(lastMessageTimeCookie.getValue());
            if (lastMessageTime.plus(timeLimitMillis, ChronoUnit.MILLIS).isAfter(LocalDateTime.now())) {
                return false;
            }
        } catch(NumberFormatException e) {
            LOGGER.info("Can't parse Cookie value to long type : '" + lastMessageTimeCookie.getValue() + "'" );
        }

        lastMessageTimeCookie.setValue(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        response.addCookie(lastMessageTimeCookie);
        return true;
    }
}
