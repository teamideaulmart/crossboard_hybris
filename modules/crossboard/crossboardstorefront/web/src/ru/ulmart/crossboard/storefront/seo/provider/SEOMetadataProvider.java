package ru.ulmart.crossboard.storefront.seo.provider;

/**
 * Created by Timofey Klyubin on 30.09.16.
 */
public interface SEOMetadataProvider {

    enum Page {
        HOMEPAGE,
        CATEGORY,
        PRODUCT
    }

    String META_TITLE = "meta.title";
    String META_DESCRIPTION = "meta.description";
    String META_KEYWORDS = "meta.keywords";
    String getMetaDataForUrl(final Page url, final String key);
}
