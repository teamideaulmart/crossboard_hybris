package ru.ulmart.crossboard.storefront.seo.provider.impl;

import ru.ulmart.crossboard.storefront.seo.provider.SEOMetadataProvider;

import java.util.Map;

/**
 * Created by Timofey Klyubin on 30.09.16.
 */
public abstract class AbstractSEOMetadataProvider implements SEOMetadataProvider {
    @Override
    public String getMetaDataForUrl(Page url, String key) {
        Map<String, String> metadata = provideDataForUrl(url);
        return metadata.getOrDefault(key, "");
    }

    protected abstract Map<String, String> provideDataForUrl(final Page url);
}
