/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package ru.ulmart.crossboard.storefront.controllers.pages;


import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ulmart.crossboard.core.services.catalog.CrossboardCategoryService;
import ru.ulmart.crossboard.core.services.catalog.CrossboardViewTypeService;
import ru.ulmart.crossboard.storefront.seo.provider.SEOMetadataProvider;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


/**
 * Controller for a category page
 */
@Controller
@RequestMapping(value = "/**/catalog")
public class CategoryPageController extends AbstractCategoryPageController {

    static final BigDecimal DEFAULT_MIN_PRICE = BigDecimal.ZERO;
    static final String LAST_FULL_TEXT_COOKIE = "lastFulltext";
    private static final String LAST_CATEGORY_COOKIE = "lastCategory";
    private static final String PRICE_DESC_CODE = "price-desc";

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @Autowired
    private CrossboardViewTypeService viewTypeService;

    @Autowired
    private SEOMetadataProvider seoMetadataProvider;

    @Autowired
    CrossboardCategoryService csCategoryService;


    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String category(@PathVariable("categoryCode") final String categoryCode, // NOSONAR
                           @RequestParam(value = "filters", required = false) final String searchQuery,
                           @RequestParam(value = "page", defaultValue = "0") final int page,
                           @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                           @RequestParam(value = "sort", required = false) final String sortCode,
                           @RequestParam(value = "viewType", required = false) final Integer viewTypeCode, final Model model,
                           @CookieValue(value = LAST_CATEGORY_COOKIE, defaultValue = "") String lastCategoryCookie,
                           final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException, CMSItemNotFoundException {

        CategoryModel category = csCategoryService.getCategoryForCode(categoryCode);
        if (category != null) {
            model.addAttribute("rotationComponent", category.getRotationComponent());
        }

        String result = performSearchAndGetResultsPage(categoryCode, viewTypeCode, searchQuery, page, showMode,
                sortCode, model, request, response, lastCategoryCookie);

        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData =
                (ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData>) model.asMap().get("searchPageData");

        if(searchPageData != null && searchPageData.getBreadcrumbs() != null) {
            for (BreadcrumbData<SearchStateData> breadcrumbData : searchPageData.getBreadcrumbs()) {
                if (breadcrumbData.getFacetCode().equals("priceValue")) {
                    Locale currentLocale = getI18nService().getCurrentLocale();
                    String facetValue = breadcrumbData.getFacetValueCode();
                    facetValue = facetValue
                            .replace("[", getMessageSource().getMessage("search.nav.from_price", new Object[]{}, currentLocale).concat(" "))
                            .replace("TO", getMessageSource().getMessage("search.nav.to_price", new Object[]{}, currentLocale))
                            .replace("]", "");
                    breadcrumbData.setFacetValueName(facetValue);
                }
            }
        }
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));

        return result;
    }

    @Override
    protected <QUERY> void updatePageTitle(CategoryModel category, List<BreadcrumbData<QUERY>> appliedFacets, Model model) {
        String title = seoMetadataProvider.getMetaDataForUrl(SEOMetadataProvider.Page.CATEGORY, SEOMetadataProvider.META_TITLE);
        storeContentPageTitleInModel(model, getLocalizedMessage(title, category.getName()));
    }

    @Override
    protected void setUpMetaDataForContentPage(Model model, ContentPageModel contentPage) {
        String keywords = seoMetadataProvider.getMetaDataForUrl(SEOMetadataProvider.Page.CATEGORY, SEOMetadataProvider.META_KEYWORDS);
        String description = seoMetadataProvider.getMetaDataForUrl(SEOMetadataProvider.Page.CATEGORY, SEOMetadataProvider.META_DESCRIPTION);
        String categoryName = (String) model.asMap().get("categoryName");
        setUpMetaData(model, getLocalizedMessage(keywords, categoryName), getLocalizedMessage(description, categoryName));
    }

    private String getLocalizedMessage(String key, Object... args) {
        return getMessageSource().getMessage(key, args, Locale.forLanguageTag(getCurrentLanguage().getIsocode()));
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
    public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode") final String categoryCode,
                                                      @RequestParam(value = "q", required = false) final String searchQuery,
                                                      @RequestParam(value = "page", defaultValue = "0") final int page,
                                                      @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                      @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetFacets(categoryCode, searchQuery, page, showMode, sortCode);
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/results", method = RequestMethod.GET)
    public SearchResultsData<ProductData> getResults(@PathVariable("categoryCode") final String categoryCode,
                                                     @RequestParam(value = "filters", required = false) final String searchQuery,
                                                     @RequestParam(value = "page", defaultValue = "0") final int page,
                                                     @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                     @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetResultsData(categoryCode, searchQuery, page, showMode, sortCode);
    }


    private String performSearchAndGetResultsPage(final String categoryCode, final Integer viewTypeCode, final String searchQuery, final int page, // NOSONAR
                                                  final ShowMode showMode, final String sortCode, final Model model, final HttpServletRequest request,
                                                  final HttpServletResponse response, final String lastCategoryCookie) throws UnsupportedEncodingException {

        final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);

        Cookie viewTypeCookie = viewTypeService.getViewTypeCookie(request, category, viewTypeCode);
        response.addCookie(viewTypeCookie);

        final String redirection = checkRequestUrl(request, response, getCategoryModelUrlResolver().resolve(category));
        if (StringUtils.isNotEmpty(redirection)) {
            return redirection;
        }

        CategoryPageModel categoryPageByView =
                csCategoryService.getCategoryPageByViewType(Integer.valueOf(viewTypeCookie.getValue()));

        final CategoryPageModel categoryPage =
                (categoryPageByView == null) ? getCategoryPage(category) : categoryPageByView;

        final CategorySearchEvaluatorCustomized categorySearch = new CategorySearchEvaluatorCustomized(categoryCode, de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil.filter(searchQuery),
                page, showMode, sortCode, categoryPage);

        final CategorySearchEvaluatorCustomized maxPriceSearch = new CategorySearchEvaluatorCustomized(categoryCode, de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil.filter(searchQuery),
                0, showMode, PRICE_DESC_CODE, categoryPage);

        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = null;
        try {
            categorySearch.doSearch();
            searchPageData = categorySearch.getSearchPageData();
        } catch (final ConversionException e) // NOSONAR
        {
            searchPageData = createEmptySearchResult(categoryCode);
        }

        final boolean showCategoriesOnly = categorySearch.isShowCategoriesOnly();

        storeCmsPageInModel(model, categorySearch.getCategoryPage());
        storeContinueUrl(request);

        populateModel(model, searchPageData, showMode);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getSearchBreadcrumbBuilder().getBreadcrumbs(categoryCode, searchPageData));
        model.addAttribute("showCategoriesOnly", Boolean.valueOf(showCategoriesOnly));
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("pageType", PageType.CATEGORY.name());
        model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());
        fillMinAndMaxPrices(model, categoryCode, maxPriceSearch, lastCategoryCookie, response);

        updatePageTitle(category, searchPageData.getBreadcrumbs(), model);

        final RequestContextData requestContextData = getRequestContextData(request);
        requestContextData.setCategory(category);
        requestContextData.setSearch(searchPageData);

        if (searchQuery != null) {
            model.addAttribute(de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.META_ROBOTS, de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
        }

        final String metaKeywords = de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil.sanitizeKeywords(category.getKeywords());
        final String metaDescription = de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil.sanitizeDescription(category.getDescription());
        setUpMetaData(model, metaKeywords, metaDescription);

        return getViewPage(categorySearch.getCategoryPage());
    }

    private void fillMinAndMaxPrices(final Model model, final String categoryCode, CategorySearchEvaluatorCustomized maxPriceSearch,
                                     String lastCategoryCookie, HttpServletResponse response) {

        final MinMaxPriceHolder minMaxPriceHolder = new MinMaxPriceHolder(categoryCode, maxPriceSearch, response).invoke();
        BigDecimal min = minMaxPriceHolder.getMin();
        BigDecimal max = minMaxPriceHolder.getMax();

        if (lastCategoryCookie.equals("")) {
            response.addCookie(new Cookie(LAST_CATEGORY_COOKIE, categoryCode
                    .concat(":")
                    .concat(min.toString())
                    .concat(":")
                    .concat(max.toString())));
        } else {
            final String[] cookieValues = lastCategoryCookie.split(":");
            if (cookieValues[0].equals(categoryCode)) {
                min = DEFAULT_MIN_PRICE; //new BigDecimal(cookieValues[1]);
                BigDecimal cookieMax = new BigDecimal(cookieValues[2]);
                if (cookieMax.compareTo(max) < 0) {
                    response.addCookie(new Cookie(LAST_CATEGORY_COOKIE, categoryCode
                            .concat(":")
                            .concat(min.toString())
                            .concat(":")
                            .concat(max.toString())));
                } else {
                    max = cookieMax;
                }
            } else {
                min = minMaxPriceHolder.getMin();
                max = minMaxPriceHolder.getMax();
                response.addCookie(new Cookie(LAST_CATEGORY_COOKIE, categoryCode
                        .concat(":")
                        .concat(min.toString())
                        .concat(":")
                        .concat(max.toString())));
            }
        }

        model.addAttribute("minPrice", min);
        model.addAttribute("maxPrice", max);
    }

    protected class CategorySearchEvaluatorCustomized {
        private final String categoryCode;
        private final SearchQueryData searchQueryData = new SearchQueryData();
        private final int page;
        private final ShowMode showMode;
        private final String sortCode;
        private CategoryPageModel categoryPage;
        private boolean showCategoriesOnly;
        private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;

        public CategorySearchEvaluatorCustomized(final String categoryCode, final String searchQuery, final int page,
                                                 final ShowMode showMode, final String sortCode, final CategoryPageModel categoryPage) {
            this.categoryCode = categoryCode;
            this.searchQueryData.setValue(searchQuery);
            this.page = page;
            this.showMode = showMode;
            this.sortCode = sortCode;
            this.categoryPage = categoryPage;
        }

        public void doSearch() {
            showCategoriesOnly = false;
            if (searchQueryData.getValue() == null) {
                // Direct category link without filtering
                if (StringUtils.isEmpty(sortCode)) {
                    searchPageData = getProductSearchFacade().categorySearch(categoryCode);
                } else {
                    final SearchStateData searchState = new SearchStateData();
                    searchState.setQuery(searchQueryData);
                    final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
                    searchPageData = getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData);
                }
                if (categoryPage != null) {
                    showCategoriesOnly = !categoryHasDefaultPage(categoryPage)
                            && CollectionUtils.isNotEmpty(searchPageData.getSubCategories());
                }
            } else {
                final SearchStateData searchState = new SearchStateData();
                searchState.setQuery(searchQueryData);

                final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
                searchPageData = getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData);
            }
        }

        public int getPage() {
            return page;
        }

        public CategoryPageModel getCategoryPage() {
            return categoryPage;
        }

        public boolean isShowCategoriesOnly() {
            return showCategoriesOnly;
        }

        public ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> getSearchPageData() {
            return searchPageData;
        }
    }

    static class MinMaxPriceHolder {
        private String categoryCode;
        private List<ProductData> productData;
        private HttpServletResponse response;
        private BigDecimal min;
        private BigDecimal max;

        MinMaxPriceHolder(String categoryCode, final CategorySearchEvaluatorCustomized maxPriceSearch, HttpServletResponse response) {
            this.categoryCode = categoryCode;
            maxPriceSearch.doSearch();
            ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> data = maxPriceSearch.getSearchPageData();
            this.productData = data.getResults();
            this.response = response;
            this.min = BigDecimal.ZERO;
            this.max = BigDecimal.ZERO;
        }

        MinMaxPriceHolder(String categoryCode, final List<ProductData> productData, HttpServletResponse response) {
            this.categoryCode = categoryCode;
            this.productData = productData;
            this.response = response;
            this.min = BigDecimal.ZERO;
            this.max = BigDecimal.ZERO;
        }

        public BigDecimal getMin() {
            //return min;
            return DEFAULT_MIN_PRICE;
        }

        public BigDecimal getMax() {
            return max;
        }

        public MinMaxPriceHolder invoke() {
            if (productData != null && !productData.isEmpty()) {
                List<PriceData> prices = productData.stream()
                        .filter(product -> product.getPrice() != null)
                        .map(ProductData::getPrice)
                        .collect(Collectors.toList());

                if (!prices.isEmpty()) {
                    final PriceData minPrice = prices.stream()
                            .min((price1, price2) -> price1.getValue().compareTo(price2.getValue()))
                            .orElse(null);

                    if (minPrice != null) {
                        min = minPrice.getValue();
                    }

                    final PriceData maxPrice = prices.stream()
                            .max((price1, price2) -> price1.getValue().compareTo(price2.getValue()))
                            .orElse(null);

                    if (maxPrice != null) {
                        max = maxPrice.getValue();
                    }
                }
            }
            return this;
        }
    }
}
