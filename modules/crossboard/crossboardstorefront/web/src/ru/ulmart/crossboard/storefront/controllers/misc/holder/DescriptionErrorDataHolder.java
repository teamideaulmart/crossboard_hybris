package ru.ulmart.crossboard.storefront.controllers.misc.holder;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Timofey Klyubin on 11.10.16.
 */

public class DescriptionErrorDataHolder {

    public static final String CURRENT_URL = "currentUrl";
    public static final String USER_MESSAGE = "userMessage";
    public static final String DESCRIPTION_ERROR = "descriptionError";


    @JsonProperty(CURRENT_URL)
    public String currentUrl;

    @JsonProperty(USER_MESSAGE)
    public String userMessage;

    @JsonProperty(DESCRIPTION_ERROR)
    public String descriptionError;
}
