package ru.ulmart.crossboard.storefront.controllers.cms;

import com.google.common.collect.EvictingQueue;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.ulmart.crossboard.core.constants.CrossboardCoreConstants;
import ru.ulmart.crossboard.core.model.components.ViewedProductHistoryComponentModel;
import ru.ulmart.crossboard.storefront.controllers.ControllerConstants;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

@Controller("ViewedProductHistoryComponentController")
@RequestMapping (value = ControllerConstants.Actions.Cms.ViewedProductHistoryComponentController)
public class ViewedProductHistoryComponentController extends AbstractAcceleratorCMSComponentController<ViewedProductHistoryComponentModel>{

    private static final Logger LOGGER = Logger.getLogger(ViewedProductHistoryComponentController.class);

    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.PRICE);

    @Resource(name = "productService")
    private ProductService productService;

    @Resource(name = "accProductFacade")
    private ProductFacade productFacade;

    private TIConfigUtils tiConfigUtils;

    @Override
    protected void fillModel(HttpServletRequest httpServletRequest, Model model, ViewedProductHistoryComponentModel component) {
        final String productsNumberProperty = getTiConfigUtils().getPropertyValue("product.history.number", "20");

        int availableProductsNumber=component.getProductNumbers() == null ? Integer.parseInt(productsNumberProperty):component.getProductNumbers();
        final List<ProductData> products = new ArrayList<>();

        Cookie productsHistoryCookie= WebUtils.getCookie(httpServletRequest,CrossboardCoreConstants.PRODUCTS_HISTORY);
        Collection<String> productsList;
        if (productsHistoryCookie != null){
            String productsHistory = productsHistoryCookie.getValue();
            productsList = EvictingQueue.create(availableProductsNumber);
            productsList.addAll(Arrays.asList(productsHistory.split(",")));
        } else{
            productsList = Collections.emptyList();
        }

        for (String code: productsList) {
            String decodedCode = "";
            try {
                decodedCode = URLDecoder.decode(code, "UTF-8");
                ProductModel productModel = productService.getProductForCode(decodedCode);
                if (productModel != null && BooleanUtils.isNotTrue(productModel.getDeleted())) {
                    products.add(productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS));
                }
            } catch (UnknownIdentifierException ex) {
                //Most likely the product was deleted from backoffice by admin, so no exceptions
            } catch (RuntimeException ex) {
                LOGGER.error(String.format("Couldn't find product for code %1$s", decodedCode));
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("Couldn't decode with UTF-8: " + code, e);
            }
        }

        model.addAttribute("title", component.getTitle());
        model.addAttribute("productData", products);
    }

    private TIConfigUtils getTiConfigUtils() {
        if (tiConfigUtils == null) {
            tiConfigUtils = Registry.getApplicationContext().getBean("tiConfigUtils", TIConfigUtils.class);
        }
        return tiConfigUtils;
    }
}