package ru.ulmart.crossboard.storefront.seo.provider.impl;

import de.hybris.platform.util.localization.Localization;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.ulmart.crossboard.storefront.seo.provider.SEOMetadataProvider;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Timofey Klyubin on 30.09.16.
 */
@Component
public class DefaultSEOMetadataProvider extends AbstractSEOMetadataProvider {

    private static final Map<Page, Map<String, String>> DEFAULT_METADATA;
    static {
        DEFAULT_METADATA = new HashMap<>();
        Map<String, String> homePageMap = new HashMap<>();
        homePageMap.put(SEOMetadataProvider.META_DESCRIPTION, "crossboard.seo.homepage.meta.description");
        homePageMap.put(SEOMetadataProvider.META_TITLE, "crossboard.seo.homepage.meta.title");
        homePageMap.put(SEOMetadataProvider.META_KEYWORDS, "crossboard.seo.homepage.meta.keywords");
        DEFAULT_METADATA.put(Page.HOMEPAGE, homePageMap);

        Map<String, String> categoryPageMap = new HashMap<>();
        categoryPageMap.put(SEOMetadataProvider.META_DESCRIPTION, "crossboard.seo.categorypage.meta.description");
        categoryPageMap.put(SEOMetadataProvider.META_TITLE, "crossboard.seo.categorypage.meta.title");
        categoryPageMap.put(SEOMetadataProvider.META_KEYWORDS, "crossboard.seo.categorypage.meta.keywords");
        DEFAULT_METADATA.put(Page.CATEGORY, categoryPageMap);

        Map<String, String> productPageMap = new HashMap<>();
        productPageMap.put(SEOMetadataProvider.META_DESCRIPTION, "crossboard.seo.productpage.meta.description");
        productPageMap.put(SEOMetadataProvider.META_TITLE, "crossboard.seo.productpage.meta.title");
        productPageMap.put(SEOMetadataProvider.META_KEYWORDS, "crossboard.seo.productpage.meta.keywords");
        DEFAULT_METADATA.put(Page.PRODUCT, productPageMap);
    }

    @Override
    protected Map<String, String> provideDataForUrl(Page url) {
        return DEFAULT_METADATA.getOrDefault(url, Collections.emptyMap());
    }
}
