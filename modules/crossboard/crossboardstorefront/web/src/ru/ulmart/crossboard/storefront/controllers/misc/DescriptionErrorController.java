package ru.ulmart.crossboard.storefront.controllers.misc;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.teamidea.utils.util.TimestampSequenceProvider;
import ru.ulmart.crossboard.core.model.email.DescriptionErrorNotificationProcessModel;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductService;
import ru.ulmart.crossboard.storefront.controllers.misc.holder.DescriptionErrorDataHolder;
import ru.ulmart.crossboard.storefront.security.cookie.RequestThrottler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 * Created by Timofey Klyubin on 11.10.16.
 */
@RestController
@RequestMapping("/ajax")
public class DescriptionErrorController {

    private CrossboardProductService productService;
    private BusinessProcessService businessProcessService;
    private CommonI18NService i18NService;
    private RequestThrottler requestThrottler;
    private TIConfigUtils tiConfigUtils;



    @ResponseBody
    @RequestMapping(value = "/descriptionError", method = RequestMethod.POST)
    public String sendDescriptionError(
            @RequestBody DescriptionErrorDataHolder errorData,
            HttpServletRequest request, HttpServletResponse response) {

        String defaultTimeLimitString = tiConfigUtils.getPropertyValue("cookie.errorMessageTimeLimitMillis", "300000");
        long defaultTimeLimit = Long.valueOf(defaultTimeLimitString);

        if (!requestThrottler.isTimeLimitExpired(request, response, defaultTimeLimit)) {
            return "{\"success\": \"requestLimit\", \"minutes\": \"" +
                    TimeUnit.MILLISECONDS.toMinutes(defaultTimeLimit) + "\" }";
        }

        boolean success = true;

        DescriptionErrorNotificationProcessModel processModel = businessProcessService.createProcess(
                    "description-error-notification-process-" + TimestampSequenceProvider.getSequenceNumber(),
                    "description-error-notification-process"
            );
        processModel.setEmailLanguage(i18NService.getLanguage("ru"));
        processModel.setUrl(errorData.currentUrl);
        processModel.setUserMessage(errorData.userMessage);
        processModel.setDescriptionError(errorData.descriptionError);

        try {
            businessProcessService.startProcess(processModel);
        } catch (Exception e) {
            success = false;
        }
        return "{\"success\":" + (success ? "\"true\"" : "\"false\"") + "}";
    }

    @Autowired
    public void setProductService(CrossboardProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    @Autowired
    public void setI18NService(CommonI18NService i18NService) {
        this.i18NService = i18NService;
    }

    @Autowired
    public void setRequestThrottler(RequestThrottler requestThrottler) {
        this.requestThrottler = requestThrottler;
    }

    @Autowired
    public void setTiConfigUtils(TIConfigUtils tiConfigUtils) {
        this.tiConfigUtils = tiConfigUtils;
    }
}
