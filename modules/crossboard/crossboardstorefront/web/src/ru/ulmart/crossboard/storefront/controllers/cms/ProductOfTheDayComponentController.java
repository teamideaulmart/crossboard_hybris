package ru.ulmart.crossboard.storefront.controllers.cms;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ulmart.crossboard.core.model.cms.ProductOfTheDayComponentModel;
import ru.ulmart.crossboard.storefront.controllers.ControllerConstants;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Oksana Kurushina on 13.09.2016.
 */

@Controller("ProductOfTheDayComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.ProductOfTheDayComponent)
public class ProductOfTheDayComponentController extends AbstractAcceleratorCMSComponentController<ProductOfTheDayComponentModel> {
    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE);

    @Resource(name = "accProductFacade")
    private ProductFacade productFacade;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final ProductOfTheDayComponentModel component)
    {
        Date currentDate = new Date();
        if (currentDate.before(component.getStartDate()) || currentDate.after(component.getEndDate())
                               || component.getProducts().size() == 0) return;
        final List<ProductData> products = new ArrayList<>();
        for (final ProductModel productModel : component.getProducts()) {
            products.add(productFacade.getProductForOptions(productModel, PRODUCT_OPTIONS));
        }
        int randomIndex = new Random().nextInt(products.size());
        model.addAttribute("productData", products.get(randomIndex));
    }
}
