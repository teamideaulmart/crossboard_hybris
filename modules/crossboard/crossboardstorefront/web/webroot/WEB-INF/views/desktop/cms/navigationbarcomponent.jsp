<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="dropdownMenu">
<ul>
<c:forEach items="${component.subCategories}" var="child">
	<li class="itm_topCont">
		<div class="itm itm--top">
			<c:if test="${not empty child.thumbnail.url}">
				<img class="dropdownMenu_icon" src="${child.thumbnail.url}" alt="${child.thumbnail.altText}" title="${child.name}"/>
			</c:if>${child.name}
        </div>
		<div class="subitemsCont">
			<ul class="subitems">
			<c:forEach items="${child.subCategories}" varStatus="i" var="childlink" begin="${i.index}" end="${i.index + 9}">
					<li class="subitems_col">
						<a href="${childlink.url}" class="subitems_hdr">${childlink.name}</a>
						<c:if test="${not empty childlink.subCategories}">
							<ul class="subitems_col">
								<c:forEach items="${childlink.subCategories}" var="inner">
									<li>
										<a class="itm subitems_itm js-gtm-catalog-item" data-gtm-name="${inner.name}" href="${inner.url}" title="${inner.name}">${inner.name}</a>
									</li>
								</c:forEach>
							</ul>
						</c:if>
					</li>
			</c:forEach>
				<c:if test="${not empty categoryBrandsMap.get(child.code)}">
					<li class="subitems_col">
						<span class="subitems_hdr"><spring:theme code="menu.brands.word" text="Brands" /></span>
						<ul class="dropdownMenu_brandList">
							<c:forEach items="${categoryBrandsMap.get(child.code)}" var="item">
								<li class="dropdownMenu_brandItem">
									<a href="${item.url}">
										<img src="${item.picture.url}" alt="${item.name}" title="${item.name}" height="50" width="50"/>
									</a>
								</li>
							</c:forEach>
						</ul>
					</li>
				</c:if>
			</ul>
		</div>
	</li>
</c:forEach>
</ul>
</div>

