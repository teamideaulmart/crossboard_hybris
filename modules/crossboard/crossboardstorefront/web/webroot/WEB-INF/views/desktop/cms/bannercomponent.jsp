<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value="${not empty page ? page.label : urlLink}" var="encodedUrl" />
<script>
	if (typeof dataLayerBanners != 'undefined') {
		dataLayerBanners.push({
			'eventContext':"${headline}",
			'eventContent':'${headline}',
			'eventLocation':'Banner',
			'eventPosition':'1'
		});
	}
</script>
<c:choose>
	<c:when test="${empty encodedUrl || encodedUrl eq '#'}">
		<div class="bannerItem js-gtm-banner" data-gtm-type="banner" data-gtm-id="${id}" data-gtm-name="${headline}" data-gtm-num="1">
			<div class="bannerItem_ttl">${headline}</div>
			<div class="bannerItem_img">
				<img title="${headline}" alt="${media.altText}" src="${media.url}" />
			</div>
			<div class="bannerItem_txt">${content}</div>
		</div>
	</c:when>
	<c:otherwise>
		<a class="bannerItem js-gtm-banner" href="${encodedUrl}" data-gtm-type="banner" data-gtm-id="${id}" data-gtm-name="${headline}" data-gtm-num="1">
			<span class="bannerItem_ttl">${headline}</span>
			<span class="bannerItem_img">
				<img title="${headline}" alt="${media.altText}" src="${media.url}" />
			</span>
			<span class="bannerItem_txt">${content}</span>
		</a>
	</c:otherwise>
</c:choose>