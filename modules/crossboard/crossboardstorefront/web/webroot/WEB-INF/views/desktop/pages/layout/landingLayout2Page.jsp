<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="dfp" tagdir="/WEB-INF/tags/desktop/dfp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
    <jsp:attribute name="pageHeadScripts">
        <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
        <script>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            //баннер слева
            googletag.cmd.push(function() {
                googletag.defineSlot('/109000988/cb2_main_240x400', [240, 400], 'div-gpt-ad-1474897269456-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'Main').setTargeting('city', '${cityId}');
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            //скролл баннеров 5 шт
            googletag.cmd.push(function() {
                googletag.defineSlot('/109000988/cb2_main_194x255_1', [200, 200], 'div-gpt-ad-1474897657847-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'Main').setTargeting('city', '${cityId}');
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            googletag.cmd.push(function() {
                googletag.defineSlot('/109000988/cb2_main_194x255_2', [200, 200], 'div-gpt-ad-1474897724915-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'Main').setTargeting('city', '${cityId}');
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            googletag.cmd.push(function() {
                googletag.defineSlot('/109000988/cb2_main_194x255_3', [200, 200], 'div-gpt-ad-1474897838163-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'Main').setTargeting('city', '${cityId}');
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            googletag.cmd.push(function() {
                googletag.defineSlot('/109000988/cb2_main_194x255_4', [200, 200], 'div-gpt-ad-1474897969019-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'Main').setTargeting('city', '${cityId}');
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            googletag.cmd.push(function() {
                googletag.defineSlot('/109000988/cb2_main_194x255_5', [200, 200], 'div-gpt-ad-1474898016186-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'Main').setTargeting('city', '${cityId}');
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });

            //Баннер 4 (главная, футер) – 970*90
            googletag.cmd.push(function() {
                googletag.defineSlot('/109000988/cb2_main_970_90', [970, 90], 'div-gpt-ad-1474898104468-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'Main').setTargeting('city', '${cityId}');
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>
    </jsp:attribute>
    <jsp:attribute name="dfpBanner">
        <!-- /109000988/cb2_main_240x400 -->
        <div id='div-gpt-ad-1474897269456-0' style='height:400px; width:240px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1474897269456-0'); });
            </script>
        </div>
    </jsp:attribute>
	<jsp:attribute name="dataLayerJs">
	<script>
		dataLayer = [{
			'pageType': 'Main'
		}];
		var dataLayerProducts = [],
			dataLayerBanners = [];
	</script>
    </jsp:attribute>
    <jsp:body>
	<div class="hidden" id="mainMenu--opened"></div>

	<section class="section section--withLeftCol">
		<section class="section section--topAbout">
			<div class="topAbout_main">
				<div class="links">
					<div class="links_mark"></div>
					<a class="links_item js-gtm-question-link" data-gtm-name="<spring:theme code="landing.links.item1" />" href="<c:url value="/help#kak_zakazat"/>"><spring:theme code="landing.links.item1" /></a>
					<a class="links_item links_item--border js-gtm-question-link" data-gtm-name="<spring:theme code="landing.links.item2" />" href="<c:url value="/help#kak_oplatit"/>"><spring:theme code="landing.links.item2" /></a>
					<a class="links_item links_item--border js-gtm-question-link" data-gtm-name="<spring:theme code="landing.links.item3" />" href="<c:url value="/help#o_dostavke"/>"><spring:theme code="landing.links.item3" /></a>
					<a class="links_item js-gtm-question-link" href="<c:url value="/help#garantii"/>" data-gtm-name="<spring:theme code="landing.links.item4" />"><spring:theme code="landing.links.item4" /></a>
				</div>
			</div>
			<div class="topAbout_seeVideoButton popUpButton popUpButton--video js-gtm-video-btn" for="popUp_videoTutorial"></div>
			<div class="popUpCont" id="popUp_videoTutorial"><div class="popUp popUp--video">
				<script src="https://www.youtube.com/iframe_api"></script>
				<iframe class="ytplayer" id="ytplayer" width="100%" height="100%" src="https://www.youtube.com/embed/4YgZ2GZq3b0?rel=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
			</div></div>
		</section>

		<section class="section section--withRightItem">
			<%-- Хиты продаж gridproductcarouselcomponent.jsp --%>
			<cms:pageSlot position="Section3A" var="feature">
				<cms:component component="${feature}"/>
			</cms:pageSlot>

			<cms:pageSlot position="Section2A" var="feature" element="div" class="rightItem productOfTheDay">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
        </section>

		<section class="section section--withRightItem section--topRotator">
			<%-- Карусель баннеров. Шаблон - WEB-INF\views\desktop\cms\rotatingimagescomponent.jsp --%>
			<cms:pageSlot position="Section1" var="feature">
				<cms:component component="${feature}" element="div" class="imageRotator"/>
			</cms:pageSlot>
			<cms:pageSlot position="Section2B" var="feature" element="div" class="rightItem suppliers">
				<div class="sectionHdr"><spring:theme code="landing.section.merchants" /></div>
				<cms:component component="${feature}" />
				<a href="<c:url value="/providers"/>" class="seeAll"><spring:theme code="landing.section.seeAll.merchants" /></a>
			</cms:pageSlot>
		</section>

        <%-- Новые gridproductcarouselcomponent.jsp --%>
        <cms:pageSlot position="Section3B" var="feature">
            <cms:component component="${feature}"/>
        </cms:pageSlot>

        <%-- Акции и предложения gridproductcarouselcomponent.jsp --%>
        <cms:pageSlot position="Section3C" var="feature">
            <cms:component component="${feature}"/>
        </cms:pageSlot>

        <%--<div class="section productRow js_bannerTicker section--bannerScroller">
            <dfp:homepageBannerScroll />
        </div>--%>

		<%-- Длинный баннер внизу --%>
        <%-- /109000988/cb2_main_970_90 --%>
        <div id='div-gpt-ad-1474898104468-0' class="section section--bannerWide">
            <script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1474898104468-0'); });</script>
        </div>
    </section>

	<%-- Вы смотрели viewedproducthistorycomponent.jsp --%>
	<cms:pageSlot position="Section2C" var="feature">
		<cms:component component="${feature}"/>
	</cms:pageSlot>

	<div id="globalMessages"><common:globalMessages/></div>
    </jsp:body>
</template:page>