<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:url value="/search/" var="searchUrl"/>
<c:url value="/search/autocomplete/${component.uid}" var="autocompleteUrl"/>

<div class="mainSearch">
	<form name="search_form_${component.uid}" method="get" action="${searchUrl}">
		<div class="mainSearch_inputCont">
			<spring:theme code="search.placeholder" var="searchPlaceholder"/>
			<ycommerce:testId code="header_search_input">
                <c:catch var="exception">
                    <c:set var="textSearchInput" value="${searchPageData.freeTextSearch}"/>
                </c:catch>
				<input
					id="input_${component.uid}"
					class="mainSearch_input siteSearchInput"
					type="text"
					name="text"
                    value="${not empty exception ? '' : textSearchInput}"
					maxlength="100"
					placeholder="${searchPlaceholder}"
					data-options='{"autocompleteUrl" : "${autocompleteUrl}","minCharactersBeforeRequest" : "${component.minCharactersBeforeRequest}","waitTimeBeforeRequest" : "${component.waitTimeBeforeRequest}","displayProductImages" : ${component.displayProductImages}}'/>
			</ycommerce:testId>
			<ycommerce:testId code="header_search_button">
				<button class="mainSearch_button" type="submit"/><spring:theme code="text.search"/></button>
			</ycommerce:testId>
		</div>
	</form>
</div>