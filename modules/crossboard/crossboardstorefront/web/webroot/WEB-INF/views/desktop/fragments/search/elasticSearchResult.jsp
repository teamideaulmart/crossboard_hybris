<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:if test="${goodsAmountUlmart > 0 || goodsAmountUlmartDiscount > 0}">
    <div class="searchOtherPlaces">
        <h4><spring:theme code="find.in.other.places"/></h4>
        <div class="searchOtherPlaces--inner">
            <c:if test="${goodsAmountUlmart > 0}">
                <a href='https://www.ulmart.ru/search?string=${searchText}'>
                    <i class='b-ico b-ico_u-commerce'></i><spring:theme code="ulmart"/>&nbsp;<span class='text-muted'>${goodsAmountUlmart}</span><br/>
                    <small class='text-muted'><spring:theme code="ecommerce.number.one"/></small>
                </a>
            </c:if>
            <c:if test="${goodsAmountUlmartDiscount > 0}">
                <a href='https://discount.ulmart.ru/search?string=${searchText}'>
                    <i class='b-ico b-ico_u-discount'></i><spring:theme code="ulmart.second"/>&nbsp;<span class='text-muted'>${goodsAmountUlmartDiscount}</span><br/>
                    <small class='text-muted'><spring:theme code="sales.products"/></small>
                </a>
            </c:if>
        </div>
    </div>
</c:if>