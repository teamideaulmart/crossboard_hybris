<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>

<c:choose>
	<c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
		<div class="scroller">
			<div class="sectionHdr">${component.title}</div>
			<div class="">
				<ul class="js_bannerTicker section--productList">
					<c:forEach end="${component.maximumNumberProducts}" items="${productReferences}" var="productReference" varStatus="status">
						<c:url value="${productReference.target.url}/quickView" var="productQuickViewUrl"/>
						<li class="js_bannerTickerItem">
							<script>
								if (typeof dataLayerProducts != 'undefined') {
									var productPrice = '<format:fromPriceValue priceData="${productReference.target.price}"/>';
									dataLayerProducts.push({
										'eventProductId':'${productReference.target.code}',
										'eventProductName':'${productReference.target.name}',
										'eventLocation':'${component.title}',
										'eventPosition':'${status.count}',
										'eventVendorName':'${productReference.target.manufacturer}',
										'eventProductPrice':productPrice == '' ? '' : Number(productPrice.match(/\d+/)[0]).toFixed(2),
										'eventCategoryId':'${productReference.target.categories[0].code}',
										'eventCategoryName':'${productReference.target.categories[0].name}'
									});
								}
							</script>
								<a class="scrollerProduct productItem js-gtm-product-item" href="${productReference.target.url}"
								   data-gtm-location="${component.title}"
								   data-gtm-num="${status.count}"
								   data-gtm-id="${productReference.target.code}"
								   data-gtm-name="${productReference.target.name}"
								   data-gtm-category-name="${productReference.target.categories[0].name}"
								   data-gtm-category-id="${productReference.target.categories[0].code}"
								   data-gtm-vendor-name="${productReference.target.manufacturer}"
								   data-gtm-price="<format:fromPriceValue priceData="${productReference.target.price}"/>">
									<div class="productItem_img">
										<product:productPrimaryImage product="${productReference.target}" format="product"/>
									</div>
									<div class="productItem_title">
										<c:choose>
											<c:when test="${not empty productReference.target.shortName}">
												${productReference.target.shortName}
											</c:when>
											<c:otherwise>
												<span class="js-two-lines">${productReference.target.name}</span>
											</c:otherwise>
										</c:choose>
									</div>
									<div class="productItem_price rub priceContainer"><format:fromPrice priceData="${productReference.target.price}"/></div>
								</a>
						</li>

					</c:forEach>
				</ul>
			</div>
		</div>
	</c:when>

	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>

