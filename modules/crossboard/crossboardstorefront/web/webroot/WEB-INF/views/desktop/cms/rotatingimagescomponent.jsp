<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="sliderComponent">
	<div id="homepage_slider" class="svw rotationComponent"  intervalValue="${(empty timeout)||(timeout <=0) ? '8' : timeout}">
		<ul>
			<c:forEach items="${banners}" var="banner" varStatus="status">
				<c:if test="${ycommerce:evaluateRestrictions(banner)}">
					<c:url value="${banner.urlLink}" var="encodedUrl" />
					<li>
						<script>
							if (typeof dataLayerBanners != 'undefined') {
								dataLayerBanners.push({
									'eventContext':"${banner.uid}",
									'eventContent':'${not empty banner.headline ? banner.headline : banner.media.altText}',
									'eventLocation':'Homepage slider',
									'eventPosition':'${status.count}'
								});
							}
						</script>
						<a class="js-gtm-banner" data-gtm-type="Homepage slider" data-gtm-id="${banner.uid}" data-gtm-name="${not empty banner.headline ? banner.headline : banner.media.altText}" data-gtm-num="${status.count}" tabindex="-1" href="${encodedUrl}"<c:if test="${banner.external}"> target="_blank"</c:if>><img src="${banner.media.url}" alt="${not empty banner.headline ? banner.headline : banner.media.altText}" title="${not empty banner.headline ? banner.headline : banner.media.altText}"/></a></li>
				</c:if>
			</c:forEach>
		</ul>
	</div>
</div>
