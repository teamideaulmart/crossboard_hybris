<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
	<c:when test="${product.stock.getStockLevelStatus() == 'outOfStock'}">
		<c:set var="productAvailability" value="notAvailable"/>
	</c:when>
	<c:otherwise>
		<c:set var="productAvailability" value="available"/>
	</c:otherwise>
</c:choose>


<template:page pageTitle="${pageTitle}">
	<jsp:attribute name="dataLayerJs">
	<script>
		var productPrice = '<format:fromPriceValue priceData="${product.price}"/>';
		dataLayer = [{
			'pageType': 'ProductPage',
			'categoryName':'${product.categories[0].name}',
			'categoryId':'${product.categories[0].code}',
			'vendorName':'${product.manufacturer}',
			'productName':'${product.name}',
			'productId':'${product.code}',
			"productPrice": productPrice == '' ? '' : Number(productPrice.match(/\d+/)[0]).toFixed(2),
			'productAvailability':'${productAvailability}',
			'sellerId':'${product.merchant.code}',
			'sellerName':'${product.merchant.name}'
		}];
		var dataLayerProducts = [];
	</script>
    </jsp:attribute>
	<jsp:body>
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	<cms:pageSlot position="Section1" var="comp" element="div" class="span-24 section1 cms_disp-img_slot">
		<cms:component component="${comp}"/>
	</cms:pageSlot>
	<product:productDetailsPanel product="${product}" galleryImages="${galleryImages}"/>
	<cms:pageSlot position="CrossSelling" var="comp" element="div" class="span-24">
		<cms:component component="${comp}"/>
	</cms:pageSlot>
	<cms:pageSlot position="Section3" var="feature" element="div" class="span-20 section3 cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	<cms:pageSlot position="UpSelling" var="comp" element="section" class="section productRow section--lastSeen">
		<cms:component component="${comp}"/>
	</cms:pageSlot>
	<product:productPageTabs />

	<%-- Вы смотрели productcarouselcomponent.jsp --%>
	<cms:pageSlot position="Section4" var="feature" element="div">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	<div class="productCard_bottomText">
		<p><spring:theme code="productCard.bottomText1" text="Product description is for informational purposes and may differ from the description provided in the technical documentation of the manufacturer." /></p>
		<p><spring:theme code="productCard.bottomText2" text="Recommend when buying check the desired functions and features." /></p>
		<p><spring:theme code="productCard.bottomText3" text="You can report an error in the product description - select it and click" /><span class="shift_enter_img"></span></p>
	</div>
	</jsp:body>
</template:page>