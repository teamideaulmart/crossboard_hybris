<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/desktop/storepickup" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>


<template:page pageTitle="${pageTitle}">
	<jsp:attribute name="pageScripts">
		<script type="text/javascript" src="${commonResourcePath}/js/acc.paginationsort.js"></script>
	</jsp:attribute>
	<jsp:attribute name="dataLayerJs">
	<script>
		dataLayer = [{
			'pageType': 'SearchResults',
			'productsQuantityTotal':'${searchPageData.pagination.totalNumberOfResults}'
		}];
		var dataLayerProducts = [];
	</script>
	</jsp:attribute>
	<jsp:body>
		<div id="globalMessages"><common:globalMessages/></div>
		<h1 class="section section--pageHeader"><spring:theme code="search.page.resultText" /></h1>
		<div class="section">
            <div class="pagination--top">
                <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
            </div>
			<div class="section section--menuCol">
				<cms:pageSlot position="ProductLeftRefinements" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
			<div class="section section--withLeftCol">
				<cms:pageSlot position="SearchResultsListSlot" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
		</div>
		<storepickup:pickupStorePopup />
	</jsp:body>
</template:page>