<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:if test="${not empty productData}">
    <script>
		if (typeof dataLayerProducts != 'undefined') {
			var productPrice = '<format:fromPriceValue priceData="${productData.price}"/>';
			dataLayerProducts.push({
				'eventProductId':'${productData.code}',
				'eventProductName':'${productData.name}',
				'eventLocation':'<spring:theme code='product.ofTheDay'/>',
				'eventVendorName':'${productData.manufacturer}',
				'eventProductPrice': productPrice == '' ? '' : Number(productPrice.match(/\d+/)[0]).toFixed(2),
				'eventCategoryId':'${productData.categories[0].code}',
				'eventCategoryName':'${productData.categories[0].name}'
			});
		}
    </script>
	<div class="productItem productItem--productOfTheDay js-gtm-product-item" data-gtm-location="<spring:theme code="product.ofTheDay"/>" data-gtm-num="1" data-gtm-id="${productData.code}" data-gtm-name="${productData.name}" data-gtm-category-name="${productData.categories[0].name}" data-gtm-category-id="${productData.categories[0].code}" data-gtm-price="<format:fromPriceValue priceData="${productData.price}"/>" data-gtm-vendor-name="${productData.manufacturer}">
        <div class="label_productOfTheDay"><spring:theme code="product.ofTheDay"/></div>
		<div class="productItem_ttl">
			<a href="<c:url value='${productData.url}'/>">
				<c:choose>
					<c:when test="${not empty product.shortName}">
						${productData.shortName}
					</c:when>
					<c:otherwise>
						<span class="js-two-lines">${productData.name}</span>
					</c:otherwise>
				</c:choose>
			</a>
		</div>
		<c:if test="${not empty productData.price}">
			<div class="productItem_price rub"><format:price priceData="${productData.price}"/></div>
		</c:if>
		<div class="productItem_img">
			<a href="<c:url value='${productData.url}'/>">
				<product:productPrimaryImage product="${productData}" format="product"/>
			</a>
		</div>
		<div class="label_superPrice"><spring:theme code="product.superPrice"/></div>
	</div>
</c:if>