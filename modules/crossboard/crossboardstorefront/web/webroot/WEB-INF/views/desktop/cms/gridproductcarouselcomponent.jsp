<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>

<c:choose>
	<c:when test="${not empty productData}">
		<div class="section productRow">
			<div class="sectionHdr">${title}</div>
			<ul class="js_bannerTicker">
				<c:forEach items="${productData}" var="product" varStatus="status">
					<c:url value="${product.url}" var="productUrl"/>
					<li class="js_bannerTickerItem">
						<script>
							if (typeof dataLayerProducts != 'undefined') {
								var productPrice = '<format:fromPriceValue priceData="${product.price}"/>';
								dataLayerProducts.push({
									'eventProductId':'${product.code}',
									'eventProductName':'${product.name}',
									'eventLocation':'${component.title}',
									'eventPosition':'${status.count}',
									'eventVendorName':'${product.manufacturer}',
									'eventProductPrice':productPrice == '' ? '' : Number(productPrice.match(/\d+/)[0]).toFixed(2),
									'eventCategoryId':'${product.categories[0].code}',
									'eventCategoryName':'${product.categories[0].name}'
								});
							}
						</script>
						<a href="${productUrl}" class="productItem js-gtm-product-item" data-gtm-location="${component.title}" data-gtm-num="${status.count}" data-gtm-id="${product.code}" data-gtm-name="${product.name}" data-gtm-category-name="${product.categories[0].name}" data-gtm-category-id="${product.categories[0].name}" data-gtm-vendor-name="${product.manufacturer}" data-gtm-price="<format:fromPriceValue priceData="${product.price}"/>" >
							<span class="productItem_img"><product:productPrimaryImage product="${product}" format="product"/></span>
							<span class="productItem_ttl">
								<span class="js-two-lines">${product.name}</span>
							</span>
							<c:if test="${not empty product.price}">
								<span class="productItem_price rub"><format:fromPrice priceData="${product.price}"/></span>
							</c:if>
						</a>
					</li>
				</c:forEach>
			</ul>
		</div>
	</c:when>

	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>
