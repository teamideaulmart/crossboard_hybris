<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cb" uri="http://cb.ulmart.ru/tld/crossboard" %>


<c:set var="buttonType">submit</c:set>

<c:set var="productIsOutOfStock" value="${product.stock.stockLevelStatus.code eq 'outOfStock' ? true : false}"/>

<c:choose>
	<c:when test="${pageType == 'CATEGORY'}">
		<c:set var="googleTagPageType">MainCatalog</c:set>
	</c:when>
	<c:when test="${pageType == 'PRODUCTSEARCH'}">
		<c:set var="googleTagPageType">SearchResults</c:set>
	</c:when>
</c:choose>

<div class="cart">
	<c:choose>
		<c:when test="${productIsOutOfStock}">
			<c:if test="${productIsOutOfStock}">
				<span class="productItem_outOfStock"><spring:theme code="pickup.out.of.stock" text="Out of stock"/></span>
			</c:if>
		</c:when>
		<c:otherwise>
			<c:url value="/cart/add" var="addToCartUrl"/>
			<ycommerce:testId code="searchPage_addToCart_button_${product.code}">
				<form:form id="addToCartForm${product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
					<input type="hidden" name="productCodePost" value="${product.code}"/>
					<input type="hidden" name="productNamePost" value="${product.name}"/>
					<input type="hidden" name="productPostPrice" value="${product.price.value}"/>

					<c:if test="${not empty cb:sanitizeUrl(product.merchantUrl)}">
						<c:set var="ulmartCookieName" value="${jspPropertyConfigurer.getProperty('ulmart.cookie.userid')}" />
						<c:set var="ulmartUrl" value="${not empty cookie[ulmartCookieName].value and cookie[ulmartCookieName].value ne '-1' ?
                    cb:patternUrl(cb:sanitizeUrl(product.merchantUrl), product.merchant.siteUrlPattern, cookie[ulmartCookieName].value) :
                    cb:sanitizeUrl(product.merchantUrl)}"/>

						<a type="${buttonType}" target="_blank" class="productItem_goButton balloon_trigger js-gtm-item-go-btn<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock' }"> out-of-stock</c:if>" data-gtm-location="${not empty googleTagPageType ? googleTagPageType : pageType}"
						   href="${ulmartUrl}"><spring:theme code="productCard.goButtonText" /><span class="balloon"><spring:theme code="productCard.goDescription" /></span>
						</a>
					</c:if>
				</form:form>
			</ycommerce:testId>
		</c:otherwise>
	</c:choose>
</div>