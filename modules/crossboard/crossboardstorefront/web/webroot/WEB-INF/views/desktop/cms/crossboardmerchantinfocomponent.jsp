<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>

<c:choose>
	<c:when test="${not empty merchant}">
		<a class="supplier js-gtm-supplier" href="${merchant.url}" data-gtm-name="${merchant.name}" >
			<div class="supplier_img"><img src="${merchant.picture.url}" alt="${merchant.name}" title="${merchant.name}"/></div>
			<div class="supplier_ttl">${merchant.name}</div>
			<div class="supplier_subttl"><spring:theme code="productCard.china"/></div>
			<div class="supplier_txt">${merchant.description}</div>
		</a>
	</c:when>
	<c:otherwise>
		<component:emptyComponent/>
	</c:otherwise>
</c:choose>