<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:page pageTitle="${pageTitle}">
	<jsp:attribute name="pageScripts">
		<script type="text/javascript" src="${commonResourcePath}/js/acc.paginationsort.js"></script>
	</jsp:attribute>
	<jsp:attribute name="pageHeadScripts">
		<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
		<script>
			var googletag = googletag || {};
			googletag.cmd = googletag.cmd || [];
			googletag.cmd.push(function() {
				googletag.defineSlot('/109000988/cb2_category_240_400', [240, 400], 'div-gpt-ad-1474898340003-0').addService(googletag.pubads()).setTargeting('Domain', 'crossboard').setTargeting('PageType', 'MainCatalog').setTargeting('city', '${cityId}').setTargeting('subcat', '${searchPageData.categoryCode}');
				googletag.pubads().enableSingleRequest();
				googletag.enableServices();
			});
		</script>
	</jsp:attribute>
	<jsp:attribute name="dataLayerJs">
	<script>
		dataLayer = [{
			'pageType': 'MainCatalog',
			'categoryName':'${requestScope.categoryName}',
			'categoryId':'${searchPageData.categoryCode}',
			"productsQuantityTotal":"${searchPageData.pagination.totalNumberOfResults}"
		}];
		var dataLayerProducts = [];
	</script>
    </jsp:attribute>
	<jsp:body>
		<div id="globalMessages"><common:globalMessages/></div>
		<h1 class="section section--pageHeader">${requestScope.categoryName}</h1>
		<div class="section">
			<div class="pagination--top">
				<nav:facetNavAppliedFilters pageData="${searchPageData}"/>
			</div>
			<div class="section section--menuCol">
				<cms:pageSlot position="ProductLeftRefinements" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>

				<!-- /109000988/cb2_category_240_400 -->
				<div id='div-gpt-ad-1474898340003-0' style='height:400px; width:240px;' class="mainMenu_suppliers">
					<script>
						googletag.cmd.push(function() { googletag.display('div-gpt-ad-1474898340003-0'); });
					</script>
				</div>
			</div>
			<div class="section section--withLeftCol">
				<cms:pageSlot position="ProductListSlot" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
                <%--rotation image component--%>
                <c:choose>
                    <c:when test="${not empty rotationComponent}">
                        <div class="category-carousel">
                            <cms:component component="${rotationComponent}"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <component:emptyComponent/>
                    </c:otherwise>
                </c:choose>
			</div>
		</div>
	</jsp:body>
</template:page>