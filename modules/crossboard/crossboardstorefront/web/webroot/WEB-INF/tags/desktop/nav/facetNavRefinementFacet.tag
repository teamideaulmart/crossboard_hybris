<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
	<c:when test="${pageType == 'CATEGORY'}">
		<c:set var="googlePageType" value="MainCatalog"/>
	</c:when>
	<c:when test="${pageType == 'PRODUCTSEARCH'}">
		<c:set var="googlePageType" value="SearchResults"/>
	</c:when>
</c:choose>

<c:if test="${not empty facetData.values}">
	<div class="facet ${facetData.multiSelect ? 'facet--multiSelect' : 'facet--stack'}" data-gtm-location="${googlePageType}" data-gtm-name="${facetData.name}">
		<spring:theme code="text.hideFacet" var="hideFacetText"/>
		<spring:theme code="text.showFacet" var="showFacetText"/>

		<c:if test="${facetData.multiSelect}">
			<a class="facet_head spoiler refinementToggle" href="#" data-hide-facet-text="${hideFacetText}" data-show-facet-text="${showFacetText}">${facetData.name}</a>
		</c:if>

		<ycommerce:testId code="facetNav_facet${facetData.name}_links">
			<div class="facetValues">
				<c:if test="${not empty facetData.topValues}">
					<ul class="facet_block">
						<c:forEach items="${facetData.topValues}" var="facetValue">
							<li>
								<c:if test="${facetData.multiSelect}">
									<form class="facet_checkboxed" action="#" method="get">
                                        <input type="hidden" name="q" value=":${fn:escapeXml(facetData.code)}:${fn:escapeXml(facetValue.code)}"/>
										<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
										<label class="facet_name ${facetValue.selected ? 'checked' : ''} facet_block-label">
											<input class="gtm-facet-checkbox" type="checkbox" data-gtm-name="${facetData.name}" data-gtm-value="${facetValue.name}" ${facetValue.selected ? 'checked="checked"' : ''} />
											<span>${facetValue.name}</span>
											<span class="facet_valueCount"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
										</label>
									</form>
								</c:if>
								<c:if test="${not facetData.multiSelect}">
									<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
									<a class="facet_itm" href="${facetValueQueryUrl}&amp;text=${searchPageData.freeTextSearch}">
										<span>${facetValue.name}</span>
										<span class="facet_valueCount"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</a>
								</c:if>
							</li>
						</c:forEach>
					</ul>
				</c:if>
					<ul class="facet_block spoilerContent${not empty facetData.topValues ? ' spoilerContent--close' : ''}">
						<c:forEach items="${facetData.values}" var="facetValue">
							<li>
								<c:if test="${facetData.multiSelect}">
									<form class="facet_checkboxed" action="#" method="get">
										<input type="hidden" name="q" value=":${fn:escapeXml(facetData.code)}:${fn:escapeXml(facetValue.code)}"/>
										<input type="hidden" name="text" value="${searchPageData.freeTextSearch}"/>
										<label class="facet_name ${facetValue.selected ? 'checked' : ''} facet_block-label">
											<input type="checkbox" class="gtm-facet-checkbox" data-gtm-name="${facetData.name}" data-gtm-value="${facetValue.name}" ${facetValue.selected ? 'checked="checked"' : ''} />
											<span>${facetValue.name}</span>
											<span class="facet_valueCount"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
										</label>
									</form>
								</c:if>
								<c:if test="${not facetData.multiSelect}">
									<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
									<a class="facet_itm" href="${facetValueQueryUrl}">
										<span>${facetValue.name}</span>
										<span class="facet_valueCount"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span>
									</a>
								</c:if>
							</li>
						</c:forEach>
					</ul>
				<c:if test="${not empty facetData.topValues}">
					<spring:theme code="search.nav.facetShowLess_${facetData.code}" var="spoilerText_open" />
					<spring:theme code="search.nav.facetShowMore_${facetData.code}" var="spoilerText_close" />
					<a href="#" class="facet_more spoiler${not empty facetData.topValues ? ' spoiler--close' : ''}"
						data-text-open="${spoilerText_open}" data-text-close="${spoilerText_close}"><spring:theme code="search.nav.facetShowMore_${facetData.code}" /></a>
				</c:if>
			</div>
		</ycommerce:testId>
	</div>
</c:if>
