<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cb" uri="http://cb.ulmart.ru/tld/crossboard" %>

<spring:theme code="text.addToCart" var="addToCartText"/>
<c:set var="productIsOutOfStock" value="${product.stock.stockLevelStatus.code eq 'outOfStock' ? true : false}"/>

<article class="productCard_panel" itemscope itemtype="http://schema.org/Product">
	<header class="productCard_header">
		<h1 itemprop="name" class="productCard_title">
			${product.name}
		</h1>
		<span class="productCard_shortDesc">
			${product.summary}
		</span>
	</header>
	<div class="productCard_subHeader" id="productCode_field" tabindex="0">
		<div class="productCard_code" >
			<span class="word"><spring:theme code="productCard.num" text="Art. " /></span>
			<span id="productCodeValue">${product.code}</span>
		</div>
		<div class="shareBlock_wrapper js_tooltipContainer">
			<button class="shareBlock_trigger js_tooltipTrigger"></button>
			<div class="shareBlock_content">
				<i class="ico ico_glob"></i>
				<span><spring:theme code="share.social" text="Share with friends"/></span>
				<%--yandex share--%>
				<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
				<script src="//yastatic.net/share2/share.js"></script>
				<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,lj"></div>
				<%--yandex share--%>
			</div>
		</div>
		<%--<div class="rating">--%>
			<%--<span class="rating_title"><spring:theme code="productCard.evaluation" text="Evaluation" /></span>--%>
			<%--<div class="rating_stars">--%>
				<%--<i class="s5"></i>--%>
				<%--<i class="s4"></i>--%>
				<%--<i class="s3"></i>--%>
				<%--<i class="s2"></i>--%>
				<%--<i class="s1"></i>--%>
			<%--</div>--%>
		<%--</div>--%>
	</div>

	<div class="productCard_layout">
		<div class="productCard_galleryCol">
			<product:productImagePanel product="${product}" galleryImages="${galleryImages}"/>
		</div>
		<div class="productCard_descCol">
			<div class="productCard_descFull" id="goodsdesc" tabindex="0" itemprop="description">
				${product.description}
				<div class="yandex_copyright">
					<c:if test="${isRusLocale and product.translated}">
						<span><spring:theme code="yandex.copyrightText" text="Tranlated by Yandex service" /></span>
						<a href="http://translate.yandex.ru/">http://translate.yandex.ru/</a>
					</c:if>
				</div>
				<meta itemprop="brand" content="${product.brand}">
				<meta itemprop="manufacturer" content="${product.manufacturer}">
			</div>
			<product:productVariantSelector product="${product}"/>
			<%--<ul class="productCard_options">--%>
				<%--<li class="productCard_optionsItem">--%>
					<%--<span class="title"><span>Type of model</span> <span class="decor"></span></span>--%>
					<%--<span class="desc">Quadrostuff</span></li>--%>
				<%--<li class="productCard_optionsItem">--%>
					<%--<span class="title"><span>Test option</span> <span class="decor"></span></span>--%>
					<%--<span class="desc">Test option desc</span></li>--%>
				<%--<li class="productCard_optionsItem">--%>
					<%--<span class="title"><span>Short</span> <span class="decor"></span></span>--%>
					<%--<span class="desc">Short</span></li>--%>
				<%--<li class="productCard_optionsItem">--%>
					<%--<span class="title"><span>Long Long Long</span> <span class="decor"></span></span>--%>
					<%--<span class="desc">Long Long Long</span></li>--%>
			<%--</ul>--%>
			<%--<a href="#" class="productCard_moreOptions">--%>
				<%--<span><spring:theme code="productCard.moreButtonText" text="More options" /></span>--%>
			<%--</a>--%>
		</div>
		<div class="productCard_priceCol">
			<div class="productCard_priceColIn">
				<div class="productCard_priceSection">
					<c:if test="${not empty product.price}">
						<span class="productCard_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							<meta itemprop="priceCurrency" content="<format:fromPriceCurrency priceData="${product.price}"/>">
							<meta itemprop="price" content="<format:fromPriceValue priceData="${product.price}"/>">
							<span class="rub"><format:fromPrice priceData="${product.price}"/></span>
							<span class="danger">*</span>
						</span>
					</c:if>
					<c:choose>
						<c:when test="${productIsOutOfStock}">
							<c:if test="${productIsOutOfStock}">
								<span class="productCard_outOfStock"><spring:theme code="pickup.out.of.stock" text="Out of stock"/></span>
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${not empty cb:sanitizeUrl(product.merchantUrl)}">
								<c:set var="ulmartCookieName" value="${jspPropertyConfigurer.getProperty('ulmart.cookie.userid')}" />

								<c:set var="ulmartUrl" value="${not empty cookie[ulmartCookieName].value and cookie[ulmartCookieName].value ne '-1' ?
						    cb:patternUrl(cb:sanitizeUrl(product.merchantUrl), product.merchant.siteUrlPattern, cookie[ulmartCookieName].value) :
						    cb:sanitizeUrl(product.merchantUrl)}"/>

								<div class="productCard_goWrap">
									<a href="${ulmartUrl}" target="_blank"
									   class="productCard_goBtn js_toMerchantBtn js-gtm-product-go-btn"
									   data-gtm-id="${product.code}"
									   data-gtm-name="${product.name}"
									   data-gtm-category-name="${product.categories[0].name}"
									   data-gtm-category-id="${product.categories[0].code}"
									   data-gtm-vendor-id="${product.merchant.code}"
									   data-gtm-vendor-name="${product.merchant.name}"
									   data-gtm-price="<format:fromPriceValue priceData="${product.price}"/>"><spring:theme
											code="product.goButtonText2" text="Buy"/></a>
									<div class="productCard_goDescription">
										<spring:theme code="productCard.goDescription" text="Go description on English"/>
									</div>
								</div>
							</c:if>
						</c:otherwise>
					</c:choose>
					<c:if test="${not empty product.price}">
						<div class="danger_text"><spring:theme code="productCard.dangerText" text="Danger text on English"/></div>
					</c:if>
				</div>
				<div class="productCard_deliverySection">
					<div class="productCard_deliverySectionIn">
						<span class="productCard_deliveryFrom"><spring:theme code="productCard.deliveryFrom"
																			 text="Product from China"/></span>
						<span class="productCard_deliveryTime"><spring:theme code="productCard.deliveryLong"
																			 text="10-15 days"/></span>
						<a href="<c:url value="/help"/>" class="productCard_deliveryBtn js-gtm-delivery-btn"><spring:theme
								code="productCard.deliveryBtnText" text="More about abrode products"/></a>
					</div>
				</div>
				<c:choose>
					<c:when test="${not empty product.merchant}">
						<a href="${product.merchant.url}" class="supplier">
							<div class="supplier_img">
								<img src="${product.merchant.picture.url}" alt="${product.merchant.picture.altText}" title="${product.merchant.picture.altText}"/>
							</div>
							<div class="supplier_ttl">${product.merchant.name}</div>
							<div class="supplier_subttl"><spring:theme code="productCard.china" text="China"/></div>
						</a>
					</c:when>
				</c:choose>
			</div>
		</div>
	</div>

	<%--<div class="span-10 productDescription last">--%>
		<%--<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">--%>
			<%--<product:productPricePanel product="${product}"/>--%>
		<%--</ycommerce:testId>--%>
		<%--<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">--%>
			<%--<h1>--%>
				<%--${product.name}--%>
			<%--</h1>--%>
		<%--</ycommerce:testId>--%>

		<%--<product:productReviewSummary product="${product}"/>--%>


		<%--<div class="summary">--%>
			<%--${product.summary}--%>
		<%--</div>--%>

		<%--<product:productPromotionSection product="${product}"/>--%>

		<%--<cms:pageSlot position="VariantSelector" var="component" element="div">--%>
			<%--<cms:component component="${component}"/>--%>
		<%--</cms:pageSlot>--%>

		<%--<cms:pageSlot position="AddToCart" var="component" element="div" class="span-10 last add-to-cart">--%>
			<%--<cms:component component="${component}"/>--%>
		<%--</cms:pageSlot>--%>
	<%--</div>--%>

	<%--<cms:pageSlot position="Section2" var="feature" element="div" class="span-8 section2 cms_disp-img_slot last">--%>
		<%--<cms:component component="${feature}"/>--%>
	<%--</cms:pageSlot>--%>
</article>
<product:productErorsPopups/>