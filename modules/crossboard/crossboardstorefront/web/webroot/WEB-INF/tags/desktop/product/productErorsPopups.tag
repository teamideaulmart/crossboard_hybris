<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="findErrorsPopup" class="popupBlock mfp-hide">
	<div class="popupBlock_close js-popup-close">
		<span class="text"><spring:theme code="sendPopup.close" text="Close"/></span>
		<span class="cross">&#x2716;</span>
	</div>
	<div class="popupBlock_body">
		<form action="#" class="popupBlock_form">
			<div class="popupBlock_formItem">
				<label class="popupBlock_label" for="addressTextarea"><spring:theme code="sendPopup.address" text="Address"/></label>
				<textarea readonly="true" class="popupBlock_textarea" id="addressTextarea"></textarea>
			</div>
			<div class="popupBlock_formItem">
				<label class="popupBlock_label" for="errorTextarea"><spring:theme code="sendPopup.error" text="Error"/></label>
				<textarea readonly="true" class="popupBlock_textarea" id="errorTextarea"></textarea>
			</div>
			<div class="popupBlock_formItem">
				<label class="popupBlock_label" for="errorCommentTextarea"><spring:theme code="sendPopup.comment" text="Comment"/></label>
				<textarea class="popupBlock_textarea" id="errorCommentTextarea"></textarea>
			</div>
			<div class="popupBlock_formItem">
				<button id="findErrorsSend" class="popupBlock_btn"><spring:theme code="sendPopup.sendButtonText" text="Send"/></button>
			</div>
		</form>
	</div>
</div>

<div id="findErrorsPopupSucess" class="popupBlock mfp-hide">
	<div class="popupBlock_close js-popup-close">
		<span class="text"><spring:theme code="sendPopup.close" text="Close"/></span>
		<span class="cross">&#x2716;</span>
	</div>
	<div class="popupBlock_body">
		<span><spring:theme code="sendPopup.successResult" text="Sucess"/></span>
	</div>
</div>

<div id="findErrorsPopupFailed" class="popupBlock mfp-hide">
	<div class="popupBlock_close js-popup-close">
		<span class="text"><spring:theme code="sendPopup.close" text="Close"/></span>
		<span class="cross">&#x2716;</span>
	</div>
	<div class="popupBlock_body">
		<span><spring:theme code="sendPopup.failedResult" text="Failed"/></span>
	</div>
</div>

<div id="findErrorsPopupLimit" class="popupBlock mfp-hide">
	<div class="popupBlock_close js-popup-close">
		<span class="text"><spring:theme code="sendPopup.close" text="Close"/></span>
		<span class="cross">&#x2716;</span>
	</div>
	<div class="popupBlock_body">
		<span><spring:theme code="sendPopup.limitResult" text="Failed"/></span>
	</div>
</div>