<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${not empty pageData.breadcrumbs}">
	<%-- div class="headline"><spring:theme code="search.nav.appliedFilters"/>	</div --%>
	<div class="facet facet--selectedItems">
		<ul class="facet_block">
			<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
                <li class="facetSelectedItem">
                    <span class="facetSelectedItem_name">${breadcrumb.facetValueName}</span>
                    <c:set var="searchText" value=""/>
                    <c:if test="${not empty searchPageData.freeTextSearch}">
                        <c:set var="searchText" value="&text=${searchPageData.freeTextSearch}"/>
                    </c:if>
                    <c:url value="${breadcrumb.removeQuery.url}${searchText}" var="removeQueryUrl"/>
                    <a class="facetSelectedItem_remove" href="${removeQueryUrl}" >x</a>
                    <input type="hidden" value=":${breadcrumb.facetCode}:${breadcrumb.facetValueCode}" class="js-applied-filter"/>
				</li>
			</c:forEach>
            <c:set var="searchText" value=""/>
            <c:if test="${not empty searchPageData.freeTextSearch}">
                <c:set var="searchText" value="?text=${searchPageData.freeTextSearch}"/>
            </c:if>
			<a class="facetSelectedItem facetSelectedItem_dropAll js-gtm-facet-reset" href="${requestScope['javax.servlet.forward.request_uri']}${searchText}">
				<span class="facetSelectedItem_remove">x</span>
				<span class="facetSelectedItem_name"><spring:theme code="search.nav.facet.dropAll" /></span>
			</a>
		</ul>
	</div>
</c:if>
