<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>


<div class="span-14 productImage">
	<div class="productImagePrimary" id="primary_image">
		<c:if test="${fn:contains(product.url, '?sku=')}">
			<c:url value="${fn:substringBefore(product.url, '?sku=')}/zoomImages" var="productZoomImagesUrl"/>
		</c:if>
		<c:if test="${not fn:contains(product.url, '?sku=')}">
			<c:url value="${product.url}/zoomImages" var="productZoomImagesUrl"/>
		</c:if>
		<div class="<c:if test="${not empty product.images[0].url}">productImagePrimaryLink</c:if> js-gtm-product-image" data-gtm-name="${product.name}" data-gtm-id="${product.code}">
			<c:choose>
				<c:when test="${not empty product.images[0].url}">
					<img src="${product.images[0].url}&resize=380x380" alt="${product.name}" data-galleryposition="0" />
					<ycommerce:testId code="productDetails_zoomImage_button">
						<span class="productImageZoomLink"></span>
					</ycommerce:testId>
				</c:when>
				<c:otherwise>
					<img class="productCard_missingImg" src="<spring:theme code="img.missingProductImage.product" text="img.missingProductImage.product"/>" alt="">
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<div class="productImageGallery">
		<ul class="productImageGallery_list">
			<c:forEach items="${product.images}" var="image" varStatus="varStatus">
				<li class="productImageGallery_item">
			<span class="thumb ${(varStatus.index==0)? "active":""}">
				<img src="${image.url}&resize=50x50" alt="${product.name}_${varStatus.index}" data-primaryimagesrc="${image.url}&resize=380x380" data-galleryposition="${varStatus.index}" title="${product.name}_${varStatus.index}">
			</span>
				</li>
			</c:forEach>
		</ul>
	</div>
	<div class="fotorama is_hidden" data-width="700" data-allowfullscreen="true" data-fit="contain" data-nav="thumbs" data-transition="crossfade" data-auto="false">
		<c:forEach items="${product.images}" var="image" varStatus="varStatus">
			<img  src="${image.url}&resize=1000x1000" alt="${image.altText}" data-primaryimagesrc="${image.url}" data-galleryposition="${varStatus.index}" title="${image.altText}">
		</c:forEach>
	</div>
</div>