<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="indexInList" required="false" type="java.lang.Integer" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/desktop/storepickup" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action" %>

<spring:theme code="text.addToCart" var="addToCartText"/>
<c:url value="${product.url}" var="productUrl"/>

<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>

<c:set var="product" value="${product}" scope="request"/>
<c:set var="addToCartText" value="${addToCartText}" scope="request"/>
<c:set var="addToCartUrl" value="${addToCartUrl}" scope="request"/>

<c:choose>
	<c:when test="${pageType == 'CATEGORY'}">
		<c:set var="googlePageType" value="MainCatalog"/>
	</c:when>
	<c:when test="${pageType == 'PRODUCTSEARCH'}">
		<c:set var="googlePageType" value="SearchResults"/>
	</c:when>
</c:choose>

<ycommerce:testId code="product_wholeProduct">
	<div class="productItem ${hasPromotion ? 'productItem--promo' : ''}">
		<script>
			if (typeof dataLayerProducts != 'undefined') {
				var productPrice = '<format:fromPriceValue priceData="${product.price}"/>';
				dataLayerProducts.push({
					'eventProductId':'${product.code}',
					'eventProductName':'${product.name}',
					'eventLocation':'${googlePageType}',
					'eventPosition':'${not empty indexInList ? indexInList : ''}',
					'eventVendorName':'${product.manufacturer}',
					'eventProductPrice': productPrice == '' ? '' : Number(productPrice.match(/\d+/)[0]).toFixed(2),
					'eventCategoryId':'${product.categories[0].code}',
					'eventCategoryName':'${product.categories[0].name}'
				});
			}
		</script>
		<a href="${productUrl}" title="${product.name}" class="productMainLink js-gtm-product-item"
		   data-gtm-location="${googlePageType}"
		   data-gtm-num="${not empty indexInList ? indexInList : ''}"
		   data-gtm-id="${product.code}"
		   data-gtm-name="${product.name}"
		   data-gtm-category-name="${product.categories[0].name}"
		   data-gtm-category-id="${product.categories[0].code}"
		   data-gtm-vendor-name="${product.manufacturer}"
		   data-gtm-price="<format:fromPriceValue priceData="${product.price}"/>">
			<span class="productItem_img">
				<product:productPrimaryImage product="${product}" format="product"/>
				<c:if test="${not empty product.potentialPromotions and not empty product.potentialPromotions[0].productBanner}">
					<img class="promo" src="${product.potentialPromotions[0].productBanner.url}" alt="${product.potentialPromotions[0].description}" title="${product.potentialPromotions[0].description}"/>
				</c:if>
			</span>
			<span class="productItem_ttl js-two-lines">
				<ycommerce:testId code="product_productName">${product.name}</ycommerce:testId>
			</span>
			<div class="productItem_code">
				<span class="word"><spring:theme code="product.code"/></span>
				<span class="num"> ${product.code}</span>
			</div>
		</a>
		<div class="productItem_info">
				<%--<c:choose>--%>
				<%--<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">--%>
				<%--<c:set var="buttonType">button</c:set>--%>
				<%--<spring:theme code="text.addToCart.outOfStock" var="addToCartText"/>--%>
				<%--<span class='listProductLowStock listProductOutOfStock mlist-stock'>${addToCartText}</span>--%>
				<%--</c:when>--%>
				<%--</c:choose>--%>

			<div class="productItem_infoSplit">
				<c:if test="${not empty product.price}">
					<div class="productItem_price">
						<div class="rub">
								<c:set var="buttonType">submit</c:set>
								<ycommerce:testId code="product_productPrice">
									<format:fromPrice priceData="${product.price}"/>
								</ycommerce:testId>
						</div>
						<a class="priceAsterix" href="#footer_text">*</a>
					</div>
				</c:if>
				<div class="productItem_fromThereWrap">
					<div class="productItem_fromThere balloon_trigger js-gtm-item-from"
						 data-gtm-country="<spring:theme code="product.fromChina" />">
						<spring:theme code="product.fromChina"/>
						<div class="balloon"><spring:theme code="product.fromThere.tooltip"/></div>
					</div>
				</div>
			</div>
			<div id="actions-container-for-${component.uid}" class="listAddPickupContainer clearfix">
				<action:actions element="div" parentComponent="${component}"/>
			</div>

				<%--<div class="twoCols">--%>
				<%--<c:if test="${not empty product.averageRating}">--%>
				<%--<product:productStars rating="${product.averageRating}"/>--%>
				<%--</c:if>--%>

				<%--<c:set var="product" value="${product}" scope="request"/>--%>
				<%--<c:set var="addToCartText" value="${addToCartText}" scope="request"/>--%>
				<%--<c:set var="addToCartUrl" value="${addToCartUrl}" scope="request"/>--%>
				<%--</div>--%>
		</div>
	</div>
</ycommerce:testId>
