<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="breadcrumbs" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="/" var="homeUrl"/>
<div id="breadcrumb" class="breadcrumbs">
	<ul class="breadcrumbs_list" itemscope itemtype="http://schema.org/BreadcrumbList">
		<li class="breadcrumbs_listItem" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<a itemprop="item" class="breadcrumbs--home" href="${homeUrl}"><span itemprop="name"><spring:theme code="breadcrumb.home"/></span><meta itemprop="position" content="1" /></a>
		</li>
		<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
			<c:choose>
				<c:when test="${breadcrumb.url eq '#'}">
					<li class="breadcrumbs_listItem" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" >
						<a itemprop="item" class="<c:if test="${not empty breadcrumb.linkClass}"> ${breadcrumb.linkClass}</c:if><c:if test="${status.last}"> last</c:if>" href="#" onclick="return false;">
							<span itemprop="name">${fn:escapeXml(breadcrumb.name)}</span>
							<meta itemprop="position" content="${status.count + 1}" />
						</a>
					</li>
				</c:when>

				<c:otherwise>
					<c:url value="${breadcrumb.url}" var="breadcrumbUrl"/>
					<li class="breadcrumbs_listItem" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" >
						<a itemprop="item"
							<c:if test="${status.last}">class="breadcrumbs--last"</c:if>
							<c:if test="${!status.last}">href="${breadcrumbUrl}"</c:if>>
							<span itemprop="name">${breadcrumb.name}</span>
						</a>
						<meta itemprop="position" content="${status.count + 1}" />
					</li>
				</c:otherwise>
			</c:choose>

			<c:if test="${status.count} == 1">
				<c:set value="${breadcrumb.name}" var="categoryName" scope="request"/>
			</c:if>
		</c:forEach>
	</ul>
</div>

