<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<div id="productTabs" class="productTabs">
	<c:if test="${not empty product.attributes}">
		<div class="productTabs_detailsHeader">
			<div class="sectionHdr"><spring:theme code="section.title.options" text="Product options" /></div>
			<div class="text">
				<span><spring:theme code="productCard.adviceText" text="If you find error push Shift + Enter" /></span>
			</div>
		</div>
		<div class="productOptions" id="productOptionsDesc" tabindex="0">
			<div class="productOptions_section">
					<%--<h3 class="productOptions_sectionTitle">Title of the category</h3>--%>
				<ul class="productOptions_list">
					<c:forEach items="${product.attributes}" var="attribute">
						<li class="productOptions_item">
							<span class="title"><span>${attribute.name}</span> <span class="decor"></span></span>
							<span class="desc">${attribute.value}&nbsp;${attribute.unit}</span>
						</li>
					</c:forEach>
					<c:if test="${not empty product.length}">
						<li class="productOptions_item">
							<span class="title"><span><spring:theme code="productChars.length" text="length"/></span> <span class="decor"></span></span>
							<span class="desc">${product.length}&nbsp;<spring:theme code="unit.mm" text="mm"/></span>
						</li>
					</c:if>
					<c:if test="${not empty product.width}">
						<li class="productOptions_item">
							<span class="title"><span><spring:theme code="productChars.width" text="width"/></span> <span class="decor"></span></span>
							<span class="desc">${product.width}&nbsp;<spring:theme code="unit.mm" text="mm"/></span>
						</li>
					</c:if>
					<c:if test="${not empty product.height}">
						<li class="productOptions_item">
							<span class="title"><span><spring:theme code="productChars.height" text="height"/></span> <span class="decor"></span></span>
							<span class="desc">${product.height}&nbsp;<spring:theme code="unit.mm" text="mm"/></span>
						</li>
					</c:if>
					<c:if test="${not empty product.volume}">
						<li class="productOptions_item">
							<span class="title"><span><spring:theme code="productChars.volume" text="volume"/></span> <span class="decor"></span></span>
							<span class="desc">${product.volume}&nbsp;<spring:theme code="unit.weight" text="dm3"/></span>
						</li>
					</c:if>
					<c:if test="${not empty product.warrantyPeriod}">
						<li class="productOptions_item">
							<span class="title"><span><spring:theme code="productChars.warrantyPeriod" text="warranty period"/></span> <span class="decor"></span></span>
							<span class="desc">${product.warrantyPeriod}&nbsp;<c:if test="${product.warrantyPeriod > 0}"><spring:theme code="unit.mth" text="mth"/></c:if></span>
						</li>
					</c:if>
				</ul>
			</div>
		</div>
	</c:if>

	<%--<div class="tabHead"><spring:theme code="product.product.details" /></div>--%>
	<%--<div class="tabBody"><product:productDetailsTab product="${product}"/></div>--%>
	<%--<div class="tabHead" id="tab-reviews"><spring:theme code="review.reviews" /></div>--%>
	<%--<div class="tabBody" ><product:productPageReviewsTab product="${product}"/></div>--%>
	<cms:pageSlot position="Tabs" var="tabs">
		<cms:component component="${tabs}"/>
	</cms:pageSlot>
</div>