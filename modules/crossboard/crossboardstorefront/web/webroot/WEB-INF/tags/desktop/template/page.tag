<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>
<%@ attribute name="pageHeadScripts" required="false" fragment="true" %>
<%@ attribute name="dfpBanner" required="false" fragment="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>
<%@ attribute name="dataLayerJs" required="false" fragment="true" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/desktop/common/footer" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<template:master pageTitle="${pageTitle}">
	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss"/>
	</jsp:attribute>
 
	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts"/>
	</jsp:attribute>

    <jsp:attribute name="pageHeadScripts">
		<jsp:invoke fragment="pageHeadScripts"/>
	</jsp:attribute>

	<jsp:attribute name="dataLayerJs">
		<jsp:invoke fragment="dataLayerJs"/>
	</jsp:attribute>

	<jsp:body>

	<div id="page" data-currency-iso-code="${currentCurrency.isocode}" class="b-page_theme_normal">
        <a name="up" href="#" id="up"></a>
		<div class="dummy dummy_header b-page__header"></div>
        <input type="hidden" id="cookie_csrf" value="${cookie['_csrf']}"/>
        <c:if test="${empty cookie['_csrf']}">
            <iframe id="ulmart-frame" src="https://www.ulmart.ru/remindSend" width="1" height="1" frameborder="0"></iframe>
        </c:if>
		<div class="container-fluid">
			<section class="section section--menuSearch">
				<div class="mainMenu">
					<div class="button mainMenu_header"><spring:theme code="text.header.cataloge"/></div>
					<nav:topNavigation/>

					<jsp:invoke fragment="dfpBanner"/>
				</div>
				<cms:pageSlot position="SearchBox" var="component">
					<cms:component component="${component}"  />
				</cms:pageSlot>
			</section>
			<header:bottomHeader />
			<jsp:doBody/>
		</div>

		<footer:footer/>

		<div class="dummy dummy_footer"></div>
	</div>

	</jsp:body>
</template:master>
