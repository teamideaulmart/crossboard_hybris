<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>


<%-- div class="headline"><spring:theme code="search.nav.refinements"/></div --%>
<c:forEach items="${pageData.facets}" var="facet">
	<c:choose>
		<c:when test="${facet.code eq 'availableInStores'}">
			<nav:facetNavRefinementStoresFacet facetData="${facet}" userLocation="${userLocation}"/>
		</c:when>
		<c:otherwise>
			<nav:facetNavRefinementFacet facetData="${facet}"/>
		</c:otherwise>
	</c:choose>
</c:forEach>

<%--price slider--%>
<div class="facet facet--multiSelect">
	<a class="facet_head spoiler refinementToggle" href="#"><spring:theme code="facet.price" text="price"/></a>
	<div class="facetValues">
		<div class="facet_block spoilerContent js_uiSliderParent">
			<button class="facet_clearBtn js_clearBtn"><spring:theme code="facet.reset" text="Clear filter" /></button>
			<div class="form_fieldSection form_fieldSection--line">
				<label for="price_min" class="visually_hidden">Min value</label>
				<input type="text" id="price_min" class="form_field form_field--default form_field--slider js_uiSliderMinField">
				<span class="separator"><spring:theme code="symols.longDash" text="-" /></span>
				<label for="price_max" class="visually_hidden">Max value</label>
				<input type="text" id="price_max" class="form_field form_field--default form_field--slider js_uiSliderMaxField">
				<span class="separator"><spring:theme code="facet.currency" text="rub"/></span>
			</div>
			<div class="js_uiSlider ui-slider-theme-default ui-slider-horizontal" data-min="${minPrice}" data-max="${maxPrice}" data-step="10">
				<div class="ui-slider-min"></div>
				<div class="ui-slider-max"></div>
			</div>
		</div>
	</div>
</div>


