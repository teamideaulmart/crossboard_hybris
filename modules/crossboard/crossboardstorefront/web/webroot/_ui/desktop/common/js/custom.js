function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

$(document).ready(function() {
	// инициализация скроллера js_bannerTicker
	$(".js_bannerTicker").each(function(){
		// проверяем, достаточно ли содержимого для скроллера
		var contentWidth = 0;
		$(this).find('.js_bannerTickerItem').each(function() {
			contentWidth += $(this).outerWidth();
		});

		if(contentWidth > $(this).width()) {
			$(this).smoothDivScroll();
		}
	});

	// плавный скролл к анкеру
	$('a').each(function(){
		var target = $(this).attr('href');
		if(target && target.length > 1 && target[0] == '#') {
			$(this).click(function(){
				$('html, body').animate({ scrollTop: $(target).offset().top }, 500);
				return false;
			})
		}
	});


	// Спойлер
	// Переключается по нажатию на элемент с классом .spoiler
	// Пытается переключать следующий за переключателем .spoilerContent, если не находит, ищет его рядом
	$('.spoiler').each(function(){
		$(this).click(function(event){
			var $this = $(this), $spoilerCont;
			if($(this).next('.spoilerContent')[0]){
				$spoilerCont = $(this).next('.spoilerContent');
			} else {
				$spoilerCont = $(this).parent().find('.spoilerContent');
			}

			if(parseInt($spoilerCont.css('height')) == 0){
				$this.css('margin','').css('overflow','');
				$this.removeClass('spoiler--close');

				$spoilerCont.animate(
					{height:50},300,
					function(){
						$spoilerCont.removeClass('spoilerContent--close');
						$spoilerCont.css('height','');
				});
				if($this.attr('data-text-open')){
					$this.text($this.attr('data-text-open'))
				}
			} else {
				$spoilerCont.animate(
					{height:0},300,
					function(){
						$this.addClass('spoiler--close');
						$spoilerCont.addClass('spoilerContent--close');
						$spoilerCont.css('height','');
					}
				);
				if($this.attr('data-text-close')){
					$this.text($this.attr('data-text-close'))
				}
			}
			event.preventDefault();
		})
	});

	// carousel
	$('.js-productLineCarousel').jcarousel({itemFallbackDimension: 210});

	// tooltip
	var $tooltipTrigger = $(".js_tooltipTrigger"),
		$tooltipContainer = $('.js_tooltipContainer');
	$tooltipTrigger.on('click',function() {
		$(this).closest($tooltipContainer).toggleClass('is_show');
	});
	// hide tooltip if user clicks outside
	$(window).on('click',function() {
		$tooltipContainer.removeClass('is_show');
	});

	$tooltipContainer.click(function(event){
		event.stopPropagation();
	});
	$(".rotationComponent").each(function(){
		$(this).waitForImages(function (){
			var intervalValue=$(this).attr('intervalValue')*1000;
			$(this).slideView({toolTip: false, ttOpacity: 0.6, autoPlay: true, autoPlayTime: intervalValue});
		});
	});

	// popUp
	$('.popUpButton').each(function(){
		$(this).click(function(){
			var $popUpCont = $('#'+$(this).attr('for'));

			//try 2 run YouTube video
			var $ytVideo = $popUpCont.find($('.ytplayer'));
			if($ytVideo) {
				yt_players[$ytVideo.attr('id')].playVideo();
			}
			//end_try

			$popUpCont.addClass('popUpCont--show');

			$popUpCont.click(function(){
				$(this).removeClass('popUpCont--show');

				//try 2 pause YouTube video
				var $ytVideo = $(this).find($('.ytplayer'));
				if($ytVideo) {
					yt_players[$ytVideo.attr('id')].pauseVideo();
				}
				//end_try
			})
		})
	});


	// Код управления YouTube плеерами на странице. У всех должен быть класс .ytplayer
	if($(".ytplayer")){
		var yt_int, yt_players={},
			initYT = function() {
				$(".ytplayer").each(function() {
					yt_players[this.id] = new YT.Player(this.id);
				});
			};
		$.getScript("//www.youtube.com/player_api", function() {
			yt_int = setInterval(function(){
				if(typeof YT === "object"){
					initYT();
					clearInterval(yt_int);
				}
			},500);
		});
    }
    //yt_players['player_id'].playVideo();

	// Ограничение названия товара в 2 строки. 42 - высота строки в пикселях

	$(".js-two-lines").shave(42);

	// price jquery ui slider
	$('.js_uiSlider').each(function () {
		var $slider = $(this);
		var minVal = Number($slider.data('min'));
		var maxVal = calculateSliderMaxValue( Number($slider.data('max')) );
		var	step = $slider.data('step');
		var	$parent = $slider.closest('.js_uiSliderParent');
		var	$minField = $parent.find('.js_uiSliderMinField');
		var	$maxField = $parent.find('.js_uiSliderMaxField');
		var	$minText = $parent.find(".ui-slider-min");
		var	$maxText = $parent.find(".ui-slider-max");
		var	$clearBtn = $parent.find('.js_clearBtn');
		var currQuery = decodeURIComponent(location.search);
		var queryRE = /priceValue:\[(\d+)(\+|\s)TO(\+|\s)(\d+)\]/;
		var tempMin = 0;
		var tempMax = 0;
		var valueCurrRange = [];

		$minField.val(minVal);
		$maxField.val(maxVal);
		$minText.text(minVal);
		$maxText.text(maxVal);

		if(queryRE.test(currQuery)) {
			tempMin = currQuery.match(queryRE)[1];
			tempMax = currQuery.match(queryRE)[4];
			valueCurrRange = [tempMin, tempMax];
			$('#price_min').val(tempMin);
			$('#price_max').val(tempMax);
		} else {
			valueCurrRange = [minVal, maxVal];
			$('#price_min').val(minVal);
			$('#price_max').val(maxVal);
		}

		$slider.slider({
			range: true,
			min: minVal,
			max: maxVal,
			values: valueCurrRange,
			step: step,
			slide: function (event, ui) {
				$minField.val(ui.values[0]);
				$maxField.val(ui.values[1]);
			}
		});

		// при изменении минимального поля меняем его в слайдере, максимальное поле оставляем прежним
		$minField.on('change', function () {
            var newMinVal = $(this).val(),
                currentMaxVal = $slider.slider("values")[1];
            if (!(newMinVal >= minVal && newMinVal <= maxVal)) {
                newMinVal = minVal;
            }
            $(this).val(newMinVal);
            $slider.slider("values", [newMinVal, currentMaxVal]);
        });

		// тоже самое с максимальным полем
		$maxField.on('change', function () {
			var newMaxVal = $(this).val(),
				currentMinVal = $slider.slider("values")[0];
			if (!(newMaxVal >= minVal && newMaxVal <= maxVal)) {
				newMaxVal = maxVal;
			}
			$(this).val(newMaxVal);
			$slider.slider("values", [currentMinVal, newMaxVal]);
		});

		// Очищаем фильтр по клику на clearBtn
		$clearBtn.on('click', function () {
			$minField.val(minVal);
			$maxField.val(maxVal);
			$slider.slider("values", [minVal, maxVal]);
		});
	});

	// Выставляет активную кнопку в зависимости от типа отображения в куках 0 - Grid, 1 - List
	if($('.js-viewTypeButtons')){
		$('.js-viewTypeButtons_mode').each(function(){
			$(this).removeClass('js-viewTypeButtons_mode--active')
		});
		if(getCookie('viewType') == 0) {
			$('.js-viewTypeButtons_mode--grid').addClass('js-viewTypeButtons_mode--active');
		} else {
			$('.js-viewTypeButtons_mode--list').addClass('js-viewTypeButtons_mode--active');
		}
	}

	$(".js-popup-close").on("click", function() {
		$.magnificPopup.close();
	});
});

function cutSymbols(value, wordwise, max, tail) {
	if (!value) return '';

	max = parseInt(max, 10);
	if (!max) return value;
	if (value.length <= max) return value;

	value = value.substr(0, max);
	if (wordwise) {
		var lastspace = value.lastIndexOf(' ');
		if (lastspace != -1) {
			//Also remove . and , so its gives a cleaner result.
			if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
				lastspace = lastspace - 1;
			}
			value = value.substr(0, lastspace);
		}
	}

	return value + (tail || ' …');
}

var sendDescriptionError = function(input) {
	$.ajax('/ajax/descriptionError', {
		type: 'POST',
		headers: {
			'Content-Type': 'application/json; charset=UTF-8'
		},
		dataType: 'json',
		data: JSON.stringify({
			descriptionError: input.descriptionError,
			currentUrl:input.currentUrl,
			userMessage: input.userMessage
		})
	}).done(function(data) {
		if (typeof data === 'string') {
			input.callback(JSON.parse(data));
		} else {
			input.callback(data);
		}
	});
};

if (document.getElementById("goodsdesc")) {
	document.getElementById("goodsdesc").addEventListener("keydown", modifyText, false);
}
if (document.getElementById("productCode_field")) {
	document.getElementById("productCode_field").addEventListener("keydown", modifyText, false);
}
if (document.getElementById("productOptionsDesc")) {
	document.getElementById("productOptionsDesc").addEventListener("keydown", modifyText, false);
}

function modifyText(event) {
	if (event.shiftKey && event.keyCode == 13) {
		var txt = getSelectionText(),
			currentUrl = window.location.href;
		if (txt) {
			$("#addressTextarea").text(currentUrl);
			$("#errorTextarea").text(txt);
			$.magnificPopup.open({
				items: {
					src: '#findErrorsPopup',
					type: 'inline',
					showCloseBtn:false
				}
			});
			$("#findErrorsSend").on("click", function(e) {
				var userMessage = $("#errorCommentTextarea").val();
				sendDescriptionError({
					descriptionError:txt,
					currentUrl:currentUrl,
					userMessage:userMessage,
					callback: function(data) {
						$.magnificPopup.close();

						if(data.success === 'true'){
							setTimeout(function(){
								$.magnificPopup.open({
									items: {
										src: '#findErrorsPopupSucess',
										type: 'inline',
										showCloseBtn:false
									}
								});
							}, 500);
						} else if (data.success === 'false') {
							setTimeout(function(){
								$.magnificPopup.open({
									items: {
										src: '#findErrorsPopupFailed',
										type: 'inline',
										showCloseBtn:false
									}
								});
							}, 500);
						} else if(data.success === 'requestLimit') {
							setTimeout(function(){
								$(".js-sendLimit").text(data.minutes);
								$.magnificPopup.open({
									items: {
										src: '#findErrorsPopupLimit',
										type: 'inline',
										showCloseBtn:false
									}
								});
							}, 500);
						} else {
							$.magnificPopup.close();
						}
						setTimeout(function(){
							$.magnificPopup.close();
						}, 4000);
					}
				});
				e.preventDefault();
			});
		}
	}
}

function messageboxRemove(){
	$('.messageboxContent').animate({
		opacity: 0
	},300,function(){
		$('.messageboxContent').remove()
	})
}

// Not IE - getSelection()
// IE - createRange()
function getSelectionText() {
	var txt;
	if (txt = window.getSelection) {
		txt = window.getSelection().toString();
	} else {
	   txt = document.selection.createRange().text;
	}
	return txt;
}

function calculateSliderMaxValue(value) {
	value = Math.ceil(value / 10) * 10;
	return value;
}