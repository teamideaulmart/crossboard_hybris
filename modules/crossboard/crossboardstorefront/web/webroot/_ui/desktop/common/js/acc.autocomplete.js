ACC.autocomplete = {

  bindAll: function () {
    this.bindSearchAutocomplete();
  },

  bindSearchAutocomplete: function () {
    // extend the default autocomplete widget, to solve issue on multiple instances of the searchbox component
    $.widget('custom.yautocomplete', $.ui.autocomplete, {
      _create: function () {

        // get instance specific options form the html data attr
        var option = this.element.data('options');
        var activeMenuItem = null;
        // set the options to the widget
        this._setOptions({
          minLength: option.minCharactersBeforeRequest,
          displayProductImages: option.displayProductImages,
          delay: option.waitTimeBeforeRequest,
          autocompleteUrl: option.autocompleteUrl,
          source: this.source
        });

        // call the _super()
        $.ui.autocomplete.prototype._create.call(this);

        this._on( this.element, {
            keydown: function( event ) {
                var keyCode = $.ui.keyCode;
                if (event.keyCode === keyCode.UP || event.keyCode === keyCode.DOWN) {
                  activeMenuItem = this.menu.active;
                } else if (event.keyCode == keyCode.ENTER) {
                  var itemUrl = $(activeMenuItem).find('a').attr('href');
                  if (itemUrl != undefined) {
                    window.location.href = itemUrl;
                  }
                }
            }
        });

      },
      options: {
        cache: {}, // init cache per instance
        focus: function () {
          // return false;
        }, // prevent textfield value replacement on item focus
        select: function (event, ui) {
          if (ui.item.url != undefined) {
	          window.location.href = ui.item.url;
          }
        }
      },
      _renderItem: function (ul, item) {
        var renderHtml = '';
        if (item.type === 'autoSuggestionHeader') {
          renderHtml = '<a style="pointer-events: none;">Возможно, вы имели в виду:</a>'
          return $('<li style="pointer-events: none" class="ui-menu-item_section">').data("item.autocomplete", item).append(renderHtml).appendTo(ul);
        } else if (item.type == 'autoSuggestion') {
          renderHtml = '<a href="' + item.url + '" class="clearfix">' + item.value + '</a>';
          return $("<li class='suggestions'>")
            .data('item.autocomplete', item)
            .append(renderHtml)
            .appendTo(ul);
        } else if (item.type == "ulmartResultsHeader") {
          renderHtml = '<a style="pointer-events: none;">Найдено в других пространствах Юлмарта</a>';
          return $('<li style="pointer-events: none" class="ui-menu-item_section">').data('item.autocomplete', item).append(renderHtml).appendTo(ul);
        } else if (item.type == "ulmartResults") {
          renderHtml = '<a class="text-lg" href="https://www.ulmart.ru/search?string=' + item.value + '"><i class="b-ico b-ico_u-commerce"></i>Юлмарт <small class="text-muted">E-commerce №1 (' + item.amount + ')</small></a>';
          return $('<li class="ulmart-result">').data('item.autocomplete', item).append(renderHtml).appendTo(ul);
        } else if (item.type == "ulmartResultsDiscount") {
          renderHtml = '<a class="text-lg" href="https://discount.ulmart.ru/search?string=' + item.value + '"><i class="b-ico b-ico_u-discount"></i>Юлмарт Second <small class="text-muted">Уцененные товары (' + item.amount + ')</small></a>';
          return $('<li class="ulmart-result">').data('item.autocomplete', item).append(renderHtml).appendTo(ul);
        } else if (item.type === 'hitProductHeader') {
          renderHtml = '<a style="pointer-events: none;">Категории</a>'
          return $('<li style="pointer-events: none;" class="ui-menu-item_section">').data("item.autocomplete", item).append(renderHtml).appendTo(ul);
        } else if (item.type === 'hitProduct') {
          renderHtml += '<a href="' + item.url + '" class="product clearfix">';
          renderHtml += '<span class="title">' + item.value + '</span>';
          renderHtml += '</a>';
          return $("<li class='product'>").data('item.autocomplete', item).append(renderHtml).appendTo(ul);
        } else if (item.type === 'productsHeader') {
          renderHtml = '<a style="pointer-events: none;">Товары</a>'
          return $('<li style="pointer-events: none;" class="ui-menu-item_section">').data("item.autocomplete", item).append(renderHtml).appendTo(ul);
        } else if (item.type == 'productResult') {
          renderHtml += '<a href="' + item.url + '" class="product clearfix">';
          renderHtml += '<span class="title">' + item.value + '</span>';
          renderHtml += '<span class="text-smaller text-strong text-from">' + 'из-за рубежа' + '</span>';
          renderHtml += '</a>';
          return $("<li class='product'>").data('item.autocomplete', item).append(renderHtml).appendTo(ul);
        }
      },
      source: function (request, response) {
        var self = this;
        var term = request.term.toLowerCase();

        if (term in self.options.cache) {
          return response(self.options.cache[term]);
        }
        $.getJSON(self.options.autocompleteUrl, {term: request.term}, function (data) {
          var autoSearchData = [];
          if (data.categories && data.categories.length > 0) {
            autoSearchData.push({
              type: 'hitProductHeader'
            });
            $.each(data.categories, function (i, obj) {
              autoSearchData.push({
                value: obj.name,
                url: obj.url,
                type: 'hitProduct'
              });
            });
          }
          if (data.suggestions.length > 0) {
            autoSearchData.push({
              type: 'autoSuggestionHeader'
            });
            $.each(data.suggestions, function (i, obj) {
              autoSearchData.push({
                value: obj.term,
                url: ACC.config.encodedContextPath + '/search?text=' + obj.term,
                type: 'autoSuggestion'
              });
            });
          }
          if (data.goodsAmountUlmart > 0 || data.goodsAmountDiscount > 0) {
            autoSearchData.push({
              type: 'ulmartResultsHeader'
            });
          }
          if (data.goodsAmountUlmart > 0) {
            autoSearchData.push({
              type: 'ulmartResults',
              value: request.term,
              amount: data.goodsAmountUlmart
            });
          }
          if (data.goodsAmountDiscount > 0) {
            autoSearchData.push({
              type: 'ulmartResultsDiscount',
              value: request.term,
              amount: data.goodsAmountDiscount
            });
          }
          if (data.products.length > 0) {
            autoSearchData.push({
              type: 'productsHeader'
            });
            $.each(data.products, function (i, obj) {
              autoSearchData.push({
                value: obj.name,
                code: obj.code,
                desc: obj.description,
                manufacturer: obj.manufacturer,
                url: ACC.config.encodedContextPath + obj.url,
                price: obj.price ? obj.price.formattedValue : '',
                type: 'productResult'
              });
            });
          }
          self.options.cache[term] = autoSearchData;
          return response(autoSearchData);
        });
      }
    });

    $search = $(".siteSearchInput");
    if ($search.length > 0) {
      $search.yautocomplete()
    }
  }
};

$(document).ready(function () {
  ACC.autocomplete.bindAll();
});
