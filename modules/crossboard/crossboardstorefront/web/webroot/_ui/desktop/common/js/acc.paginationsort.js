ACC.paginationsort = {

	downUpKeysPressed: false,

	bindAll: function ()
	{
		this.bindPaginaSort();
        this.bindFacetChange();
        if (window.location.href.indexOf("/search/") != -1) {
            this.getElasticSearchResults();
        }
	},
    getElasticSearchResults: function() {
        $.ajax({
            type: 'get',
            url: '/ulmart/getulmartsearchresults',
            data: {searchText: $('.breadcrumbs--last span').text()},
            success: function(data) {
                if (data != "") {
                    $('.section--withLeftCol').prepend(data);
                }
            }
        });
    },
	bindPaginaSort: function ()
	{
		with (ACC.paginationsort)
		{
			bindSortForm($('#sort_form1'));
			bindSortForm($('#sort_form2'));
		}
	},
	bindSortForm: function (sortForm) {
		if ($.browser.msie) {
			this.sortFormIEFix($(sortForm).children('select'), $(sortForm).children('select').val());
		}

		sortForm.change(function () {
			if (!$.browser.msie)
			{
				this.submit();
			}
			else
			{
				if (!ACC.paginationsort.downUpPressed)
				{
					this.submit();
				}
				ACC.paginationsort.downUpPressed = false;
			}
		});
	},
	sortFormIEFix: function (sortOptions, selectedOption)
	{
		sortOptions.keydown(function (e) {
			// Pressed up or down keys
			if (e.keyCode === 38 || e.keyCode === 40)
			{
				ACC.paginationsort.downUpPressed = true;
			}
			// Pressed enter
			else if (e.keyCode === 13 && selectedOption !== $(this).val())
			{
				$(this).parent().submit();
			}
			// Any other key
			else
			{
				ACC.paginationsort.downUpPressed = false;
			}
		});
	},
	buildUrl: function () {
		var qList = [];
		var text = '';
		var priceIsExist = -1;
		// запушили в массив qList значение фильтра
		// чекбоксы
		$.each($('.facet li'), function (index, item) {
			var form = $(item).find('form');
			if (form.find('input[type="checkbox"]:checked').length > 0) {
				var q = form.find('input[name="q"]').val();
				qList.push(q);
			}
			text = form.find('input[name="text"]').val();
		});
		// лейблы(категории)
		$.each($('.js-applied-filter'), function (index, item) {
			qList.push($(item).val());
		});

		qList.forEach(function (item, i) {
			var priceMask = /:priceValue:\[\d+ TO \d+\]/;
			if (priceMask.test(item)) {
				priceIsExist = i;
			}
		});

		// слайдер
		$('.facet .js_uiSlider').each(function (index, item) {
			var values = $(this).slider('values');
			var sliderRangeString = ':priceValue:[' + values[0] + ' TO ' + values[1] + ']';
			if (priceIsExist === -1) {
				qList.push(sliderRangeString);
			} else {
				qList[priceIsExist] = sliderRangeString;
			}
		});

		// проходимся по массиву qList
		// если какого-то элемента нет в массиве clearList - пушим его
		var clearList = new Array();
		for (var i = 0; i < qList.length; i++) {
			if (clearList.indexOf(qList[i]) == -1) {
				clearList.push(qList[i]);
			}
		}
		// если текст не пустой делаем url для фильтра с прибавлением текста на конце
		var facetUrl = '';
		if (text != '') {
			facetUrl += '?filters=';
		}
		if (text != '') {
			facetUrl += text;
		}
		// если текст пустой - делаем url для фильтра без прибавления текста на конце
		if (clearList.length > 0) {
			if (text == "") facetUrl += '?filters=';
			// добавляем текущее значение сортировки все данные из массива clearList
			facetUrl += ':' + $('#sortOptions1 option:selected').val();
			for (var i = 0; i < clearList.length; i++) {
				facetUrl += clearList[i];
			}
			facetUrl += '&text=' + text;
		} else if (text != '') {
			facetUrl += '&text=' + text;
		}
		var pageUrl = window.location.href.split(/[?#]/)[0];
		if (pageUrl.substr(pageUrl.length - 1) == "/") pageUrl = pageUrl.substring(0, pageUrl.length - 1);
		var searchUrl = pageUrl + facetUrl;
		if (searchUrl.indexOf('/search', searchUrl.length - '/search'.length) !== -1 && facetUrl == '') {
			searchUrl += '?filters=';
		}

		// возвращаем объект с url для поиска и строкой для сортировки
		return {
			searchUrl: searchUrl,
			facetUrl: facetUrl,
			pageUrl: pageUrl
		};
	},
	appendCountTip: function(urls, parent) {
		var resultCountHtml = '<div class="facet_resultPopup"><span>Выбрано моделей: <strong></strong><a class="buttonRed js-gtm-facet-show-btn" href="'+urls.searchUrl+'">Показать</a></span></div>';
		$('.facet_resultPopup').remove();
		$(parent).append(resultCountHtml);
		$.ajax({
			type: 'get',
			url: urls.pageUrl + '/results' + urls.facetUrl,
			success: function(data) {
				var totalNumberOfResults = data.pagination.totalNumberOfResults;
				$('.facet_resultPopup strong').text(totalNumberOfResults);
			}
		});
	},
    bindFacetChange: function() {
		// checkbox changes
        $('.facet input[type="checkbox"]').change(function() {
            if (!$(this).is(':checked')) {
                $('.js-applied-filter[value="'+$(this).closest('form').find('input[name="q"]').val()+'"]').remove();
            }
	        $(this).closest('label').toggleClass('checked');
	        var urls = ACC.paginationsort.buildUrl();
	        ACC.paginationsort.appendCountTip(urls,$(this).closest('li'));
        });
        // ui slider changes
        $(".js_uiSlider").on( "slidechange", function( event, ui ) {
	        var urls = ACC.paginationsort.buildUrl();
	        ACC.paginationsort.appendCountTip(urls,$(this).closest('.js_uiSliderParent'));
        } );
    }
};

$(document).ready(function ()
{
	ACC.paginationsort.bindAll();
});
