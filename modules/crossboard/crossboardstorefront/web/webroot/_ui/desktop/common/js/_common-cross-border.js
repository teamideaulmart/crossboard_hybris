/* desktop.blocks/common/common.js (part) */
var backendHost = window.backendHost? window.backendHost: "//ulmart.ru";
var qtipApi;
function initQtip(e, el){

    var self = $(el).eq(0);

    var targetContainer = false;
    if (self.data('qtip-in-modal')) targetContainer = $('.modal-scrollable');

    if (e) e.preventDefault();

    if (!self.data('qtip-ready')) {
        var tooltip = self.qtip({
            content: {
                text: function(event, api) {
                    if (self.data('qtip-href') !== undefined) {
                        $.ajax({
                            url: self.data('qtip-href'),
                            xhrFields: {
                                withCredentials: true
                            }
                        })
                            .then(function(content) {
                                content=content.replace(new RegExp("src=([\"'])/([^\"']+)","g"), "src=$1" + backendHost + "/$2");
                                var elements = $.parseHTML(content);
                                api.set('content.text', elements);
                                api.elements.content.find('.js-qtip-close').on('click', function(event){
                                    api.hide(event);
                                });
                            }, function(xhr, status, error) {
                            });
                    } else if (self.data('qtip-rel') !== undefined) {
                        return $('#' + self.data('qtip-rel')).show();
                    } else if (self.next('.js-qtip-text') !== undefined) {
                        return self.next('.js-qtip-text').html();
                    }
                    return $.parseHTML('<i class="b-ico b-ico_preloader_sm"></i>');
                }
            },
            position: {
                adjust: {
                    x: self.data('qtip-adjust-x') || undefined,
                    y: self.data('qtip-adjust-y') || undefined,
                    method: 'shift none',
                    resize: true,
                    scroll: true
                },
                viewport: $(window),
                container: targetContainer,
                my: self.data('qtip-my') || 'top left',
                at: self.data('qtip-at') || 'bottom right',
            },
            events: {
                render: function(event, api){
                    $(window).on('keydown', function(event) {
                        if(event.keyCode === 27) { api.hide(event); }
                    });
                    self.trigger('qtiprender');
                },
                show: function(event, api){
                    var el = $(api.elements.target[0]);
                    el.qtip('option', 'position.my', el.data('qtip-my') || 'top center');
                    el.qtip('option', 'position.at', el.data('qtip-at') || 'bottom center');
                    self.trigger('qtipshow');
                },
                visible: function(event, api){
                    api.elements.content.find('.js-qtip-close').on('click', function(event){
                        api.hide(event);
                    });
                    self.trigger('qtipvisible');
                },
                hidden: function(event, api){
                    api.elements.content.find('.js-qtip-close').off('click');
                    self.trigger('qtiphidden');
                }
            },
            show: {
                event: self.data('qtip-show-event') || 'click',
                effect: {
                    type: 'fade',
                    length: 300
                },
                solo: !(true && self.data('qtip-solo-event'))
            },
            hide: {
                event: self.data('qtip-hide-event') || 'unfocus',
                fixed: true,
                delay: 2000,
                effect: {
                    type: 'fade',
                    length: 300
                }
            },
            style: {
                classes: self.data('qtip-classes') || 'qtip-width-2 qtip_pad2'
            }
        }).data('qtip-ready', true);

        qtipApi = tooltip.qtip('api');
    }
}
$(document).ready(function(){
    $('body').delegate('.js-qtip-click-handle', 'click', function(e){
        var self = $(this);
        var isLink = $(e.target).closest('a').length > 0;

        if (isLink) {
            e.stopPropagation();
        }
        initQtip(e, self);
        self.qtip('show');
    });

    $('body').delegate('.js-qtip-hover-handle', 'hover', function(e){
        var self = $(this);
        var isLink = $(e.target).closest('a').length > 0;

        if (isLink) {
            e.stopPropagation();
        }
        initQtip(e, self);
        self.qtip('show');
    });


    $(document).on('click','.js-qtip-close-all', function(){
        $('div.qtip:visible').qtip('hide');
    });


    $("body").delegate("a.double-hover", "mouseover", function(e) {
        $(this).parents(".double-hover-wrap:first").find("a[href='" + $(this).attr("href") + "']").addClass("pseudo-hover");
    });

    $("body").delegate("a.double-hover", "mouseout", function(e) {
        $(this).parents(".double-hover-wrap:first").find("a[href='" + $(this).attr("href") + "']").removeClass("pseudo-hover");
    });

    // up
    (function($){
        //var lnk = $('#up-handle');
        //var ico =  lnk.find('.b-ico');
        $(document).on('click', '#up-handle', function(){
            if ($(this).hasClass('disabled')){
                return false;
            } else {
                return anchorScroller(this, 300, 0);
            }
        });
        $(window).on('scroll', function(){
            var lnk = $('#up-handle');
            var scrollTop = $(document).scrollTop();
            if (scrollTop > 300) {
                lnk.removeClass('disabled');
            } else {
                lnk.addClass('disabled');
            }
        });
    })(jQuery);

    $(document).on('click', '.site-lnk-close', function(){
        $(this).hide().next('.site-lnk').slideUp('300');
        return false;
    })

    $(document).on('show.bs.tab.data-api', '[data-toggle="tab"][data-target^=#]', function (e) {
        var url = $(this).attr('href');
        var selector = $(this).data('target');
        var preloader = $('<i class="b-ico b-ico_preloader b-ico_preloader_md"></i>');
        if (url !== undefined) {
            $(selector).empty().append(preloader).load(url, function(){
                preloader.remove();
            });
        }
    });

    (function($){
        if($('#b-dropdown-catalog-menu').hasClass("b-dropdown_autoheight")){
            return false;
        }
        var space = $('#h-space-catalog-menu');
        var dropdownbodyfirst = $('#b-dropdown-catalog-menu').find('.b-dropdown__body:first');
        var dropdownheader = $('#b-dropdown-catalog-menu').find('.b-dropdown__body__h');
        var dropdownheaderfirst = dropdownheader.filter(':first');
        var dropdownfooterfirst = $('#b-dropdown-catalog-menu').find('.b-dropdown__body__f:first');
        var hpadleft = $('#h-pad-left');

        dropdownheader.css('min-height', 0);

        if (dropdownbodyfirst.is(':hidden')) {
            dropdownbodyfirst.css('opacity', 0);
            dropdownbodyfirst.css('display', 'block');
        }

        var spaceh = space.innerHeight();
        var dropdownheaderh = dropdownheaderfirst.innerHeight();
        var dropdownfooterh = dropdownfooterfirst.innerHeight();
        var height = Math.max(spaceh, dropdownheaderh + dropdownfooterh);

        dropdownbodyfirst.css('opacity', '');
        dropdownbodyfirst.css('display', '');

        space.css('height', height + 'px');
        dropdownheaderfirst.css('height', height + 4 + 'px');
        dropdownheader.not(dropdownheaderfirst)
            .css('min-height', height + 4 + 'px')
            .css('height', 'auto');

        var $banners = $(".b-page__body > .container-fluid > .l-col-wrap_side_left_fix");
        if($banners.find(".games-banners").length){
            $banners.css({'min-height': height + 4 - 20 + 'px'});
        }

        /*
         space.find('.b-products:first').css({
         position: 'absolute',
         left: hpadleft.css('padding-left'),
         right:0,
         bottom:0
         })
         */
    })(jQuery);

    if ($('.selectpicker').length>0)
    {
        if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            $('.selectpicker').selectpicker();
        } else {
            $('.selectpicker').selectpicker('mobile');
        }
    }

    $('.popover').on('shown.bs.popover', function(e){
        var popover = $(this);
        popover.find('[data-dismiss="popover"]').on('click', function(e){
            popover.popover('hide');
        });
    });

    (function(doc) {
        var viewport = doc.getElementById('viewport');

        if (viewport == null)
            return;

        if ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)) {
            viewport.setAttribute("content", "width=device-width,initial-scale=0.3");
        } else if ( navigator.userAgent.match(/iPad/i) ) {
            viewport.setAttribute("content", "width=device-width,initial-scale=0.7");
        }
    }(document));

    $(document).on("click", "a.js-scrollto", function(e) {

        e.preventDefault();

        var self = $(this);
        var id = self.attr('href').split('#')[1];

        if (!self.hasClass('disabled')) {
            $('html,body').stop().animate({scrollTop: $('#'+id).offset().top}, 300, 'easeInOutExpo');
        }
    });
});


/* desktop.blocks/func/func.js (part) */
function formDelivery(selector){
    groupButtonsHandler(selector);
    datePicker();
}

function formSiteSettings(selector){
    groupButtonsHandler(selector);
}

function formFeedback(selector){
    groupButtonsHandler(selector);
}
// Group buttons
function groupButtonsHandler(selector) {
    var container = $(selector).find('.b-group-buttons');
    container.find('a').on('click', function(){
        var currentItem = $(this).parents('LI:first');
        var items = $(this).parents('.b-group-buttons').find('LI');
        items.removeClass('selected').filter(currentItem).addClass('selected');
        return false;
    });
    container.find('input').on('change', function(){
        var currentItem = $(this).parents('LI:first');
        var items = $(this).parents('.b-group-buttons').find('LI');
        items.removeClass('selected').filter(currentItem).addClass('selected');
    });
}
// parseUri
function parseUri (str) {
    var	o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};
/* js/common.js */
$(document).ajaxSuccess(function (event, xrh, ajaxOptions) {
    var uriVars = parseUri(ajaxOptions.url);
    var url = uriVars.host + uriVars.path;
    switch (url) {
        case '/checkout/loginPopup':
        case 'checkout/loginPopup':
            formEnter('#form_enter');
            formEnterOrdering('#form_enter');
            break;
        case '/header/cities.json':
        case 'header/cities.json':
            postalCitiesHandler();
            citiesHandler('#cities');
            break;
        case '/getGoodAvailabilities':
            //$(window).scroll();
            $(window).resize();
            break;

    }
});

$(document).on('click','button[name=age-confirmation]',function(){
    var isConfirmed = $(this).data('confirm');

    if(isConfirmed){
        if($("input[id='age']:checked").length > 0){
            $("#age-confirmation").hide();
            $(".modal-scrollable").remove();
            $('.modal-backdrop ').remove();
            $('body').css('overflow', 'visible');
        }else{
            return;
        }
    }else{
        window.location = "/";
    }
    setCookie("intim_cats", isConfirmed, 30, "/");
});

var Underscore = {
    renderTemplate: function (id, viewData) {
        var template = $('#' + id).html();
        var compiled = _.template(template);
        return compiled(viewData);
    }
}

function getStringDescription(val, oneDesc, twoDesc, fiveDesc) {
    if (val >= 0) {
        var remainder = val % 100;
        var tens = (remainder - remainder % 10) / 10;
        if (tens == 1) {
            return fiveDesc;
        } else {
            var digits = remainder % 10;
            if (digits == 1) {
                return oneDesc;
            } else if (digits > 4 || digits == 0) {
                return fiveDesc;
            } else {
                return twoDesc;
            }
        }
    } else {
        return '';
    }
}

function postalCitiesHandler() {
    $('#postLink').on('click', function () {
        $(this).parents('.b-tabs').eq(0).find('dt').eq(1).click();
        return false;
    });
}

function formEnterOrdering(selector) {
    var closeEl = $(selector).parents('.b-popup').find('.b-popup__close');
    closeEl.on('click', close);

    function close() {
        jQuery.get("/checkout/closeLoginPopup")
        closeEl.off('click', close);
    }
}

/*
 function changeContractor() {
 uniChangeContractor('main');
 }

 function changeContractorCheckout(){
 uniChangeContractor('/checkout');
 }
 */

function checkSoftwareGoods(id, isJuristic) {
    $.ajax({
        type: "POST",
        url: backendHost + "/cabinet/checkCart",
        data: {isJuristic: isJuristic},
        xhrFields: {
            withCredentials: true
        }
    }).success(function (data) {
        var preloader = $('#prldr');
        preloader.remove();
        var $sel = $("#contractor" + id);
        $sel.removeClass('disabled');
        if (!data) {
            uniChangeContractor(id, true);
        } else {
            var modal = $(data);

            var yesBtn = modal.find('.js-contractor-switch-yes');
            yesBtn.data('id', id);
            yesBtn.on('click', function () {
                var self = $(this);
                self.addClass('disabled');
                uniChangeContractor(self.data('id'), true);
            });
            modal.modal();
            modal.on('hide.bs.modal', function () {
                preloader.remove();
            })
        }
    });
}

function uniChangeContractor(id, dontCheckConfirm, isJuristic) {
    if (!$('#prldr').length) {
        $('.b-pseudolink').addClass('disabled');
        $('.btn').attr('disabled', 'disabled');
        $('.checkbox-loyalty').attr('disabled', 'disabled');
        var $sel = $("#contractor" + id);
        $sel.prepend('<i class="b-ico b-ico_preloader b-ico_preloader_sm" id="prldr"></i>');
        if (!dontCheckConfirm) {
            checkSoftwareGoods(id, isJuristic);
            return;
        }
        var loyalty = false;
        if ($("input").is("#cb_not_loyalty" + id)) {
            loyalty = !$("#cb_not_loyalty" + id).prop("checked");
        }
        $.ajax({
            type: "POST",
            url: backendHost + '/cabinet/contractor',
            data: {
                agentId: id,
                loyalty: loyalty
            },
            xhrFields: {
                withCredentials: true
            }
        }).done(function (locate) {
            setTimeout(function () {
                    window.location = (locate == null) ? '/' : locate;
                }, 500
            );
        }).fail(function () {
            $('#prldr').replaceWith("Что-то пошло не так, попробуйте обновить страницу или удалить файлы cookies");
        });
    }
}

function changeCity(id, cityAlias, cityName) {
    $.ajax({
        type: 'get',
        url: '/ulmart/putcity',
        data: {city: id}
    });
    var location = window.location + "";
    setCookie("city", id, 14, location);
    removeCookie('availabilitySelector', location);
    removeCookie('warehouseFilter', location);
    var index = location.indexOf("#");
    if (index > -1) {
        location = location.substring(0, index);
    }
    location = location.replace(/w_\d+%2C/g, "");
    if (typeof gtmHelper != 'undefined' && typeof gtmHelper.visitorCity != 'undefined') {
        gtmHelper.visitorCity(cityName);
    }
    var pathElems = window.location.pathname.split("/");
    if (pathElems.length > 0 && (pathElems[1] == "help" || pathElems[1] == "about" || pathElems[1] == "vacancies")) {
        if (pathElems.length >= 4) {
            var menuAlias = pathElems[3];
            var newLocation1 = window.location.protocol + "//" + window.location.host + "/" + pathElems[1] + "/" + cityAlias + "/" + menuAlias;
            window.location = newLocation1;
        } else {
            var newLocation2 = window.location.protocol + "//" + window.location.host + "/" + pathElems[1] + "/" + cityAlias;
            window.location = newLocation2;
        }
        /*
         } else if(pathElems.length > 3 && pathElems[1] == "s") {
         var path;
         var siteAlias = pathElems[2];
         if (pathElems.length > 4) {
         var menuAlias = pathElems[4];
         path = "/s/" + siteAlias + "/" + cityAlias + "/" + menuAlias;
         } else {
         path = "/s/" + siteAlias + "/" + cityAlias;
         }
         window.location = window.location.protocol + "//" + window.location.host + path;
         */
    } else if (id == '55555' && pathElems.length > 0 && pathElems[1] == "autoparts") {
        window.location = "/";
    } else {
        window.location = location;
    }
}

function setCookie(cookieName, cookieValue, nDays, location) {
    var today = new Date();
    var expire = new Date();
    if (nDays == null) {
        nDays = 1;
    }
    var domain = "";
    if (location.indexOf("ulmart.ru") != -1) {
        domain += ";domain=ulmart.ru";
    }
    if (nDays !== 0) {
        expire.setTime(today.getTime() + 3600000 * 24 * nDays);
        document.cookie = cookieName + "=" + escape(cookieValue) +
            ";expires=" + expire.toGMTString() + ";path=/" + domain;
    } else {
        document.cookie = cookieName + "=" + escape(cookieValue) + ";path=/" + domain;
    }
}

function getCookie(name) {
    var value = document.cookie;
    var start = value.indexOf(" " + name + "=");
    if (start == -1) {
        start = value.indexOf(name + "=");
    }
    if (start == -1) {
        value = null;
    } else {
        start = value.indexOf("=", start) + 1;
        var end = value.indexOf(";", start);
        if (end == -1) {
            end = value.length;
        }
        value = unescape(value.substring(start, end));
    }
    return value;
}

/**
 * Ставит куку
 *
 * @param cookieName название куки
 * @param cookieValue значение
 * @param expires -1, если бесконечная. абсолютное, иначе
 * @param path путь, мб пустым
 * @param domain домен, мб пустым
 */
function setCookieInternal(cookieName, cookieValue, expires, path, domain) {
    var cookie = cookieName + '=' + escape(cookieValue);
    if (expires !== -1) {
        cookie += ';expires=' + new Date(expires).toGMTString();
    }
    if (path) {
        cookie += ';path=' + path;
    }
    if (domain) {
        cookie += ';domain=' + domain;
    }
    document.cookie = cookie;
}

/**
 * Удаляет куку
 *
 * @param {String} cookieName название куки
 * @param {String} location window.location as string
 */
function removeCookie(cookieName, location) {
    var domain = '';
    if (location.indexOf('ulmart.ru') != -1) {
        domain = 'ulmart.ru';
    }
    setCookieInternal(cookieName, '', 0, '/', domain);
}

function updateBalanceOrders() {
    jQuery.get('/cabinet/balance', {
        viewType: document.getElementById("viewTypeId").value,
        timePeriod: document.getElementById("timePeriodId").value
    }, function (data) {
        $("#cabinet_balance").parent().html(data);
    }, 'html');
}

function updateCompareTable() {
    jQuery.get('/cart/compare', {chosenCategory: document.getElementById("chosenCategory").value}, function (data) {
        $(".l-body").parent().html(data);
    }, 'html');
}

function getURLParameter(param) {
    return getParamFromUrlByName(History.getState().url, param);
}

function getParamFromUrlByName(url, param) {
    param = param.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var exp = "[\\?&]" + param + "=([^&#]*)";
    var regexp = new RegExp(exp);
    var results = regexp.exec(url);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

function testParam(value) {
    return value != undefined && value != null && value != 'null' && value != '';
}

function checkReserve() {
    var warehouseId = $('input:radio[name=warehouseId]:checked').val();
    $('#reserveResults').load('/checkout/checkReserve?warehouseId=' + warehouseId);
}

function checkContact() {
    var errors = 0;

    var contactName = $('input:text[name=contactName]').val();
    if (!contactName || contactName.trim().length == 0) {
        errors++;
    }

    var contactEmailInput = $('input:text[name=contactEmail]');
    if (contactEmailInput.exists()) {
        var contactEmail = contactEmailInput.val();
        if (!contactEmail || contactEmail.trim().length == 0) {
            errors++;
        }
    }

    var contactPhone = $('input:text[name=contactPhone]').val();
    if (!contactPhone || contactPhone.trim().length == 0) {
        errors++;
    }


    var addressInput = $('input:text[name=address]');
    if (addressInput.exists()) {
        var address = addressInput.val();
        if (!address || address.trim().length == 0) {
            errors++;
        }
    }

    var dateInput = $('input:text[name=date]');
    if (dateInput.exists()) {
        var date = dateInput.val();
        var parsed = Date.parse(date);
        if (!parsed || !date || date.trim().length == 0) {
            errors++;
        }
    }

    switchContinue(errors == 0);
}

function switchContinue(enabled) {
    if (enabled) {
        $('#continueButton').removeAttr('disabled');
    } else {
        $('#continueButton').attr('disabled', 'disabled');
    }
}

function switchPreloader(preloader, button, loading) {
    if (preloader && button) {
        if (loading) {
            preloader.show();
            button.hide();
        } else {
            preloader.hide();
            button.show();
        }
    }
}

var headerCatalogLoading = false;
$(document).ready(function () {

    // BUY button for goods of day in category
    $(document).on("click", ".js-buy-goodsOfDay", function (eventObject) {
        eventObject.preventDefault();
        var $this = $(this);
        var $inputCount = $this.closest('.buy-wrap').find('.input-count');
        var count = Number($inputCount.val());
        var countParam = count ? {count: count} : undefined;
        $.get($this.attr("href"), countParam, function (data) {
            $('#cartPanel').find('.js-tooltip-buy').triggerHandler('animate2', function () {
                var $data = $(data);
                $('#bottomCart').replaceWith($data.filter('#bottomCart'));
                $('#bottomButton').replaceWith($data.filter('#bottomButton'));
                restoreLink($("#cart_link"));
                tooltipHandler('#cartPanel');
            });
            if ($this.data('mailru')) {  //mailru counter
                var img = $("<img />").attr('src', $this.data('mailru'))
                    .load(function () {
                    });
            }
        }, "html");
        $this.attr("href", "/cart").removeClass("js-buy").removeClass("js-buy-goodsOfDay");
    });

    jQuery('#goodsCatalogButton').mouseover(function () {
        if (!document.getElementById("header-categories").hasChildNodes() && !headerCatalogLoading) {
            headerCatalogLoading = true;
            jQuery.get('/getCategories', function (data) {
                jQuery('#header-categories').append(data);
            }, 'html');
        }
    });

    try {
        if (!document.getElementById("header-categories-collapsed").hasChildNodes() && !headerCatalogLoading) {
            headerCatalogLoading = true;
            jQuery.get('/getCategories', function (data) {
                jQuery('#header-categories-collapsed').append(data);
            }, 'html');
        }
    } catch (e) {
    }
    setGoodAjaxLoad('click', document, '.js-goods-estimate', 5, '/getGoodsEstimate');
    setGoodAjaxLoad('mouseover', document, '.js-goods-action', 5, '/getGoodsActions');

    function setGoodAjaxLoad(event, staticParentClass, clazz, pos, url) {
        jQuery(staticParentClass).on(event, clazz, function (eventObject) {
            var hyperlinkElement = jQuery(this, document);
            var wrap = hyperlinkElement.find('.b-tooltip__wrap');
            if (!hyperlinkElement.hasClass('_completed') && !hyperlinkElement.hasClass('_inprogress')) {
                hyperlinkElement.addClass('_inprogress');
                var goodId = hyperlinkElement.attr('class').split(' ')[pos];
                jQuery.get(url, {goodId: goodId}, function (data) {
                    wrap.empty();
                    wrap.append(data);
                    hyperlinkElement.addClass('_completed');
                    hyperlinkElement.removeClass('_inprogress');
                }, 'html');
            }
            eventObject.preventDefault();
        });
    }

    $(document).on('click', '#cityOk', function() {
        $(this).parents('.popover').hide();
    });

    $("#bonus").bind('input', function () {
        changedBonus($(this).val(), $(this).data('limit'));
    });
    setSearchCategorySwitchHand();

    $(document).on('click', '.js-header-payOnlineSAP', function (event) {
        var orderId = $(this).data('id');
        var type = $(this).data('type');
        var requestId = $(this).data('requestId');
        var agentId = $(this).data('agent-id');
        $('#payOnlineHeader' + orderId).empty().html('<i class="b-ico b-ico_preloader_sm btn-ico-first"></i>Идет проверка...').attr('disabled', 'disabled');
        var url = $(this).attr('href');
        jQuery.get(backendHost + '/checkOrderDOL', {
            orderId: orderId,
            type: type,
            requestId: requestId,
            agentId: agentId
        }, function (data) {
            if (data.length > 0) {
                $('#payOnlineHeader' + orderId).empty().html('Оплатить онлайн').attr('disabled', false);
                $('#payOnlineMsgError').html(data);
            } else {
                window.location = url;
            }
        }, 'html');
        event.preventDefault();
    });

    $('[data-toggle="popover"]').hover(function () {
        $(this).popover('show');
    });

    initPasswordValidation();

    if($("#age-confirmation").length > 0){
        commonShowModal($("#age-confirmation"), 'width-main-16 modal-pad2', "#age-confirmation");
    }
});

function initPasswordValidation() {
    addPasswordChangeListener('#myself_password', '#passwordWarning');
    addPasswordChangeListener('#password1', '#password_change_error');
    addPasswordChangeListener('#change_pass_new_pass', '#password_change_error');
}

function addPasswordChangeListener(input, label) {
    $(document).on('input', input, function (event) {
        if ($('#updatePassword').length) {
            $('#updatePassword').empty();
        }
        ;
        var $input = $(input);
        var password = $input.val();
        jQuery.post(backendHost + '/password/validate', {password: password},
            function (data) {
                var $label = $(label);
                $input.removeClass("has-error has-warning has-success");
                if (data.length > 0) {
                    var errors = data.split("\n");
                    var viewData = {
                        errors: errors
                    };
                    $('#errors').html(Underscore.renderTemplate('templatePasswordValidate', viewData));
                    $input.addClass("has-error");
                } else {
                    $('#errors').html(Underscore.renderTemplate('templatePasswordValidate'));
                    var result = zxcvbn(password);
                    if (result.score <= 1) {
                        $('#errors').html(Underscore.renderTemplate('templatePasswordValidate',
                            {errors: ['Слишком простой пароль, рекомендуем придумать более сложный пароль']}));
                        //$label.text("\n Слишком простой пароль, рекомендуем придумать более сложный пароль");
                        $input.addClass("has-warning");
                    } else if (result.score == 2) {
                        $('#errors').html(Underscore.renderTemplate('templatePasswordValidate',
                            {errors: ['Пароль средней сложности']}));
                        //$label.text("\n Пароль средней сложности");
                        $input.addClass("has-warning");
                    } else {
                        $('#errors').html(Underscore.renderTemplate('templatePasswordValidate',
                            {errors: ['Надежный пароль']}));
                        //$label.text("\n Надежный пароль");
                        $input.addClass("has-success");
                    }
                }
            });
    });
}

function setSearchCategorySwitchHand() {
    $(".js-search-cat-list a").click(function () {
        $('#searchField').focus();
    });
}

$(document).ajaxSuccess(function (event, xrh, ajaxOptions) {
//    if (ajaxOptions.url.match(/\/myprice\/[0-9]+/)) {
//        formEnter('#form_myprice');
//    } else {
    var uriVars = parseUri(ajaxOptions.url);
    var url = uriVars.host + uriVars.path;
    switch (url) {
        case '/login':
            formEnter('#form_enter');
            break;
        case '/office/online':
            tabsHandler('#video');
            break;
        case '/changeContractor':
            formKontragent('#form_kontragent');
            break;
    }
//    }
});

function removeKPPforIP() {
    var $orgKpp = $('._kpp');
    if ($('#org_inn').val().length == 12) {
        $orgKpp.hide();
    } else {
        $orgKpp.show();
    }
}

function addToFavorites(el, goodId) {
    var goodId = goodId || el.attr('id').replace('toFavorites', '');
    $.get(el.data('href'), function (data) {
        addToFavAnimation(data);
    }, "html");
    $('#inFavorites' + goodId).show();
    $('#inFavorites_' + goodId).show();//resolve dublicate with itself on product card page

    var hideFavorites = {
        'visibility': 'hidden',
        'position': 'absolute',
        'left': '0px',
        'top': '20px'
    };
    $('#toFavorites' + goodId).css(hideFavorites);
    $('#toFavorites_' + goodId).css(hideFavorites);//resolve dublicate with itself on product card page

}

/**
 * Анимация нижней панели при добавлении товара в закладки
 */
function addToFavAnimation(data) {
    $('#cartPanel .js-tooltip-put').triggerHandler('animate2', function () {
        $('#bottomFavorites').replaceWith(data);
        restoreLink($("#fav_link"));
        tooltipHandler('#cartPanel');
    });
}

/**
 * Показать поле создания списка закладок
 */
function showNewFavListInput(el) {
    $(el).hide();
    var $i = el.next();
    $i.show('slow');
    $i.find('input').focus();
}

/**
 * Скрыть поле создания списка закладок
 */
function hideNewFavListInput(el) {
    $(el).hide();
    var $i = el.prev();
    $i.show('slow');
}

/**
 * Добавляет новый список закладок и в него перемещает товар
 */
function addListToFavsByPopup(el) {
    el.parent().parent().hide('slow');
    var $i = el.parent().find('.js-fav-list-input');
    if ($i.val().length > 0) {
        var url = backendHost + '/favorites/addList';
        var params = {
            name: $i.val(),
            fromListId: 0,
            goodId: el.children().data('id')
        };
        if (el.data('type') != null) {
            params.type = el.data('type');
            url = backendHost + '/favorites/digital/addList';
        }
        $.post(url, params, function (res) {
            el.closest('.qtip').remove();
        });
    }
}

/**
 * Перемещение товара в другой список закладок
 */
function favMoveToList(event) {
    event = event || window.event;
    var el = $(event.target);
    $.get(el.data('href'), function (data) {
    }, "html");
    el.closest('.qtip').remove();
}

function processComplaint() {
    $('#requestError').hide();
    var $parent = $('#complaint_form');
    switchPreloader($('#nextPreloader'), $('#buttonNext'), true);
    var hasErrors = false;
    if ($.trim($parent.find('#description').val()) == '') {
        $('#description_error').show();
        hasErrors = true;
    } else {
        $('#description_error').hide();
    }
    if ($.trim($parent.find('#name').val()) == '') {
        $('#name_error').show();
        hasErrors = true;
    }
    var email = $.trim($parent.find('#email').val());
    if (email == "") {
        $('#email_format_error').hide();
        $('#email_error').show();
        hasErrors = true;
    } else {
        if (validateEmail(email)) {
            $('#email_format_error').hide();
            $('#email_error').hide();
        }
        else {
            $('#email_error').hide();
            $('#email_format_error').show();
            hasErrors = true;
        }
    }
    /*
     var phone = $.trim($parent.find('#phone').val());
     if(phone=="")
     $('#phone_format_error').hide();
     else{
     if(validatePhone(phone))
     $('#phone_format_error').hide();
     else {
     $('#phone_format_error').show();
     hasErrors = true;
     }
     }
     */
    if (hasErrors) {
        switchPreloader($('#nextPreloader'), $('#buttonNext'), false);
    }
    return !hasErrors;
}

function checkoutConfirm(url) {
    url = url || backendHost + '/checkout/confirm';
    var buttonNext = $('#buttonNext');
    if (buttonNext.hasClass('_disabled') || buttonNext.hasClass('disabled')) return;

    var errorBlock = $("#requestErrorDiv");
    errorBlock.hide();
    switchPreloader($('#nextPreloader'), $('#buttonNext'), true);

    if ($('#bonus_pass').is(":visible")) {
        $.ajax({
            type: "POST",
            url: backendHost + "/checkAuthXXL",
            xhrFields: {
                withCredentials: true
            },
            data: {login: $('#promo_login').val(), pass: $('#promo_pass').val()}
        }).done(function (msg) {
            if (msg.length > 0) {
                $(".pass_error").show();
                switchPreloader($('#nextPreloader'), $('#buttonNext'), false);
            } else {
                var data = $('#confirmForm').serializeArray();
                post_to_url(url, data);
            }
        }).fail(function () {
            switchPreloader($('#nextPreloader'), $('#buttonNext'), false);
            errorBlock.show();
        });
    } else {
        var data = $('#confirmForm').serializeArray();
        post_to_url(url, data);
    }
}

function convertPluses() {
    var pluses = $('#trans_to_bonus').val();
    if (pluses.match(/^\d+$/g) && pluses > 0) {
        $('#preloader').css("display", "inline-block");
        $('#cancel_button').css("display", "none");
        $('#convert_button').css("display", "none");
        $.ajax({
            type: "POST",
            url: backendHost + '/cabinet/uPlusToXxl',
            xhrFields: {
                withCredentials: true
            },
            data: {uPluses: pluses}
        }).done(function (data) {
            $('#preloader').css("display", "none");
            $('#pluses_input').html('');
            $('#convert_button').css("display", "inline-block").removeAttr('onclick').addClass('js-popup-close-handle').text('Закрыть окно');
            if (data != -1) {
                $('#pluses_message').html('Ваш бонусный счет пополнен на <span class="main-strong">' + data + '</span> руб.');
                var bonus = parseInt($('#user_popup_bonus').text()) + parseInt(data);
                var pluses = parseInt($('#user_popup_pluses').text()) - parseInt(data);
                $('#user_popup_bonus').text(bonus);
                $('#user_popup_pluses').text(pluses);
                $('#info_str_bonus').data("formated", false).text(bonus);
                $('#info_str_pluses').data("formated", false).text(pluses);
//                    priceHandler('body');
            } else {
                $('#pluses_message').html('Ошибка перевода, попробуйте снова');
            }
        }).fail(function () {
            $('#preloader').css("display", "none");
            $('#pluses_message').html('Произошла ошибка при обработке запроса');
        });
    }
}

function post_to_url(path, fields, method) {
    method = method || "post"; // Set method to post by default, if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    $.each(fields, function (i, field) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", field.name);
        hiddenField.setAttribute("value", field.value);

        form.appendChild(hiddenField);
    });

    document.body.appendChild(form);
    form.submit();
}

function feedbackForm(event, opts) {
    event = event || window.event;
    opts = opts || {};
    var isTSP = !!opts.isTSP;
    var isConsultAutopart = !!opts.isConsultAutopart;
    var consultautopart = isConsultAutopart ? 'consultautopart' : '';
    if (isTSP) {
        $('#qtip-0').hide();
        $('#qtip-1').hide();
    }
//    var fromCabinet = (document.URL.indexOf('/cabinet/feedback')) !== -1;
    var fromCabinet = (document.URL.indexOf('/cabinet')) !== -1;
    console.log("testing");
    // $.ajax({
    //     type: 'get',
    //     url: backendHost + '/feedbackForm',
    //     data: { fromCabinet: fromCabinet, isTSP: isTSP, consultautopart: consultautopart },
    //     xhrFields: {
    //         withCredentials: true
    //     }
    // }).done(function (data) {
    //         if (data != null && data.length > 0) {
    //             data = data.replace(new RegExp("src=([\"'])/([^\"']+)","g"), "src=$1 "+ backendHost + "/$2");
    //             var el = $(data);
    //             if ($('#feedback_modal').length) {
    //                 el = el.find('.js-form-feedback');
    //                 $('.js-form-feedback').replaceWith(el);
    //             } else {
    //                 commonShowModal(el, isConsultAutopart ? '' : 'width-main-16 modal-pad2', "#feedback_modal");
    //             }
    //             formFeedback('#form_feedback');
    //         }
    //     })
}

function commonShowModal(el, classes, sel) {
    el.addClass(classes);
    $("body").append(el);
    var modal = $(sel);
    $('body').css('overflow', 'hidden');
    modal.on('hidden.bs.modal', function (e) {
        modal.remove();
        $(".modal-scrollable").remove();
        $('.modal-backdrop ').remove();
        $('body').css('overflow', 'visible');
    });
    modal.modal({
        keyboard: false,
        backdrop: 'static'
    }, 'show');
}

function processFeedback(data){
    $("#requestError").hide();
    var $parent=$("#complaint_form");
    switchPreloader($("#nextPreloader"),$("#buttonNext"),true);
    var hasErrors=false;
    if($.trim(document.getElementById("description").value)==""){
        $("#description_error").show();
        hasErrors=true
    }else $("#description_error").hide();
    var name=$.trim($parent.find("#name").val());
    if(name==""){
        $("#name_error").show();
        hasErrors=true
    }
    var email=$.trim($parent.find("#email").val());
    if(email==""){
        $("#email_format_error").hide();
        $("#email_error").show();
        hasErrors=true;
    }else if(validateEmail(email)){
        $("#email_format_error").hide();
        $("#email_error").hide()
    }else{
        $("#email_error").hide();
        $("#email_format_error").show();
        hasErrors=true
    }
    var phone=$.trim(document.getElementById("phone").value);
    if(!hasErrors)
        $.ajax({
            type:"POST",
            url: backendHost + "/feedbackSend",
            data:$("#complaint_form").serialize(),
            xhrFields: {
                withCredentials: true
            }
        }).done(function(msg){
            switchPreloader($("#nextPreloader"),$("#buttonNext"),false);
            $(".js-form-feedback").replaceWith(msg);
        }).fail(function(){
            switchPreloader($("#nextPreloader"),$("#buttonNext"),false);
            $("#requestError").show()
        });
    else switchPreloader($("#nextPreloader"),$("#buttonNext"),false);
    jQuery(".js-popup-close-handle").on("click",function(eventObject){
        if(document.getElementById("cabfeed")!=null)window.location.reload();
        eventObject.preventDefault()
    });
    var closeEl=$("#form_feedback").parents(".b-popup").find(".b-popup__close");
    closeEl.on("click",close);
    function close(){if(document.getElementById("cabfeed")!=null){window.location.reload();closeEl.off("click",close)}}
}

function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.\+])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return reg.test(email);
}

function validatePhone(phone) {
    phone = phone.replace(/ /g, "").replace(/-/g, "").replace(/\(/g, "").replace(/\)/g, "").replace(/\+/g, "");
    var reg = /^\d+$/;
    var is = reg.test(phone);
    return ((reg.test(phone)) && (phone.length >= 5) && (phone.length <= 11))
}

function processAuth(msg) {
    var captchaInvisible = !$('.capcha_popup').is(':visible');

    if (msg.length == 0 || msg == "3" && captchaInvisible) {
        return true;
    } else {
        switchPreloader($('#loginPreloader'), $('#loginButton'), false);
        if (msg == "1") {
            $('#loginErrorDiv').show();
        } else {
            if (msg == "2") {
                if (captchaInvisible) {
                    $('._capcha').show();
                } else {
                    $('#loginErrorDiv').show();
                    $('#captchaError').hide();
                    $("#captchaImg").attr("src", backendHost + "/jcaptcha?empty=" + Math.random());
                }
            } else if (msg == "3") {
                $('#loginErrorDiv').hide();
                $('#captchaError').show();
                $("#captchaImg").attr("src", backendHost + "/jcaptcha?empty=" + Math.random());
            } else if (msg == "4") {
                $('#loginErrorDiv').hide();
                if (captchaInvisible) {
                    $('#loginErrorDiv').show();
                    $('._capcha').show();
                } else {
                    $('#captchaError').show();
                    $("#captchaImg").attr("src", backendHost + "/jcaptcha?empty=" + Math.random());
                }
            }
            $("#captchaInput").val("");
            $('#loginButton').addClass('_disabled').addClass('disabled');
        }
    }
    return false;
}

function getWordSuffixByCount(count) {
    var remainder = count % 100;
    var tens = remainder / 10;
    if (tens == 1) {
        return "ов";
    } else {
        var digits = remainder % 10;
        if (digits == 1) return "";
        return digits > 4 || digits == 0 ? "ов" : "а";
    }
}

function checkMyOrder() {
    $('#checkCaptchaDiv').removeClass("has-error");
    $('#checkOrderDiv').removeClass("has-error");
    $('#falseCodeError').hide();
    $('#emptyCodeError').hide();
    $('#emptyNumberError').hide();
    $('#generalError').hide();
    var checkMyOrderResponse = $('#checkMyOrderResponse');
    checkMyOrderResponse.html('<i class="b-ico b-ico_preloader_30x30"></i>');
    $.ajax({
        type: "POST",
        url: backendHost + '/checkMyOrder',
        xhrFields: {
            withCredentials: true
        },
        data: {orderId: $('#order-number').val(), captcha: $('#orderCaptchaInput').val()}
    }).done(function (data) {
            var dataWrap = $('<div>' + data + '</div>');
            var redirect = dataWrap.find('#redirect');
            if (redirect.length > 0)
                window.location.href = redirect.val();
            else {
                var travelOrder = dataWrap.find('#travelOrder');
                var insuranceOrder = dataWrap.find('#insuranceOrder');
                if (travelOrder.length > 0 || insuranceOrder.length > 0) {
                    $('#checkMyOrderPopup').parent().html(dataWrap.find('#checkMyOrderPopup'));
                } else {
                    checkMyOrderResponse.html(data);
                    if ($('#checkError').length > 0) {
                        var errorMessage = $('#checkError').val();
                        switch (errorMessage) {
                            case "falseCode" :
                                $('#checkCaptchaDiv').addClass("has-error");
                                $('#falseCodeError').show();
                                break;
                            case "enterCode":
                                $('#checkCaptchaDiv').addClass("has-error");
                                $('#emptyCodeError').show();
                                break;
                            case "enterNumber":
                                $('#checkOrderDiv').addClass("has-error");
                                $('#emptyNumberError').show();
                                break;
                        }
                    }
                    $('#captchaImg').attr('src', backendHost + '/jcaptcha?empty=' + Math.random());
                    $('#orderCaptchaInput').val('');
                }
                $('#qtip-close').on('click', function (event) {
                    $(this).parents('.qtip:first').qtip('hide');
                    return false;
                });
            }
            
        }
    ).fail(function () {
            $('#checkOrderDiv').addClass("has-error");
            $('#generalError').show();
            $('#captchaImg').attr('src', backendHost + '/jcaptcha?empty=' + Math.random());
            $('#orderCaptchaInput').val('');
            checkMyOrderResponse.empty();
        });
}

function checkMyTravelOrder() {
    var orderId = $('#order-number').val();
    var loadingBlock = $('#checkMyTravelOrderLoading');
    loadingBlock.html('<i class="b-ico b-ico_preloader_30x30"></i>');
    $.ajax({
        type: "POST",
        url: backendHost + "/checkMyTravelOrder",
        xhrFields: {
            withCredentials: true
        },
        data: {orderId: orderId, pinCode: $('#pin-code').val()}
    }).done(function (data) {
        var dataWrap = $('<div>' + data + '</div>');
        var redirect = dataWrap.find('#redirect');
        if (redirect.length > 0)
            window.location.href = redirect.val();
        else {
            $('#checkMyOrderPopup').parent().html(data);
            $('#qtip-close').on('click', function (event) {
                $(this).parents('.qtip:first').qtip('hide');
                return false;
            });
        }
    }).fail(function () {
        $('#checkOrderDiv').addClass("has-error");
        $('#generalError').show();
    });
}

function checkMyInsuranceOrder() {
    var orderId = $('#order-number').val();
    var loadingBlock = $('#checkMyInsuranceOrderLoading');
    loadingBlock.html('<i class="b-ico b-ico_preloader_30x30"></i>');
    $.ajax({
        type: "POST",
        url: backendHost + "/checkMyInsuranceOrder",
        xhrFields: {
            withCredentials: true
        },
        data: {orderId: orderId, pinCode: $('#pin-code').val()}
    }).done(function (data) {
        var dataWrap = $('<div>' + data + '</div>');
        var redirect = dataWrap.find('#redirect');
        if (redirect.length > 0)
            window.location.href = redirect.val();
        else {
            $('#checkMyOrderPopup').parent().html(data);
            $('#qtip-close').on('click', function (event) {
                $(this).parents('.qtip:first').qtip('hide');
                return false;
            });
        }
    }).fail(function () {
        $('#checkOrderDiv').addClass("has-error");
        $('#generalError').show();
    });
}

function checkMyOrderFeedback() {
    $('#checkCaptchaDiv2').removeClass("has-error");
    $('#checkOrderDiv2').removeClass("has-error");
    $('#falseCodeError2').hide();
    $('#emptyCodeError2').hide();
    $('#emptyNumberError2').hide();
    $('#generalError2').hide();
    $('#checkOrderPreloader').show();
    var checkMyOrderResponse = $('#checkMyOrderResponse2');
    $.ajax({
        type: "POST",
        url: backendHost + '/checkMyOrder',
        xhrFields: {
            withCredentials: true
        },
        data: {orderId: $('#order-number2').val(), captcha: $('#orderCaptchaInput2').val(), fromFeedback: 'true'}
    }).done(function (data) {
            $('#checkOrderPreloader').hide();
            checkMyOrderResponse.html(data);
            if ($('#checkError2').length > 0) {
                var errorMessage = $('#checkError2').val();
                switch (errorMessage) {
                    case "falseCode" :
                        $('#checkCaptchaDiv2').addClass("has-error");
                        $('#falseCodeError2').show();
                        break;
                    case "enterCode":
                        $('#checkCaptchaDiv2').addClass("has-error");
                        $('#emptyCodeError2').show();
                        break;
                    case "enterNumber":
                        $('#checkOrderDiv2').addClass("has-error");
                        $('#emptyNumberError2').show();
                        break;
                }
            }
            updateCapthca()
        }
    ).fail(function () {
            $('#checkOrderPreloader').hide();
            $('#checkOrderDiv2').addClass("has-error");
            $('#generalError2').show();
            checkMyOrderResponse.html('');
            updateCapthca()
        });
}

function updateCapthca() {
    $('#captchaImg2').attr('src', backendHost + '/jcaptcha?empty=' + Math.random());
    $('#orderCaptchaInput2').val('');
}

function signInSN(where) {   //используется в ЛК
    document.location.href = where + '&referer=' + document.location.href;
}

/**
 * For SEO: replacing <i> for robots by <a> for users
 */
function restoreLink($links) {
    $links.each(function () {
        $(this).replaceWith("<a " + a_attr(this) + " >" + $(this).html() + "</a>");

        function a_attr(self) {
            var s = " ";
            var attr = self.attributes;
            for (var i = 0; i < attr.length; i++) {
                if (attr[i].name == 'title') {
                    s += "href='" + attr[i].value + "' ";
                } else {
                    s += attr[i].name + "='" + attr[i].value + "' ";
                }
            }
            return s;
        }
    });
}

function showOnlyAvailableNow(trg) {
    showOnlyAvailable(trg, "");
}

function showOnlyAvailable(trg, id) {
    var tableRow = ".b-table__row" + (id != "" ? (".id" + id) : "");
    if ($(trg).attr('checked')) {
        $("span.main-text-good.removable, span.text-warning.removable").closest(tableRow).hide();
        $("span.main-text-fade.removable, span.text-muted.removable").closest(tableRow).hide();
    } else {
        $("span.main-text-good.removable, span.text-warning.removable").closest(tableRow).show();
        $("span.main-text-fade.removable, span.text-muted.removable").closest(tableRow).show();
    }
    var table = $(tableRow).closest(".b-table__body");
    var visibleOutPosts = table.find(".b-table__row.b-table__row_head").eq(1).nextAll(".b-table__row:visible");
    if (visibleOutPosts.length == 0) {
        table.find(".js-av-text").hide();
    } else {
        table.find(".js-av-text").show();
    }
    $(window).trigger('scroll');
}

function checkTerminalPrint(orderId, sType) {
    $.post(backendHost + '/checkout/check_terminalPrint', {orderId: orderId, s: sType}, function (data) {
        var el = $(data);
        popupInit(el);
    });
}

/**
 * Верификация email
 */
function emailVerification(btn) {
    var receiveCheckbox = $("#email_receive").is(":checked");
    var isReceiveEmail = receiveCheckbox || false;
    var email = $("#email");
    var validationError = $("#emailVerificationErrorBlock");
    appendPreloader(btn);
    $.ajax({
        type: "POST",
        url: backendHost + "/verification/email",
        xhrFields: {
            withCredentials: true
        },
        data: {email: email.val(), receiveEmail: isReceiveEmail}
    }).done(function (data) {
        removePreloader(btn);
        var $errorInEmail = $("#error_in_email");
        switch (data) {
            case "":
            case "ok":
                validationError.hide();
                $("#emailVerificationBlock").hide();
                $errorInEmail.hide();
                $("#emailContent").html(email.val());
                $("#emailVerificationSuccessBlock").show();
                break;
            case "in_use":
                validationError.hide();
                $errorInEmail.show();
                $errorInEmail.html("Пользователь с таким E-mail уже существует");
                break;
            case "empty":
                $errorInEmail.show();
                $errorInEmail.html("Сначала заполните поле");
                break;
            case "short":
                $errorInEmail.show();
                $errorInEmail.html("Слишком длинный адрес");
                break;
            case "wrong_format":
                $errorInEmail.show();
                $errorInEmail.html("Неправильный формат E-mail");
                break;
            default:
                showError(data);
        }
    }).fail(function () {
        removePreloader(btn);
        showError("Ошибка связи");
    });

    function showError(error) {
        validationError.show();
        validationError.children('span').html(error);
    }

    return false;
}


function updatePassword(btn) {
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    var phone = $("#phone");
    var password1field = $("#password1").val();
    var password2field = $("#password2").val();
    if (password1field != password2field) {
        $("#password_equals_error").show();
        return false;
    } else {
        $("#password_equals_error").hide();
    }
    var token = getUrlVars()['token'];
    var form = {password1: password1field, password2: password2field, token: token};
    appendPreloader(btn);
    var $passChangeError = $("#password_change_error");
    $.ajax({
        type: "POST",
        url: backendHost + "/verification/updatepass",
        xhrFields: {
            withCredentials: true
        },
        data: form
    }).done(function (data) {
        removePreloader(btn);
        switch (data) {
            case "ok":
                $("#phoneContainerEnd").html(phone.val());
                $("#phoneVerificationPasswordBlock").hide();
                $passChangeError.hide();
                $("#phoneVerificationSuccessBlock").show();
                //если на странице подтверждения емаил
                $("#emailPasswordBlock").hide();
                $("#emailConfirmedBlock").show();
                break;
            case "error":
                $passChangeError.show();
                $passChangeError.html("Ошибка");
                break;
            case "long":
                $passChangeError.show();
                $passChangeError.html("Слишком длинный пароль");
                break;
            case "short":
                $passChangeError.show();
                $passChangeError.html("Пароль должен быть длиннее");
                break;
            case "wrong_format":
                $passChangeError.show();
                $passChangeError.html("Неправильный формат");
                break;
            case "blacklist":
                $passChangeError.show();
                $passChangeError.html("Этот пароль слишком распространен. Во избежание взлома придумайте более сложный пароль");
                break;
            case "empty":
                $passChangeError.show();
                $passChangeError.html("Сначала заполните поле");
                break;
            case "verification_err":
                location.href = "/verification/email/confirm?token=" + token + "&error=" + true;
            default:
                $passChangeError.html(data);
                $passChangeError.show();
                break;
        }
    }).fail(function (data) {
        removePreloader(btn);
        $passChangeError.show();
    });
    return false;
}
$(document).ready(function () {
    $(".js-informer-btn").click(function () {
        var queryData = {informerId: $(this).data('id')};
        var btn = $(this);
        $.ajax({
            type: "GET",
            url: backendHost + '/hideHeaderInformer',
            xhrFields: {
                withCredentials: true
            },
            data: queryData
        }).done(function (data) {
            switch (data) {
                case "ok":
                    btn.closest(".js-informer-wrapper").hide();
                    break;
            }
        });
    })
});

/**
 * Hiding "my price" popup on click
 */
function hidePricePopup(el) {
    el.parents('.qtip:first').qtip('hide');
}

/**
 * Sending request for "my price"
 */
//function processMyPrice() {
//    var $submit = $('#submit');
//    if ($submit.length < 0 || !$submit.hasClass('_disabled')) {
//        $('.b-form__error-wrap, .error-message').hide();
//        switchPreloader($('#nextPreloader'), $submit, true);
//        var data = $('#myPriceForm').serialize();
//        $.ajax({
//            type:"POST",
//            url:"/myprice",
//            data:data
//        }).done(function (errors) {
//                var split = errors.split(';');
//                var hasErrors = false;
//                for (i=0;i<split.length;i++) {
//                    var err = split[i];
//                    if(err.indexOf(':')!==-1) {
//                        var array = err.split(':');
//                        var elementId = array[0];
//                        if (array[1]==("on")) {
//                            $('#' + elementId).show();
//                            hasErrors = true;
//                        }
//                    }
//                }
//                if (!hasErrors) {
//                    $('#popup').trigger('close');
//                    var el = $('<section class="b-form _myprice" id="form_myprice"><section class="b-form _result" id="form_popup_result"><h2 class="main-h2 main-h2_bold">Запрос на изменение цены</h2><h4 class="main-h4 main-h4_bold"><i class="b-ico _check_38x30"></i>Запрос отправлен</h4><div class="b-form__line-wrap g-cleared"><div class="b-form__line _button"><a href="#" class="b-button _size3 js-popup-close-handle"><span class="b-button__i">Закрыть окно</span></a></div></div></section></section>');
//                    popupInit(el);
//                }
//                else switchPreloader($('#nextPreloader'), $submit, false);
//            });
//    }
//}

function changedBonus(price, limit) {
    if (isNaN(price)) {
        return;
    }
    limit = limit || 300
    if (price > limit) {
        $("#phone_sms_check").show();
        $("#pass_check").hide();
    } else {
        $("#phone_sms_check").hide();
        $("#pass_check").show();
    }
}

/**
 * get preloader img with size
 */
function getPreloaderImg(size) {
    var pxs;
    if (size) {
        pxs = size + "px";
    } else {
        pxs = "30px";
    }
    var preloader = $("<img>", {
        id: "preloader-img",
        src: "/resources/i/preloader_30x30.gif"
    }).css({
        height: pxs,
        width: pxs
    });
    return preloader;
}

/**
 * attach to element preloder in request
 */
function appendPreloader(btn, disabled) {
    //disable or hide
    if (disabled == null) {
        disabled = true;
    }
    var $btn = $(btn);
    $btn.after(getPreloaderImg($btn.height()));

    if (disabled) {
        $btn.attr("disabled", true);
    } else {
        $btn.hide();
    }
}

/**
 * remove preloder after request
 */
function removePreloader(btn) {
    var $btn = $(btn);
    $btn.next().remove();
    $btn.attr("disabled", false);
    $btn.show();
}

/**
 * Помещает лоадер в указанный контейнер
 * @param $container
 * @param switchType
 */
function switchContainerPreloader($container, switchType) {
    if (switchType == 'show') {
        $container.empty().append(getPreloaderImg());
    } else if (switchType == 'hide') {
        //может этот блок тоже будет полезен
    }
}

/**
 * сортровка отзывов
 * @param target
 */
function sortReviews(target) {
    var $this = $(target);
    // Получаем панельку сортировки
    var $b_sort = $this.closest('.js-b-sort');
    var listId = $b_sort.attr('data-list-id');
    var urlPattern = $b_sort.attr('data-url-pattern');
    // Новый итем сортировки
    var $selectedItem = $this.closest('.js-b-sort-item');
    // Тип сортировки
    var type = $selectedItem.attr('data-order-type');
    var $reviewList = $('#' + listId);
    var $reviewsContent = $reviewList.parent();
    var $reviewNav = $reviewsContent.find('.b-reviews__nav');
    $.get(urlPattern + type, function (data) {
        // Удаляем старый список
        $reviewList.remove();
        // Только для комментов: удаляем "Показать еще"
        $b_sort.remove();
        $reviewNav.remove();
        $reviewsContent.append($(data));
        // Обновляем выбранный тип сортировки
        try {
            reviewsHandler('#reviews');
        } catch (ex) {
        }
        tooltipHandler('#faq');
        reviewsFaqHandler('#faq');
    });
}


function rejectBonuses() {
    popupInit($('<section class="form form-margin2" id="form_popup_result"><header class="main-header">' +
        '<h2 class="main-h2 main-header__line text-error">К сожалению, оплата бонусами временно недоступна</h2></header></section>'), {hasClose: 'true'});
}

//для работы ui-spinner на мобильном устройстве
function fixUiSpinnerForMobile() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('.ui-spinner-up').click(function () {
            var input = $(this).parent().find('input');
            input.val(+input.val() + 1);
            input.change();
        });

        $('.ui-spinner-down').click(function () {
            var input = $(this).parent().find('input');
            input.val(+input.val() - 1);
            input.change();
        })
    }
}

function getStringParameterValue(string, param) {
    param = param.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var exp = "[\\?&]" + param + "=([^&#]*)";
    var regexp = new RegExp(exp);
    var results = regexp.exec(string);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

function hideInformer() {
    $.ajax({
        type: "GET",
        url: backendHost + '/hideInformer',
        xhrFields: {
            withCredentials: true
        }
    }).done(function (data) {
        switch (data) {
            case "ok":
                $(".js-informer-wrapper").hide();
                break;
        }
    });
}

function getStringDescription(val, oneDesc, twoDesc, fiveDesc) {
    if (val >= 0) {
        var remainder = val % 100;
        var tens = (remainder - remainder % 10) / 10;
        if (tens == 1) {
            return fiveDesc;
        } else {
            var digits = remainder % 10;
            if (digits == 1) {
                return oneDesc;
            } else if (digits > 4 || digits == 0) {
                return fiveDesc;
            } else {
                return twoDesc;
            }
        }
    } else {
        return '';
    }
}

//библиотека парсит html и позволяет получить "куски" html по id
/**
 * @param html
 */
var htmlPartsParser = function (html) {
    this.html = $('<div>').html(html)
}

/**
 * @param id
 * @param remove bool
 * @returns string
 */
htmlPartsParser.prototype.getById = function (id, remove) {
    if (remove == null) {
        remove = false;
    }
    var element = this.html.find('#' + id);
    if (element.length > 0) {
        var html = this.html.find('#' + id).html();
        if (remove) {
            element.remove();
        }
        return html;
    } else {
        return false;
    }
}

/**
 *
 * @returns {*|jQuery}
 */
htmlPartsParser.prototype.getHtml = function () {
    return this.html.html();
};

function hideChildrenAlert() {
    var settings = {};
    settings.hideChildrenPay = true;
    $.post(backendHost + '/cabinet/site', {settings: JSON.stringify(settings)})
        .success(function () {
            $('#childrenAlert').hide();
        })
}

function popupInitError(title, message) {
    var el = $('<section class="form form-margin2" id="form_popup_result"><form><h1 class="main-h1">' + title + '</h1><p class="">' + message + '</p><br/><div class="form-line g-helper-last"><a href="#" class="btn btn_theme_normal btn-sm btn-default js-popup-close-handle">Закрыть окно</a></div></form></section>')
    popupInit(el);
}

function CancelRemovePopup(popupText, cancelFunction) {
    this.popupText = popupText;
    this.cancelFunction = cancelFunction;
    if (CancelRemovePopup.popup != null) {
        this.removePopup()();
    }
    this.createPopup();
}

CancelRemovePopup.popup = null;
CancelRemovePopup.timer = null;

CancelRemovePopup.prototype.show = function () {
    document.body.appendChild(CancelRemovePopup.popup);
    CancelRemovePopup.timer = setTimeout(this.removePopup(), 5000);
};

CancelRemovePopup.prototype.createPopup = function () {
    CancelRemovePopup.popup = this.createOuterModalDiv();
};

CancelRemovePopup.prototype.createOuterModalDiv = function () {
    var div = this.createElem({
        elem: 'div',
        classes: 'modal modal_theme_normal modal_alert modal_alert_warning',
        attr: [{name: 'tabindex', value: '-1'}],
        inner: '',
        style: [{name: 'display', value: 'block'}, {name: 'position', value: 'fixed'}, {name: 'width', value: '250px'}]
    });
    div.appendChild(this.createInnerModalDiv());
    return div;
};

CancelRemovePopup.prototype.createInnerModalDiv = function () {
    var div = this.createElem({
        elem: 'div',
        classes: 'modal-body',
        inner: ''
    });
    div.appendChild(this.createInnerContent());
    return div;
};

CancelRemovePopup.prototype.createInnerContent = function () {
    var div = this.createElem({
        elem: 'div',
        classes: 'b-text b-text_theme_normal b-text_aux-lg',
        inner: '',
        style: [{name: 'text-align', value: 'center'}]
    });
    div.appendChild(this.createSpan());
    div.appendChild(this.createButton());
    return div;
};

CancelRemovePopup.prototype.createSpan = function () {
    var self = this;
    return this.createElem({
        elem: 'span',
        classes: '',
        inner: self.popupText + ' &nbsp;&nbsp;'
    })
};

CancelRemovePopup.prototype.createButton = function () {
    var button = this.createElem({
        elem: 'button',
        classes: 'btn btn_theme_normal btn-sm btn-white',
        inner: 'Отменить',
        attr: [{name: 'type', value: 'button'}]
    });
    button.addEventListener('click', this.buttonClickEvent());
    this.button = button;
    return button;
};

CancelRemovePopup.prototype.buttonClickEvent = function () {
    var self = this;
    return function () {
        self.cancelFunction(self.removePopup(), self.errorFunction());
        clearTimeout(CancelRemovePopup.timer);
        CancelRemovePopup.timer = null;
        self.button.disabled = true;
        self.button.innerHTML = ('<span class="b-ico b-ico_preloader_sm"></span> Отмена');
    }
};


CancelRemovePopup.prototype.removePopup = function () {
    var popup = CancelRemovePopup.popup;
    return function () {
        popup.parentNode.removeChild(popup);
        CancelRemovePopup.popup = null;
        clearTimeout(CancelRemovePopup.timer);
    }
};

CancelRemovePopup.prototype.errorFunction = function () {
    var self = this;
    return function () {
        self.removePopup()();
        alert('Произошла ошибка, отменить действие не удалось. Приносим свои извинения за доставленные неудобства.')
    }
};

CancelRemovePopup.prototype.createElem = function (params) {
    var createdElem = document.createElement(params.elem);
    createdElem.className = params.classes;
    createdElem.innerHTML = params.inner;
    if (params.attr && params.attr.length) {
        params.attr.forEach(function (at) {
            createdElem.setAttribute(at.name, at.value);
        });
    }
    if (params.style && params.style.length) {
        params.style.forEach(function (at) {
            createdElem.style[at.name] = at.value;
        });
    }
    return createdElem;
};

function getCancelRemovePopup(text, cancelFunction) {
    var popup = new CancelRemovePopup(text, cancelFunction);
    popup.show();
}
Array.prototype.max = function () {
    var max = this[0];
    var len = this.length;
    for (var i = 1; i < len; i++) if (this[i] > max) max = this[i];
    return max;
};

Array.prototype.min = function () {
    var min = this[0];
    var len = this.length;
    for (var i = 1; i < len; i++) if (this[i] < min) min = this[i];
    return min;
};

/**
 * Добавляет слой поверх заданного слоя. Действия над элементами становятся недоступными
 * @param $el
 */
function applyMaskOverlay($el) {
    $("<div>").attr({
        class: "mask-overlay",
        style: "position:absolute; top:0; left:0; width:100%; height:100%; background: rgba(0, 0, 0, 0);z-index:2000;"
    }).prependTo($el);
}

/**
 * Удаляет слой созданный выше
 * @param $el
 */
function removeMaskOverlay($el) {
    $el.find('.mask-overlay').remove();
}

/*
 //TODO: commented because it breaks Photo functionality for all Ulmart sites
 function scrollTo(element){
 $('html, body').animate({
 scrollTop: element.offset().top + 'px'
 }, 'slow');
 }
 */

//works like sprintf, String.appFormat('{0} is dead, but {1} is alive! {0} {2}', 'ASP', 'ASP.NET');
if (!String.appFormat) {
    String.appFormat = function(format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

function safecall(callable, default_value) {
    var return_value = default_value;
    if(typeof(callable) === 'function') {
        try {
            return_value = callable();
        } catch(e) {
            // ignore exception
        }
    }
    return return_value;
}

function html5_history_api_supported() {
    return !!(window.history && window.history.pushState);
}




/* js/autocomplete.js */
var metaAutocompleteInfoList = window.metaAutocompleteInfoList || [];

$(document).ready(function () {
    var discount = $('#searchForm input[name=discount]');
    var rootCategory = $('#search-cat-id');
    var searchField = $('#searchField');
    var searchBargainType = $('#search-bargain-type');
    var itemAll = '<div class="ui-autocomplete__after"><button type="submit">Все результаты поиска</button></div>';
    if (searchField.length) {
        var autocomplete = searchField.autocomplete({
            minLength: 2,
            delay: 300,
            appendTo: "#searchContainer",
            source: function (request, response) {
                // результы поиска
                var autocompleteSearchResult = {
                    main: [],
                    other: []
                };

                // завершенные запросы
                var isAutocompleteFinished = {
                    main: false,
                    other: []
                };

                $.ajax({
                    //url: "/search/autocomplete", //HARDCODE!
                    url: backendHost + "/ajax/autocomplete.json",
                    xhrFields: {
                        withCredentials: true
                    },
                    type: "POST",
                    dataType: "json",
                    data: {
                        name_startsWith: request.term,
                        discount: (discount ? discount.attr('value') : false),
                        rootCategory: ((rootCategory.length) ? rootCategory.attr('value') : null),
                        bargainType: ((searchBargainType.length) ? searchBargainType.attr('value') : null)
                    },
                    success: function (data) {
                        if (data) {
                            autocompleteSearchResult.main = data;
                            var allData = getAllData(autocompleteSearchResult, metaAutocompleteInfoList);
                            response($.map(allData, function (item) {
                                return {
                                    html: item.html,
                                    tag: item.tag,
                                    label: item.name,
                                    value: item.name,
                                    href: item.href
                                }
                            }));
                        }
                    },
                    complete: function () {
                        isAutocompleteFinished.main = true;
                        if (isAllAutocompleteFinished(isAutocompleteFinished, metaAutocompleteInfoList) && getAllData(autocompleteSearchResult, metaAutocompleteInfoList).length == 0) {
                            response();
                        }
                    }
                });

                if (metaAutocompleteInfoList) {
                    for (var i = 0; i < metaAutocompleteInfoList.length; i++) {
                        (function(metaAutocompleteInfo) {
                            $.ajax({
                                url: metaAutocompleteInfo.searchCountUrlTemplate.replace("${string}", encodeURIComponent(request.term)),
                                xhrFields: {
                                    withCredentials: true
                                },
                                timeout: 60 * 1000,
                                success: function (data) {
                                    var count;
                                    if (metaAutocompleteInfo.applicationType == "siteCoupons") {
                                        count = (data && data.res && data.res.goodsAmount) ? data.res.goodsAmount : 0;
                                    } else {
                                        count = data ? data : 0;
                                    }

                                    if (count > 0) {
                                        var href = metaAutocompleteInfo.searchUrlTemplate.replace("${string}", encodeURIComponent(request.term));

                                        autocompleteSearchResult.other[metaAutocompleteInfo.applicationType] = [{
                                            html: metaAutocompleteInfo.html.replace("${href}", href).replace("${count}", count),
                                            tag: metaAutocompleteInfo.tag,
                                            href: href
                                        }];

                                        var allData = getAllData(autocompleteSearchResult, metaAutocompleteInfoList);
                                        response($.map(allData, function (item) {
                                            return {
                                                html: item.html,
                                                tag: item.tag,
                                                label: item.name,
                                                value: item.name,
                                                href: item.href
                                            }
                                        }));
                                    }
                                },
                                complete: function () {
                                    isAutocompleteFinished.other[metaAutocompleteInfo.applicationType] = true;
                                    if (isAllAutocompleteFinished(isAutocompleteFinished, metaAutocompleteInfoList) && getAllData(autocompleteSearchResult, metaAutocompleteInfoList).length == 0) {
                                        response();
                                    }
                                }
                            });
                        })(metaAutocompleteInfoList[i]);
                    }
                }
            },
            open: function (event, ui) {
                var $ul = $("#searchContainer ul");
                if ($ul.length) {
                    $ul.addClass("ui-autocomplete_stock");
                    $ul.after(itemAll);
                    $ul.css({
                        width: 500
                    });
                    var contPos = $ul.position();
                    var contH = $ul.height();
                    var contW = $ul.width();

                    $(".ui-autocomplete__after button").on("mousedown", function(e){
                        $(this).parents('form').get(0).submit();
                    });

                    $(".ui-autocomplete__after").css({
                        top: contPos.top + contH,
                        left: contPos.left + 1,
                        width: contW
                    }).show();
                }
            },
            select: function (event, ui) {
                window.location = ui.item.href;
                return false;
            },
            close: function (event, ui) {
                $("#searchContainer .ui-autocomplete__after").remove();
            }
        });
        if (autocomplete.length != 0) {
            autocomplete.data('ui-autocomplete')._renderItem = function (ul, item) {
                return $(item.tag)
                    .data("ui-autocomplete-item", item)
                    .append(item.html)
                    .appendTo(ul);
            };
        }
    }

    // проверяем что все запросы завершены
    function isAllAutocompleteFinished(isAutocompleteFinished, metaAutocompleteInfoList) {
        if (!isAutocompleteFinished.main) {
            return false;
        }

        if (metaAutocompleteInfoList) {
            for (var i = 0; i < metaAutocompleteInfoList.length; i++) {
                var metaAutocompleteInfo = metaAutocompleteInfoList[i];
                if (!isAutocompleteFinished.other[metaAutocompleteInfo.applicationType]) {
                    return false;
                }
            }
        }

        return true;
    }

    function getAllData(autocompleteSearchResult, metaAutocompleteInfoList) {
        var otherData = [];
        if (metaAutocompleteInfoList) {
            for (var i = 0; i < metaAutocompleteInfoList.length; i++) {
                var metaAutocompleteInfo = metaAutocompleteInfoList[i];
                if (autocompleteSearchResult.other[metaAutocompleteInfo.applicationType]) {
                    if (otherData.length == 0) {
                        otherData.push({
                            html: '<a class="  " href="">Найдено в других пространствах Юлмарта</a>',
                            tag: '<li style="pointer-events: none;" class="ui-menu-item ui-menu-item_section"></li>'
                        });
                    }
                    otherData = otherData.concat(autocompleteSearchResult.other[metaAutocompleteInfo.applicationType]);
                }
            }
        }

        var categorySectionEndIndex = getCategorySectionEndIndex(autocompleteSearchResult.main);
        var allData;
        if (categorySectionEndIndex == -1) {
            allData = otherData.concat(autocompleteSearchResult.main);
        } else {
            allData = [];
            for (i = 0; i < autocompleteSearchResult.main.length; i++) {
                allData = allData.concat(autocompleteSearchResult.main[i]);
                if (i == categorySectionEndIndex) {
                    allData = allData.concat(otherData);
                }
            }
        }

        return allData;
    }

    function getCategorySectionBeginIndex(mainData) {
        var categorySectionBeginIndex = -1;
        for (var i = 0; i < mainData.length; i++) {
            if (mainData[i].tag.indexOf('ui-menu-item_section') !== -1 &&
                mainData[i].html.indexOf('Категории') !== -1) {
                categorySectionBeginIndex = i;
                break;
            }
        }
        return categorySectionBeginIndex;
    }

    function getCategorySectionEndIndex(mainData) {
        var categorySectionBeginIndex = getCategorySectionBeginIndex(mainData);
        var categorySectionEndIndex = -1;

        if (categorySectionBeginIndex != -1) {
            for(var i = categorySectionBeginIndex + 1; i < mainData.length; i++) {
                if (mainData[i].tag.indexOf('ui-menu-item_section') !== -1) {
                    categorySectionEndIndex = i - 1;
                    break;
                }
            }
        }

        return categorySectionEndIndex;
    }
});

/* desktop.bundles/template/_template.js (part)  */
/* ..\..\desktop.blocks\collapse\collapse.js begin */
/* ========================================================================
 * Bootstrap: collapse.js v3.0.3
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================================== */


+function ($) { "use strict";

    // COLLAPSE PUBLIC CLASS DEFINITION
    // ================================

    var Collapse = function (element, options) {
        this.$element      = $(element)
        this.options       = $.extend({}, Collapse.DEFAULTS, options)
        this.transitioning = null

        if (this.options.parent) this.$parent = $(this.options.parent)
        if (this.options.toggle) this.toggle()
    }

    Collapse.DEFAULTS = {
        toggle: true
    }

    Collapse.prototype.dimension = function () {
        var hasWidth = this.$element.hasClass('width')
        return hasWidth ? 'width' : 'height'
    }

    Collapse.prototype.show = function () {
        if (this.transitioning || this.$element.hasClass('in')) return

        var startEvent = $.Event('show.bs.collapse')
        this.$element.trigger(startEvent)
        if (startEvent.isDefaultPrevented()) return

        var actives = this.$parent && this.$parent.find('> .panel > .in')

        if (actives && actives.length) {
            var hasData = actives.data('bs.collapse')
            if (hasData && hasData.transitioning) return
            actives.collapse('hide')
            hasData || actives.data('bs.collapse', null)
        }

        var dimension = this.dimension()

        this.$element
            .removeClass('collapse')
            .addClass('collapsing')
            [dimension](0)

        this.transitioning = 1

        var complete = function () {
            this.$element
                .removeClass('collapsing')
                .addClass('in')
                [dimension]('auto')
            this.transitioning = 0
            this.$element.trigger('shown.bs.collapse')
        }

        if (!$.support.transition) return complete.call(this)

        var scrollSize = $.camelCase(['scroll', dimension].join('-'))

        this.$element
            .one($.support.transition.end, $.proxy(complete, this))
            .emulateTransitionEnd(350)
            [dimension](this.$element[0][scrollSize])
    }

    Collapse.prototype.hide = function () {
        if (this.transitioning || !this.$element.hasClass('in')) return

        var startEvent = $.Event('hide.bs.collapse')
        this.$element.trigger(startEvent)
        if (startEvent.isDefaultPrevented()) return

        var dimension = this.dimension()

        this.$element
            [dimension](this.$element[dimension]())
            [0].offsetHeight

        this.$element
            .addClass('collapsing')
            .removeClass('collapse')
            .removeClass('in')

        this.transitioning = 1

        var complete = function () {
            this.transitioning = 0
            this.$element
                .trigger('hidden.bs.collapse')
                .removeClass('collapsing')
                .addClass('collapse')
        }

        if (!$.support.transition) return complete.call(this)

        this.$element
            [dimension](0)
            .one($.support.transition.end, $.proxy(complete, this))
            .emulateTransitionEnd(350)
    }

    Collapse.prototype.toggle = function () {
        this[this.$element.hasClass('in') ? 'hide' : 'show']()
    }


    // COLLAPSE PLUGIN DEFINITION
    // ==========================

    var old = $.fn.collapse

    $.fn.collapse = function (option) {
        return this.each(function () {
            var $this   = $(this)
            var data    = $this.data('bs.collapse')
            var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }

    $.fn.collapse.Constructor = Collapse


    // COLLAPSE NO CONFLICT
    // ====================

    $.fn.collapse.noConflict = function () {
        $.fn.collapse = old
        return this
    }


    // COLLAPSE DATA-API
    // =================

    $(document).on('click.bs.collapse.data-api', '[data-toggle=collapse]', function (e) {
        var $this   = $(this), href
        var target  = $this.attr('data-target')
            || e.preventDefault()
            || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
        var $target = $(target)
        var data    = $target.data('bs.collapse')
        var option  = data ? 'toggle' : $this.data()
        var parent  = $this.attr('data-parent')
        var $parent = parent && $(parent)

        if (!data || !data.transitioning) {
            if ($parent) $parent.find('[data-toggle=collapse][data-parent="' + parent + '"]').not($this).addClass('collapsed')
            $this[$target.hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
        }

        $target.collapse(option)
    })

}(jQuery);
/* ..\..\desktop.blocks\collapse\collapse.js end */
/* ..\..\desktop.blocks\navbar\navbar.js begin */
$(function(){
    if($(".b-page__header .navbar .navbar-left.navbar-nav_sites").length){

        //Sites nav bar responsive behaviour
        var $topbar = $(".navbar-static-top:eq(0)");
        var $nav = $topbar.find(".navbar-left.navbar-nav_sites");
        var $navItems = $nav.find("> li:not(.dropdown)");
        var $dropdown = $nav.find("> li.dropdown");
        var $rightbar = $topbar.find(".navbar-right");

        //Create clone for calculate widths
        var $cloneNav = $nav.clone();
        var $holder = $nav.parents(".container-fluid");
        $cloneNav.css({
            position: "absolute",
            top: "-10000px",
            left: "0px",
        }).addClass("calc-clone");
        $holder.append($cloneNav);
        $cloneNav.find(" > li").removeClass("hidden");

        var rightBarWidth = $rightbar.width();
        //Calculate widths of nav items
        var liWidth = [];
        $cloneNav.find("> li:not(.dropdown)").each(function(i, li){
            liWidth[i] = $(li).width();
        });
        var dropDownWidth = $cloneNav.find("> li.dropdown").width();

        //
        function controlItems(){
            var allWidth = $topbar.find(".container-fluid").width();
            //var spaceWidth = parseInt(allWidth - rightBarWidth - allWidth*1.5625/100);
            var spaceMargin = 0;
            if (Math.abs(parseInt($rightbar.css("margin-right")))>0) spaceMargin = Math.abs(parseInt($rightbar.css("margin-right")));
            var spaceWidth = parseInt(allWidth - rightBarWidth) + spaceMargin;
            var fillW = 0;
            var breakIndex = 0;
            for(var i=0; i < liWidth.length; i++){
                if(fillW + liWidth[i] <= spaceWidth){
                    fillW += liWidth[i];
                    breakIndex = i + 1;
                }
                else{
                    breakIndex = i;
                    break;
                }
            }

            //console.log(spaceWidth, fillW, dropDownWidth, breakIndex, liWidth.length, liWidth);
            if(spaceWidth < fillW + dropDownWidth && liWidth.length != breakIndex){
                breakIndex -= 1;
            }

            $navItems.detach();
            $dropdown.detach();

            if(liWidth.length == breakIndex){
                $navItems.appendTo($nav);
            }
            else{
                $navItems.slice(0, breakIndex).appendTo($nav);
                $navItems.slice(breakIndex).appendTo($dropdown.find(".dropdown-menu"));
                $dropdown.appendTo($nav);
            }

            //console.log(spaceWidth, fillW, dropDownWidth, breakIndex, liWidth.length, liWidth);
        }
        $(window).resize(controlItems);
        controlItems();


        //remove hidden class
        $navItems.removeClass("hidden");
        $dropdown.removeClass("hidden");

    }
});

// Modal cities window. Switch classes on wide screen
// TODO: это решение для конкретного модальника, если делать это для всех
// модальников, то наверное нужно этот скрипт перенести в другое место...
/*$(function(){
 function modalWindowSwitchClass(targetWin){
 if( !(targetWin instanceof jQuery) && typeof arguments[0].data.target != "undefined"){
 targetWin = $(arguments[0].data.target);
 }
 if(typeof targetWin == "undefined"){
 return false;
 }

 var layout = {
 md: {min: 1280, max: 1439},
 lg: {min: 1440, max: 9999},
 }
 var ww = $(window).width();
 if(ww >= layout.lg.min && ww <= layout.lg.max){
 targetWin.addClass("width-main-16").removeClass("width-main-12");
 }
 else if(ww <= layout.md.max){
 targetWin.addClass("width-main-12").removeClass("width-main-16");
 }
 }

 var $link = $(".b-page__header .navbar-static-top .b-pseudolink.navbar-link[data-toggle='modal']");

 if($link.length){
 var targetWin = $link.data("target");
 $(targetWin).on('shown.bs.modal', function (e) {
 modalWindowSwitchClass($(e.currentTarget));
 $(window).resize({target: targetWin}, modalWindowSwitchClass);
 });
 $(targetWin).on('hidden.bs.modal', function (e) {
 $(window).off("resize", modalWindowSwitchClass);
 });
 }

 });*/

/* ..\..\desktop.blocks\navbar\navbar.js end */
;
/* ..\..\desktop.blocks\dropdown\dropdown.js begin */
/* ========================================================================
 * Bootstrap: dropdown.js v3.3.2
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
    'use strict';

    // DROPDOWN CLASS DEFINITION
    // =========================

    var backdrop = '.dropdown-backdrop'
    var toggle   = '[data-toggle="dropdown"]'
    var Dropdown = function (element) {
        $(element).on('click.bs.dropdown', this.toggle)
    }

    Dropdown.VERSION = '3.3.2'

    Dropdown.prototype.toggle = function (e) {
        var $this = $(this)

        if ($this.is('.disabled, :disabled')) return

        var $parent  = getParent($this)
        var isActive = $parent.hasClass('open')

        clearMenus()

        if (!isActive) {
            if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
                // if mobile we use a backdrop because click events don't delegate
                $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
            }

            var relatedTarget = {
                relatedTarget: this
            }
            $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

            if (e.isDefaultPrevented()) return

            $this
                .trigger('focus')
                .attr('aria-expanded', 'true')

            $parent
                .toggleClass('open')
                .trigger('shown.bs.dropdown', relatedTarget)
        }

        return false
    }

    Dropdown.prototype.keydown = function(e) {
        if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

        var $this = $(this)

        e.preventDefault()
        e.stopPropagation()

        if ($this.is('.disabled, :disabled')) return

        var $parent  = getParent($this)
        var isActive = $parent.hasClass('open')

        if ((!isActive && e.which != 27) || (isActive && e.which == 27)) {
            if (e.which == 27) $parent.find(toggle).trigger('focus')
            return $this.trigger('click')
        }

        var desc = ' li:not(.divider):visible a'
        var $items = $parent.find('[role="menu"]' + desc + ', [role="listbox"]' + desc)

        if (!$items.length) return

        var index = $items.index(e.target)

        if (e.which == 38 && index > 0)                 index--                        // up
        if (e.which == 40 && index < $items.length - 1) index++                        // down
        if (!~index)                                      index = 0

        $items.eq(index).trigger('focus')
    }

    function clearMenus(e) {
        if (e && e.which === 3) return
        $(backdrop).remove()
        $(toggle).each(function () {
            var $this         = $(this)
            var $parent       = getParent($this)
            var relatedTarget = { relatedTarget: this }

            if (!$parent.hasClass('open')) return

            $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

            if (e.isDefaultPrevented()) return

            $this.attr('aria-expanded', 'false')
            $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
        })
    }

    function getParent($this) {
        var selector = $this.attr('data-target')

        if (!selector) {
            selector = $this.attr('href')
            selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
        }

        var $parent = selector && $(selector)

        return $parent && $parent.length ? $parent : $this.parent()
    }


    // DROPDOWN PLUGIN DEFINITION
    // ==========================

    function Plugin(option) {
        return this.each(function () {
            var $this = $(this)
            var data  = $this.data('bs.dropdown')

            if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
            if (typeof option == 'string') data[option].call($this)
        })
    }

    var old = $.fn.dropdown

    $.fn.dropdown             = Plugin
    $.fn.dropdown.Constructor = Dropdown


    // DROPDOWN NO CONFLICT
    // ====================

    $.fn.dropdown.noConflict = function () {
        $.fn.dropdown = old
        return this
    }


    // APPLY TO STANDARD DROPDOWN ELEMENTS
    // ===================================

    $(document)
        .on('click.bs.dropdown.data-api', clearMenus)
        .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
        .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
        .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
        .on('keydown.bs.dropdown.data-api', '[role="menu"]', Dropdown.prototype.keydown)
        .on('keydown.bs.dropdown.data-api', '[role="listbox"]', Dropdown.prototype.keydown)

}(jQuery);
/* ..\..\desktop.blocks\dropdown\dropdown.js end */

/* ..\..\desktop.blocks\antiscroll\antiscroll.js begin */
(function ($) {

    /**
     * Augment jQuery prototype.
     */

    $.fn.antiscroll = function (options) {
        return this.each(function () {
            if ($(this).data('antiscroll')) {
                $(this).data('antiscroll').destroy();
            }

            $(this).data('antiscroll', new $.Antiscroll(this, options));
        });
    };

    /**
     * Expose constructor.
     */

    $.Antiscroll = Antiscroll;

    /**
     * Antiscroll pane constructor.
     *
     * @param {Element|jQuery} main pane
     * @parma {Object} options
     * @api public
     */

    function Antiscroll (el, opts) {
        this.el = $(el);
        this.options = opts || {};

        this.x = (false !== this.options.x) || this.options.forceHorizontal;
        this.y = (false !== this.options.y) || this.options.forceVertical;
        this.autoHide = false !== this.options.autoHide;
        this.padding = undefined == this.options.padding ? 2 : this.options.padding;

        this.inner = this.el.find('.antiscroll-inner');
        this.inner.css({
            'width':  '+=' + (this.y ? scrollbarSize() : 0)
            , 'height': '+=' + (this.x ? scrollbarSize() : 0)
        });

        this.refresh();
    };

    /**
     * refresh scrollbars
     *
     * @api public
     */

    Antiscroll.prototype.refresh = function() {
        var needHScroll = this.inner.get(0).scrollWidth > this.el.width() + (this.y ? scrollbarSize() : 0),
            needVScroll = this.inner.get(0).scrollHeight > this.el.height() + (this.x ? scrollbarSize() : 0);

        if (this.x) {
            if (!this.horizontal && needHScroll) {
                this.horizontal = new Scrollbar.Horizontal(this);
            } else if (this.horizontal && !needHScroll)  {
                this.horizontal.destroy();
                this.horizontal = null;
            } else if (this.horizontal) {
                this.horizontal.update();
            }
        }

        if (this.y) {
            if (!this.vertical && needVScroll) {
                this.vertical = new Scrollbar.Vertical(this);
            } else if (this.vertical && !needVScroll)  {
                this.vertical.destroy();
                this.vertical = null;
            } else if (this.vertical) {
                this.vertical.update();
            }
        }
    };

    /**
     * Cleans up.
     *
     * @return {Antiscroll} for chaining
     * @api public
     */

    Antiscroll.prototype.destroy = function () {
        if (this.horizontal) {
            this.horizontal.destroy();
            this.horizontal = null
        }
        if (this.vertical) {
            this.vertical.destroy();
            this.vertical = null
        }
        return this;
    };

    /**
     * Rebuild Antiscroll.
     *
     * @return {Antiscroll} for chaining
     * @api public
     */

    Antiscroll.prototype.rebuild = function () {
        this.destroy();
        this.inner.attr('style', '');
        Antiscroll.call(this, this.el, this.options);
        return this;
    };

    /**
     * Scrollbar constructor.
     *
     * @param {Element|jQuery} element
     * @api public
     */

    function Scrollbar (pane) {
        this.pane = pane;
        this.pane.el.append(this.el);
        this.innerEl = this.pane.inner.get(0);

        this.dragging = false;
        this.enter = false;
        this.shown = false;

        // hovering
        this.pane.el.mouseenter($.proxy(this, 'mouseenter'));
        this.pane.el.mouseleave($.proxy(this, 'mouseleave'));

        // dragging
        this.el.mousedown($.proxy(this, 'mousedown'));

        // scrolling
        this.innerPaneScrollListener = $.proxy(this, 'scroll');
        this.pane.inner.scroll(this.innerPaneScrollListener);

        // wheel -optional-
        this.innerPaneMouseWheelListener = $.proxy(this, 'mousewheel');
        this.pane.inner.bind('mousewheel', this.innerPaneMouseWheelListener);

        // show
        var initialDisplay = this.pane.options.initialDisplay;

        if (initialDisplay !== false) {
            this.show();
            if (this.pane.autoHide) {
                this.hiding = setTimeout($.proxy(this, 'hide'), parseInt(initialDisplay, 10) || 3000);
            }
        }
    };

    /**
     * Cleans up.
     *
     * @return {Scrollbar} for chaining
     * @api public
     */

    Scrollbar.prototype.destroy = function () {
        this.el.remove();
        this.pane.inner.unbind('scroll', this.innerPaneScrollListener);
        this.pane.inner.unbind('mousewheel', this.innerPaneMouseWheelListener);
        return this;
    };

    /**
     * Called upon mouseenter.
     *
     * @api private
     */

    Scrollbar.prototype.mouseenter = function () {
        this.enter = true;
        this.show();
    };

    /**
     * Called upon mouseleave.
     *
     * @api private
     */

    Scrollbar.prototype.mouseleave = function () {
        this.enter = false;

        if (!this.dragging) {
            if (this.pane.autoHide) {
                this.hide();
            }
        }
    };

    /**
     * Called upon wrap scroll.
     *
     * @api private
     */

    Scrollbar.prototype.scroll = function () {
        if (!this.shown) {
            this.show();
            if (!this.enter && !this.dragging) {
                if (this.pane.autoHide) {
                    this.hiding = setTimeout($.proxy(this, 'hide'), 1500);
                }
            }
        }

        this.update();
    };

    /**
     * Called upon scrollbar mousedown.
     *
     * @api private
     */

    Scrollbar.prototype.mousedown = function (ev) {
        ev.preventDefault();

        this.dragging = true;

        this.startPageY = ev.pageY - parseInt(this.el.css('top'), 10);
        this.startPageX = ev.pageX - parseInt(this.el.css('left'), 10);

        // prevent crazy selections on IE
        this.el[0].ownerDocument.onselectstart = function () { return false; };

        var pane = this.pane,
            move = $.proxy(this, 'mousemove'),
            self = this

        $(this.el[0].ownerDocument)
            .mousemove(move)
            .mouseup(function () {
                self.dragging = false;
                this.onselectstart = null;

                $(this).unbind('mousemove', move);

                if (!self.enter) {
                    self.hide();
                }
            });
    };

    /**
     * Show scrollbar.
     *
     * @api private
     */

    Scrollbar.prototype.show = function(duration) {
        if (!this.shown && this.update()) {
            this.el.addClass('antiscroll-scrollbar-shown');
            if (this.hiding) {
                clearTimeout(this.hiding);
                this.hiding = null;
            }
            this.shown = true;
        }
    };

    /**
     * Hide scrollbar.
     *
     * @api private
     */

    Scrollbar.prototype.hide = function() {
        if (this.pane.autoHide !== false && this.shown) {
            // check for dragging
            this.el.removeClass('antiscroll-scrollbar-shown');
            this.shown = false;
        }
    };

    /**
     * Horizontal scrollbar constructor
     *
     * @api private
     */

    Scrollbar.Horizontal = function(pane) {
        //this.el = $('<div class="antiscroll-scrollbar antiscroll-scrollbar-horizontal"/>', pane.el);
        this.el = $('<div class="antiscroll-scrollbar antiscroll-scrollbar-horizontal"/>').appendTo(pane.el);
        Scrollbar.call(this, pane);
    };

    /**
     * Inherits from Scrollbar.
     */

    inherits(Scrollbar.Horizontal, Scrollbar);

    /**
     * Updates size/position of scrollbar.
     *
     * @api private
     */

    Scrollbar.Horizontal.prototype.update = function() {
        var paneWidth = this.pane.el.width(),
            trackWidth = paneWidth - this.pane.padding * 2,
            innerEl = this.pane.inner.get(0)

        this.el
            .css('width', trackWidth * paneWidth / innerEl.scrollWidth)
            .css('left', trackWidth * innerEl.scrollLeft / innerEl.scrollWidth);

        return paneWidth < innerEl.scrollWidth;
    };

    /**
     * Called upon drag.
     *
     * @api private
     */

    Scrollbar.Horizontal.prototype.mousemove = function(ev) {
        var trackWidth = this.pane.el.width() - this.pane.padding * 2,
            pos = ev.pageX - this.startPageX,
            barWidth = this.el.width(),
            innerEl = this.pane.inner.get(0)

        // minimum top is 0, maximum is the track height
        var y = Math.min(Math.max(pos, 0), trackWidth - barWidth);

        innerEl.scrollLeft = (innerEl.scrollWidth - this.pane.el.width()) *
            y / (trackWidth - barWidth);
    };

    /**
     * Called upon container mousewheel.
     *
     * @api private
     */

    Scrollbar.Horizontal.prototype.mousewheel = function(ev, delta, x, y) {
        if ((x < 0 && 0 == this.pane.inner.get(0).scrollLeft) ||
            (x > 0 && (this.innerEl.scrollLeft + Math.ceil(this.pane.el.width()) ==
                this.innerEl.scrollWidth))) {
            ev.preventDefault();
            return false;
        }
    };

    /**
     * Vertical scrollbar constructor
     *
     * @api private
     */

    Scrollbar.Vertical = function(pane) {
        //this.el = $('<div class="antiscroll-scrollbar antiscroll-scrollbar-vertical"/>', pane.el);
        this.el = $('<div class="antiscroll-scrollbar antiscroll-scrollbar-vertical"/>').appendTo(pane.el);
        Scrollbar.call(this, pane);
    };

    /**
     * Inherits from Scrollbar.
     */

    inherits(Scrollbar.Vertical, Scrollbar);

    /**
     * Updates size/position of scrollbar.
     *
     * @api private
     */

    Scrollbar.Vertical.prototype.update = function() {
        var paneHeight = this.pane.el.height(),
            trackHeight = paneHeight - this.pane.padding * 2,
            innerEl = this.innerEl;

        var scrollbarHeight = trackHeight * paneHeight / innerEl.scrollHeight;
        scrollbarHeight = scrollbarHeight < 20 ? 20 : scrollbarHeight;

        var topPos = trackHeight * innerEl.scrollTop / innerEl.scrollHeight;

        if ((topPos + scrollbarHeight) > trackHeight) {
            var diff = (topPos + scrollbarHeight) - trackHeight;
            topPos = topPos - diff - 3;
        }

        this.el
            .css('height', scrollbarHeight)
            .css('top', topPos);

        return paneHeight < innerEl.scrollHeight;
    };

    /**
     * Called upon drag.
     *
     * @api private
     */

    Scrollbar.Vertical.prototype.mousemove = function(ev) {
        var paneHeight = this.pane.el.height(),
            trackHeight = paneHeight - this.pane.padding * 2,
            pos = ev.pageY - this.startPageY,
            barHeight = this.el.height(),
            innerEl = this.innerEl

        // minimum top is 0, maximum is the track height
        var y = Math.min(Math.max(pos, 0), trackHeight - barHeight);

        innerEl.scrollTop = (innerEl.scrollHeight - paneHeight) *
            y / (trackHeight - barHeight);
    };

    /**
     * Called upon container mousewheel.
     *
     * @api private
     */

    Scrollbar.Vertical.prototype.mousewheel = function(ev, delta, x, y) {
        if ((y > 0 && 0 == this.innerEl.scrollTop) ||
            (y < 0 && (this.innerEl.scrollTop + Math.ceil(this.pane.el.height()) ==
                this.innerEl.scrollHeight))) {
            ev.preventDefault();
            return false;
        }
    };

    /**
     * Cross-browser inheritance.
     *
     * @param {Function} constructor
     * @param {Function} constructor we inherit from
     * @api private
     */

    function inherits(ctorA, ctorB) {
        function f() {};
        f.prototype = ctorB.prototype;
        ctorA.prototype = new f;
    };

    /**
     * Scrollbar size detection.
     */

    var size;

    function scrollbarSize() {
        if (size === undefined) {
            var div = $(
                '<div class="antiscroll-inner" style="width:50px;height:50px;overflow-y:scroll;' +
                'position:absolute;top:-200px;left:-200px;"><div style="height:100px;width:100%"/>' +
                '</div>'
            );

            $('body').append(div);
            var w1 = $(div).innerWidth();
            var w2 = $('div', div).innerWidth();
            $(div).remove();

            size = w1 - w2;
        }

        return size;
    };

})(jQuery);

/* ..\..\desktop.blocks\antiscroll\antiscroll.js end */
;
/* ..\..\desktop.blocks\b-location\b-location.js begin */
$(function() {
    var citiesData = {};
    var modalTPL = "<button type='button' class='close' data-dismiss='modal' aria-hidden='true' aria-label='Close'>×</button>\
<div class='b-location'>\
	<div class='b-location__col b-location__col_region'>\
		<div class='antiscroll-wrap'>\
		<div class='b-location__box'>\
		<div class='antiscroll-inner'>\
		<div id='b-location__subjects'>\
		<ul class='b-loclist b-loclist_region' id='primary-subjects'>\
			<li class='b-loclist__item_search-result'><a data-subj='-1' href='#'>Результаты поиска</a></li>\
			<li class='b-loclist__item_all'><a class='active' data-subj='0' href='#'>Все города</a></li>\
		</ul>\
		</div>\
		</div>\
		</div>\
		</div>\
	</div>\
	<div class='b-location__col b-location__col_city'>\
		<div class='antiscroll-wrap'>\
		<div class='b-location__box'>\
		<div class='antiscroll-inner'>\
		<div id='b-location__cities'>\
		<div class='b-location__result'>\
			<div class='b-location__noresult'>\
				<p class='text-lg' class='b-location__noresult-title'><%NOT_FOUND_TITLE%></p>\
				<div class='b-location__noresult-text'>\
					<%NOT_FOUND_TEXT%>\
				</div>\
			</div>\
		</div>\
		</div>\
		</div>\
		</div>\
		</div>\
	</div>\
	<div class='b-location__col b-location__col_search'>\
		<div class='b-location__search form'>\
			<div class='form-group'>\
				<input type='text' id='b-location__search-city' class='form-control' placeholder='Поиск города'>\
				<span aria-hidden='true' class='glyphicon fa fa-search form-control-feedback'></span>\
				<a class='b-location__search-clear' href='#'>×</a>\
			</div>\
		</div>\
		<div class='b-location__delivery'>\
			<p><b><%POST_DELIVERY_T_TITLE%></b></p>\
			<p><a href='#' id='b-location__post' class='btn btn_theme_normal btn-md btn-primary-o'><%POST_DELIVERY_NAME%></a></p>\
			<p><%POST_DELIVERY_T_DESCRIPTION%><br />\
			<a href='<%POST_DELIVERY_LEARNMORE%>'><%POST_DELIVERY_LEARNMORE_TEXT%></a></p>\
		</div>\
		<div class='b-location__text'>\
			<%POST_DELIVERY_T_SUB_DESCRIPTION%>\
		</div>\
	</div>\
</div>";

    $(document).on("click", "#load-cities", function(e) {
        var url = $(this).data("href");
        var $container = $($(this).data("target"));
        $.ajax(url, {
            dataType: "json",
            xhrFields: {
                withCredentials: true
            },
            success: function(data, textStatus, jqXHR) {
                //console.log(data);
                citiesData = data;
                tplTextFill();
                $container.html(modalTPL);
                buildSubjects(data.subjects);
                buildCities(data.cities);
                bindSubjects();
                searchInit();
                hideEmpty($container);

                $("#b-location__post").attr("onclick", "changeCity(\"" + data.post.id + "\",\"" + data.post.code + "\",\"" + data.post.short + "\"); return false;").text(data.post.name);
                $("#b-location__post").on("click", function(e) {
                    e.preventDefault();
                    $(this).append(' <i class="b-ico b-ico_preloader_sm"></i>');
                });

                $container.on('shown.bs.modal', function(e) {
                    var $scroll = $(e.currentTarget).parent();
                    $scroll.addClass("b-location__modal-scroll");
                    $container.find('.antiscroll-wrap').antiscroll({
                        autoHide: false
                    });

                }).modal();
            }
        });

        e.preventDefault();
    });

    function tplTextFill() {
        modalTPL = modalTPL.replace("<%NOT_FOUND_TITLE%>", citiesData.not_found.title);
        modalTPL = modalTPL.replace("<%NOT_FOUND_TEXT%>", citiesData.not_found.text);
        modalTPL = modalTPL.replace("<%POST_DELIVERY_NAME%>", citiesData.post.name || "");
        modalTPL = modalTPL.replace("<%POST_DELIVERY_LEARNMORE%>", citiesData.post.learn_more || "");
        modalTPL = modalTPL.replace("<%POST_DELIVERY_LEARNMORE_TEXT%>", citiesData.post.learn_more_text || "");
        modalTPL = modalTPL.replace("<%POST_DELIVERY_T_TITLE%>", citiesData.post.t_title || "");
        modalTPL = modalTPL.replace("<%POST_DELIVERY_T_DESCRIPTION%>", citiesData.post.t_description || "");
        modalTPL = modalTPL.replace("<%POST_DELIVERY_T_SUB_DESCRIPTION%>", citiesData.post.t_sub_description || "");
    }

    function hideEmpty($container) {
        if (typeof citiesData.post.name == "undefined" && typeof citiesData.post.t_title == "undefined") {
            $container.find(".b-location__delivery").hide();
        }
        if (typeof citiesData.post.t_sub_description == "undefined") {
            $container.find(".b-location__text").hide();
        }
    }

    function buildSubjects(list) {
        list.sort(function(a, b) {
            if (a.primary == b.primary) {
                if (a.name < b.name) {
                    return -1;
                } else if (a.name > b.name) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                if (a.primary) {
                    return 1;
                } else {
                    return -1;
                }
            }

        });

        var $primary = $("#primary-subjects");
        $primary.after('<ul class="b-loclist b-loclist_region" id="secondary-subjects">');
        var $secondary = $("#secondary-subjects");

        var letter = '';
        var first = false;
        for (var i = 0; i < list.length; i++) {
            first = false;
            if (!list[i].primary && letter != list[i].name.substr(0, 1)) {
                letter = list[i].name.substr(0, 1);
                first = true;
            }
            var str = '<li><a href="#" data-subj="' + list[i].id + '">' + ((first) ? '<i class="b-loclist__letter">' + letter + '</i>' : '') + list[i].name + '</a></li>';
            if (list[i].primary) {
                $primary.append(str);
            } else {
                $secondary.append(str);
            }
        }
    }

    function buildCities(list) {
        list.sort(function(a, b) {
            if (a.name < b.name) {
                return -1;
            } else if (a.name > b.name) {
                return 1;
            } else {
                return 0;
            }
        });
        //console.log(list);
        var $citiesList = $("#b-location__cities");
        $citiesList.append('<ul id="all-cities" class="b-loclist b-loclist_city"></ul>');
        var allStr = '';
        var subj = {};
        //var subjStr = {};
        //var liStr = '';

        var letter = '';
        for (var i = 0; i < list.length; i++) {

            list[i].htmlStr = buildLi(list[i]);
            allStr += list[i].htmlStr;

            if (typeof subj[list[i].parent] == "undefined") {
                subj[list[i].parent] = []
            }
            subj[list[i].parent].push(list[i]);
        }
        $("#all-cities").append(allStr);

        //Cities by Subjects
        for (var key in subj) {
            var subjStr = '<ul id="subj-cities-' + key + '" class="b-loclist b-loclist_city" style="display:none">'
            subj[key].sort(function(a, b) {
                if (a.big == b.big) {
                    if (a.name < b.name) {
                        return -1;
                    } else if (a.name > b.name) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    if (a.big) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });
            for (i = 0; i < subj[key].length; i++) {
                subjStr += subj[key][i].htmlStr;
            }
            subjStr += '</ul>';
            $citiesList.append(subjStr);
        }

        //First letter in cities
        $citiesList.find('.b-loclist').each(function() {
            var $li = $(this).find("li");
            var letter = '';

            $li.each(function(i, v) {
                var l = $(this).find('a').text().substr(0, 1);
                if (l != letter) {
                    letter = l;
                    $(this).find('a').append('<i class="b-loclist__letter">' + letter + '</i>');
                }
            });

        });

        $citiesList.find('.b-loclist a').on('click', function(e) {
            e.preventDefault();
            var $ul = $(this).closest('ul');
            $ul.find('li .active i.b-ico').remove();
            $ul.find('li .active').removeClass('active');
            $(this).addClass('active').append('<i class="b-ico b-ico_preloader_sm_white_red"></i>');
        });
    }

    function buildLi(item) {
        var str = '<li class="';
        if (item.big) {
            str += 'b-loclist__item_big';
        }
        if (item.strong) {
            str += 'b-loclist__item_strong';
        }
        if (item.new) {
            str += ' b-loclist__item_new';
        }
        str += '">' +
            '<a href="#" data-gtm-event-context="' + item.name + '" class="js-gtm-city-choose" onclick="changeCity(\'' + item.id + '\',\'' + item.code + '\',\'' + item.name + '\');return false;">' +
            item.name +
            ((item.new) ? ' <i>New!</i>' : '') +
            '</a></li>';
        return str;
    }

    function bindSubjects() {
        $("#primary-subjects li a, #secondary-subjects li a").on("click", function(e) {
            var eid = $(this).data("subj");
            $(".b-location__result, .b-location__col_city .b-loclist:not(.b-location__result .b-loclist)").hide();

            if (eid == -1) {
                $(".b-location__result").show();
            } else if (eid == 0) {
                $("#all-cities").show();
            } else {
                $("#subj-cities-" + eid).show();
            }

            $("#primary-subjects li a, #secondary-subjects li a").removeClass("active");
            $(this).addClass("active");

            $('.b-location__col_city .antiscroll-wrap .antiscroll-inner').removeAttr("style");
            $('.b-location__col_city .antiscroll-wrap').antiscroll({
                autoHide: false
            });

            e.preventDefault();
        });
    }

    function searchInit() {
        var $input = $("#b-location__search-city");
        $input.val('');
        $input.on("keyup", function(e) {
            var searchValue = $(this).val();
            $(".b-loclist__item_search-result").show();

            if (searchValue.length == 0) {
                $(".b-loclist__item_search-result").hide();
                $(".b-loclist__item_all a").trigger("click");
                $(".b-location__noresult").show();
                $("#subj-cities-result").hide();
                $(".b-location__search-clear").hide();
            } else {
                $(".b-loclist__item_search-result").show();
                $(".b-location__search-clear").show();
                $(".b-loclist__item_search-result a").trigger("click");
                var filtCity = citiesData.cities.filter(function(item) {
                    re = new RegExp(searchValue, "ig");
                    return re.test(item.name);
                });

                if (filtCity.length > 0) {
                    var strCity = '';
                    for (var i = 0; i < filtCity.length; i++) {
                        strCity += buildLi(filtCity[i]);
                    }
                    if (!$("#subj-cities-result").length) {
                        $('.b-location__result').append('<ul id="subj-cities-result" class="b-loclist" style="display:none">');
                    }
                    $("#subj-cities-result").empty().append(strCity);

                    $(".b-location__noresult").hide();
                    $("#subj-cities-result").show();
                } else {
                    $(".b-location__noresult").show();
                    $("#subj-cities-result").hide();
                }
            }
            $('.b-location .antiscroll-wrap .antiscroll-inner').removeAttr("style");
            $('.b-location .antiscroll-wrap').antiscroll({
                autoHide: false
            });
        });
        $(".b-location__search-clear").on("click", function(e) {
            $input.val('').trigger("keyup");
            e.preventDefault();
        })
    }

    $(window).resize(function() {
        $('.b-location__col_city .antiscroll-wrap .antiscroll-inner').removeAttr("style");
        $('.b-location__col_city .antiscroll-wrap').antiscroll({
            autoHide: false
        });
    });

});

/* ..\..\desktop.blocks\b-location\b-location.js end */
/* ..\..\desktop.blocks\qtip\qtip.js begin */
/* qtip2 v2.2.1-29- | Plugins: tips modal viewport svg imagemap ie6 | Styles: core basic css3 | qtip2.com | Licensed MIT | Tue Dec 08 2015 05:50:23 */

! function(a, b, c) {
    ! function(a) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], a) : jQuery && !jQuery.fn.qtip && a(jQuery)
    }(function(d) {
        "use strict";

        function e(a, b, c, e) {
            this.id = c, this.target = a, this.tooltip = F, this.elements = {
                target: a
            }, this._id = S + "-" + c, this.timers = {
                img: {}
            }, this.options = b, this.plugins = {}, this.cache = {
                event: {},
                target: d(),
                disabled: E,
                attr: e,
                onTooltip: E,
                lastClass: ""
            }, this.rendered = this.destroyed = this.disabled = this.waiting = this.hiddenDuringWait = this.positioning = this.triggering = E
        }

        function f(a) {
            return a === F || "object" !== d.type(a)
        }

        function g(a) {
            return !(d.isFunction(a) || a && a.attr || a.length || "object" === d.type(a) && (a.jquery || a.then))
        }

        function h(a) {
            var b, c, e, h;
            return f(a) ? E : (f(a.metadata) && (a.metadata = {
                type: a.metadata
            }), "content" in a && (b = a.content, f(b) || b.jquery || b.done ? b = a.content = {
                text: c = g(b) ? E : b
            } : c = b.text, "ajax" in b && (e = b.ajax, h = e && e.once !== E, delete b.ajax, b.text = function(a, b) {
                var f = c || d(this).attr(b.options.content.attr) || "Loading...",
                    g = d.ajax(d.extend({}, e, {
                        context: b
                    })).then(e.success, F, e.error).then(function(a) {
                        return a && h && b.set("content.text", a), a
                    }, function(a, c, d) {
                        b.destroyed || 0 === a.status || b.set("content.text", c + ": " + d)
                    });
                return h ? f : (b.set("content.text", f), g)
            }), "title" in b && (d.isPlainObject(b.title) && (b.button = b.title.button, b.title = b.title.text), g(b.title || E) && (b.title = E))), "position" in a && f(a.position) && (a.position = {
                my: a.position,
                at: a.position
            }), "show" in a && f(a.show) && (a.show = a.show.jquery ? {
                target: a.show
            } : a.show === D ? {
                ready: D
            } : {
                event: a.show
            }), "hide" in a && f(a.hide) && (a.hide = a.hide.jquery ? {
                target: a.hide
            } : {
                event: a.hide
            }), "style" in a && f(a.style) && (a.style = {
                classes: a.style
            }), d.each(R, function() {
                this.sanitize && this.sanitize(a)
            }), a)
        }

        function i(a, b) {
            for (var c, d = 0, e = a, f = b.split("."); e = e[f[d++]];) d < f.length && (c = e);
            return [c || a, f.pop()]
        }

        function j(a, b) {
            var c, d, e;
            for (c in this.checks)
                for (d in this.checks[c])(e = new RegExp(d, "i").exec(a)) && (b.push(e), ("builtin" === c || this.plugins[c]) && this.checks[c][d].apply(this.plugins[c] || this, b))
        }

        function k(a) {
            return V.concat("").join(a ? "-" + a + " " : " ")
        }

        function l(a, b) {
            return b > 0 ? setTimeout(d.proxy(a, this), b) : void a.call(this)
        }

        function m(a) {
            this.tooltip.hasClass(ab) || (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this.timers.show = l.call(this, function() {
                this.toggle(D, a)
            }, this.options.show.delay))
        }

        function n(a) {
            if (!this.tooltip.hasClass(ab) && !this.destroyed) {
                var b = d(a.relatedTarget),
                    c = b.closest(W)[0] === this.tooltip[0],
                    e = b[0] === this.options.show.target[0];
                if (clearTimeout(this.timers.show), clearTimeout(this.timers.hide), this !== b[0] && "mouse" === this.options.position.target && c || this.options.hide.fixed && /mouse(out|leave|move)/.test(a.type) && (c || e)) try {
                    a.preventDefault(), a.stopImmediatePropagation()
                } catch (f) {} else this.timers.hide = l.call(this, function() {
                    this.toggle(E, a)
                }, this.options.hide.delay, this)
            }
        }

        function o(a) {
            !this.tooltip.hasClass(ab) && this.options.hide.inactive && (clearTimeout(this.timers.inactive), this.timers.inactive = l.call(this, function() {
                this.hide(a)
            }, this.options.hide.inactive))
        }

        function p(a) {
            this.rendered && this.tooltip[0].offsetWidth > 0 && this.reposition(a)
        }

        function q(a, c, e) {
            d(b.body).delegate(a, (c.split ? c : c.join("." + S + " ")) + "." + S, function() {
                var a = y.api[d.attr(this, U)];
                a && !a.disabled && e.apply(a, arguments)
            })
        }

        function r(a, c, f) {
            var g, i, j, k, l, m = d(b.body),
                n = a[0] === b ? m : a,
                o = a.metadata ? a.metadata(f.metadata) : F,
                p = "html5" === f.metadata.type && o ? o[f.metadata.name] : F,
                q = a.data(f.metadata.name || "qtipopts");
            try {
                q = "string" == typeof q ? d.parseJSON(q) : q
            } catch (r) {}
            if (k = d.extend(D, {}, y.defaults, f, "object" == typeof q ? h(q) : F, h(p || o)), i = k.position, k.id = c, "boolean" == typeof k.content.text) {
                if (j = a.attr(k.content.attr), k.content.attr === E || !j) return E;
                k.content.text = j
            }
            if (i.container.length || (i.container = m), i.target === E && (i.target = n), k.show.target === E && (k.show.target = n), k.show.solo === D && (k.show.solo = i.container.closest("body")), k.hide.target === E && (k.hide.target = n), k.position.viewport === D && (k.position.viewport = i.container), i.container = i.container.eq(0), i.at = new A(i.at, D), i.my = new A(i.my), a.data(S))
                if (k.overwrite) a.qtip("destroy", !0);
                else if (k.overwrite === E) return E;
            return a.attr(T, c), k.suppress && (l = a.attr("title")) && a.removeAttr("title").attr(cb, l).attr("title", ""), g = new e(a, k, c, !!j), a.data(S, g), g
        }

        function s(a) {
            return a.charAt(0).toUpperCase() + a.slice(1)
        }

        function t(a, b) {
            var d, e, f = b.charAt(0).toUpperCase() + b.slice(1),
                g = (b + " " + rb.join(f + " ") + f).split(" "),
                h = 0;
            if (qb[b]) return a.css(qb[b]);
            for (; d = g[h++];)
                if ((e = a.css(d)) !== c) return qb[b] = d, e
        }

        function u(a, b) {
            return Math.ceil(parseFloat(t(a, b)))
        }

        function v(a, b) {
            this._ns = "tip", this.options = b, this.offset = b.offset, this.size = [b.width, b.height], this.init(this.qtip = a)
        }

        function w(a, b) {
            this.options = b, this._ns = "-modal", this.init(this.qtip = a)
        }

        function x(a) {
            this._ns = "ie6", this.init(this.qtip = a)
        }
        var y, z, A, B, C, D = !0,
            E = !1,
            F = null,
            G = "x",
            H = "y",
            I = "width",
            J = "height",
            K = "top",
            L = "left",
            M = "bottom",
            N = "right",
            O = "center",
            P = "flipinvert",
            Q = "shift",
            R = {},
            S = "qtip",
            T = "data-hasqtip",
            U = "data-qtip-id",
            V = ["ui-widget", "ui-tooltip"],
            W = "." + S,
            X = "click dblclick mousedown mouseup mousemove mouseleave mouseenter".split(" "),
            Y = S + "-fixed",
            Z = S + "-default",
            $ = S + "-focus",
            _ = S + "-hover",
            ab = S + "-disabled",
            bb = "_replacedByqTip",
            cb = "oldtitle",
            db = {
                ie: function() {
                    for (var a = 4, c = b.createElement("div");
                        (c.innerHTML = "<!--[if gt IE " + a + "]><i></i><![endif]-->") && c.getElementsByTagName("i")[0]; a += 1);
                    return a > 4 ? a : 0 / 0
                }(),
                iOS: parseFloat(("" + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ""])[1]).replace("undefined", "3_2").replace("_", ".").replace("_", "")) || E
            };
        z = e.prototype, z._when = function(a) {
            return d.when.apply(d, a)
        }, z.render = function(a) {
            if (this.rendered || this.destroyed) return this;
            var b, c = this,
                e = this.options,
                f = this.cache,
                g = this.elements,
                h = e.content.text,
                i = e.content.title,
                j = e.content.button,
                k = e.position,
                l = ("." + this._id + " ", []);
            return d.attr(this.target[0], "aria-describedby", this._id), f.posClass = this._createPosClass((this.position = {
                my: k.my,
                at: k.at
            }).my), this.tooltip = g.tooltip = b = d("<div/>", {
                id: this._id,
                "class": [S, Z, e.style.classes, f.posClass].join(" "),
                width: e.style.width || "",
                height: e.style.height || "",
                tracking: "mouse" === k.target && k.adjust.mouse,
                role: "alert",
                "aria-live": "polite",
                "aria-atomic": E,
                "aria-describedby": this._id + "-content",
                "aria-hidden": D
            }).toggleClass(ab, this.disabled).attr(U, this.id).data(S, this).appendTo(k.container).append(g.content = d("<div />", {
                "class": S + "-content",
                id: this._id + "-content",
                "aria-atomic": D
            })), this.rendered = -1, this.positioning = D, i && (this._createTitle(), d.isFunction(i) || l.push(this._updateTitle(i, E))), j && this._createButton(), d.isFunction(h) || l.push(this._updateContent(h, E)), this.rendered = D, this._setWidget(), d.each(R, function(a) {
                var b;
                "render" === this.initialize && (b = this(c)) && (c.plugins[a] = b)
            }), this._unassignEvents(), this._assignEvents(), this._when(l).then(function() {
                c._trigger("render"), c.positioning = E, c.hiddenDuringWait || !e.show.ready && !a || c.toggle(D, f.event, E), c.hiddenDuringWait = E
            }), y.api[this.id] = this, this
        }, z.destroy = function(a) {
            function b() {
                if (!this.destroyed) {
                    this.destroyed = D;
                    var a, b = this.target,
                        c = b.attr(cb);
                    this.rendered && this.tooltip.stop(1, 0).find("*").remove().end().remove(), d.each(this.plugins, function() {
                        this.destroy && this.destroy()
                    });
                    for (a in this.timers) clearTimeout(this.timers[a]);
                    b.removeData(S).removeAttr(U).removeAttr(T).removeAttr("aria-describedby"), this.options.suppress && c && b.attr("title", c).removeAttr(cb), this._unassignEvents(), this.options = this.elements = this.cache = this.timers = this.plugins = this.mouse = F, delete y.api[this.id]
                }
            }
            return this.destroyed ? this.target : (a === D && "hide" !== this.triggering || !this.rendered ? b.call(this) : (this.tooltip.one("tooltiphidden", d.proxy(b, this)), !this.triggering && this.hide()), this.target)
        }, B = z.checks = {
            builtin: {
                "^id$": function(a, b, c, e) {
                    var f = c === D ? y.nextid : c,
                        g = S + "-" + f;
                    f !== E && f.length > 0 && !d("#" + g).length ? (this._id = g, this.rendered && (this.tooltip[0].id = this._id, this.elements.content[0].id = this._id + "-content", this.elements.title[0].id = this._id + "-title")) : a[b] = e
                },
                "^prerender": function(a, b, c) {
                    c && !this.rendered && this.render(this.options.show.ready)
                },
                "^content.text$": function(a, b, c) {
                    this._updateContent(c)
                },
                "^content.attr$": function(a, b, c, d) {
                    this.options.content.text === this.target.attr(d) && this._updateContent(this.target.attr(c))
                },
                "^content.title$": function(a, b, c) {
                    return c ? (c && !this.elements.title && this._createTitle(), void this._updateTitle(c)) : this._removeTitle()
                },
                "^content.button$": function(a, b, c) {
                    this._updateButton(c)
                },
                "^content.title.(text|button)$": function(a, b, c) {
                    this.set("content." + b, c)
                },
                "^position.(my|at)$": function(a, b, c) {
                    "string" == typeof c && (this.position[b] = a[b] = new A(c, "at" === b))
                },
                "^position.container$": function(a, b, c) {
                    this.rendered && this.tooltip.appendTo(c)
                },
                "^show.ready$": function(a, b, c) {
                    c && (!this.rendered && this.render(D) || this.toggle(D))
                },
                "^style.classes$": function(a, b, c, d) {
                    this.rendered && this.tooltip.removeClass(d).addClass(c)
                },
                "^style.(width|height)": function(a, b, c) {
                    this.rendered && this.tooltip.css(b, c)
                },
                "^style.widget|content.title": function() {
                    this.rendered && this._setWidget()
                },
                "^style.def": function(a, b, c) {
                    this.rendered && this.tooltip.toggleClass(Z, !!c)
                },
                "^events.(render|show|move|hide|focus|blur)$": function(a, b, c) {
                    this.rendered && this.tooltip[(d.isFunction(c) ? "" : "un") + "bind"]("tooltip" + b, c)
                },
                "^(show|hide|position).(event|target|fixed|inactive|leave|distance|viewport|adjust)": function() {
                    if (this.rendered) {
                        var a = this.options.position;
                        this.tooltip.attr("tracking", "mouse" === a.target && a.adjust.mouse), this._unassignEvents(), this._assignEvents()
                    }
                }
            }
        }, z.get = function(a) {
            if (this.destroyed) return this;
            var b = i(this.options, a.toLowerCase()),
                c = b[0][b[1]];
            return c.precedance ? c.string() : c
        };
        var eb = /^position\.(my|at|adjust|target|container|viewport)|style|content|show\.ready/i,
            fb = /^prerender|show\.ready/i;
        z.set = function(a, b) {
            if (this.destroyed) return this; {
                var c, e = this.rendered,
                    f = E,
                    g = this.options;
                this.checks
            }
            return "string" == typeof a ? (c = a, a = {}, a[c] = b) : a = d.extend({}, a), d.each(a, function(b, c) {
                if (e && fb.test(b)) return void delete a[b];
                var h, j = i(g, b.toLowerCase());
                h = j[0][j[1]], j[0][j[1]] = c && c.nodeType ? d(c) : c, f = eb.test(b) || f, a[b] = [j[0], j[1], c, h]
            }), h(g), this.positioning = D, d.each(a, d.proxy(j, this)), this.positioning = E, this.rendered && this.tooltip[0].offsetWidth > 0 && f && this.reposition("mouse" === g.position.target ? F : this.cache.event), this
        }, z._update = function(a, b) {
            var c = this,
                e = this.cache;
            return this.rendered && a ? (d.isFunction(a) && (a = a.call(this.elements.target, e.event, this) || ""), d.isFunction(a.then) ? (e.waiting = D, a.then(function(a) {
                return e.waiting = E, c._update(a, b)
            }, F, function(a) {
                return c._update(a, b)
            })) : a === E || !a && "" !== a ? E : (a.jquery && a.length > 0 ? b.children().detach().end().append(a.css({
                display: "block",
                visibility: "visible"
            })) : b.html(a), this._waitForContent(b).then(function(a) {
                c.rendered && c.tooltip[0].offsetWidth > 0 && c.reposition(e.event, !a.length)
            }))) : E
        }, z._waitForContent = function(a) {
            var b = this.cache;
            return b.waiting = D, (d.fn.imagesLoaded ? a.imagesLoaded() : d.Deferred().resolve([])).done(function() {
                b.waiting = E
            }).promise()
        }, z._updateContent = function(a, b) {
            this._update(a, this.elements.content, b)
        }, z._updateTitle = function(a, b) {
            this._update(a, this.elements.title, b) === E && this._removeTitle(E)
        }, z._createTitle = function() {
            var a = this.elements,
                b = this._id + "-title";
            a.titlebar && this._removeTitle(), a.titlebar = d("<div />", {
                "class": S + "-titlebar " + (this.options.style.widget ? k("header") : "")
            }).append(a.title = d("<div />", {
                id: b,
                "class": S + "-title",
                "aria-atomic": D
            })).insertBefore(a.content).delegate(".qtip-close", "mousedown keydown mouseup keyup mouseout", function(a) {
                d(this).toggleClass("ui-state-active ui-state-focus", "down" === a.type.substr(-4))
            }).delegate(".qtip-close", "mouseover mouseout", function(a) {
                d(this).toggleClass("ui-state-hover", "mouseover" === a.type)
            }), this.options.content.button && this._createButton()
        }, z._removeTitle = function(a) {
            var b = this.elements;
            b.title && (b.titlebar.remove(), b.titlebar = b.title = b.button = F, a !== E && this.reposition())
        }, z._createPosClass = function(a) {
            return S + "-pos-" + (a || this.options.position.my).abbrev()
        }, z.reposition = function(c, e) {
            if (!this.rendered || this.positioning || this.destroyed) return this;
            this.positioning = D;
            var f, g, h, i, j = this.cache,
                k = this.tooltip,
                l = this.options.position,
                m = l.target,
                n = l.my,
                o = l.at,
                p = l.viewport,
                q = l.container,
                r = l.adjust,
                s = r.method.split(" "),
                t = k.outerWidth(E),
                u = k.outerHeight(E),
                v = 0,
                w = 0,
                x = k.css("position"),
                y = {
                    left: 0,
                    top: 0
                },
                z = k[0].offsetWidth > 0,
                A = c && "scroll" === c.type,
                B = d(a),
                C = q[0].ownerDocument,
                F = this.mouse;
            if (d.isArray(m) && 2 === m.length) o = {
                x: L,
                y: K
            }, y = {
                left: m[0],
                top: m[1]
            };
            else if ("mouse" === m) o = {
                x: L,
                y: K
            }, (!r.mouse || this.options.hide.distance) && j.origin && j.origin.pageX ? c = j.origin : !c || c && ("resize" === c.type || "scroll" === c.type) ? c = j.event : F && F.pageX && (c = F), "static" !== x && (y = q.offset()), C.body.offsetWidth !== (a.innerWidth || C.documentElement.clientWidth) && (g = d(b.body).offset()), y = {
                left: c.pageX - y.left + (g && g.left || 0),
                top: c.pageY - y.top + (g && g.top || 0)
            }, r.mouse && A && F && (y.left -= (F.scrollX || 0) - B.scrollLeft(), y.top -= (F.scrollY || 0) - B.scrollTop());
            else {
                if ("event" === m ? c && c.target && "scroll" !== c.type && "resize" !== c.type ? j.target = d(c.target) : c.target || (j.target = this.elements.target) : "event" !== m && (j.target = d(m.jquery ? m : this.elements.target)), m = j.target, m = d(m).eq(0), 0 === m.length) return this;
                m[0] === b || m[0] === a ? (v = db.iOS ? a.innerWidth : m.width(), w = db.iOS ? a.innerHeight : m.height(), m[0] === a && (y = {
                    top: (p || m).scrollTop(),
                    left: (p || m).scrollLeft()
                })) : R.imagemap && m.is("area") ? f = R.imagemap(this, m, o, R.viewport ? s : E) : R.svg && m && m[0].ownerSVGElement ? f = R.svg(this, m, o, R.viewport ? s : E) : (v = m.outerWidth(E), w = m.outerHeight(E), y = m.offset()), f && (v = f.width, w = f.height, g = f.offset, y = f.position), y = this.reposition.offset(m, y, q), (db.iOS > 3.1 && db.iOS < 4.1 || db.iOS >= 4.3 && db.iOS < 4.33 || !db.iOS && "fixed" === x) && (y.left -= B.scrollLeft(), y.top -= B.scrollTop()), (!f || f && f.adjustable !== E) && (y.left += o.x === N ? v : o.x === O ? v / 2 : 0, y.top += o.y === M ? w : o.y === O ? w / 2 : 0)
            }
            return y.left += r.x + (n.x === N ? -t : n.x === O ? -t / 2 : 0), y.top += r.y + (n.y === M ? -u : n.y === O ? -u / 2 : 0), R.viewport ? (h = y.adjusted = R.viewport(this, y, l, v, w, t, u), g && h.left && (y.left += g.left), g && h.top && (y.top += g.top), h.my && (this.position.my = h.my)) : y.adjusted = {
                left: 0,
                top: 0
            }, j.posClass !== (i = this._createPosClass(this.position.my)) && k.removeClass(j.posClass).addClass(j.posClass = i), this._trigger("move", [y, p.elem || p], c) ? (delete y.adjusted, e === E || !z || isNaN(y.left) || isNaN(y.top) || "mouse" === m || !d.isFunction(l.effect) ? k.css(y) : d.isFunction(l.effect) && (l.effect.call(k, this, d.extend({}, y)), k.queue(function(a) {
                d(this).css({
                    opacity: "",
                    height: ""
                }), db.ie && this.style.removeAttribute("filter"), a()
            })), this.positioning = E, this) : this
        }, z.reposition.offset = function(a, c, e) {
            function f(a, b) {
                c.left += b * a.scrollLeft(), c.top += b * a.scrollTop()
            }
            if (!e[0]) return c;
            var g, h, i, j, k = d(a[0].ownerDocument),
                l = !!db.ie && "CSS1Compat" !== b.compatMode,
                m = e[0];
            do "static" !== (h = d.css(m, "position")) && ("fixed" === h ? (i = m.getBoundingClientRect(), f(k, -1)) : (i = d(m).position(), i.left += parseFloat(d.css(m, "borderLeftWidth")) || 0, i.top += parseFloat(d.css(m, "borderTopWidth")) || 0), c.left -= i.left + (parseFloat(d.css(m, "marginLeft")) || 0), c.top -= i.top + (parseFloat(d.css(m, "marginTop")) || 0), g || "hidden" === (j = d.css(m, "overflow")) || "visible" === j || (g = d(m))); while (m = m.offsetParent);
            return g && (g[0] !== k[0] || l) && f(g, 1), c
        };
        var gb = (A = z.reposition.Corner = function(a, b) {
            a = ("" + a).replace(/([A-Z])/, " $1").replace(/middle/gi, O).toLowerCase(), this.x = (a.match(/left|right/i) || a.match(/center/) || ["inherit"])[0].toLowerCase(), this.y = (a.match(/top|bottom|center/i) || ["inherit"])[0].toLowerCase(), this.forceY = !!b;
            var c = a.charAt(0);
            this.precedance = "t" === c || "b" === c ? H : G
        }).prototype;
        gb.invert = function(a, b) {
            this[a] = this[a] === L ? N : this[a] === N ? L : b || this[a]
        }, gb.string = function(a) {
            var b = this.x,
                c = this.y,
                d = b !== c ? "center" === b || "center" !== c && (this.precedance === H || this.forceY) ? [c, b] : [b, c] : [b];
            return a !== !1 ? d.join(" ") : d
        }, gb.abbrev = function() {
            var a = this.string(!1);
            return a[0].charAt(0) + (a[1] && a[1].charAt(0) || "")
        }, gb.clone = function() {
            return new A(this.string(), this.forceY)
        }, z.toggle = function(a, c) {
            var e = this.cache,
                f = this.options,
                g = this.tooltip;
            if (c) {
                if (/over|enter/.test(c.type) && e.event && /out|leave/.test(e.event.type) && f.show.target.add(c.target).length === f.show.target.length && g.has(c.relatedTarget).length) return this;
                e.event = d.event.fix(c)
            }
            if (this.waiting && !a && (this.hiddenDuringWait = D), !this.rendered) return a ? this.render(1) : this;
            if (this.destroyed || this.disabled) return this;
            var h, i, j, k = a ? "show" : "hide",
                l = this.options[k],
                m = (this.options[a ? "hide" : "show"], this.options.position),
                n = this.options.content,
                o = this.tooltip.css("width"),
                p = this.tooltip.is(":visible"),
                q = a || 1 === l.target.length,
                r = !c || l.target.length < 2 || e.target[0] === c.target;
            return (typeof a).search("boolean|number") && (a = !p), h = !g.is(":animated") && p === a && r, i = h ? F : !!this._trigger(k, [90]), this.destroyed ? this : (i !== E && a && this.focus(c), !i || h ? this : (d.attr(g[0], "aria-hidden", !a), a ? (this.mouse && (e.origin = d.event.fix(this.mouse)), d.isFunction(n.text) && this._updateContent(n.text, E), d.isFunction(n.title) && this._updateTitle(n.title, E), !C && "mouse" === m.target && m.adjust.mouse && (d(b).bind("mousemove." + S, this._storeMouse), C = D), o || g.css("width", g.outerWidth(E)), this.reposition(c, arguments[2]), o || g.css("width", ""), l.solo && ("string" == typeof l.solo ? d(l.solo) : d(W, l.solo)).not(g).not(l.target).qtip("hide", d.Event("tooltipsolo"))) : (clearTimeout(this.timers.show), delete e.origin, C && !d(W + '[tracking="true"]:visible', l.solo).not(g).length && (d(b).unbind("mousemove." + S), C = E), this.blur(c)), j = d.proxy(function() {
                a ? (db.ie && g[0].style.removeAttribute("filter"), g.css("overflow", ""), "string" == typeof l.autofocus && d(this.options.show.autofocus, g).focus(), this.options.show.target.trigger("qtip-" + this.id + "-inactive")) : g.css({
                    display: "",
                    visibility: "",
                    opacity: "",
                    left: "",
                    top: ""
                }), this._trigger(a ? "visible" : "hidden")
            }, this), l.effect === E || q === E ? (g[k](), j()) : d.isFunction(l.effect) ? (g.stop(1, 1), l.effect.call(g, this), g.queue("fx", function(a) {
                j(), a()
            })) : g.fadeTo(90, a ? 1 : 0, j), a && l.target.trigger("qtip-" + this.id + "-inactive"), this))
        }, z.show = function(a) {
            return this.toggle(D, a)
        }, z.hide = function(a) {
            return this.toggle(E, a)
        }, z.focus = function(a) {
            if (!this.rendered || this.destroyed) return this;
            var b = d(W),
                c = this.tooltip,
                e = parseInt(c[0].style.zIndex, 10),
                f = y.zindex + b.length;
            return c.hasClass($) || this._trigger("focus", [f], a) && (e !== f && (b.each(function() {
                this.style.zIndex > e && (this.style.zIndex = this.style.zIndex - 1)
            }), b.filter("." + $).qtip("blur", a)), c.addClass($)[0].style.zIndex = f), this
        }, z.blur = function(a) {
            return !this.rendered || this.destroyed ? this : (this.tooltip.removeClass($), this._trigger("blur", [this.tooltip.css("zIndex")], a), this)
        }, z.disable = function(a) {
            return this.destroyed ? this : ("toggle" === a ? a = !(this.rendered ? this.tooltip.hasClass(ab) : this.disabled) : "boolean" != typeof a && (a = D), this.rendered && this.tooltip.toggleClass(ab, a).attr("aria-disabled", a), this.disabled = !!a, this)
        }, z.enable = function() {
            return this.disable(E)
        }, z._createButton = function() {
            var a = this,
                b = this.elements,
                c = b.tooltip,
                e = this.options.content.button,
                f = "string" == typeof e,
                g = f ? e : "Close tooltip";
            b.button && b.button.remove(), b.button = e.jquery ? e : d("<a />", {
                "class": "qtip-close " + (this.options.style.widget ? "" : S + "-icon"),
                title: g,
                "aria-label": g
            }).prepend(d("<span />", {
                "class": "ui-icon ui-icon-close",
                html: "&times;"
            })), b.button.appendTo(b.titlebar || c).attr("role", "button").click(function(b) {
                return c.hasClass(ab) || a.hide(b), E
            })
        }, z._updateButton = function(a) {
            if (!this.rendered) return E;
            var b = this.elements.button;
            a ? this._createButton() : b.remove()
        }, z._setWidget = function() {
            var a = this.options.style.widget,
                b = this.elements,
                c = b.tooltip,
                d = c.hasClass(ab);
            c.removeClass(ab), ab = a ? "ui-state-disabled" : "qtip-disabled", c.toggleClass(ab, d), c.toggleClass("ui-helper-reset " + k(), a).toggleClass(Z, this.options.style.def && !a), b.content && b.content.toggleClass(k("content"), a), b.titlebar && b.titlebar.toggleClass(k("header"), a), b.button && b.button.toggleClass(S + "-icon", !a)
        }, z._storeMouse = function(a) {
            return (this.mouse = d.event.fix(a)).type = "mousemove", this
        }, z._bind = function(a, b, c, e, f) {
            if (a && c && b.length) {
                var g = "." + this._id + (e ? "-" + e : "");
                return d(a).bind((b.split ? b : b.join(g + " ")) + g, d.proxy(c, f || this)), this
            }
        }, z._unbind = function(a, b) {
            return a && d(a).unbind("." + this._id + (b ? "-" + b : "")), this
        }, z._trigger = function(a, b, c) {
            var e = d.Event("tooltip" + a);
            return e.originalEvent = c && d.extend({}, c) || this.cache.event || F, this.triggering = a, this.tooltip.trigger(e, [this].concat(b || [])), this.triggering = E, !e.isDefaultPrevented()
        }, z._bindEvents = function(a, b, c, e, f, g) {
            var h = c.filter(e).add(e.filter(c)),
                i = [];
            h.length && (d.each(b, function(b, c) {
                var e = d.inArray(c, a);
                e > -1 && i.push(a.splice(e, 1)[0])
            }), i.length && (this._bind(h, i, function(a) {
                var b = this.rendered ? this.tooltip[0].offsetWidth > 0 : !1;
                (b ? g : f).call(this, a)
            }), c = c.not(h), e = e.not(h))), this._bind(c, a, f), this._bind(e, b, g)
        }, z._assignInitialEvents = function(a) {
            function b(a) {
                return this.disabled || this.destroyed ? E : (this.cache.event = a && d.event.fix(a), this.cache.target = a && d(a.target), clearTimeout(this.timers.show), void(this.timers.show = l.call(this, function() {
                    this.render("object" == typeof a || c.show.ready)
                }, c.prerender ? 0 : c.show.delay)))
            }
            var c = this.options,
                e = c.show.target,
                f = c.hide.target,
                g = c.show.event ? d.trim("" + c.show.event).split(" ") : [],
                h = c.hide.event ? d.trim("" + c.hide.event).split(" ") : [];
            this._bind(this.elements.target, ["remove", "removeqtip"], function() {
                this.destroy(!0)
            }, "destroy"), /mouse(over|enter)/i.test(c.show.event) && !/mouse(out|leave)/i.test(c.hide.event) && h.push("mouseleave"), this._bind(e, "mousemove", function(a) {
                this._storeMouse(a), this.cache.onTarget = D
            }), this._bindEvents(g, h, e, f, b, function() {
                return this.timers ? void clearTimeout(this.timers.show) : E
            }), (c.show.ready || c.prerender) && b.call(this, a)
        }, z._assignEvents = function() {
            var c = this,
                e = this.options,
                f = e.position,
                g = this.tooltip,
                h = e.show.target,
                i = e.hide.target,
                j = f.container,
                k = f.viewport,
                l = d(b),
                q = (d(b.body), d(a)),
                r = e.show.event ? d.trim("" + e.show.event).split(" ") : [],
                s = e.hide.event ? d.trim("" + e.hide.event).split(" ") : [];
            d.each(e.events, function(a, b) {
                c._bind(g, "toggle" === a ? ["tooltipshow", "tooltiphide"] : ["tooltip" + a], b, null, g)
            }), /mouse(out|leave)/i.test(e.hide.event) && "window" === e.hide.leave && this._bind(l, ["mouseout", "blur"], function(a) {
                /select|option/.test(a.target.nodeName) || a.relatedTarget || this.hide(a)
            }), e.hide.fixed ? i = i.add(g.addClass(Y)) : /mouse(over|enter)/i.test(e.show.event) && this._bind(i, "mouseleave", function() {
                clearTimeout(this.timers.show)
            }), ("" + e.hide.event).indexOf("unfocus") > -1 && this._bind(j.closest("html"), ["mousedown", "touchstart"], function(a) {
                var b = d(a.target),
                    c = this.rendered && !this.tooltip.hasClass(ab) && this.tooltip[0].offsetWidth > 0,
                    e = b.parents(W).filter(this.tooltip[0]).length > 0;
                b[0] === this.target[0] || b[0] === this.tooltip[0] || e || this.target.has(b[0]).length || !c || this.hide(a)
            }), "number" == typeof e.hide.inactive && (this._bind(h, "qtip-" + this.id + "-inactive", o, "inactive"), this._bind(i.add(g), y.inactiveEvents, o)), this._bindEvents(r, s, h, i, m, n), this._bind(h.add(g), "mousemove", function(a) {
                if ("number" == typeof e.hide.distance) {
                    var b = this.cache.origin || {},
                        c = this.options.hide.distance,
                        d = Math.abs;
                    (d(a.pageX - b.pageX) >= c || d(a.pageY - b.pageY) >= c) && this.hide(a)
                }
                this._storeMouse(a)
            }), "mouse" === f.target && f.adjust.mouse && (e.hide.event && this._bind(h, ["mouseenter", "mouseleave"], function(a) {
                return this.cache ? void(this.cache.onTarget = "mouseenter" === a.type) : E
            }), this._bind(l, "mousemove", function(a) {
                this.rendered && this.cache.onTarget && !this.tooltip.hasClass(ab) && this.tooltip[0].offsetWidth > 0 && this.reposition(a)
            })), (f.adjust.resize || k.length) && this._bind(d.event.special.resize ? k : q, "resize", p), f.adjust.scroll && this._bind(q.add(f.container), "scroll", p)
        }, z._unassignEvents = function() {
            var c = this.options,
                e = c.show.target,
                f = c.hide.target,
                g = d.grep([this.elements.target[0], this.rendered && this.tooltip[0], c.position.container[0], c.position.viewport[0], c.position.container.closest("html")[0], a, b], function(a) {
                    return "object" == typeof a
                });
            e && e.toArray && (g = g.concat(e.toArray())), f && f.toArray && (g = g.concat(f.toArray())), this._unbind(g)._unbind(g, "destroy")._unbind(g, "inactive")
        }, d(function() {
            q(W, ["mouseenter", "mouseleave"], function(a) {
                var b = "mouseenter" === a.type,
                    c = d(a.currentTarget),
                    e = d(a.relatedTarget || a.target),
                    f = this.options;
                b ? (this.focus(a), c.hasClass(Y) && !c.hasClass(ab) && clearTimeout(this.timers.hide)) : "mouse" === f.position.target && f.position.adjust.mouse && f.hide.event && f.show.target && !e.closest(f.show.target[0]).length && this.hide(a), c.toggleClass(_, b)
            }), q("[" + U + "]", X, o)
        }), y = d.fn.qtip = function(a, b, e) {
            var f = ("" + a).toLowerCase(),
                g = F,
                i = d.makeArray(arguments).slice(1),
                j = i[i.length - 1],
                k = this[0] ? d.data(this[0], S) : F;
            return !arguments.length && k || "api" === f ? k : "string" == typeof a ? (this.each(function() {
                var a = d.data(this, S);
                if (!a) return D;
                if (j && j.timeStamp && (a.cache.event = j), !b || "option" !== f && "options" !== f) a[f] && a[f].apply(a, i);
                else {
                    if (e === c && !d.isPlainObject(b)) return g = a.get(b), E;
                    a.set(b, e)
                }
            }), g !== F ? g : this) : "object" != typeof a && arguments.length ? void 0 : (k = h(d.extend(D, {}, a)), this.each(function(a) {
                var b, c;
                return c = d.isArray(k.id) ? k.id[a] : k.id, c = !c || c === E || c.length < 1 || y.api[c] ? y.nextid++ : c, b = r(d(this), c, k), b === E ? D : (y.api[c] = b, d.each(R, function() {
                    "initialize" === this.initialize && this(b)
                }), void b._assignInitialEvents(j))
            }))
        }, d.qtip = e, y.api = {}, d.each({
            attr: function(a, b) {
                if (this.length) {
                    var c = this[0],
                        e = "title",
                        f = d.data(c, "qtip");
                    if (a === e && f && "object" == typeof f && f.options.suppress) return arguments.length < 2 ? d.attr(c, cb) : (f && f.options.content.attr === e && f.cache.attr && f.set("content.text", b), this.attr(cb, b))
                }
                return d.fn["attr" + bb].apply(this, arguments)
            },
            clone: function(a) {
                var b = (d([]), d.fn["clone" + bb].apply(this, arguments));
                return a || b.filter("[" + cb + "]").attr("title", function() {
                    return d.attr(this, cb)
                }).removeAttr(cb), b
            }
        }, function(a, b) {
            if (!b || d.fn[a + bb]) return D;
            var c = d.fn[a + bb] = d.fn[a];
            d.fn[a] = function() {
                return b.apply(this, arguments) || c.apply(this, arguments)
            }
        }), d.ui || (d["cleanData" + bb] = d.cleanData, d.cleanData = function(a) {
            for (var b, c = 0;
                (b = d(a[c])).length; c++)
                if (b.attr(T)) try {
                    b.triggerHandler("removeqtip")
                } catch (e) {}
                d["cleanData" + bb].apply(this, arguments)
        }), y.version = "2.2.1-29-", y.nextid = 0, y.inactiveEvents = X, y.zindex = 15e3, y.defaults = {
            prerender: E,
            id: E,
            overwrite: D,
            suppress: D,
            content: {
                text: D,
                attr: "title",
                title: E,
                button: E
            },
            position: {
                my: "top left",
                at: "bottom right",
                target: E,
                container: E,
                viewport: E,
                adjust: {
                    x: 0,
                    y: 0,
                    mouse: D,
                    scroll: D,
                    resize: D,
                    method: "flipinvert flipinvert"
                },
                effect: function(a, b) {
                    d(this).animate(b, {
                        duration: 200,
                        queue: E
                    })
                }
            },
            show: {
                target: E,
                event: "mouseenter",
                effect: D,
                delay: 90,
                solo: E,
                ready: E,
                autofocus: E
            },
            hide: {
                target: E,
                event: "mouseleave",
                effect: D,
                delay: 0,
                fixed: E,
                inactive: E,
                leave: "window",
                distance: E
            },
            style: {
                classes: "",
                widget: E,
                width: E,
                height: E,
                def: D
            },
            events: {
                render: F,
                move: F,
                show: F,
                hide: F,
                toggle: F,
                visible: F,
                hidden: F,
                focus: F,
                blur: F
            }
        };
        var hb, ib = "margin",
            jb = "border",
            kb = "color",
            lb = "background-color",
            mb = "transparent",
            nb = " !important",
            ob = !!b.createElement("canvas").getContext,
            pb = /rgba?\(0, 0, 0(, 0)?\)|transparent|#123456/i,
            qb = {},
            rb = ["Webkit", "O", "Moz", "ms"];
        if (ob) var sb = a.devicePixelRatio || 1,
            tb = function() {
                var a = b.createElement("canvas").getContext("2d");
                return a.backingStorePixelRatio || a.webkitBackingStorePixelRatio || a.mozBackingStorePixelRatio || a.msBackingStorePixelRatio || a.oBackingStorePixelRatio || 1
            }(),
            ub = sb / tb;
        else var vb = function(a, b, c) {
            return "<qtipvml:" + a + ' xmlns="urn:schemas-microsoft.com:vml" class="qtip-vml" ' + (b || "") + ' style="behavior: url(#default#VML); ' + (c || "") + '" />'
        };
        d.extend(v.prototype, {
            init: function(a) {
                var b, c;
                c = this.element = a.elements.tip = d("<div />", {
                    "class": S + "-tip"
                }).prependTo(a.tooltip), ob ? (b = d("<canvas />").appendTo(this.element)[0].getContext("2d"), b.lineJoin = "miter", b.miterLimit = 1e5, b.save()) : (b = vb("shape", 'coordorigin="0,0"', "position:absolute;"), this.element.html(b + b), a._bind(d("*", c).add(c), ["click", "mousedown"], function(a) {
                    a.stopPropagation()
                }, this._ns)), a._bind(a.tooltip, "tooltipmove", this.reposition, this._ns, this), this.create()
            },
            _swapDimensions: function() {
                this.size[0] = this.options.height, this.size[1] = this.options.width
            },
            _resetDimensions: function() {
                this.size[0] = this.options.width, this.size[1] = this.options.height
            },
            _useTitle: function(a) {
                var b = this.qtip.elements.titlebar;
                return b && (a.y === K || a.y === O && this.element.position().top + this.size[1] / 2 + this.options.offset < b.outerHeight(D))
            },
            _parseCorner: function(a) {
                var b = this.qtip.options.position.my;
                return a === E || b === E ? a = E : a === D ? a = new A(b.string()) : a.string || (a = new A(a), a.fixed = D), a
            },
            _parseWidth: function(a, b, c) {
                var d = this.qtip.elements,
                    e = jb + s(b) + "Width";
                return (c ? u(c, e) : u(d.content, e) || u(this._useTitle(a) && d.titlebar || d.content, e) || u(d.tooltip, e)) || 0
            },
            _parseRadius: function(a) {
                var b = this.qtip.elements,
                    c = jb + s(a.y) + s(a.x) + "Radius";
                return db.ie < 9 ? 0 : u(this._useTitle(a) && b.titlebar || b.content, c) || u(b.tooltip, c) || 0
            },
            _invalidColour: function(a, b, c) {
                var d = a.css(b);
                return !d || c && d === a.css(c) || pb.test(d) ? E : d
            },
            _parseColours: function(a) {
                var b = this.qtip.elements,
                    c = this.element.css("cssText", ""),
                    e = jb + s(a[a.precedance]) + s(kb),
                    f = this._useTitle(a) && b.titlebar || b.content,
                    g = this._invalidColour,
                    h = [];
                return h[0] = g(c, lb) || g(f, lb) || g(b.content, lb) || g(b.tooltip, lb) || c.css(lb), h[1] = g(c, e, kb) || g(f, e, kb) || g(b.content, e, kb) || g(b.tooltip, e, kb) || b.tooltip.css(e), d("*", c).add(c).css("cssText", lb + ":" + mb + nb + ";" + jb + ":0" + nb + ";"), h
            },
            _calculateSize: function(a) {
                var b, c, d, e = a.precedance === H,
                    f = this.options.width,
                    g = this.options.height,
                    h = "c" === a.abbrev(),
                    i = (e ? f : g) * (h ? .5 : 1),
                    j = Math.pow,
                    k = Math.round,
                    l = Math.sqrt(j(i, 2) + j(g, 2)),
                    m = [this.border / i * l, this.border / g * l];
                return m[2] = Math.sqrt(j(m[0], 2) - j(this.border, 2)), m[3] = Math.sqrt(j(m[1], 2) - j(this.border, 2)), b = l + m[2] + m[3] + (h ? 0 : m[0]), c = b / l, d = [k(c * f), k(c * g)], e ? d : d.reverse()
            },
            _calculateTip: function(a, b, c) {
                c = c || 1, b = b || this.size;
                var d = b[0] * c,
                    e = b[1] * c,
                    f = Math.ceil(d / 2),
                    g = Math.ceil(e / 2),
                    h = {
                        br: [0, 0, d, e, d, 0],
                        bl: [0, 0, d, 0, 0, e],
                        tr: [0, e, d, 0, d, e],
                        tl: [0, 0, 0, e, d, e],
                        tc: [0, e, f, 0, d, e],
                        bc: [0, 0, d, 0, f, e],
                        rc: [0, 0, d, g, 0, e],
                        lc: [d, 0, d, e, 0, g]
                    };
                return h.lt = h.br, h.rt = h.bl, h.lb = h.tr, h.rb = h.tl, h[a.abbrev()]
            },
            _drawCoords: function(a, b) {
                a.beginPath(), a.moveTo(b[0], b[1]), a.lineTo(b[2], b[3]), a.lineTo(b[4], b[5]), a.closePath()
            },
            create: function() {
                var a = this.corner = (ob || db.ie) && this._parseCorner(this.options.corner);
                return (this.enabled = !!this.corner && "c" !== this.corner.abbrev()) && (this.qtip.cache.corner = a.clone(), this.update()), this.element.toggle(this.enabled), this.corner
            },
            update: function(b, c) {
                if (!this.enabled) return this;
                var e, f, g, h, i, j, k, l, m = this.qtip.elements,
                    n = this.element,
                    o = n.children(),
                    p = this.options,
                    q = this.size,
                    r = p.mimic,
                    s = Math.round;
                b || (b = this.qtip.cache.corner || this.corner), r === E ? r = b : (r = new A(r), r.precedance = b.precedance, "inherit" === r.x ? r.x = b.x : "inherit" === r.y ? r.y = b.y : r.x === r.y && (r[b.precedance] = b[b.precedance])), f = r.precedance, b.precedance === G ? this._swapDimensions() : this._resetDimensions(), e = this.color = this._parseColours(b), e[1] !== mb ? (l = this.border = this._parseWidth(b, b[b.precedance]), p.border && 1 > l && !pb.test(e[1]) && (e[0] = e[1]), this.border = l = p.border !== D ? p.border : l) : this.border = l = 0, k = this.size = this._calculateSize(b), n.css({
                    width: k[0],
                    height: k[1],
                    lineHeight: k[1] + "px"
                }), j = b.precedance === H ? [s(r.x === L ? l : r.x === N ? k[0] - q[0] - l : (k[0] - q[0]) / 2), s(r.y === K ? k[1] - q[1] : 0)] : [s(r.x === L ? k[0] - q[0] : 0), s(r.y === K ? l : r.y === M ? k[1] - q[1] - l : (k[1] - q[1]) / 2)], ob ? (g = o[0].getContext("2d"), g.restore(), g.save(), g.clearRect(0, 0, 6e3, 6e3), h = this._calculateTip(r, q, ub), i = this._calculateTip(r, this.size, ub), o.attr(I, k[0] * ub).attr(J, k[1] * ub), o.css(I, k[0]).css(J, k[1]), this._drawCoords(g, i), g.fillStyle = e[1], g.fill(), g.translate(j[0] * ub, j[1] * ub), this._drawCoords(g, h), g.fillStyle = e[0], g.fill()) : (h = this._calculateTip(r), h = "m" + h[0] + "," + h[1] + " l" + h[2] + "," + h[3] + " " + h[4] + "," + h[5] + " xe", j[2] = l && /^(r|b)/i.test(b.string()) ? 8 === db.ie ? 2 : 1 : 0, o.css({
                    coordsize: k[0] + l + " " + (k[1] + l),
                    antialias: "" + (r.string().indexOf(O) > -1),
                    left: j[0] - j[2] * Number(f === G),
                    top: j[1] - j[2] * Number(f === H),
                    width: k[0] + l,
                    height: k[1] + l
                }).each(function(a) {
                    var b = d(this);
                    b[b.prop ? "prop" : "attr"]({
                        coordsize: k[0] + l + " " + (k[1] + l),
                        path: h,
                        fillcolor: e[0],
                        filled: !!a,
                        stroked: !a
                    }).toggle(!(!l && !a)), !a && b.html(vb("stroke", 'weight="' + 2 * l + 'px" color="' + e[1] + '" miterlimit="1000" joinstyle="miter"'))
                })), a.opera && setTimeout(function() {
                    m.tip.css({
                        display: "inline-block",
                        visibility: "visible"
                    })
                }, 1), c !== E && this.calculate(b, k)
            },
            calculate: function(a, b) {
                if (!this.enabled) return E;
                var c, e, f = this,
                    g = this.qtip.elements,
                    h = this.element,
                    i = this.options.offset,
                    j = (g.tooltip.hasClass("ui-widget"), {});
                return a = a || this.corner, c = a.precedance, b = b || this._calculateSize(a), e = [a.x, a.y], c === G && e.reverse(), d.each(e, function(d, e) {
                    var h, k, l;
                    e === O ? (h = c === H ? L : K, j[h] = "50%", j[ib + "-" + h] = -Math.round(b[c === H ? 0 : 1] / 2) + i) : (h = f._parseWidth(a, e, g.tooltip), k = f._parseWidth(a, e, g.content), l = f._parseRadius(a), j[e] = Math.max(-f.border, d ? k : i + (l > h ? l : -h)))
                }), j[a[c]] -= b[c === G ? 0 : 1], h.css({
                    margin: "",
                    top: "",
                    bottom: "",
                    left: "",
                    right: ""
                }).css(j), j
            },
            reposition: function(a, b, d) {
                function e(a, b, c, d, e) {
                    a === Q && j.precedance === b && k[d] && j[c] !== O ? j.precedance = j.precedance === G ? H : G : a !== Q && k[d] && (j[b] = j[b] === O ? k[d] > 0 ? d : e : j[b] === d ? e : d)
                }

                function f(a, b, e) {
                    j[a] === O ? p[ib + "-" + b] = o[a] = g[ib + "-" + b] - k[b] : (h = g[e] !== c ? [k[b], -g[b]] : [-k[b], g[b]], (o[a] = Math.max(h[0], h[1])) > h[0] && (d[b] -= k[b], o[b] = E), p[g[e] !== c ? e : b] = o[a])
                }
                if (this.enabled) {
                    var g, h, i = b.cache,
                        j = this.corner.clone(),
                        k = d.adjusted,
                        l = b.options.position.adjust.method.split(" "),
                        m = l[0],
                        n = l[1] || l[0],
                        o = {
                            left: E,
                            top: E,
                            x: 0,
                            y: 0
                        },
                        p = {};
                    this.corner.fixed !== D && (e(m, G, H, L, N), e(n, H, G, K, M), (j.string() !== i.corner.string() || i.cornerTop !== k.top || i.cornerLeft !== k.left) && this.update(j, E)), g = this.calculate(j), g.right !== c && (g.left = -g.right), g.bottom !== c && (g.top = -g.bottom), g.user = this.offset, (o.left = m === Q && !!k.left) && f(G, L, N), (o.top = n === Q && !!k.top) && f(H, K, M), this.element.css(p).toggle(!(o.x && o.y || j.x === O && o.y || j.y === O && o.x)), d.left -= g.left.charAt ? g.user : m !== Q || o.top || !o.left && !o.top ? g.left + this.border : 0, d.top -= g.top.charAt ? g.user : n !== Q || o.left || !o.left && !o.top ? g.top + this.border : 0, i.cornerLeft = k.left, i.cornerTop = k.top, i.corner = j.clone()
                }
            },
            destroy: function() {
                this.qtip._unbind(this.qtip.tooltip, this._ns), this.qtip.elements.tip && this.qtip.elements.tip.find("*").remove().end().remove()
            }
        }), hb = R.tip = function(a) {
            return new v(a, a.options.style.tip)
        }, hb.initialize = "render", hb.sanitize = function(a) {
            if (a.style && "tip" in a.style) {
                var b = a.style.tip;
                "object" != typeof b && (b = a.style.tip = {
                    corner: b
                }), /string|boolean/i.test(typeof b.corner) || (b.corner = D)
            }
        }, B.tip = {
            "^position.my|style.tip.(corner|mimic|border)$": function() {
                this.create(), this.qtip.reposition()
            },
            "^style.tip.(height|width)$": function(a) {
                this.size = [a.width, a.height], this.update(), this.qtip.reposition()
            },
            "^content.title|style.(classes|widget)$": function() {
                this.update()
            }
        }, d.extend(D, y.defaults, {
            style: {
                tip: {
                    corner: D,
                    mimic: E,
                    width: 6,
                    height: 6,
                    border: D,
                    offset: 0
                }
            }
        });
        var wb, xb, yb = "qtip-modal",
            zb = "." + yb;
        xb = function() {
            function a(a) {
                if (d.expr[":"].focusable) return d.expr[":"].focusable;
                var b, c, e, f = !isNaN(d.attr(a, "tabindex")),
                    g = a.nodeName && a.nodeName.toLowerCase();
                return "area" === g ? (b = a.parentNode, c = b.name, a.href && c && "map" === b.nodeName.toLowerCase() ? (e = d("img[usemap=#" + c + "]")[0], !!e && e.is(":visible")) : !1) : /input|select|textarea|button|object/.test(g) ? !a.disabled : "a" === g ? a.href || f : f
            }

            function c(a) {
                k.length < 1 && a.length ? a.not("body").blur() : k.first().focus()
            }

            function e(a) {
                if (i.is(":visible")) {
                    var b, e = d(a.target),
                        h = f.tooltip,
                        j = e.closest(W);
                    b = j.length < 1 ? E : parseInt(j[0].style.zIndex, 10) > parseInt(h[0].style.zIndex, 10), b || e.closest(W)[0] === h[0] || c(e), g = a.target === k[k.length - 1]
                }
            }
            var f, g, h, i, j = this,
                k = {};
            d.extend(j, {
                init: function() {
                    return i = j.elem = d("<div />", {
                        id: "qtip-overlay",
                        html: "<div></div>",
                        mousedown: function() {
                            return E
                        }
                    }).hide(), d(b.body).bind("focusin" + zb, e), d(b).bind("keydown" + zb, function(a) {
                        f && f.options.show.modal.escape && 27 === a.keyCode && f.hide(a)
                    }), i.bind("click" + zb, function(a) {
                        f && f.options.show.modal.blur && f.hide(a)
                    }), j
                },
                update: function(b) {
                    f = b, k = b.options.show.modal.stealfocus !== E ? b.tooltip.find("*").filter(function() {
                        return a(this)
                    }) : []
                },
                toggle: function(a, e, g) {
                    var k = (d(b.body), a.tooltip),
                        l = a.options.show.modal,
                        m = l.effect,
                        n = e ? "show" : "hide",
                        o = i.is(":visible"),
                        p = d(zb).filter(":visible:not(:animated)").not(k);
                    return j.update(a), e && l.stealfocus !== E && c(d(":focus")), i.toggleClass("blurs", l.blur), e && i.appendTo(b.body), i.is(":animated") && o === e && h !== E || !e && p.length ? j : (i.stop(D, E), d.isFunction(m) ? m.call(i, e) : m === E ? i[n]() : i.fadeTo(parseInt(g, 10) || 90, e ? 1 : 0, function() {
                        e || i.hide()
                    }), e || i.queue(function(a) {
                        i.css({
                            left: "",
                            top: ""
                        }), d(zb).length || i.detach(), a()
                    }), h = e, f.destroyed && (f = F), j)
                }
            }), j.init()
        }, xb = new xb, d.extend(w.prototype, {
            init: function(a) {
                var b = a.tooltip;
                return this.options.on ? (a.elements.overlay = xb.elem, b.addClass(yb).css("z-index", y.modal_zindex + d(zb).length), a._bind(b, ["tooltipshow", "tooltiphide"], function(a, c, e) {
                    var f = a.originalEvent;
                    if (a.target === b[0])
                        if (f && "tooltiphide" === a.type && /mouse(leave|enter)/.test(f.type) && d(f.relatedTarget).closest(xb.elem[0]).length) try {
                            a.preventDefault()
                        } catch (g) {} else(!f || f && "tooltipsolo" !== f.type) && this.toggle(a, "tooltipshow" === a.type, e)
                }, this._ns, this), a._bind(b, "tooltipfocus", function(a, c) {
                    if (!a.isDefaultPrevented() && a.target === b[0]) {
                        var e = d(zb),
                            f = y.modal_zindex + e.length,
                            g = parseInt(b[0].style.zIndex, 10);
                        xb.elem[0].style.zIndex = f - 1, e.each(function() {
                            this.style.zIndex > g && (this.style.zIndex -= 1)
                        }), e.filter("." + $).qtip("blur", a.originalEvent), b.addClass($)[0].style.zIndex = f, xb.update(c);
                        try {
                            a.preventDefault()
                        } catch (h) {}
                    }
                }, this._ns, this), void a._bind(b, "tooltiphide", function(a) {
                    a.target === b[0] && d(zb).filter(":visible").not(b).last().qtip("focus", a)
                }, this._ns, this)) : this
            },
            toggle: function(a, b, c) {
                return a && a.isDefaultPrevented() ? this : void xb.toggle(this.qtip, !!b, c)
            },
            destroy: function() {
                this.qtip.tooltip.removeClass(yb), this.qtip._unbind(this.qtip.tooltip, this._ns), xb.toggle(this.qtip, E), delete this.qtip.elements.overlay
            }
        }), wb = R.modal = function(a) {
            return new w(a, a.options.show.modal)
        }, wb.sanitize = function(a) {
            a.show && ("object" != typeof a.show.modal ? a.show.modal = {
                on: !!a.show.modal
            } : "undefined" == typeof a.show.modal.on && (a.show.modal.on = D))
        }, y.modal_zindex = y.zindex - 200, wb.initialize = "render", B.modal = {
            "^show.modal.(on|blur)$": function() {
                this.destroy(), this.init(), this.qtip.elems.overlay.toggle(this.qtip.tooltip[0].offsetWidth > 0)
            }
        }, d.extend(D, y.defaults, {
            show: {
                modal: {
                    on: E,
                    effect: D,
                    blur: D,
                    stealfocus: D,
                    escape: D
                }
            }
        }), R.viewport = function(c, d, e, f, g, h, i) {
            function j(a, b, c, e, f, g, h, i, j) {
                var k = d[f],
                    s = u[a],
                    t = v[a],
                    w = c === Q,
                    x = s === f ? j : s === g ? -j : -j / 2,
                    y = t === f ? i : t === g ? -i : -i / 2,
                    z = q[f] + r[f] - (n ? 0 : m[f]),
                    A = z - k,
                    B = k + j - (h === I ? o : p) - z,
                    C = x - (u.precedance === a || s === u[b] ? y : 0) - (t === O ? i / 2 : 0);
                return w ? (C = (s === f ? 1 : -1) * x, d[f] += A > 0 ? A : B > 0 ? -B : 0, d[f] = Math.max(-m[f] + r[f], k - C, Math.min(Math.max(-m[f] + r[f] + (h === I ? o : p), k + C), d[f], "center" === s ? k - x : 1e9))) : (e *= c === P ? 2 : 0, A > 0 && (s !== f || B > 0) ? (d[f] -= C + e, l.invert(a, f)) : B > 0 && (s !== g || A > 0) && (d[f] -= (s === O ? -C : C) + e, l.invert(a, g)), d[f] < q && -d[f] > B && (d[f] = k, l = u.clone())), d[f] - k
            }
            var k, l, m, n, o, p, q, r, s = e.target,
                t = c.elements.tooltip,
                u = e.my,
                v = e.at,
                w = e.adjust,
                x = w.method.split(" "),
                y = x[0],
                z = x[1] || x[0],
                A = e.viewport,
                B = e.container,
                C = (c.cache, {
                    left: 0,
                    top: 0
                });
            return A.jquery && s[0] !== a && s[0] !== b.body && "none" !== w.method ? (m = B.offset() || C, n = "static" === B.css("position"), k = "fixed" === t.css("position"), o = A[0] === a ? A.width() : A.outerWidth(E), p = A[0] === a ? A.height() : A.outerHeight(E), q = {
                left: k ? 0 : A.scrollLeft(),
                top: k ? 0 : A.scrollTop()
            }, r = A.offset() || C, ("shift" !== y || "shift" !== z) && (l = u.clone()), C = {
                left: "none" !== y ? j(G, H, y, w.x, L, N, I, f, h) : 0,
                top: "none" !== z ? j(H, G, z, w.y, K, M, J, g, i) : 0,
                my: l
            }) : C
        }, R.polys = {
            polygon: function(a, b) {
                var c, d, e, f = {
                        width: 0,
                        height: 0,
                        position: {
                            top: 1e10,
                            right: 0,
                            bottom: 0,
                            left: 1e10
                        },
                        adjustable: E
                    },
                    g = 0,
                    h = [],
                    i = 1,
                    j = 1,
                    k = 0,
                    l = 0;
                for (g = a.length; g--;) c = [parseInt(a[--g], 10), parseInt(a[g + 1], 10)], c[0] > f.position.right && (f.position.right = c[0]), c[0] < f.position.left && (f.position.left = c[0]), c[1] > f.position.bottom && (f.position.bottom = c[1]), c[1] < f.position.top && (f.position.top = c[1]), h.push(c);
                if (d = f.width = Math.abs(f.position.right - f.position.left), e = f.height = Math.abs(f.position.bottom - f.position.top), "c" === b.abbrev()) f.position = {
                    left: f.position.left + f.width / 2,
                    top: f.position.top + f.height / 2
                };
                else {
                    for (; d > 0 && e > 0 && i > 0 && j > 0;)
                        for (d = Math.floor(d / 2), e = Math.floor(e / 2), b.x === L ? i = d : b.x === N ? i = f.width - d : i += Math.floor(d / 2), b.y === K ? j = e : b.y === M ? j = f.height - e : j += Math.floor(e / 2), g = h.length; g-- && !(h.length < 2);) k = h[g][0] - f.position.left, l = h[g][1] - f.position.top, (b.x === L && k >= i || b.x === N && i >= k || b.x === O && (i > k || k > f.width - i) || b.y === K && l >= j || b.y === M && j >= l || b.y === O && (j > l || l > f.height - j)) && h.splice(g, 1);
                    f.position = {
                        left: h[0][0],
                        top: h[0][1]
                    }
                }
                return f
            },
            rect: function(a, b, c, d) {
                return {
                    width: Math.abs(c - a),
                    height: Math.abs(d - b),
                    position: {
                        left: Math.min(a, c),
                        top: Math.min(b, d)
                    }
                }
            },
            _angles: {
                tc: 1.5,
                tr: 7 / 4,
                tl: 5 / 4,
                bc: .5,
                br: .25,
                bl: .75,
                rc: 2,
                lc: 1,
                c: 0
            },
            ellipse: function(a, b, c, d, e) {
                var f = R.polys._angles[e.abbrev()],
                    g = 0 === f ? 0 : c * Math.cos(f * Math.PI),
                    h = d * Math.sin(f * Math.PI);
                return {
                    width: 2 * c - Math.abs(g),
                    height: 2 * d - Math.abs(h),
                    position: {
                        left: a + g,
                        top: b + h
                    },
                    adjustable: E
                }
            },
            circle: function(a, b, c, d) {
                return R.polys.ellipse(a, b, c, c, d)
            }
        }, R.svg = function(a, c, e) {
            for (var f, g, h, i, j, k, l, m, n, o = (d(b), c[0]), p = d(o.ownerSVGElement), q = o.ownerDocument, r = (parseInt(c.css("stroke-width"), 10) || 0) / 2; !o.getBBox;) o = o.parentNode;
            if (!o.getBBox || !o.parentNode) return E;
            switch (o.nodeName) {
                case "ellipse":
                case "circle":
                    m = R.polys.ellipse(o.cx.baseVal.value, o.cy.baseVal.value, (o.rx || o.r).baseVal.value + r, (o.ry || o.r).baseVal.value + r, e);
                    break;
                case "line":
                case "polygon":
                case "polyline":
                    for (l = o.points || [{
                            x: o.x1.baseVal.value,
                            y: o.y1.baseVal.value
                        }, {
                            x: o.x2.baseVal.value,
                            y: o.y2.baseVal.value
                        }], m = [], k = -1, i = l.numberOfItems || l.length; ++k < i;) j = l.getItem ? l.getItem(k) : l[k], m.push.apply(m, [j.x, j.y]);
                    m = R.polys.polygon(m, e);
                    break;
                default:
                    m = o.getBBox(), m = {
                        width: m.width,
                        height: m.height,
                        position: {
                            left: m.x,
                            top: m.y
                        }
                    }
            }
            return n = m.position, p = p[0], p.createSVGPoint && (g = o.getScreenCTM(), l = p.createSVGPoint(), l.x = n.left, l.y = n.top, h = l.matrixTransform(g), n.left = h.x, n.top = h.y), q !== b && "mouse" !== a.position.target && (f = d((q.defaultView || q.parentWindow).frameElement).offset(), f && (n.left += f.left, n.top += f.top)), q = d(q), n.left += q.scrollLeft(), n.top += q.scrollTop(), m
        }, R.imagemap = function(a, b, c) {
            b.jquery || (b = d(b));
            var e, f, g, h, i, j = (b.attr("shape") || "rect").toLowerCase().replace("poly", "polygon"),
                k = d('img[usemap="#' + b.parent("map").attr("name") + '"]'),
                l = d.trim(b.attr("coords")),
                m = l.replace(/,$/, "").split(",");
            if (!k.length) return E;
            if ("polygon" === j) h = R.polys.polygon(m, c);
            else {
                if (!R.polys[j]) return E;
                for (g = -1, i = m.length, f = []; ++g < i;) f.push(parseInt(m[g], 10));
                h = R.polys[j].apply(this, f.concat(c))
            }
            return e = k.offset(), e.left += Math.ceil((k.outerWidth(E) - k.width()) / 2), e.top += Math.ceil((k.outerHeight(E) - k.height()) / 2), h.position.left += e.left, h.position.top += e.top, h
        };
        var Ab, Bb = '<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:\'\';"  style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=0); -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";"></iframe>';
        d.extend(x.prototype, {
            _scroll: function() {
                var b = this.qtip.elements.overlay;
                b && (b[0].style.top = d(a).scrollTop() + "px")
            },
            init: function(c) {
                var e = c.tooltip;
                d("select, object").length < 1 && (this.bgiframe = c.elements.bgiframe = d(Bb).appendTo(e), c._bind(e, "tooltipmove", this.adjustBGIFrame, this._ns, this)), this.redrawContainer = d("<div/>", {
                    id: S + "-rcontainer"
                }).appendTo(b.body), c.elements.overlay && c.elements.overlay.addClass("qtipmodal-ie6fix") && (c._bind(a, ["scroll", "resize"], this._scroll, this._ns, this), c._bind(e, ["tooltipshow"], this._scroll, this._ns, this)), this.redraw()
            },
            adjustBGIFrame: function() {
                var a, b, c = this.qtip.tooltip,
                    d = {
                        height: c.outerHeight(E),
                        width: c.outerWidth(E)
                    },
                    e = this.qtip.plugins.tip,
                    f = this.qtip.elements.tip;
                b = parseInt(c.css("borderLeftWidth"), 10) || 0, b = {
                    left: -b,
                    top: -b
                }, e && f && (a = "x" === e.corner.precedance ? [I, L] : [J, K], b[a[1]] -= f[a[0]]()), this.bgiframe.css(b).css(d)
            },
            redraw: function() {
                if (this.qtip.rendered < 1 || this.drawing) return this;
                var a, b, c, d, e = this.qtip.tooltip,
                    f = this.qtip.options.style,
                    g = this.qtip.options.position.container;
                return this.qtip.drawing = 1, f.height && e.css(J, f.height), f.width ? e.css(I, f.width) : (e.css(I, "").appendTo(this.redrawContainer), b = e.width(), 1 > b % 2 && (b += 1), c = e.css("maxWidth") || "", d = e.css("minWidth") || "", a = (c + d).indexOf("%") > -1 ? g.width() / 100 : 0, c = (c.indexOf("%") > -1 ? a : 1) * parseInt(c, 10) || b, d = (d.indexOf("%") > -1 ? a : 1) * parseInt(d, 10) || 0, b = c + d ? Math.min(Math.max(b, d), c) : b, e.css(I, Math.round(b)).appendTo(g)), this.drawing = 0, this
            },
            destroy: function() {
                this.bgiframe && this.bgiframe.remove(), this.qtip._unbind([a, this.qtip.tooltip], this._ns)
            }
        }), Ab = R.ie6 = function(a) {
            return 6 === db.ie ? new x(a) : E
        }, Ab.initialize = "render", B.ie6 = {
            "^content|style$": function() {
                this.redraw()
            }
        }
    })
}(window, document);
//# sourceMappingURL=jquery.qtip.min.map
/* ..\..\desktop.blocks\qtip\qtip.js end */

/* ..\..\desktop.blocks\b-dropdown\_catalog-menu\b-dropdown_catalog-menu.js begin */
function dropdownCatalogMenu() {
    // Each set of .dropdown_catalog-menu items are separate context
    $('.dropdown_catalog-menu:first-child').parent().each(function() {
        var $parent = $(this);

        var openedDD = false;
        var itemShowTimeout = false,
            itemHideTimeout = false;

        $parent.find(' > .dropdown_catalog-menu').not('.open').each(function(i) {
            var self = $(this),
                handle = self.find('.dropdown-toggle').eq(0),
                body = self.find('.dropdown-menu').eq(0);
            var is_fashion = $('body').hasClass('b-page_theme_fashion'),
                fashionFactor = is_fashion ? 0.1 : 0.5;

            var sub_menu = self.find(".dropdown_catalog-menu").eq(0);
            var sub_handle = sub_menu.find(".b-dropdown__handle").eq(0);

            handle
                .on('click', function() {
                    self.addClass('hover');
                    showHandler(self);
                });

            self.hover(function() {
                self.addClass('hover');
                if (openedDD != false) {
                    // If some dropdown is showing, we didn't hide him for now
                    // We need to reset hide timeout... on leave this menu item
                    // Or we hide him before showing dropdown of this item (if user is staying)
                    clearTimeout(itemHideTimeout);
                }
                //console.log('mouse in. hover');

                clearTimeout(itemShowTimeout);
                itemShowTimeout = setTimeout(function() {
                    // If user staying on this item
                    closeHandler(); // Hide opened dropdown
                    showHandler(self); // Show exact dropdown
                    //fashion specials
                    if (is_fashion && sub_menu.length > 0) {
                        sub_menu.addClass("open");
                        sub_handle.addClass("hover");
                        self.find(".b-dropdown__body__h_theme_fashion, .dropdown_catalog-menu_bg").css({
                            "min-height": 0
                        });
                        var eq_h_blocks = self.find(".b-dropdown__body__h_theme_fashion")
                            .add(self.closest(".b-dropdown__body__h_theme_fashion"));
                        var heights = [0];
                        eq_h_blocks.each(function() {
                            heights.push($(this).height());
                        });
                        var maxHeight = Math.max.apply(null, heights);
                        if (is_fashion) maxHeight += 10;
                        eq_h_blocks.add($(".dropdown_catalog-menu_bg")).css({
                            "min-height": maxHeight
                        });
                    }

                }, 300 * fashionFactor);
            }, function() {
                self.removeClass('hover');
                // If user is leaving this item, we no need to show him this dropdown
                clearTimeout(itemShowTimeout);
                //console.log('mouse out. hover');
                // Start count down to hide this item dropdown
                itemHideTimeout = setTimeout(function() {
                    closeHandler();
                    //fashion specials
                    if (is_fashion && sub_menu.length > 0) {
                        sub_menu.removeClass("open");
                        sub_handle.removeClass("hover");
                        //self.find(".b-dropdown__body__h_theme_fashion, .dropdown_catalog-menu_bg").css({"min-height": 0});
                        $(".dropdown_catalog-menu_bg").css({
                            "min-height": 0
                        });
                    }
                }, 300 * fashionFactor);
            });

            body
                .hover(function() {
                    $(this).addClass('hover');
                }, function() {
                    $(this).removeClass('hover');
                });



            function showHandler(item) {
                item.addClass('open');
                openedDD = item;
                //console.log('open');
            }

            function closeHandler() {
                var dropdown;
                if (openedDD != false) {
                    dropdown = openedDD.find('.dropdown-menu').eq(0);
                    if (!dropdown.hasClass('hover')) {
                        openedDD.removeClass('open');
                        openedDD = false;
                        //console.log('close');
                    }
                }
            }

            $(document)
                .on('click', function(event) {
                    if (!is_fashion || (is_fashion && $(event.target).closest('.js-dropdownmenu-item').length == 0)) {
                        var dropdown = $(event.target).closest(self);
                        if (dropdown.length) {} else {
                            closeHandler();
                        }
                    }
                })
                .on('keyup', function(event) {
                    if (event.keyCode == 27) {
                        closeHandler();
                    }
                });
        });
    });

}

$(document).ready(function() {
    dropdownCatalogMenu();
});

/* ..\..\desktop.blocks\b-dropdown\_catalog-menu\b-dropdown_catalog-menu.js end */
;
/* ..\..\desktop.blocks\modal\modal.js begin */
/* ========================================================================
 * Bootstrap: modal.js v3.3.2
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+

function($) {
    'use strict';

    // MODAL CLASS DEFINITION
    // ======================

    var Modal = function(element, options) {
        this.options = options
        this.$body = $(document.body)
        this.$element = $(element)
        this.$backdrop =
            this.isShown = null
        this.scrollbarWidth = 0

        if (this.options.remote) {
            this.$element
                .find('.modal-content')
                .load(this.options.remote, $.proxy(function() {
                    this.$element.trigger('loaded.bs.modal')
                }, this))
        }
    }

    Modal.VERSION = '3.3.2'

    Modal.TRANSITION_DURATION = 300
    Modal.BACKDROP_TRANSITION_DURATION = 150

    Modal.DEFAULTS = {
        backdrop: true,
        keyboard: true,
        show: true
    }

    Modal.prototype.toggle = function(_relatedTarget) {
        return this.isShown ? this.hide() : this.show(_relatedTarget)
    }

    Modal.prototype.show = function(_relatedTarget) {
        var that = this
        var e = $.Event('show.bs.modal', {
            relatedTarget: _relatedTarget
        })

        this.$element.trigger(e)

        if (this.isShown || e.isDefaultPrevented()) return

        this.isShown = true

        this.checkScrollbar()
        this.setScrollbar()
        this.$body.addClass('modal-open')

        this.escape()
        this.resize()

        this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

        this.backdrop(function() {
            var transition = $.support.transition && that.$element.hasClass('fade')

            if (!that.$element.parent().length) {
                that.$element.appendTo(that.$body) // don't move modals dom position
            }

            that.$element
                .show()
                .scrollTop(0)

            if (that.options.backdrop) that.adjustBackdrop()
            that.adjustDialog()

            if (transition) {
                that.$element[0].offsetWidth // force reflow
            }

            that.$element
                .addClass('in')
                .attr('aria-hidden', false)

            that.enforceFocus()

            var e = $.Event('shown.bs.modal', {
                relatedTarget: _relatedTarget
            })

            transition ?
                that.$element.find('.modal-dialog') // wait for modal to slide in
                .one('bsTransitionEnd', function() {
                    that.$element.trigger('focus').trigger(e)
                })
                .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
                that.$element.trigger('focus').trigger(e)
        })
    }

    Modal.prototype.hide = function(e) {
        if (e) e.preventDefault()

        e = $.Event('hide.bs.modal')

        this.$element.trigger(e)

        if (!this.isShown || e.isDefaultPrevented()) return

        this.isShown = false

        this.escape()
        this.resize()

        $(document).off('focusin.bs.modal')

        this.$element
            .removeClass('in')
            .attr('aria-hidden', true)
            .off('click.dismiss.bs.modal')

        $.support.transition && this.$element.hasClass('fade') ?
            this.$element
            .one('bsTransitionEnd', $.proxy(this.hideModal, this))
            .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
            this.hideModal()
    }

    Modal.prototype.enforceFocus = function() {
        $(document)
            .off('focusin.bs.modal') // guard against infinite focus loop
            .on('focusin.bs.modal', $.proxy(function(e) {
                if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
                    this.$element.trigger('focus')
                }
            }, this))
    }

    Modal.prototype.escape = function() {
        if (this.isShown && this.options.keyboard) {
            this.$element.on('keydown.dismiss.bs.modal', $.proxy(function(e) {
                e.which == 27 && this.hide()
            }, this))
        } else if (!this.isShown) {
            this.$element.off('keydown.dismiss.bs.modal')
        }
    }

    Modal.prototype.resize = function() {
        if (this.isShown) {
            $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
        } else {
            $(window).off('resize.bs.modal')
        }
    }

    Modal.prototype.hideModal = function() {
        var that = this
        this.$element.hide()
        this.backdrop(function() {
            that.$body.removeClass('modal-open')
            that.resetAdjustments()
            that.resetScrollbar()
            that.$element.trigger('hidden.bs.modal')
        })
    }

    Modal.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove()
        this.$backdrop = null
    }

    Modal.prototype.backdrop = function(callback) {
        var that = this
        var animate = this.$element.hasClass('fade') ? 'fade' : ''

        if (this.isShown && this.options.backdrop) {
            var doAnimate = $.support.transition && animate

            this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
                .prependTo(this.$element)
                .on('click.dismiss.bs.modal', $.proxy(function(e) {
                    if (e.target !== e.currentTarget) return
                    this.options.backdrop == 'static' ?
                        this.$element[0].focus.call(this.$element[0]) :
                        this.hide.call(this)
                }, this))

            if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

            this.$backdrop.addClass('in')

            if (!callback) return

            doAnimate ?
                this.$backdrop
                .one('bsTransitionEnd', callback)
                .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
                callback()

        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass('in')

            var callbackRemove = function() {
                that.removeBackdrop()
                callback && callback()
            }
            $.support.transition && this.$element.hasClass('fade') ?
                this.$backdrop
                .one('bsTransitionEnd', callbackRemove)
                .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
                callbackRemove()

        } else if (callback) {
            callback()
        }
    }

    // these following methods are used to handle overflowing modals

    Modal.prototype.handleUpdate = function() {
        if (this.options.backdrop) this.adjustBackdrop()
        this.adjustDialog()
    }

    Modal.prototype.adjustBackdrop = function() {
        this.$backdrop
            .css('height', 0)
            .css('height', this.$element[0].scrollHeight)
    }

    Modal.prototype.adjustDialog = function() {
        var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
            paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
        })
    }

    Modal.prototype.resetAdjustments = function() {
        this.$element.css({
            paddingLeft: '',
            paddingRight: ''
        })
    }

    Modal.prototype.checkScrollbar = function() {
        this.bodyIsOverflowing = document.body.scrollHeight > document.documentElement.clientHeight
        this.scrollbarWidth = this.measureScrollbar()
    }

    Modal.prototype.setScrollbar = function() {
        var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
        if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
    }

    Modal.prototype.resetScrollbar = function() {
        this.$body.css('padding-right', '')
    }

    Modal.prototype.measureScrollbar = function() { // thx walsh
        var scrollDiv = document.createElement('div')
        scrollDiv.className = 'modal-scrollbar-measure'
        this.$body.append(scrollDiv)
        var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
        this.$body[0].removeChild(scrollDiv)
        return scrollbarWidth
    }


    // MODAL PLUGIN DEFINITION
    // =======================

    function Plugin(option, _relatedTarget) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.modal')
            var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
            if (typeof option == 'string') data[option](_relatedTarget)
            else if (options.show) data.show(_relatedTarget)
        })
    }

    var old = $.fn.modal

    $.fn.modal = Plugin
    $.fn.modal.Constructor = Modal


    // MODAL NO CONFLICT
    // =================

    $.fn.modal.noConflict = function() {
        $.fn.modal = old
        return this
    }


    // MODAL DATA-API
    // ==============

    $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function(e) {
        var $this = $(this)
        var href = $this.attr('href')
        var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
        var option = $target.data('bs.modal') ? 'toggle' : $.extend({
            remote: !/#/.test(href) && href
        }, $target.data(), $this.data())

        if ($this.is('a')) e.preventDefault()

        $target.one('show.bs.modal', function(showEvent) {
            if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
            $target.one('hidden.bs.modal', function() {
                $this.is(':visible') && $this.trigger('focus')
            })
        })
        Plugin.call($target, option, this)
    })

}(jQuery);

/* ===========================================================
 * bootstrap-modalmanager.js v2.2.5
 * ===========================================================
 * Copyright 2012 Jordan Schroter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */

! function($) {

    "use strict"; // jshint ;_;

    /* MODAL MANAGER CLASS DEFINITION
     * ====================== */

    var ModalManager = function(element, options) {
        this.init(element, options);
    };

    ModalManager.prototype = {

        constructor: ModalManager,

        init: function(element, options) {
            this.$element = $(element);
            this.options = $.extend({}, $.fn.modalmanager.defaults, this.$element.data(), typeof options == 'object' && options);
            this.stack = [];
            this.backdropCount = 0;

            if (this.options.resize) {
                var resizeTimeout,
                    that = this;

                $(window).on('resize.modal', function() {
                    resizeTimeout && clearTimeout(resizeTimeout);
                    resizeTimeout = setTimeout(function() {
                        for (var i = 0; i < that.stack.length; i++) {
                            that.stack[i].isShown && that.stack[i].layout();
                        }
                    }, 10);
                });
            }
        },

        createModal: function(element, options) {
            $(element).modal($.extend({
                manager: this
            }, options));
        },

        appendModal: function(modal) {
            this.stack.push(modal);

            var that = this;

            modal.$element.on('show.modalmanager', targetIsSelf(function(e) {

                var showModal = function() {
                    modal.isShown = true;

                    var transition = $.support.transition && modal.$element.hasClass('fade');

                    that.$element
                        .toggleClass('modal-open', that.hasOpenModal())
                        .toggleClass('page-overflow', $(window).height() < that.$element.height());

                    modal.$parent = modal.$element.parent();

                    modal.$container = that.createContainer(modal);

                    modal.$element.appendTo(modal.$container);

                    that.backdrop(modal, function() {
                        modal.$element.show();

                        if (transition) {
                            //modal.$element[0].style.display = 'run-in';
                            modal.$element[0].offsetWidth;
                            //modal.$element.one($.support.transition.end, function () { modal.$element[0].style.display = 'block' });
                        }

                        modal.layout();

                        modal.$element
                            .addClass('in')
                            .attr('aria-hidden', false);

                        var complete = function() {
                            that.setFocus();
                            modal.$element.trigger('shown');
                        };

                        transition ?
                            modal.$element.one($.support.transition.end, complete) :
                            complete();
                    });
                };

                modal.options.replace ?
                    that.replace(showModal) :
                    showModal();
            }));

            modal.$element.on('hidden.modalmanager', targetIsSelf(function(e) {
                that.backdrop(modal);
                // handle the case when a modal may have been removed from the dom before this callback executes
                if (!modal.$element.parent().length) {
                    that.destroyModal(modal);
                } else if (modal.$backdrop) {
                    var transition = $.support.transition && modal.$element.hasClass('fade');

                    // trigger a relayout due to firebox's buggy transition end event
                    if (transition) {
                        modal.$element[0].offsetWidth;
                    }
                    $.support.transition && modal.$element.hasClass('fade') ?
                        modal.$backdrop.one($.support.transition.end, function() {
                            modal.destroy();
                        }) :
                        modal.destroy();
                } else {
                    modal.destroy();
                }

            }));

            modal.$element.on('destroyed.modalmanager', targetIsSelf(function(e) {
                that.destroyModal(modal);
            }));
        },

        getOpenModals: function() {
            var openModals = [];
            for (var i = 0; i < this.stack.length; i++) {
                if (this.stack[i].isShown) openModals.push(this.stack[i]);
            }

            return openModals;
        },

        hasOpenModal: function() {
            return this.getOpenModals().length > 0;
        },

        setFocus: function() {
            var topModal;

            for (var i = 0; i < this.stack.length; i++) {
                if (this.stack[i].isShown) topModal = this.stack[i];
            }

            if (!topModal) return;

            topModal.focus();
        },

        destroyModal: function(modal) {
            modal.$element.off('.modalmanager');
            if (modal.$backdrop) this.removeBackdrop(modal);
            this.stack.splice(this.getIndexOfModal(modal), 1);

            var hasOpenModal = this.hasOpenModal();

            this.$element.toggleClass('modal-open', hasOpenModal);

            if (!hasOpenModal) {
                this.$element.removeClass('page-overflow');
            }

            this.removeContainer(modal);

            this.setFocus();
        },

        getModalAt: function(index) {
            return this.stack[index];
        },

        getIndexOfModal: function(modal) {
            for (var i = 0; i < this.stack.length; i++) {
                if (modal === this.stack[i]) return i;
            }
        },

        replace: function(callback) {
            var topModal;

            for (var i = 0; i < this.stack.length; i++) {
                if (this.stack[i].isShown) topModal = this.stack[i];
            }

            if (topModal) {
                this.$backdropHandle = topModal.$backdrop;
                topModal.$backdrop = null;

                callback && topModal.$element.one('hidden',
                    targetIsSelf($.proxy(callback, this)));

                topModal.hide();
            } else if (callback) {
                callback();
            }
        },

        removeBackdrop: function(modal) {
            modal.$backdrop.remove();
            modal.$backdrop = null;
        },

        createBackdrop: function(animate, tmpl) {
            var $backdrop;

            if (!this.$backdropHandle) {
                $backdrop = $(tmpl)
                    .addClass(animate)
                    .appendTo(this.$element);
            } else {
                $backdrop = this.$backdropHandle;
                $backdrop.off('.modalmanager');
                this.$backdropHandle = null;
                this.isLoading && this.removeSpinner();
            }

            return $backdrop;
        },

        removeContainer: function(modal) {
            modal.$container.remove();
            modal.$container = null;
        },

        createContainer: function(modal) {
            var $container;

            $container = $('<div class="modal-scrollable">')
                .css('z-index', getzIndex('modal', this.getOpenModals().length))
                .appendTo(this.$element);

            if (modal && modal.options.backdrop != 'static') {
                $container.on('click.modal', targetIsSelf(function(e) {
                    modal.hide();
                }));
            } else if (modal) {
                $container.on('click.modal', targetIsSelf(function(e) {
                    modal.attention();
                }));
            }

            return $container;

        },

        backdrop: function(modal, callback) {
            var animate = modal.$element.hasClass('fade') ? 'fade' : '',
                showBackdrop = modal.options.backdrop &&
                this.backdropCount < this.options.backdropLimit;

            if (modal.isShown && showBackdrop) {
                var doAnimate = $.support.transition && animate && !this.$backdropHandle;

                modal.$backdrop = this.createBackdrop(animate, modal.options.backdropTemplate);

                modal.$backdrop.css('z-index', getzIndex('backdrop', this.getOpenModals().length));

                if (doAnimate) modal.$backdrop[0].offsetWidth; // force reflow

                modal.$backdrop.addClass('in');

                this.backdropCount += 1;

                doAnimate ?
                    modal.$backdrop.one($.support.transition.end, callback) :
                    callback();

            } else if (!modal.isShown && modal.$backdrop) {
                modal.$backdrop.removeClass('in');

                this.backdropCount -= 1;

                var that = this;

                $.support.transition && modal.$element.hasClass('fade') ?
                    modal.$backdrop.one($.support.transition.end, function() {
                        that.removeBackdrop(modal)
                    }) :
                    that.removeBackdrop(modal);

            } else if (callback) {
                callback();
            }
        },

        removeSpinner: function() {
            this.$spinner && this.$spinner.remove();
            this.$spinner = null;
            this.isLoading = false;
        },

        removeLoading: function() {
            this.$backdropHandle && this.$backdropHandle.remove();
            this.$backdropHandle = null;
            this.removeSpinner();
        },

        loading: function(callback) {
            callback = callback || function() {};

            this.$element
                .toggleClass('modal-open', !this.isLoading || this.hasOpenModal())
                .toggleClass('page-overflow', $(window).height() < this.$element.height());

            if (!this.isLoading) {

                this.$backdropHandle = this.createBackdrop('fade', this.options.backdropTemplate);

                this.$backdropHandle[0].offsetWidth; // force reflow

                var openModals = this.getOpenModals();

                this.$backdropHandle
                    .css('z-index', getzIndex('backdrop', openModals.length + 1))
                    .addClass('in');

                var $spinner = $(this.options.spinner)
                    .css('z-index', getzIndex('modal', openModals.length + 1))
                    .appendTo(this.$element)
                    .addClass('in');

                this.$spinner = $(this.createContainer())
                    .append($spinner)
                    .on('click.modalmanager', $.proxy(this.loading, this));

                this.isLoading = true;

                $.support.transition ?
                    this.$backdropHandle.one($.support.transition.end, callback) :
                    callback();

            } else if (this.isLoading && this.$backdropHandle) {
                this.$backdropHandle.removeClass('in');

                var that = this;
                $.support.transition ?
                    this.$backdropHandle.one($.support.transition.end, function() {
                        that.removeLoading()
                    }) :
                    that.removeLoading();

            } else if (callback) {
                callback(this.isLoading);
            }
        }
    };

    /* PRIVATE METHODS
     * ======================= */

    // computes and caches the zindexes
    var getzIndex = (function() {
        var zIndexFactor,
            baseIndex = {};

        return function(type, pos) {

            if (typeof zIndexFactor === 'undefined') {
                var $baseModal = $('<div class="modal hide" />').appendTo('body'),
                    $baseBackdrop = $('<div class="modal-backdrop hide" />').appendTo('body');

                baseIndex['modal'] = +$baseModal.css('z-index');
                baseIndex['backdrop'] = +$baseBackdrop.css('z-index');
                zIndexFactor = baseIndex['modal'] - baseIndex['backdrop'];

                $baseModal.remove();
                $baseBackdrop.remove();
                $baseBackdrop = $baseModal = null;
            }

            return baseIndex[type] + (zIndexFactor * pos);

        }
    }());

    // make sure the event target is the modal itself in order to prevent
    // other components such as tabsfrom triggering the modal manager.
    // if Boostsrap namespaced events, this would not be needed.
    function targetIsSelf(callback) {
        return function(e) {
            if (e && this === e.target) {
                return callback.apply(this, arguments);
            }
        }
    }


    /* MODAL MANAGER PLUGIN DEFINITION
     * ======================= */

    $.fn.modalmanager = function(option, args) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('modalmanager');

            if (!data) $this.data('modalmanager', (data = new ModalManager(this, option)));
            if (typeof option === 'string') data[option].apply(data, [].concat(args))
        })
    };

    $.fn.modalmanager.defaults = {
        backdropLimit: 999,
        resize: true,
        spinner: '<div class="loading-spinner fade" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div>',
        backdropTemplate: '<div class="modal-backdrop" />'
    };

    $.fn.modalmanager.Constructor = ModalManager

    // ModalManager handles the modal-open class so we need
    // to remove conflicting bootstrap 3 event handlers
    $(function() {
        $(document).off('show.bs.modal').off('hidden.bs.modal');
    });

}(jQuery);


/* ===========================================================
 * bootstrap-modal.js v2.2.5
 * ===========================================================
 * Copyright 2012 Jordan Schroter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


! function($) {

    "use strict"; // jshint ;_;

    /* MODAL CLASS DEFINITION
     * ====================== */

    var Modal = function(element, options) {
        this.init(element, options);
    };

    Modal.prototype = {

        constructor: Modal,

        init: function(element, options) {
            var that = this;

            this.options = options;

            this.$element = $(element)
                .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this));

            this.options.remote && this.$element.find('.modal-body').load(this.options.remote, function() {
                var e = $.Event('loaded');
                that.$element.trigger(e);
            });

            var manager = typeof this.options.manager === 'function' ?
                this.options.manager.call(this) : this.options.manager;

            manager = manager.appendModal ?
                manager : $(manager).modalmanager().data('modalmanager');

            manager.appendModal(this);
        },

        toggle: function() {
            return this[!this.isShown ? 'show' : 'hide']();
        },

        show: function() {
            var e = $.Event('show');

            if (this.isShown) return;

            this.$element.trigger(e);

            if (e.isDefaultPrevented()) return;

            this.escape();

            this.tab();

            this.options.loading && this.loading();
        },

        hide: function(e) {
            e && e.preventDefault();

            e = $.Event('hide');

            this.$element.trigger(e);

            if (!this.isShown || e.isDefaultPrevented()) return;

            this.isShown = false;

            this.escape();

            this.tab();

            this.isLoading && this.loading();

            $(document).off('focusin.modal');

            this.$element
                .removeClass('in')
                .removeClass('animated')
                .removeClass(this.options.attentionAnimation)
                .removeClass('modal-overflow')
                .attr('aria-hidden', true);

            $.support.transition && this.$element.hasClass('fade') ?
                this.hideWithTransition() :
                this.hideModal();
        },

        layout: function() {
            var prop = this.options.height ? 'height' : 'max-height',
                value = this.options.height || this.options.maxHeight;

            if (this.options.width) {
                this.$element.css('width', this.options.width);

                var that = this;
                this.$element.css('margin-left', function() {
                    if (/%/ig.test(that.options.width)) {
                        return -(parseInt(that.options.width) / 2) + '%';
                    } else {
                        return -($(this).width() / 2) + 'px';
                    }
                });
            } else {
                this.$element.css('width', '');
                this.$element.css('margin-left', '');
            }

            this.$element.find('.modal-body')
                .css('overflow', '')
                .css(prop, '');

            if (value) {
                this.$element.find('.modal-body')
                    .css('overflow', 'auto')
                    .css(prop, value);
            }

            var modalOverflow = $(window).height() - 10 < this.$element.height();

            if (modalOverflow || this.options.modalOverflow) {
                this.$element
                    .css('margin-top', 0)
                    .addClass('modal-overflow');
            } else {
                this.$element
                    .css('margin-top', 0 - this.$element.height() / 2)
                    .removeClass('modal-overflow');
            }
        },

        tab: function() {
            var that = this;

            if (this.isShown && this.options.consumeTab) {
                this.$element.on('keydown.tabindex.modal', '[data-tabindex]', function(e) {
                    if (e.keyCode && e.keyCode == 9) {
                        var elements = [],
                            tabindex = Number($(this).data('tabindex'));

                        that.$element.find('[data-tabindex]:enabled:visible:not([readonly])').each(function(ev) {
                            elements.push(Number($(this).data('tabindex')));
                        });
                        elements.sort(function(a, b) {
                            return a - b
                        });

                        var arrayPos = $.inArray(tabindex, elements);
                        if (!e.shiftKey) {
                            arrayPos < elements.length - 1 ?
                                that.$element.find('[data-tabindex=' + elements[arrayPos + 1] + ']').focus() :
                                that.$element.find('[data-tabindex=' + elements[0] + ']').focus();
                        } else {
                            arrayPos == 0 ?
                                that.$element.find('[data-tabindex=' + elements[elements.length - 1] + ']').focus() :
                                that.$element.find('[data-tabindex=' + elements[arrayPos - 1] + ']').focus();
                        }

                        e.preventDefault();
                    }
                });
            } else if (!this.isShown) {
                this.$element.off('keydown.tabindex.modal');
            }
        },

        escape: function() {
            var that = this;
            if (this.isShown && this.options.keyboard) {
                if (!this.$element.attr('tabindex')) this.$element.attr('tabindex', -1);

                this.$element.on('keyup.dismiss.modal', function(e) {
                    e.which == 27 && that.hide();
                });
            } else if (!this.isShown) {
                this.$element.off('keyup.dismiss.modal')
            }
        },

        hideWithTransition: function() {
            var that = this,
                timeout = setTimeout(function() {
                    that.$element.off($.support.transition.end);
                    that.hideModal();
                }, 500);

            this.$element.one($.support.transition.end, function() {
                clearTimeout(timeout);
                that.hideModal();
            });
        },

        hideModal: function() {
            var prop = this.options.height ? 'height' : 'max-height';
            var value = this.options.height || this.options.maxHeight;

            if (value) {
                this.$element.find('.modal-body')
                    .css('overflow', '')
                    .css(prop, '');
            }

            this.$element
                .hide()
                .trigger('hidden');
        },

        removeLoading: function() {
            this.$loading.remove();
            this.$loading = null;
            this.isLoading = false;
        },

        loading: function(callback) {
            callback = callback || function() {};

            var animate = this.$element.hasClass('fade') ? 'fade' : '';

            if (!this.isLoading) {
                var doAnimate = $.support.transition && animate;

                this.$loading = $('<div class="loading-mask ' + animate + '">')
                    .append(this.options.spinner)
                    .appendTo(this.$element);

                if (doAnimate) this.$loading[0].offsetWidth; // force reflow

                this.$loading.addClass('in');

                this.isLoading = true;

                doAnimate ?
                    this.$loading.one($.support.transition.end, callback) :
                    callback();

            } else if (this.isLoading && this.$loading) {
                this.$loading.removeClass('in');

                var that = this;
                $.support.transition && this.$element.hasClass('fade') ?
                    this.$loading.one($.support.transition.end, function() {
                        that.removeLoading()
                    }) :
                    that.removeLoading();

            } else if (callback) {
                callback(this.isLoading);
            }
        },

        focus: function() {
            var $focusElem = this.$element.find(this.options.focusOn);

            $focusElem = $focusElem.length ? $focusElem : this.$element;

            $focusElem.focus();
        },

        attention: function() {
            // NOTE: transitionEnd with keyframes causes odd behaviour

            if (this.options.attentionAnimation) {
                this.$element
                    .removeClass('animated')
                    .removeClass(this.options.attentionAnimation);

                var that = this;

                setTimeout(function() {
                    that.$element
                        .addClass('animated')
                        .addClass(that.options.attentionAnimation);
                }, 0);
            }


            this.focus();
        },


        destroy: function() {
            var e = $.Event('destroy');

            this.$element.trigger(e);

            if (e.isDefaultPrevented()) return;

            this.$element
                .off('.modal')
                .removeData('modal')
                .removeClass('in')
                .attr('aria-hidden', true);

            if (this.$parent !== this.$element.parent()) {
                this.$element.appendTo(this.$parent);
            } else if (!this.$parent.length) {
                // modal is not part of the DOM so remove it.
                this.$element.remove();
                this.$element = null;
            }

            this.$element.trigger('destroyed');
        }
    };


    /* MODAL PLUGIN DEFINITION
     * ======================= */

    $.fn.modal = function(option, args) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('modal'),
                options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option);

            if (!data) $this.data('modal', (data = new Modal(this, options)));
            if (typeof option == 'string') data[option].apply(data, [].concat(args));
            else if (options.show) data.show()
        })
    };

    $.fn.modal.defaults = {
        keyboard: true,
        backdrop: true,
        loading: false,
        show: true,
        width: null,
        height: null,
        maxHeight: null,
        modalOverflow: false,
        consumeTab: true,
        focusOn: null,
        replace: false,
        resize: false,
        attentionAnimation: 'shake',
        manager: 'body',
        spinner: '<div class="loading-spinner" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div>',
        backdropTemplate: '<div class="modal-backdrop" />'
    };

    $.fn.modal.Constructor = Modal;


    /* MODAL DATA-API
     * ============== */

    $(function() {
        $(document).off('click.modal').on('click.modal.data-api', '[data-toggle="modal"]', function(e) {
            var $this = $(this),
                href = $this.attr('href'),
                $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))), //strip for ie7
                option = $target.data('modal') ? 'toggle' : $.extend({
                    remote: !/#/.test(href) && href
                }, $target.data(), $this.data());

            e.preventDefault();
            $target
                .modal(option)
                .one('hide', function() {
                    $this.focus();
                })
        });
    });

}(window.jQuery);


$(document).ready(function() {

    $(document).on('click', '[data-toggle=modal][data-target=#ajax-modal]', function() {
        var $modal = $('#ajax-modal');
        var self = $(this)
        var href = self.attr('href') || self.data('href')
        var params = {}

        if ((self.get(0).onclick != null) && (typeof self.get(0).onclick().modal === 'object')) {
            params = $.extend(params, self.get(0).onclick().modal);
        }

        $('body').modalmanager('loading');

        if (href !== undefined) {
            $modal.load(href, '', function() {
                $modal.modal()
                $modal.addClass(params.classes)
            });
        }

        return false;
    });

    $(document).on('click', '#ajax-modal .update', function() {
        var $modal = $('#ajax-modal');
        $modal.modal('loading').find('.modal-body').prepend('<div class="alert alert-info fade in">' + '���������!<button type="button" class="close" data-dismiss="alert">&times;</button>' + '</div>');
    });
});
/* ..\..\desktop.blocks\modal\modal.js end */
;
/* ..\..\desktop.blocks\ui-core\ui-core.js begin */
/*! jQuery UI - v1.11.3 - 2015-02-12
 * http://jqueryui.com
 * Includes: core.js */

(function(factory) {
    if (typeof define === "function" && define.amd) {

        // AMD. Register as an anonymous module.
        define(["jquery"], factory);
    } else {

        // Browser globals
        factory(jQuery);
    }
}(function($) {
    /*!
     * jQuery UI Core 1.11.3
     * http://jqueryui.com
     *
     * Copyright jQuery Foundation and other contributors
     * Released under the MIT license.
     * http://jquery.org/license
     *
     * http://api.jqueryui.com/category/ui-core/
     */


    // $.ui might exist from components with no dependencies, e.g., $.ui.position
    $.ui = $.ui || {};

    $.extend($.ui, {
        version: "1.11.3",

        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    });

    // plugins
    $.fn.extend({
        scrollParent: function(includeHidden) {
            var position = this.css("position"),
                excludeStaticParent = position === "absolute",
                overflowRegex = includeHidden ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
                scrollParent = this.parents().filter(function() {
                    var parent = $(this);
                    if (excludeStaticParent && parent.css("position") === "static") {
                        return false;
                    }
                    return overflowRegex.test(parent.css("overflow") + parent.css("overflow-y") + parent.css("overflow-x"));
                }).eq(0);

            return position === "fixed" || !scrollParent.length ? $(this[0].ownerDocument || document) : scrollParent;
        },

        uniqueId: (function() {
            var uuid = 0;

            return function() {
                return this.each(function() {
                    if (!this.id) {
                        this.id = "ui-id-" + (++uuid);
                    }
                });
            };
        })(),

        removeUniqueId: function() {
            return this.each(function() {
                if (/^ui-id-\d+$/.test(this.id)) {
                    $(this).removeAttr("id");
                }
            });
        }
    });

    // selectors
    function focusable(element, isTabIndexNotNaN) {
        var map, mapName, img,
            nodeName = element.nodeName.toLowerCase();
        if ("area" === nodeName) {
            map = element.parentNode;
            mapName = map.name;
            if (!element.href || !mapName || map.nodeName.toLowerCase() !== "map") {
                return false;
            }
            img = $("img[usemap='#" + mapName + "']")[0];
            return !!img && visible(img);
        }
        return (/^(input|select|textarea|button|object)$/.test(nodeName) ?
                !element.disabled :
                "a" === nodeName ?
                element.href || isTabIndexNotNaN :
                isTabIndexNotNaN) &&
            // the element and all of its ancestors must be visible
            visible(element);
    }

    function visible(element) {
        return $.expr.filters.visible(element) &&
            !$(element).parents().addBack().filter(function() {
                return $.css(this, "visibility") === "hidden";
            }).length;
    }

    $.extend($.expr[":"], {
        data: $.expr.createPseudo ?
            $.expr.createPseudo(function(dataName) {
                return function(elem) {
                    return !!$.data(elem, dataName);
                };
            }) :
            // support: jQuery <1.8
            function(elem, i, match) {
                return !!$.data(elem, match[3]);
            },

        focusable: function(element) {
            return focusable(element, !isNaN($.attr(element, "tabindex")));
        },

        tabbable: function(element) {
            var tabIndex = $.attr(element, "tabindex"),
                isTabIndexNaN = isNaN(tabIndex);
            return (isTabIndexNaN || tabIndex >= 0) && focusable(element, !isTabIndexNaN);
        }
    });

    // support: jQuery <1.8
    if (!$("<a>").outerWidth(1).jquery) {
        $.each(["Width", "Height"], function(i, name) {
            var side = name === "Width" ? ["Left", "Right"] : ["Top", "Bottom"],
                type = name.toLowerCase(),
                orig = {
                    innerWidth: $.fn.innerWidth,
                    innerHeight: $.fn.innerHeight,
                    outerWidth: $.fn.outerWidth,
                    outerHeight: $.fn.outerHeight
                };

            function reduce(elem, size, border, margin) {
                $.each(side, function() {
                    size -= parseFloat($.css(elem, "padding" + this)) || 0;
                    if (border) {
                        size -= parseFloat($.css(elem, "border" + this + "Width")) || 0;
                    }
                    if (margin) {
                        size -= parseFloat($.css(elem, "margin" + this)) || 0;
                    }
                });
                return size;
            }

            $.fn["inner" + name] = function(size) {
                if (size === undefined) {
                    return orig["inner" + name].call(this);
                }

                return this.each(function() {
                    $(this).css(type, reduce(this, size) + "px");
                });
            };

            $.fn["outer" + name] = function(size, margin) {
                if (typeof size !== "number") {
                    return orig["outer" + name].call(this, size);
                }

                return this.each(function() {
                    $(this).css(type, reduce(this, size, true, margin) + "px");
                });
            };
        });
    }

    // support: jQuery <1.8
    if (!$.fn.addBack) {
        $.fn.addBack = function(selector) {
            return this.add(selector == null ?
                this.prevObject : this.prevObject.filter(selector)
            );
        };
    }

    // support: jQuery 1.6.1, 1.6.2 (http://bugs.jquery.com/ticket/9413)
    if ($("<a>").data("a-b", "a").removeData("a-b").data("a-b")) {
        $.fn.removeData = (function(removeData) {
            return function(key) {
                if (arguments.length) {
                    return removeData.call(this, $.camelCase(key));
                } else {
                    return removeData.call(this);
                }
            };
        })($.fn.removeData);
    }

    // deprecated
    $.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());

    $.fn.extend({
        focus: (function(orig) {
            return function(delay, fn) {
                return typeof delay === "number" ?
                    this.each(function() {
                        var elem = this;
                        setTimeout(function() {
                            $(elem).focus();
                            if (fn) {
                                fn.call(elem);
                            }
                        }, delay);
                    }) :
                    orig.apply(this, arguments);
            };
        })($.fn.focus),

        disableSelection: (function() {
            var eventType = "onselectstart" in document.createElement("div") ?
                "selectstart" :
                "mousedown";

            return function() {
                return this.bind(eventType + ".ui-disableSelection", function(event) {
                    event.preventDefault();
                });
            };
        })(),

        enableSelection: function() {
            return this.unbind(".ui-disableSelection");
        },

        zIndex: function(zIndex) {
            if (zIndex !== undefined) {
                return this.css("zIndex", zIndex);
            }

            if (this.length) {
                var elem = $(this[0]),
                    position, value;
                while (elem.length && elem[0] !== document) {
                    // Ignore z-index if position is set to a value where z-index is ignored by the browser
                    // This makes behavior of this function consistent across browsers
                    // WebKit always returns auto if the element is positioned
                    position = elem.css("position");
                    if (position === "absolute" || position === "relative" || position === "fixed") {
                        // IE returns 0 when zIndex is not specified
                        // other browsers return a string
                        // we ignore the case of nested elements with an explicit value of 0
                        // <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
                        value = parseInt(elem.css("zIndex"), 10);
                        if (!isNaN(value) && value !== 0) {
                            return value;
                        }
                    }
                    elem = elem.parent();
                }
            }

            return 0;
        }
    });

    // $.ui.plugin is deprecated. Use $.widget() extensions instead.
    $.ui.plugin = {
        add: function(module, option, set) {
            var i,
                proto = $.ui[module].prototype;
            for (i in set) {
                proto.plugins[i] = proto.plugins[i] || [];
                proto.plugins[i].push([option, set[i]]);
            }
        },
        call: function(instance, name, args, allowDisconnected) {
            var i,
                set = instance.plugins[name];

            if (!set) {
                return;
            }

            if (!allowDisconnected && (!instance.element[0].parentNode || instance.element[0].parentNode.nodeType === 11)) {
                return;
            }

            for (i = 0; i < set.length; i++) {
                if (instance.options[set[i][0]]) {
                    set[i][1].apply(instance.element, args);
                }
            }
        }
    };
}));
/* ..\..\desktop.blocks\ui-core\ui-core.js end */
;
/* ..\..\desktop.blocks\ui-widget\ui-widget.js begin */
/*!
 * jQuery UI Widget 1.11.3
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/jQuery.widget/
 */


var widget_uuid = 0,
    widget_slice = Array.prototype.slice;

$.cleanData = (function(orig) {
    return function(elems) {
        var events, elem, i;
        for (i = 0;
            (elem = elems[i]) != null; i++) {
            try {

                // Only trigger remove when necessary to save time
                events = $._data(elem, "events");
                if (events && events.remove) {
                    $(elem).triggerHandler("remove");
                }

                // http://bugs.jquery.com/ticket/8235
            } catch (e) {}
        }
        orig(elems);
    };
})($.cleanData);

$.widget = function(name, base, prototype) {
    var fullName, existingConstructor, constructor, basePrototype,
        // proxiedPrototype allows the provided prototype to remain unmodified
        // so that it can be used as a mixin for multiple widgets (#8876)
        proxiedPrototype = {},
        namespace = name.split(".")[0];

    name = name.split(".")[1];
    fullName = namespace + "-" + name;

    if (!prototype) {
        prototype = base;
        base = $.Widget;
    }

    // create selector for plugin
    $.expr[":"][fullName.toLowerCase()] = function(elem) {
        return !!$.data(elem, fullName);
    };

    $[namespace] = $[namespace] || {};
    existingConstructor = $[namespace][name];
    constructor = $[namespace][name] = function(options, element) {
        // allow instantiation without "new" keyword
        if (!this._createWidget) {
            return new constructor(options, element);
        }

        // allow instantiation without initializing for simple inheritance
        // must use "new" keyword (the code above always passes args)
        if (arguments.length) {
            this._createWidget(options, element);
        }
    };
    // extend with the existing constructor to carry over any static properties
    $.extend(constructor, existingConstructor, {
        version: prototype.version,
        // copy the object used to create the prototype in case we need to
        // redefine the widget later
        _proto: $.extend({}, prototype),
        // track widgets that inherit from this widget in case this widget is
        // redefined after a widget inherits from it
        _childConstructors: []
    });

    basePrototype = new base();
    // we need to make the options hash a property directly on the new instance
    // otherwise we'll modify the options hash on the prototype that we're
    // inheriting from
    basePrototype.options = $.widget.extend({}, basePrototype.options);
    $.each(prototype, function(prop, value) {
        if (!$.isFunction(value)) {
            proxiedPrototype[prop] = value;
            return;
        }
        proxiedPrototype[prop] = (function() {
            var _super = function() {
                    return base.prototype[prop].apply(this, arguments);
                },
                _superApply = function(args) {
                    return base.prototype[prop].apply(this, args);
                };
            return function() {
                var __super = this._super,
                    __superApply = this._superApply,
                    returnValue;

                this._super = _super;
                this._superApply = _superApply;

                returnValue = value.apply(this, arguments);

                this._super = __super;
                this._superApply = __superApply;

                return returnValue;
            };
        })();
    });
    constructor.prototype = $.widget.extend(basePrototype, {
        // TODO: remove support for widgetEventPrefix
        // always use the name + a colon as the prefix, e.g., draggable:start
        // don't prefix for widgets that aren't DOM-based
        widgetEventPrefix: existingConstructor ? (basePrototype.widgetEventPrefix || name) : name
    }, proxiedPrototype, {
        constructor: constructor,
        namespace: namespace,
        widgetName: name,
        widgetFullName: fullName
    });

    // If this widget is being redefined then we need to find all widgets that
    // are inheriting from it and redefine all of them so that they inherit from
    // the new version of this widget. We're essentially trying to replace one
    // level in the prototype chain.
    if (existingConstructor) {
        $.each(existingConstructor._childConstructors, function(i, child) {
            var childPrototype = child.prototype;

            // redefine the child widget using the same prototype that was
            // originally used, but inherit from the new version of the base
            $.widget(childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto);
        });
        // remove the list of existing child constructors from the old constructor
        // so the old child constructors can be garbage collected
        delete existingConstructor._childConstructors;
    } else {
        base._childConstructors.push(constructor);
    }

    $.widget.bridge(name, constructor);

    return constructor;
};

$.widget.extend = function(target) {
    var input = widget_slice.call(arguments, 1),
        inputIndex = 0,
        inputLength = input.length,
        key,
        value;
    for (; inputIndex < inputLength; inputIndex++) {
        for (key in input[inputIndex]) {
            value = input[inputIndex][key];
            if (input[inputIndex].hasOwnProperty(key) && value !== undefined) {
                // Clone objects
                if ($.isPlainObject(value)) {
                    target[key] = $.isPlainObject(target[key]) ?
                        $.widget.extend({}, target[key], value) :
                        // Don't extend strings, arrays, etc. with objects
                        $.widget.extend({}, value);
                    // Copy everything else by reference
                } else {
                    target[key] = value;
                }
            }
        }
    }
    return target;
};

$.widget.bridge = function(name, object) {
    var fullName = object.prototype.widgetFullName || name;
    $.fn[name] = function(options) {
        var isMethodCall = typeof options === "string",
            args = widget_slice.call(arguments, 1),
            returnValue = this;

        if (isMethodCall) {
            this.each(function() {
                var methodValue,
                    instance = $.data(this, fullName);
                if (options === "instance") {
                    returnValue = instance;
                    return false;
                }
                if (!instance) {
                    return $.error("cannot call methods on " + name + " prior to initialization; " +
                        "attempted to call method '" + options + "'");
                }
                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                    return $.error("no such method '" + options + "' for " + name + " widget instance");
                }
                methodValue = instance[options].apply(instance, args);
                if (methodValue !== instance && methodValue !== undefined) {
                    returnValue = methodValue && methodValue.jquery ?
                        returnValue.pushStack(methodValue.get()) :
                        methodValue;
                    return false;
                }
            });
        } else {

            // Allow multiple hashes to be passed on init
            if (args.length) {
                options = $.widget.extend.apply(null, [options].concat(args));
            }

            this.each(function() {
                var instance = $.data(this, fullName);
                if (instance) {
                    instance.option(options || {});
                    if (instance._init) {
                        instance._init();
                    }
                } else {
                    $.data(this, fullName, new object(options, this));
                }
            });
        }

        return returnValue;
    };
};

$.Widget = function( /* options, element */ ) {};
$.Widget._childConstructors = [];

$.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    defaultElement: "<div>",
    options: {
        disabled: false,

        // callbacks
        create: null
    },
    _createWidget: function(options, element) {
        element = $(element || this.defaultElement || this)[0];
        this.element = $(element);
        this.uuid = widget_uuid++;
        this.eventNamespace = "." + this.widgetName + this.uuid;

        this.bindings = $();
        this.hoverable = $();
        this.focusable = $();

        if (element !== this) {
            $.data(element, this.widgetFullName, this);
            this._on(true, this.element, {
                remove: function(event) {
                    if (event.target === element) {
                        this.destroy();
                    }
                }
            });
            this.document = $(element.style ?
                // element within the document
                element.ownerDocument :
                // element is window or document
                element.document || element);
            this.window = $(this.document[0].defaultView || this.document[0].parentWindow);
        }

        this.options = $.widget.extend({},
            this.options,
            this._getCreateOptions(),
            options);

        this._create();
        this._trigger("create", null, this._getCreateEventData());
        this._init();
    },
    _getCreateOptions: $.noop,
    _getCreateEventData: $.noop,
    _create: $.noop,
    _init: $.noop,

    destroy: function() {
        this._destroy();
        // we can probably remove the unbind calls in 2.0
        // all event bindings should go through this._on()
        this.element
            .unbind(this.eventNamespace)
            .removeData(this.widgetFullName)
            // support: jquery <1.6.3
            // http://bugs.jquery.com/ticket/9413
            .removeData($.camelCase(this.widgetFullName));
        this.widget()
            .unbind(this.eventNamespace)
            .removeAttr("aria-disabled")
            .removeClass(
                this.widgetFullName + "-disabled " +
                "ui-state-disabled");

        // clean up events and states
        this.bindings.unbind(this.eventNamespace);
        this.hoverable.removeClass("ui-state-hover");
        this.focusable.removeClass("ui-state-focus");
    },
    _destroy: $.noop,

    widget: function() {
        return this.element;
    },

    option: function(key, value) {
        var options = key,
            parts,
            curOption,
            i;

        if (arguments.length === 0) {
            // don't return a reference to the internal hash
            return $.widget.extend({}, this.options);
        }

        if (typeof key === "string") {
            // handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }
            options = {};
            parts = key.split(".");
            key = parts.shift();
            if (parts.length) {
                curOption = options[key] = $.widget.extend({}, this.options[key]);
                for (i = 0; i < parts.length - 1; i++) {
                    curOption[parts[i]] = curOption[parts[i]] || {};
                    curOption = curOption[parts[i]];
                }
                key = parts.pop();
                if (arguments.length === 1) {
                    return curOption[key] === undefined ? null : curOption[key];
                }
                curOption[key] = value;
            } else {
                if (arguments.length === 1) {
                    return this.options[key] === undefined ? null : this.options[key];
                }
                options[key] = value;
            }
        }

        this._setOptions(options);

        return this;
    },
    _setOptions: function(options) {
        var key;

        for (key in options) {
            this._setOption(key, options[key]);
        }

        return this;
    },
    _setOption: function(key, value) {
        this.options[key] = value;

        if (key === "disabled") {
            this.widget()
                .toggleClass(this.widgetFullName + "-disabled", !!value);

            // If the widget is becoming disabled, then nothing is interactive
            if (value) {
                this.hoverable.removeClass("ui-state-hover");
                this.focusable.removeClass("ui-state-focus");
            }
        }

        return this;
    },

    enable: function() {
        return this._setOptions({
            disabled: false
        });
    },
    disable: function() {
        return this._setOptions({
            disabled: true
        });
    },

    _on: function(suppressDisabledCheck, element, handlers) {
        var delegateElement,
            instance = this;

        // no suppressDisabledCheck flag, shuffle arguments
        if (typeof suppressDisabledCheck !== "boolean") {
            handlers = element;
            element = suppressDisabledCheck;
            suppressDisabledCheck = false;
        }

        // no element argument, shuffle and use this.element
        if (!handlers) {
            handlers = element;
            element = this.element;
            delegateElement = this.widget();
        } else {
            element = delegateElement = $(element);
            this.bindings = this.bindings.add(element);
        }

        $.each(handlers, function(event, handler) {
            function handlerProxy() {
                // allow widgets to customize the disabled handling
                // - disabled as an array instead of boolean
                // - disabled class as method for disabling individual parts
                if (!suppressDisabledCheck &&
                    (instance.options.disabled === true ||
                        $(this).hasClass("ui-state-disabled"))) {
                    return;
                }
                return (typeof handler === "string" ? instance[handler] : handler)
                    .apply(instance, arguments);
            }

            // copy the guid so direct unbinding works
            if (typeof handler !== "string") {
                handlerProxy.guid = handler.guid =
                    handler.guid || handlerProxy.guid || $.guid++;
            }

            var match = event.match(/^([\w:-]*)\s*(.*)$/),
                eventName = match[1] + instance.eventNamespace,
                selector = match[2];
            if (selector) {
                delegateElement.delegate(selector, eventName, handlerProxy);
            } else {
                element.bind(eventName, handlerProxy);
            }
        });
    },

    _off: function(element, eventName) {
        eventName = (eventName || "").split(" ").join(this.eventNamespace + " ") +
            this.eventNamespace;
        element.unbind(eventName).undelegate(eventName);

        // Clear the stack to avoid memory leaks (#10056)
        this.bindings = $(this.bindings.not(element).get());
        this.focusable = $(this.focusable.not(element).get());
        this.hoverable = $(this.hoverable.not(element).get());
    },

    _delay: function(handler, delay) {
        function handlerProxy() {
            return (typeof handler === "string" ? instance[handler] : handler)
                .apply(instance, arguments);
        }
        var instance = this;
        return setTimeout(handlerProxy, delay || 0);
    },

    _hoverable: function(element) {
        this.hoverable = this.hoverable.add(element);
        this._on(element, {
            mouseenter: function(event) {
                $(event.currentTarget).addClass("ui-state-hover");
            },
            mouseleave: function(event) {
                $(event.currentTarget).removeClass("ui-state-hover");
            }
        });
    },

    _focusable: function(element) {
        this.focusable = this.focusable.add(element);
        this._on(element, {
            focusin: function(event) {
                $(event.currentTarget).addClass("ui-state-focus");
            },
            focusout: function(event) {
                $(event.currentTarget).removeClass("ui-state-focus");
            }
        });
    },

    _trigger: function(type, event, data) {
        var prop, orig,
            callback = this.options[type];

        data = data || {};
        event = $.Event(event);
        event.type = (type === this.widgetEventPrefix ?
            type :
            this.widgetEventPrefix + type).toLowerCase();
        // the original event may come from any element
        // so we need to reset the target on the new event
        event.target = this.element[0];

        // copy original event properties over to the new event
        orig = event.originalEvent;
        if (orig) {
            for (prop in orig) {
                if (!(prop in event)) {
                    event[prop] = orig[prop];
                }
            }
        }

        this.element.trigger(event, data);
        return !($.isFunction(callback) &&
            callback.apply(this.element[0], [event].concat(data)) === false ||
            event.isDefaultPrevented());
    }
};

$.each({
    show: "fadeIn",
    hide: "fadeOut"
}, function(method, defaultEffect) {
    $.Widget.prototype["_" + method] = function(element, options, callback) {
        if (typeof options === "string") {
            options = {
                effect: options
            };
        }
        var hasOptions,
            effectName = !options ?
            method :
            options === true || typeof options === "number" ?
            defaultEffect :
            options.effect || defaultEffect;
        options = options || {};
        if (typeof options === "number") {
            options = {
                duration: options
            };
        }
        hasOptions = !$.isEmptyObject(options);
        options.complete = callback;
        if (options.delay) {
            element.delay(options.delay);
        }
        if (hasOptions && $.effects && $.effects.effect[effectName]) {
            element[method](options);
        } else if (effectName !== method && element[effectName]) {
            element[effectName](options.duration, options.easing, callback);
        } else {
            element.queue(function(next) {
                $(this)[method]();
                if (callback) {
                    callback.call(element[0]);
                }
                next();
            });
        }
    };
});

var widget = $.widget;
/* ..\..\desktop.blocks\ui-widget\ui-widget.js end */
;
/* ..\..\desktop.blocks\ui-position\ui-position.js begin */
/*!
 * jQuery UI Position 1.11.3
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/position/
 */

(function() {

    $.ui = $.ui || {};

    var cachedScrollbarWidth, supportsOffsetFractions,
        max = Math.max,
        abs = Math.abs,
        round = Math.round,
        rhorizontal = /left|center|right/,
        rvertical = /top|center|bottom/,
        roffset = /[\+\-]\d+(\.[\d]+)?%?/,
        rposition = /^\w+/,
        rpercent = /%$/,
        _position = $.fn.position;

    function getOffsets(offsets, width, height) {
        return [
            parseFloat(offsets[0]) * (rpercent.test(offsets[0]) ? width / 100 : 1),
            parseFloat(offsets[1]) * (rpercent.test(offsets[1]) ? height / 100 : 1)
        ];
    }

    function parseCss(element, property) {
        return parseInt($.css(element, property), 10) || 0;
    }

    function getDimensions(elem) {
        var raw = elem[0];
        if (raw.nodeType === 9) {
            return {
                width: elem.width(),
                height: elem.height(),
                offset: {
                    top: 0,
                    left: 0
                }
            };
        }
        if ($.isWindow(raw)) {
            return {
                width: elem.width(),
                height: elem.height(),
                offset: {
                    top: elem.scrollTop(),
                    left: elem.scrollLeft()
                }
            };
        }
        if (raw.preventDefault) {
            return {
                width: 0,
                height: 0,
                offset: {
                    top: raw.pageY,
                    left: raw.pageX
                }
            };
        }
        return {
            width: elem.outerWidth(),
            height: elem.outerHeight(),
            offset: elem.offset()
        };
    }

    $.position = {
        scrollbarWidth: function() {
            if (cachedScrollbarWidth !== undefined) {
                return cachedScrollbarWidth;
            }
            var w1, w2,
                div = $("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                innerDiv = div.children()[0];

            $("body").append(div);
            w1 = innerDiv.offsetWidth;
            div.css("overflow", "scroll");

            w2 = innerDiv.offsetWidth;

            if (w1 === w2) {
                w2 = div[0].clientWidth;
            }

            div.remove();

            return (cachedScrollbarWidth = w1 - w2);
        },
        getScrollInfo: function(within) {
            var overflowX = within.isWindow || within.isDocument ? "" :
                within.element.css("overflow-x"),
                overflowY = within.isWindow || within.isDocument ? "" :
                within.element.css("overflow-y"),
                hasOverflowX = overflowX === "scroll" ||
                (overflowX === "auto" && within.width < within.element[0].scrollWidth),
                hasOverflowY = overflowY === "scroll" ||
                (overflowY === "auto" && within.height < within.element[0].scrollHeight);
            return {
                width: hasOverflowY ? $.position.scrollbarWidth() : 0,
                height: hasOverflowX ? $.position.scrollbarWidth() : 0
            };
        },
        getWithinInfo: function(element) {
            var withinElement = $(element || window),
                isWindow = $.isWindow(withinElement[0]),
                isDocument = !!withinElement[0] && withinElement[0].nodeType === 9;
            return {
                element: withinElement,
                isWindow: isWindow,
                isDocument: isDocument,
                offset: withinElement.offset() || {
                    left: 0,
                    top: 0
                },
                scrollLeft: withinElement.scrollLeft(),
                scrollTop: withinElement.scrollTop(),

                // support: jQuery 1.6.x
                // jQuery 1.6 doesn't support .outerWidth/Height() on documents or windows
                width: isWindow || isDocument ? withinElement.width() : withinElement.outerWidth(),
                height: isWindow || isDocument ? withinElement.height() : withinElement.outerHeight()
            };
        }
    };

    $.fn.position = function(options) {
        if (!options || !options.of) {
            return _position.apply(this, arguments);
        }

        // make a copy, we don't want to modify arguments
        options = $.extend({}, options);

        var atOffset, targetWidth, targetHeight, targetOffset, basePosition, dimensions,
            target = $(options.of),
            within = $.position.getWithinInfo(options.within),
            scrollInfo = $.position.getScrollInfo(within),
            collision = (options.collision || "flip").split(" "),
            offsets = {};

        dimensions = getDimensions(target);
        if (target[0].preventDefault) {
            // force left top to allow flipping
            options.at = "left top";
        }
        targetWidth = dimensions.width;
        targetHeight = dimensions.height;
        targetOffset = dimensions.offset;
        // clone to reuse original targetOffset later
        basePosition = $.extend({}, targetOffset);

        // force my and at to have valid horizontal and vertical positions
        // if a value is missing or invalid, it will be converted to center
        $.each(["my", "at"], function() {
            var pos = (options[this] || "").split(" "),
                horizontalOffset,
                verticalOffset;

            if (pos.length === 1) {
                pos = rhorizontal.test(pos[0]) ?
                    pos.concat(["center"]) :
                    rvertical.test(pos[0]) ? ["center"].concat(pos) : ["center", "center"];
            }
            pos[0] = rhorizontal.test(pos[0]) ? pos[0] : "center";
            pos[1] = rvertical.test(pos[1]) ? pos[1] : "center";

            // calculate offsets
            horizontalOffset = roffset.exec(pos[0]);
            verticalOffset = roffset.exec(pos[1]);
            offsets[this] = [
                horizontalOffset ? horizontalOffset[0] : 0,
                verticalOffset ? verticalOffset[0] : 0
            ];

            // reduce to just the positions without the offsets
            options[this] = [
                rposition.exec(pos[0])[0],
                rposition.exec(pos[1])[0]
            ];
        });

        // normalize collision option
        if (collision.length === 1) {
            collision[1] = collision[0];
        }

        if (options.at[0] === "right") {
            basePosition.left += targetWidth;
        } else if (options.at[0] === "center") {
            basePosition.left += targetWidth / 2;
        }

        if (options.at[1] === "bottom") {
            basePosition.top += targetHeight;
        } else if (options.at[1] === "center") {
            basePosition.top += targetHeight / 2;
        }

        atOffset = getOffsets(offsets.at, targetWidth, targetHeight);
        basePosition.left += atOffset[0];
        basePosition.top += atOffset[1];

        return this.each(function() {
            var collisionPosition, using,
                elem = $(this),
                elemWidth = elem.outerWidth(),
                elemHeight = elem.outerHeight(),
                marginLeft = parseCss(this, "marginLeft"),
                marginTop = parseCss(this, "marginTop"),
                collisionWidth = elemWidth + marginLeft + parseCss(this, "marginRight") + scrollInfo.width,
                collisionHeight = elemHeight + marginTop + parseCss(this, "marginBottom") + scrollInfo.height,
                position = $.extend({}, basePosition),
                myOffset = getOffsets(offsets.my, elem.outerWidth(), elem.outerHeight());

            if (options.my[0] === "right") {
                position.left -= elemWidth;
            } else if (options.my[0] === "center") {
                position.left -= elemWidth / 2;
            }

            if (options.my[1] === "bottom") {
                position.top -= elemHeight;
            } else if (options.my[1] === "center") {
                position.top -= elemHeight / 2;
            }

            position.left += myOffset[0];
            position.top += myOffset[1];

            // if the browser doesn't support fractions, then round for consistent results
            if (!supportsOffsetFractions) {
                position.left = round(position.left);
                position.top = round(position.top);
            }

            collisionPosition = {
                marginLeft: marginLeft,
                marginTop: marginTop
            };

            $.each(["left", "top"], function(i, dir) {
                if ($.ui.position[collision[i]]) {
                    $.ui.position[collision[i]][dir](position, {
                        targetWidth: targetWidth,
                        targetHeight: targetHeight,
                        elemWidth: elemWidth,
                        elemHeight: elemHeight,
                        collisionPosition: collisionPosition,
                        collisionWidth: collisionWidth,
                        collisionHeight: collisionHeight,
                        offset: [atOffset[0] + myOffset[0], atOffset[1] + myOffset[1]],
                        my: options.my,
                        at: options.at,
                        within: within,
                        elem: elem
                    });
                }
            });

            if (options.using) {
                // adds feedback as second argument to using callback, if present
                using = function(props) {
                    var left = targetOffset.left - position.left,
                        right = left + targetWidth - elemWidth,
                        top = targetOffset.top - position.top,
                        bottom = top + targetHeight - elemHeight,
                        feedback = {
                            target: {
                                element: target,
                                left: targetOffset.left,
                                top: targetOffset.top,
                                width: targetWidth,
                                height: targetHeight
                            },
                            element: {
                                element: elem,
                                left: position.left,
                                top: position.top,
                                width: elemWidth,
                                height: elemHeight
                            },
                            horizontal: right < 0 ? "left" : left > 0 ? "right" : "center",
                            vertical: bottom < 0 ? "top" : top > 0 ? "bottom" : "middle"
                        };
                    if (targetWidth < elemWidth && abs(left + right) < targetWidth) {
                        feedback.horizontal = "center";
                    }
                    if (targetHeight < elemHeight && abs(top + bottom) < targetHeight) {
                        feedback.vertical = "middle";
                    }
                    if (max(abs(left), abs(right)) > max(abs(top), abs(bottom))) {
                        feedback.important = "horizontal";
                    } else {
                        feedback.important = "vertical";
                    }
                    options.using.call(this, props, feedback);
                };
            }

            elem.offset($.extend(position, {
                using: using
            }));
        });
    };

    $.ui.position = {
        fit: {
            left: function(position, data) {
                var within = data.within,
                    withinOffset = within.isWindow ? within.scrollLeft : within.offset.left,
                    outerWidth = within.width,
                    collisionPosLeft = position.left - data.collisionPosition.marginLeft,
                    overLeft = withinOffset - collisionPosLeft,
                    overRight = collisionPosLeft + data.collisionWidth - outerWidth - withinOffset,
                    newOverRight;

                // element is wider than within
                if (data.collisionWidth > outerWidth) {
                    // element is initially over the left side of within
                    if (overLeft > 0 && overRight <= 0) {
                        newOverRight = position.left + overLeft + data.collisionWidth - outerWidth - withinOffset;
                        position.left += overLeft - newOverRight;
                        // element is initially over right side of within
                    } else if (overRight > 0 && overLeft <= 0) {
                        position.left = withinOffset;
                        // element is initially over both left and right sides of within
                    } else {
                        if (overLeft > overRight) {
                            position.left = withinOffset + outerWidth - data.collisionWidth;
                        } else {
                            position.left = withinOffset;
                        }
                    }
                    // too far left -> align with left edge
                } else if (overLeft > 0) {
                    position.left += overLeft;
                    // too far right -> align with right edge
                } else if (overRight > 0) {
                    position.left -= overRight;
                    // adjust based on position and margin
                } else {
                    position.left = max(position.left - collisionPosLeft, position.left);
                }
            },
            top: function(position, data) {
                var within = data.within,
                    withinOffset = within.isWindow ? within.scrollTop : within.offset.top,
                    outerHeight = data.within.height,
                    collisionPosTop = position.top - data.collisionPosition.marginTop,
                    overTop = withinOffset - collisionPosTop,
                    overBottom = collisionPosTop + data.collisionHeight - outerHeight - withinOffset,
                    newOverBottom;

                // element is taller than within
                if (data.collisionHeight > outerHeight) {
                    // element is initially over the top of within
                    if (overTop > 0 && overBottom <= 0) {
                        newOverBottom = position.top + overTop + data.collisionHeight - outerHeight - withinOffset;
                        position.top += overTop - newOverBottom;
                        // element is initially over bottom of within
                    } else if (overBottom > 0 && overTop <= 0) {
                        position.top = withinOffset;
                        // element is initially over both top and bottom of within
                    } else {
                        if (overTop > overBottom) {
                            position.top = withinOffset + outerHeight - data.collisionHeight;
                        } else {
                            position.top = withinOffset;
                        }
                    }
                    // too far up -> align with top
                } else if (overTop > 0) {
                    position.top += overTop;
                    // too far down -> align with bottom edge
                } else if (overBottom > 0) {
                    position.top -= overBottom;
                    // adjust based on position and margin
                } else {
                    position.top = max(position.top - collisionPosTop, position.top);
                }
            }
        },
        flip: {
            left: function(position, data) {
                var within = data.within,
                    withinOffset = within.offset.left + within.scrollLeft,
                    outerWidth = within.width,
                    offsetLeft = within.isWindow ? within.scrollLeft : within.offset.left,
                    collisionPosLeft = position.left - data.collisionPosition.marginLeft,
                    overLeft = collisionPosLeft - offsetLeft,
                    overRight = collisionPosLeft + data.collisionWidth - outerWidth - offsetLeft,
                    myOffset = data.my[0] === "left" ?
                    -data.elemWidth :
                    data.my[0] === "right" ?
                    data.elemWidth :
                    0,
                    atOffset = data.at[0] === "left" ?
                    data.targetWidth :
                    data.at[0] === "right" ?
                    -data.targetWidth :
                    0,
                    offset = -2 * data.offset[0],
                    newOverRight,
                    newOverLeft;

                if (overLeft < 0) {
                    newOverRight = position.left + myOffset + atOffset + offset + data.collisionWidth - outerWidth - withinOffset;
                    if (newOverRight < 0 || newOverRight < abs(overLeft)) {
                        position.left += myOffset + atOffset + offset;
                    }
                } else if (overRight > 0) {
                    newOverLeft = position.left - data.collisionPosition.marginLeft + myOffset + atOffset + offset - offsetLeft;
                    if (newOverLeft > 0 || abs(newOverLeft) < overRight) {
                        position.left += myOffset + atOffset + offset;
                    }
                }
            },
            top: function(position, data) {
                var within = data.within,
                    withinOffset = within.offset.top + within.scrollTop,
                    outerHeight = within.height,
                    offsetTop = within.isWindow ? within.scrollTop : within.offset.top,
                    collisionPosTop = position.top - data.collisionPosition.marginTop,
                    overTop = collisionPosTop - offsetTop,
                    overBottom = collisionPosTop + data.collisionHeight - outerHeight - offsetTop,
                    top = data.my[1] === "top",
                    myOffset = top ?
                    -data.elemHeight :
                    data.my[1] === "bottom" ?
                    data.elemHeight :
                    0,
                    atOffset = data.at[1] === "top" ?
                    data.targetHeight :
                    data.at[1] === "bottom" ?
                    -data.targetHeight :
                    0,
                    offset = -2 * data.offset[1],
                    newOverTop,
                    newOverBottom;
                if (overTop < 0) {
                    newOverBottom = position.top + myOffset + atOffset + offset + data.collisionHeight - outerHeight - withinOffset;
                    if (newOverBottom < 0 || newOverBottom < abs(overTop)) {
                        position.top += myOffset + atOffset + offset;
                    }
                } else if (overBottom > 0) {
                    newOverTop = position.top - data.collisionPosition.marginTop + myOffset + atOffset + offset - offsetTop;
                    if (newOverTop > 0 || abs(newOverTop) < overBottom) {
                        position.top += myOffset + atOffset + offset;
                    }
                }
            }
        },
        flipfit: {
            left: function() {
                $.ui.position.flip.left.apply(this, arguments);
                $.ui.position.fit.left.apply(this, arguments);
            },
            top: function() {
                $.ui.position.flip.top.apply(this, arguments);
                $.ui.position.fit.top.apply(this, arguments);
            }
        }
    };

    // fraction support test
    (function() {
        var testElement, testElementParent, testElementStyle, offsetLeft, i,
            body = document.getElementsByTagName("body")[0],
            div = document.createElement("div");

        //Create a "fake body" for testing based on method used in jQuery.support
        testElement = document.createElement(body ? "div" : "body");
        testElementStyle = {
            visibility: "hidden",
            width: 0,
            height: 0,
            border: 0,
            margin: 0,
            background: "none"
        };
        if (body) {
            $.extend(testElementStyle, {
                position: "absolute",
                left: "-1000px",
                top: "-1000px"
            });
        }
        for (i in testElementStyle) {
            testElement.style[i] = testElementStyle[i];
        }
        testElement.appendChild(div);
        testElementParent = body || document.documentElement;
        testElementParent.insertBefore(testElement, testElementParent.firstChild);

        div.style.cssText = "position: absolute; left: 10.7432222px;";

        offsetLeft = $(div).offset().left;
        supportsOffsetFractions = offsetLeft > 10 && offsetLeft < 11;

        testElement.innerHTML = "";
        testElementParent.removeChild(testElement);
    })();

})();

var position = $.ui.position;
/* ..\..\desktop.blocks\ui-position\ui-position.js end */
;
/* ..\..\desktop.blocks\ui-autocomplete\ui-autocomplete.js begin */
/*!
 * jQuery UI Autocomplete 1.11.3
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/autocomplete/
 */


$.widget("ui.autocomplete", {
    version: "1.11.3",
    defaultElement: "<input>",
    options: {
        appendTo: null,
        autoFocus: false,
        delay: 500,
        minLength: 1,
        position: {
            my: "left top",
            at: "left bottom",
            collision: "none"
        },
        source: null,

        // callbacks
        change: null,
        close: null,
        focus: null,
        open: null,
        response: null,
        search: null,
        select: null
    },

    requestIndex: 0,
    pending: 0,

    _create: function() {
        // Some browsers only repeat keydown events, not keypress events,
        // so we use the suppressKeyPress flag to determine if we've already
        // handled the keydown event. #7269
        // Unfortunately the code for & in keypress is the same as the up arrow,
        // so we use the suppressKeyPressRepeat flag to avoid handling keypress
        // events when we know the keydown event was used to modify the
        // search term. #7799
        var suppressKeyPress, suppressKeyPressRepeat, suppressInput,
            nodeName = this.element[0].nodeName.toLowerCase(),
            isTextarea = nodeName === "textarea",
            isInput = nodeName === "input";

        this.isMultiLine =
            // Textareas are always multi-line
            isTextarea ? true :
            // Inputs are always single-line, even if inside a contentEditable element
            // IE also treats inputs as contentEditable
            isInput ? false :
            // All other element types are determined by whether or not they're contentEditable
            this.element.prop("isContentEditable");

        this.valueMethod = this.element[isTextarea || isInput ? "val" : "text"];
        this.isNewMenu = true;

        this.element
            .addClass("ui-autocomplete-input")
            .attr("autocomplete", "off");

        this._on(this.element, {
            keydown: function(event) {
                if (this.element.prop("readOnly")) {
                    suppressKeyPress = true;
                    suppressInput = true;
                    suppressKeyPressRepeat = true;
                    return;
                }

                suppressKeyPress = false;
                suppressInput = false;
                suppressKeyPressRepeat = false;
                var keyCode = $.ui.keyCode;
                switch (event.keyCode) {
                    case keyCode.PAGE_UP:
                        suppressKeyPress = true;
                        this._move("previousPage", event);
                        break;
                    case keyCode.PAGE_DOWN:
                        suppressKeyPress = true;
                        this._move("nextPage", event);
                        break;
                    case keyCode.UP:
                        suppressKeyPress = true;
                        this._keyEvent("previous", event);
                        break;
                    case keyCode.DOWN:
                        suppressKeyPress = true;
                        this._keyEvent("next", event);
                        break;
                    case keyCode.ENTER:
                        // when menu is open and has focus
                        if (this.menu.active) {
                            // #6055 - Opera still allows the keypress to occur
                            // which causes forms to submit
                            suppressKeyPress = true;
                            event.preventDefault();
                            this.menu.select(event);
                        }
                        break;
                    case keyCode.TAB:
                        if (this.menu.active) {
                            this.menu.select(event);
                        }
                        break;
                    case keyCode.ESCAPE:
                        if (this.menu.element.is(":visible")) {
                            if (!this.isMultiLine) {
                                this._value(this.term);
                            }
                            this.close(event);
                            // Different browsers have different default behavior for escape
                            // Single press can mean undo or clear
                            // Double press in IE means clear the whole form
                            event.preventDefault();
                        }
                        break;
                    default:
                        suppressKeyPressRepeat = true;
                        // search timeout should be triggered before the input value is changed
                        this._searchTimeout(event);
                        break;
                }
            },
            keypress: function(event) {
                if (suppressKeyPress) {
                    suppressKeyPress = false;
                    if (!this.isMultiLine || this.menu.element.is(":visible")) {
                        event.preventDefault();
                    }
                    return;
                }
                if (suppressKeyPressRepeat) {
                    return;
                }

                // replicate some key handlers to allow them to repeat in Firefox and Opera
                var keyCode = $.ui.keyCode;
                switch (event.keyCode) {
                    case keyCode.PAGE_UP:
                        this._move("previousPage", event);
                        break;
                    case keyCode.PAGE_DOWN:
                        this._move("nextPage", event);
                        break;
                    case keyCode.UP:
                        this._keyEvent("previous", event);
                        break;
                    case keyCode.DOWN:
                        this._keyEvent("next", event);
                        break;
                }
            },
            input: function(event) {
                if (suppressInput) {
                    suppressInput = false;
                    event.preventDefault();
                    return;
                }
                this._searchTimeout(event);
            },
            focus: function() {
                this.selectedItem = null;
                this.previous = this._value();
            },
            blur: function(event) {
                if (this.cancelBlur) {
                    delete this.cancelBlur;
                    return;
                }

                clearTimeout(this.searching);
                this.close(event);
                this._change(event);
            }
        });

        this._initSource();
        this.menu = $("<ul>")
            .addClass("ui-autocomplete ui-front")
            .appendTo(this._appendTo())
            .menu({
                // disable ARIA support, the live region takes care of that
                role: null
            })
            .hide()
            .menu("instance");

        this._on(this.menu.element, {
            mousedown: function(event) {
                // prevent moving focus out of the text field
                event.preventDefault();

                // IE doesn't prevent moving focus even with event.preventDefault()
                // so we set a flag to know when we should ignore the blur event
                this.cancelBlur = true;
                this._delay(function() {
                    delete this.cancelBlur;
                });

                // clicking on the scrollbar causes focus to shift to the body
                // but we can't detect a mouseup or a click immediately afterward
                // so we have to track the next mousedown and close the menu if
                // the user clicks somewhere outside of the autocomplete
                var menuElement = this.menu.element[0];
                if (!$(event.target).closest(".ui-menu-item").length) {
                    this._delay(function() {
                        var that = this;
                        this.document.one("mousedown", function(event) {
                            if (event.target !== that.element[0] &&
                                event.target !== menuElement &&
                                !$.contains(menuElement, event.target)) {
                                that.close();
                            }
                        });
                    });
                }
            },
            menufocus: function(event, ui) {
                var label, item;
                // support: Firefox
                // Prevent accidental activation of menu items in Firefox (#7024 #9118)
                if (this.isNewMenu) {
                    this.isNewMenu = false;
                    if (event.originalEvent && /^mouse/.test(event.originalEvent.type)) {
                        this.menu.blur();

                        this.document.one("mousemove", function() {
                            $(event.target).trigger(event.originalEvent);
                        });

                        return;
                    }
                }

                item = ui.item.data("ui-autocomplete-item");
                if (false !== this._trigger("focus", event, {
                        item: item
                    })) {
                    // use value to match what will end up in the input, if it was a key event
                    if (event.originalEvent && /^key/.test(event.originalEvent.type)) {
                        this._value(item.value);
                    }
                }

                // Announce the value in the liveRegion
                label = ui.item.attr("aria-label") || item.value;
                if (label && $.trim(label).length) {
                    this.liveRegion.children().hide();
                    $("<div>").text(label).appendTo(this.liveRegion);
                }
            },
            menuselect: function(event, ui) {
                var item = ui.item.data("ui-autocomplete-item"),
                    previous = this.previous;

                // only trigger when focus was lost (click on menu)
                if (this.element[0] !== this.document[0].activeElement) {
                    this.element.focus();
                    this.previous = previous;
                    // #6109 - IE triggers two focus events and the second
                    // is asynchronous, so we need to reset the previous
                    // term synchronously and asynchronously :-(
                    this._delay(function() {
                        this.previous = previous;
                        this.selectedItem = item;
                    });
                }

                if (false !== this._trigger("select", event, {
                        item: item
                    })) {
                    this._value(item.value);
                }
                // reset the term after the select event
                // this allows custom select handling to work properly
                this.term = this._value();

                this.close(event);
                this.selectedItem = item;
            }
        });

        this.liveRegion = $("<span>", {
                role: "status",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            })
            .addClass("ui-helper-hidden-accessible")
            .appendTo(this.document[0].body);

        // turning off autocomplete prevents the browser from remembering the
        // value when navigating through history, so we re-enable autocomplete
        // if the page is unloaded before the widget is destroyed. #7790
        this._on(this.window, {
            beforeunload: function() {
                this.element.removeAttr("autocomplete");
            }
        });
    },

    _destroy: function() {
        clearTimeout(this.searching);
        this.element
            .removeClass("ui-autocomplete-input")
            .removeAttr("autocomplete");
        this.menu.element.remove();
        this.liveRegion.remove();
    },

    _setOption: function(key, value) {
        this._super(key, value);
        if (key === "source") {
            this._initSource();
        }
        if (key === "appendTo") {
            this.menu.element.appendTo(this._appendTo());
        }
        if (key === "disabled" && value && this.xhr) {
            this.xhr.abort();
        }
    },

    _appendTo: function() {
        var element = this.options.appendTo;

        if (element) {
            element = element.jquery || element.nodeType ?
                $(element) :
                this.document.find(element).eq(0);
        }

        if (!element || !element[0]) {
            element = this.element.closest(".ui-front");
        }

        if (!element.length) {
            element = this.document[0].body;
        }
        return element;
    },

    _initSource: function() {
        var array, url,
            that = this;
        if ($.isArray(this.options.source)) {
            array = this.options.source;
            this.source = function(request, response) {
                response($.ui.autocomplete.filter(array, request.term));
            };
        } else if (typeof this.options.source === "string") {
            url = this.options.source;
            this.source = function(request, response) {
                if (that.xhr) {
                    that.xhr.abort();
                }
                that.xhr = $.ajax({
                    url: url,
                    data: request,
                    dataType: "json",
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function(data) {
                        response(data);
                    },
                    error: function() {
                        response([]);
                    }
                });
            };
        } else {
            this.source = this.options.source;
        }
    },

    _searchTimeout: function(event) {
        clearTimeout(this.searching);
        this.searching = this._delay(function() {

            // Search if the value has changed, or if the user retypes the same value (see #7434)
            var equalValues = this.term === this._value(),
                menuVisible = this.menu.element.is(":visible"),
                modifierKey = event.altKey || event.ctrlKey || event.metaKey || event.shiftKey;

            if (!equalValues || (equalValues && !menuVisible && !modifierKey)) {
                this.selectedItem = null;
                this.search(null, event);
            }
        }, this.options.delay);
    },

    search: function(value, event) {
        value = value != null ? value : this._value();

        // always save the actual value, not the one passed as an argument
        this.term = this._value();

        if (value.length < this.options.minLength) {
            return this.close(event);
        }

        if (this._trigger("search", event) === false) {
            return;
        }

        return this._search(value);
    },

    _search: function(value) {
        this.pending++;
        this.element.addClass("ui-autocomplete-loading");
        this.cancelSearch = false;

        this.source({
            term: value
        }, this._response());
    },

    _response: function() {
        var index = ++this.requestIndex;

        return $.proxy(function(content) {
            if (index === this.requestIndex) {
                this.__response(content);
            }

            this.pending--;
            if (!this.pending) {
                this.element.removeClass("ui-autocomplete-loading");
            }
        }, this);
    },

    __response: function(content) {
        if (content) {
            content = this._normalize(content);
        }
        this._trigger("response", null, {
            content: content
        });
        if (!this.options.disabled && content && content.length && !this.cancelSearch) {
            this._hideAllResults();
            this._suggest(content);
            this._trigger("open");
        } else {
            // use ._close() instead of .close() so we don't cancel future searches
            this._close();
        }
    },

    close: function(event) {
        this.cancelSearch = true;
        this._close(event);
    },

    _close: function(event) {
        if (this.menu.element.is(":visible")) {
            this.menu.element.hide();
            this.menu.blur();
            this.isNewMenu = true;
            this._trigger("close", event);
            this._hideAllResults();
        }
    },

    _change: function(event) {
        if (this.previous !== this._value()) {
            this._trigger("change", event, {
                item: this.selectedItem
            });
        }
    },

    _normalize: function(items) {
        // assume all items have the right format when the first item is complete
        if (items.length && items[0].label && items[0].value) {
            return items;
        }
        return $.map(items, function(item) {
            if (typeof item === "string") {
                return {
                    label: item,
                    value: item
                };
            }
            return $.extend({}, item, {
                label: item.label || item.value,
                value: item.value || item.label
            });
        });
    },

    _suggest: function(items) {
        var ul = this.menu.element.empty();
        this._renderMenu(ul, items);
        this.isNewMenu = true;
        this.menu.refresh();

        // size and position menu
        ul.show();
        this._resizeMenu();
        ul.position($.extend({
            of: this.element
        }, this.options.position));
        this._showAllResults();
        this._getAllResults();
        if (this.options.autoFocus) {
            this.menu.next();
        }
    },

    _resizeMenu: function() {
        var ul = this.menu.element;
        var input = this.bindings[0];
        var showAll = this.showAll;
        ul.outerWidth(Math.max(
            ul.width(input.offsetWidth - 3).outerWidth() + 1,
            this.element.outerWidth()
        ));
    },

    _showAllResults: function() {
        var ul = this.menu.element;
        var showAll = '<div class="ui-autocomplete__after"><button type="submit">Все результаты поиска</button></div>';
        ul.after(showAll);
        ul.next().css({
            display: 'block',
            width: ul.width() + 3,
            top: ul.offset().top + ul.height(),
            left: ul.offset().left - 1
        });
    },

    _getAllResults: function() {
        var ul = this.menu.element;
        var showAll = ul.next();
        var form = this.bindings.parents('form').get(0);
        showAll.mousedown(function(e) {
            form.submit();
        });
    },

    _hideAllResults: function() {
        var ul = this.menu.element;
        ul.next().remove();
    },

    _renderMenu: function(ul, items) {
        var that = this;
        $.each( items, function( index, item ) {
            that._renderItemData( ul, item );
        });
    },

    _renderItemData: function( ul, item ) {
        return this._renderItem( ul, item ).data( "ui-autocomplete-item", item );
    },

    _renderItem: function( ul, item ) {
        return $( "<li>" ).text( item.label ).appendTo( ul );
    },

    _move: function( direction, event ) {
        if ( !this.menu.element.is( ":visible" ) ) {
            this.search( null, event );
            return;
        }
        if ( this.menu.isFirstItem() && /^previous/.test( direction ) ||
            this.menu.isLastItem() && /^next/.test( direction ) ) {

            if ( !this.isMultiLine ) {
                this._value( this.term );
            }

            this.menu.blur();
            return;
        }
        this.menu[ direction ]( event );
    },

    widget: function() {
        return this.menu.element;
    },

    _value: function() {
        return this.valueMethod.apply( this.element, arguments );
    },

    _keyEvent: function( keyEvent, event ) {
        if ( !this.isMultiLine || this.menu.element.is( ":visible" ) ) {
            this._move( keyEvent, event );

            // prevents moving cursor to beginning/end of the text field in some browsers
            event.preventDefault();
        }
    }
});

$.extend( $.ui.autocomplete, {
    escapeRegex: function( value ) {
        return value.replace( /[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&" );
    },
    filter: function( array, term ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( term ), "i" );
        return $.grep( array, function( value ) {
            return matcher.test( value.label || value.value || value );
        });
    }
});

// live region extension, adding a `messages` option
// NOTE: This is an experimental API. We are still investigating
// a full solution for string manipulation and internationalization.
$.widget( "ui.autocomplete", $.ui.autocomplete, {
    options: {
        messages: {
            noResults: "No search results.",
            results: function( amount ) {
                return amount + ( amount > 1 ? " results are" : " result is" ) +
                    " available, use up and down arrow keys to navigate.";
            }
        }
    },

    __response: function( content ) {
        var message;
        this._superApply( arguments );
        if ( this.options.disabled || this.cancelSearch ) {
            return;
        }
        if ( content && content.length ) {
            message = this.options.messages.results( content.length );
        } else {
            message = this.options.messages.noResults;
        }
        this.liveRegion.children().hide();
        $( "<div>" ).text( message ).appendTo( this.liveRegion );
    }
});

var autocomplete = $.ui.autocomplete;
/* ..\..\desktop.blocks\ui-autocomplete\ui-autocomplete.js end */
;
/* ..\..\desktop.blocks\ui-menu\ui-menu.js begin */
/*!
 * jQuery UI Menu 1.11.3
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/menu/
 */


var menu = $.widget( "ui.menu", {
    version: "1.11.3",
    defaultElement: "<ul>",
    delay: 300,
    options: {
        icons: {
            submenu: "ui-icon-carat-1-e"
        },
        items: "> *",
        menus: "ul",
        position: {
            my: "left-1 top",
            at: "right top"
        },
        role: "menu",

        // callbacks
        blur: null,
        focus: null,
        select: null
    },

    _create: function() {
        this.activeMenu = this.element;

        // Flag used to prevent firing of the click handler
        // as the event bubbles up through nested menus
        this.mouseHandled = false;
        this.element
            .uniqueId()
            .addClass( "ui-menu ui-widget ui-widget-content" )
            .toggleClass( "ui-menu-icons", !!this.element.find( ".ui-icon" ).length )
            .attr({
                role: this.options.role,
                tabIndex: 0
            });

        if ( this.options.disabled ) {
            this.element
                .addClass( "ui-state-disabled" )
                .attr( "aria-disabled", "true" );
        }

        this._on({
            // Prevent focus from sticking to links inside menu after clicking
            // them (focus should always stay on UL during navigation).
            "mousedown .ui-menu-item": function( event ) {
                event.preventDefault();
            },
            "click .ui-menu-item": function( event ) {
                var target = $( event.target );
                if ( !this.mouseHandled && target.not( ".ui-state-disabled" ).length ) {
                    this.select( event );

                    // Only set the mouseHandled flag if the event will bubble, see #9469.
                    if ( !event.isPropagationStopped() ) {
                        this.mouseHandled = true;
                    }

                    // Open submenu on click
                    if ( target.has( ".ui-menu" ).length ) {
                        this.expand( event );
                    } else if ( !this.element.is( ":focus" ) && $( this.document[ 0 ].activeElement ).closest( ".ui-menu" ).length ) {

                        // Redirect focus to the menu
                        this.element.trigger( "focus", [ true ] );

                        // If the active item is on the top level, let it stay active.
                        // Otherwise, blur the active item since it is no longer visible.
                        if ( this.active && this.active.parents( ".ui-menu" ).length === 1 ) {
                            clearTimeout( this.timer );
                        }
                    }
                }
            },
            "mouseenter .ui-menu-item": function( event ) {
                // Ignore mouse events while typeahead is active, see #10458.
                // Prevents focusing the wrong item when typeahead causes a scroll while the mouse
                // is over an item in the menu
                if ( this.previousFilter ) {
                    return;
                }
                var target = $( event.currentTarget );
                // Remove ui-state-active class from siblings of the newly focused menu item
                // to avoid a jump caused by adjacent elements both having a class with a border
                target.siblings( ".ui-state-active" ).removeClass( "ui-state-active" );
                this.focus( event, target );
            },
            mouseleave: "collapseAll",
            "mouseleave .ui-menu": "collapseAll",
            focus: function( event, keepActiveItem ) {
                // If there's already an active item, keep it active
                // If not, activate the first item
                var item = this.active || this.element.find( this.options.items ).eq( 0 );

                if ( !keepActiveItem ) {
                    this.focus( event, item );
                }
            },
            blur: function( event ) {
                this._delay(function() {
                    if ( !$.contains( this.element[0], this.document[0].activeElement ) ) {
                        this.collapseAll( event );
                    }
                });
            },
            keydown: "_keydown"
        });

        this.refresh();

        // Clicks outside of a menu collapse any open menus
        this._on( this.document, {
            click: function( event ) {
                if ( this._closeOnDocumentClick( event ) ) {
                    this.collapseAll( event );
                }

                // Reset the mouseHandled flag
                this.mouseHandled = false;
            }
        });
    },

    _destroy: function() {
        // Destroy (sub)menus
        this.element
            .removeAttr( "aria-activedescendant" )
            .find( ".ui-menu" ).addBack()
            .removeClass( "ui-menu ui-widget ui-widget-content ui-menu-icons ui-front" )
            .removeAttr( "role" )
            .removeAttr( "tabIndex" )
            .removeAttr( "aria-labelledby" )
            .removeAttr( "aria-expanded" )
            .removeAttr( "aria-hidden" )
            .removeAttr( "aria-disabled" )
            .removeUniqueId()
            .show();

        // Destroy menu items
        this.element.find( ".ui-menu-item" )
            .removeClass( "ui-menu-item" )
            .removeAttr( "role" )
            .removeAttr( "aria-disabled" )
            .removeUniqueId()
            .removeClass( "ui-state-hover" )
            .removeAttr( "tabIndex" )
            .removeAttr( "role" )
            .removeAttr( "aria-haspopup" )
            .children().each( function() {
                var elem = $( this );
                if ( elem.data( "ui-menu-submenu-carat" ) ) {
                    elem.remove();
                }
            });

        // Destroy menu dividers
        this.element.find( ".ui-menu-divider" ).removeClass( "ui-menu-divider ui-widget-content" );
    },

    _keydown: function( event ) {
        var match, prev, character, skip,
            preventDefault = true;

        switch ( event.keyCode ) {
            case $.ui.keyCode.PAGE_UP:
                this.previousPage( event );
                break;
            case $.ui.keyCode.PAGE_DOWN:
                this.nextPage( event );
                break;
            case $.ui.keyCode.HOME:
                this._move( "first", "first", event );
                break;
            case $.ui.keyCode.END:
                this._move( "last", "last", event );
                break;
            case $.ui.keyCode.UP:
                this.previous( event );
                break;
            case $.ui.keyCode.DOWN:
                this.next( event );
                break;
            case $.ui.keyCode.LEFT:
                this.collapse( event );
                break;
            case $.ui.keyCode.RIGHT:
                if ( this.active && !this.active.is( ".ui-state-disabled" ) ) {
                    this.expand( event );
                }
                break;
            case $.ui.keyCode.ENTER:
            case $.ui.keyCode.SPACE:
                this._activate( event );
                break;
            case $.ui.keyCode.ESCAPE:
                this.collapse( event );
                break;
            default:
                preventDefault = false;
                prev = this.previousFilter || "";
                character = String.fromCharCode( event.keyCode );
                skip = false;

                clearTimeout( this.filterTimer );

                if ( character === prev ) {
                    skip = true;
                } else {
                    character = prev + character;
                }

                match = this._filterMenuItems( character );
                match = skip && match.index( this.active.next() ) !== -1 ?
                    this.active.nextAll( ".ui-menu-item" ) :
                    match;

                // If no matches on the current filter, reset to the last character pressed
                // to move down the menu to the first item that starts with that character
                if ( !match.length ) {
                    character = String.fromCharCode( event.keyCode );
                    match = this._filterMenuItems( character );
                }

                if ( match.length ) {
                    this.focus( event, match );
                    this.previousFilter = character;
                    this.filterTimer = this._delay(function() {
                        delete this.previousFilter;
                    }, 1000 );
                } else {
                    delete this.previousFilter;
                }
        }

        if ( preventDefault ) {
            event.preventDefault();
        }
    },

    _activate: function( event ) {
        if ( !this.active.is( ".ui-state-disabled" ) ) {
            if ( this.active.is( "[aria-haspopup='true']" ) ) {
                this.expand( event );
            } else {
                this.select( event );
            }
        }
    },

    refresh: function() {
        var menus, items,
            that = this,
            icon = this.options.icons.submenu,
            submenus = this.element.find( this.options.menus );

        this.element.toggleClass( "ui-menu-icons", !!this.element.find( ".ui-icon" ).length );

        // Initialize nested menus
        submenus.filter( ":not(.ui-menu)" )
            .addClass( "ui-menu ui-widget ui-widget-content ui-front" )
            .hide()
            .attr({
                role: this.options.role,
                "aria-hidden": "true",
                "aria-expanded": "false"
            })
            .each(function() {
                var menu = $( this ),
                    item = menu.parent(),
                    submenuCarat = $( "<span>" )
                        .addClass( "ui-menu-icon ui-icon " + icon )
                        .data( "ui-menu-submenu-carat", true );

                item
                    .attr( "aria-haspopup", "true" )
                    .prepend( submenuCarat );
                menu.attr( "aria-labelledby", item.attr( "id" ) );
            });

        menus = submenus.add( this.element );
        items = menus.find( this.options.items );

        // Initialize menu-items containing spaces and/or dashes only as dividers
        items.not( ".ui-menu-item" ).each(function() {
            var item = $( this );
            if ( that._isDivider( item ) ) {
                item.addClass( "ui-widget-content ui-menu-divider" );
            }
        });

        // Don't refresh list items that are already adapted
        items.not( ".ui-menu-item, .ui-menu-divider" )
            .addClass( "ui-menu-item" )
            .uniqueId()
            .attr({
                tabIndex: -1,
                role: this._itemRole()
            });

        // Add aria-disabled attribute to any disabled menu item
        items.filter( ".ui-state-disabled" ).attr( "aria-disabled", "true" );

        // If the active item has been removed, blur the menu
        if ( this.active && !$.contains( this.element[ 0 ], this.active[ 0 ] ) ) {
            this.blur();
        }
    },

    _itemRole: function() {
        return {
            menu: "menuitem",
            listbox: "option"
        }[ this.options.role ];
    },

    _setOption: function( key, value ) {
        if ( key === "icons" ) {
            this.element.find( ".ui-menu-icon" )
                .removeClass( this.options.icons.submenu )
                .addClass( value.submenu );
        }
        if ( key === "disabled" ) {
            this.element
                .toggleClass( "ui-state-disabled", !!value )
                .attr( "aria-disabled", value );
        }
        this._super( key, value );
    },

    focus: function( event, item ) {
        var nested, focused;
        this.blur( event, event && event.type === "focus" );

        this._scrollIntoView( item );

        this.active = item.first();
        focused = this.active.addClass( "ui-state-focus" ).removeClass( "ui-state-active" );
        // Only update aria-activedescendant if there's a role
        // otherwise we assume focus is managed elsewhere
        if ( this.options.role ) {
            this.element.attr( "aria-activedescendant", focused.attr( "id" ) );
        }

        // Highlight active parent menu item, if any
        this.active
            .parent()
            .closest( ".ui-menu-item" )
            .addClass( "ui-state-active" );

        if ( event && event.type === "keydown" ) {
            this._close();
        } else {
            this.timer = this._delay(function() {
                this._close();
            }, this.delay );
        }

        nested = item.children( ".ui-menu" );
        if ( nested.length && event && ( /^mouse/.test( event.type ) ) ) {
            this._startOpening(nested);
        }
        this.activeMenu = item.parent();

        this._trigger( "focus", event, { item: item } );
    },

    _scrollIntoView: function( item ) {
        var borderTop, paddingTop, offset, scroll, elementHeight, itemHeight;
        if ( this._hasScroll() ) {
            borderTop = parseFloat( $.css( this.activeMenu[0], "borderTopWidth" ) ) || 0;
            paddingTop = parseFloat( $.css( this.activeMenu[0], "paddingTop" ) ) || 0;
            offset = item.offset().top - this.activeMenu.offset().top - borderTop - paddingTop;
            scroll = this.activeMenu.scrollTop();
            elementHeight = this.activeMenu.height();
            itemHeight = item.outerHeight();

            if ( offset < 0 ) {
                this.activeMenu.scrollTop( scroll + offset );
            } else if ( offset + itemHeight > elementHeight ) {
                this.activeMenu.scrollTop( scroll + offset - elementHeight + itemHeight );
            }
        }
    },

    blur: function( event, fromFocus ) {
        if ( !fromFocus ) {
            clearTimeout( this.timer );
        }

        if ( !this.active ) {
            return;
        }

        this.active.removeClass( "ui-state-focus" );
        this.active = null;

        this._trigger( "blur", event, { item: this.active } );
    },

    _startOpening: function( submenu ) {
        clearTimeout( this.timer );

        // Don't open if already open fixes a Firefox bug that caused a .5 pixel
        // shift in the submenu position when mousing over the carat icon
        if ( submenu.attr( "aria-hidden" ) !== "true" ) {
            return;
        }

        this.timer = this._delay(function() {
            this._close();
            this._open( submenu );
        }, this.delay );
    },

    _open: function( submenu ) {
        var position = $.extend({
            of: this.active
        }, this.options.position );

        clearTimeout( this.timer );
        this.element.find( ".ui-menu" ).not( submenu.parents( ".ui-menu" ) )
            .hide()
            .attr( "aria-hidden", "true" );

        submenu
            .show()
            .removeAttr( "aria-hidden" )
            .attr( "aria-expanded", "true" )
            .position( position );
    },

    collapseAll: function( event, all ) {
        clearTimeout( this.timer );
        this.timer = this._delay(function() {
            // If we were passed an event, look for the submenu that contains the event
            var currentMenu = all ? this.element :
                $( event && event.target ).closest( this.element.find( ".ui-menu" ) );

            // If we found no valid submenu ancestor, use the main menu to close all sub menus anyway
            if ( !currentMenu.length ) {
                currentMenu = this.element;
            }

            this._close( currentMenu );

            this.blur( event );
            this.activeMenu = currentMenu;
        }, this.delay );
    },

    // With no arguments, closes the currently active menu - if nothing is active
    // it closes all menus.  If passed an argument, it will search for menus BELOW
    _close: function( startMenu ) {
        if ( !startMenu ) {
            startMenu = this.active ? this.active.parent() : this.element;
        }

        startMenu
            .find( ".ui-menu" )
            .hide()
            .attr( "aria-hidden", "true" )
            .attr( "aria-expanded", "false" )
            .end()
            .find( ".ui-state-active" ).not( ".ui-state-focus" )
            .removeClass( "ui-state-active" );
    },

    _closeOnDocumentClick: function( event ) {
        return !$( event.target ).closest( ".ui-menu" ).length;
    },

    _isDivider: function( item ) {

        // Match hyphen, em dash, en dash
        return !/[^\-\u2014\u2013\s]/.test( item.text() );
    },

    collapse: function( event ) {
        var newItem = this.active &&
            this.active.parent().closest( ".ui-menu-item", this.element );
        if ( newItem && newItem.length ) {
            this._close();
            this.focus( event, newItem );
        }
    },

    expand: function( event ) {
        var newItem = this.active &&
            this.active
                .children( ".ui-menu " )
                .find( this.options.items )
                .first();

        if ( newItem && newItem.length ) {
            this._open( newItem.parent() );

            // Delay so Firefox will not hide activedescendant change in expanding submenu from AT
            this._delay(function() {
                this.focus( event, newItem );
            });
        }
    },

    next: function( event ) {
        this._move( "next", "first", event );
    },

    previous: function( event ) {
        this._move( "prev", "last", event );
    },

    isFirstItem: function() {
        return this.active && !this.active.prevAll( ".ui-menu-item" ).length;
    },

    isLastItem: function() {
        return this.active && !this.active.nextAll( ".ui-menu-item" ).length;
    },

    _move: function( direction, filter, event ) {
        var next;
        if ( this.active ) {
            if ( direction === "first" || direction === "last" ) {
                next = this.active
                    [ direction === "first" ? "prevAll" : "nextAll" ]( ".ui-menu-item" )
                    .eq( -1 );
            } else {
                next = this.active
                    [ direction + "All" ]( ".ui-menu-item" )
                    .eq( 0 );
            }
        }
        if ( !next || !next.length || !this.active ) {
            next = this.activeMenu.find( this.options.items )[ filter ]();
        }

        this.focus( event, next );
    },

    nextPage: function( event ) {
        var item, base, height;

        if ( !this.active ) {
            this.next( event );
            return;
        }
        if ( this.isLastItem() ) {
            return;
        }
        if ( this._hasScroll() ) {
            base = this.active.offset().top;
            height = this.element.height();
            this.active.nextAll( ".ui-menu-item" ).each(function() {
                item = $( this );
                return item.offset().top - base - height < 0;
            });

            this.focus( event, item );
        } else {
            this.focus( event, this.activeMenu.find( this.options.items )
                [ !this.active ? "first" : "last" ]() );
        }
    },

    previousPage: function( event ) {
        var item, base, height;
        if ( !this.active ) {
            this.next( event );
            return;
        }
        if ( this.isFirstItem() ) {
            return;
        }
        if ( this._hasScroll() ) {
            base = this.active.offset().top;
            height = this.element.height();
            this.active.prevAll( ".ui-menu-item" ).each(function() {
                item = $( this );
                return item.offset().top - base + height > 0;
            });

            this.focus( event, item );
        } else {
            this.focus( event, this.activeMenu.find( this.options.items ).first() );
        }
    },

    _hasScroll: function() {
        return this.element.outerHeight() < this.element.prop( "scrollHeight" );
    },

    select: function( event ) {
        // TODO: It should never be possible to not have an active item at this
        // point, but the tests don't trigger mouseenter before click.
        this.active = this.active || $( event.target ).closest( ".ui-menu-item" );
        var ui = { item: this.active };
        if ( !this.active.has( ".ui-menu" ).length ) {
            this.collapseAll( event, true );
        }
        this._trigger( "select", event, ui );
    },

    _filterMenuItems: function(character) {
        var escapedCharacter = character.replace( /[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&" ),
            regex = new RegExp( "^" + escapedCharacter, "i" );

        return this.activeMenu
            .find( this.options.items )

            // Only match on items, not dividers or other content (#10571)
            .filter( ".ui-menu-item" )
            .filter(function() {
                return regex.test( $.trim( $( this ).text() ) );
            });
    }
});
/* ..\..\desktop.blocks\ui-menu\ui-menu.js end */
;
function checkForNumbers(field){
    var corrected=field.value.replace(/\D+/,"");
    if(field.value!=corrected)field.value=corrected
}
