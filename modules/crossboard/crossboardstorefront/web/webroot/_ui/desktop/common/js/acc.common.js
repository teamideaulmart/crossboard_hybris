ACC.common = {
    currentCurrency: "USD",
    $page: $("#page"),
    cssAvailability: false,
    localStylesDownloaded: false,
    headerFooterIsInserted: false,

    setCurrentCurrency: function ()
    {
        ACC.common.currentCurrency = ACC.common.$page.data("currencyIsoCode");
    },

    refreshScreenReaderBuffer: function ()
    {
        // changes a value in a hidden form field in order
        // to trigger a buffer update in a screen reader
        $('#accesibility_refreshScreenReaderBufferField').attr('value', new Date().getTime());
    },

    bindAll: function ()
    {
        ACC.common.bindToUiCarouselLink();
        ACC.common.bindShowProcessingMessageToSubmitButton();
    },

    bindToUiCarouselLink: function ()
    {
        $("ul.carousel > li a.popup").colorbox({
            onComplete: function ()
            {
                ACC.common.refreshScreenReaderBuffer();
                $.colorbox.resize()
                ACC.product.initQuickviewLightbox();
            },

            onClosed: function ()
            {
                ACC.common.refreshScreenReaderBuffer();
            }
        });
    },

    processingMessage: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif'/>"),

    bindShowProcessingMessageToSubmitButton: function ()
    {

        $(':submit.show_processing_message').each(function ()
        {
            $(this).on("click", ACC.common.showProcessingMessageAndBlockForm)
        });
    },

    showProcessingMessageAndBlockForm: function ()
    {
        $("#checkoutContentPanel").block({ message: ACC.common.processingMessage });
    },

    blockFormAndShowProcessingMessage: function (submitButton)
    {
        var form = submitButton.parents('form:first');
        form.block({ message: ACC.common.processingMessage });
    },

    showSpinnerById: function(id) {
        $('#'+id).show();
    },

    hideSpinnerById: function(id) {
        $('#'+id).hide();
    },
    headerHtml: "",
    footerHtml: "",
    header: "",
    footer: "",

    getUlmartData: function() {
        var headerIsReceived = false;
        var watingTimer;
        var timeout = ACC.config.ulmartHeaderWatingTimeout == "" ? 10000 : ACC.config.ulmartHeaderWatingTimeout;
        console.log("Receiving header/footer form Ulmart timeout set to " + timeout);
        $.ajax({
            type: 'get',
            url: ACC.config.encodedContextPath + '/ulmart/getdata',
            dataType: 'json',
            withCredentials: true,
            beforeSend: function() {
                watingTimer = setTimeout(function() {
                    if (!headerIsReceived) {
                        ACC.common.showBackupHeaderFooter();
                        headerIsReceived = true;
                    }
                },  timeout);
            },
            success: function(data) {
                if (!headerIsReceived) {
                    headerIsReceived = true;
                    clearTimeout(watingTimer);
                    console.log("Ulmart header-footer is received");
                    if (data != "") {
                        try {
                            var json = JSON.parse(data);

                            ACC.common.headerHtml = json.header;
                            ACC.common.footerHtml = json.footer;
                            ACC.common.tokenHeader = json._csrf_header;
                            ACC.common.token = json._csrf;

                            ACC.common.changeLinkAttrReg("data-qtip-href");
                            ACC.common.changeLinkAttrReg("data-href");
                            ACC.common.changeLinkAttrReg("href");
                            ACC.common.changeLinkAttrReg("src");
                            ACC.common.changeLinkAttrReg("title");

                            ACC.common.headerHtml = ACC.common.headerHtml.replace(new RegExp("target=\/getHeaderStatic", 'g'), "target=\/");

                            ACC.common.headerHtml = $.parseHTML(ACC.common.headerHtml);
                            ACC.common.footerHtml = $.parseHTML(ACC.common.footerHtml);

                            ACC.common.csrf(ACC.common.tokenHeader, ACC.common.token);
	                        var $headerHelpLinks = $(ACC.common.headerHtml).find('a[href*="ulmart.ru/help"]');

	                        $headerHelpLinks.each(function(index, item) {
                                var helpTargetPage = $(item).attr('href').split('#')[1];
                                var helpTargetPageUrl = '/help#' + helpTargetPage;
		                        $(item).attr('href',helpTargetPageUrl);
                            });

                            var m1 = $(ACC.common.headerHtml).find('.must_be_href'), m2 = $(ACC.common.footerHtml).find('.must_be_href'), mustBeHref = new Array();

                            for (var i = 0; i < m1.length; i++) mustBeHref.push(m1[i]);
                            for (var i = 0; i < m2.length; i++) mustBeHref.push(m2[i]);

                            for (var i = 0; i < mustBeHref.length; i++) {
                                mustBeHref[i].outerHTML = mustBeHref[i].outerHTML.replace("i", "a").replace("title", "href");
                            }

                            var headerPart1 = $(ACC.common.headerHtml).find('nav.navbar.navbar_theme_normal.navbar_color_default.navbar_size_sm.navbar-static-top');
                            var headerPart2 = $(ACC.common.headerHtml).find('nav.navbar-static-top.navbar_color_white.navbar_theme_normal.navbar_headermain-responsive');

                            ACC.common.header = headerPart1.prop('outerHTML') + headerPart2.prop('outerHTML');
                            ACC.common.footer = ACC.common.footerHtml;

                            if (ACC.common.cssAvailability || ACC.common.localStylesDownloaded) {
                                ACC.common.insertHeaderFooterHtml();
                            }

                            // gtm dataLayer pushing
                            // if user is authorized

                            if (json.userAuth) {
                                dataLayer.push({
	                                'event': 'pageview',
                                    'cartProductsQuantity': json.cartProductsQuantity,
                                    'cityId': json.cityId == null ? "" : json.cityId.toString(),
                                    'cityName': json.cityName,
                                    'userAuth': json.userAuth == null ? "" : json.userAuth.toString(),
                                    'userBonuses': json.userBonuses == null ? "" : json.userBonuses.toString(),
                                    'userId': json.userId == null ? "" : json.userId.toString(),
                                    'userType': json.userType,
                                    'touchpoint': 'site'
                                });
                            } else {
                                dataLayer.push({
	                                'event': 'pageview',
                                    'cartProductsQuantity': json.cartProductsQuantity,
                                    'cityId': json.cityId == null ? "" : json.cityId.toString(),
                                    'cityName': json.cityName,
                                    'userAuth': json.userAuth == null ? "" : json.userAuth.toString(),
                                    'touchpoint': 'site'
                                });
                            }

                            pushProductsToGA();

                        } catch (e) {
                            console.log('Error ' + e.name + ":" + e.message + "\n" + e.stack);
                            ACC.common.showBackupHeaderFooter();
	                        pushProductsToGA();
                        }
                    } else {
                        ACC.common.showBackupHeaderFooter();
	                    pushProductsToGA();
                    }
                }
            },
            error: function(e) {
                console.error(e);
	            pushProductsToGA();
            }
        });
    },
    csrf: function(tokenHeader, token) {
        //console.log("CSRF header = " + tokenHeader + " CSRF token = " + token);
        if(token == "" || token.length == 0 || tokenHeader == "" || tokenHeader.length == 0) {
            console.log("CSRF metadata was not found");
        } else {
            $(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(tokenHeader, token);
            });
        }
        $("head").prepend("<meta name='_csrf' content='" + token + "'>")
            .prepend("<meta name='_csrf_header' content='"+ tokenHeader + "'>");
    },
    showCrossboardTab: function(){
        if($(".b-page__header .navbar .navbar-left.navbar-nav_sites").length) {
            var $topbar = $(".navbar-static-top:eq(0)");
            var $nav = $topbar.find(".navbar-left.navbar-nav_sites");
            var $navItems = $nav.find("> li:not(.dropdown)");
            var $dropdown = $nav.find("> li.dropdown");
            var $rightbar = $topbar.find(".navbar-right");
            var $cloneNav = $nav.clone();
            var $holder = $nav.parents(".container-fluid");
            $cloneNav.css({
                position: "absolute",
                top: "-10000px",
                left: "0px"
            }).addClass("calc-clone");
            $holder.append($cloneNav);
            $cloneNav.find(" > li").removeClass("hidden");
            var rightBarWidth = $rightbar.width();
            var liWidth = [];
            $cloneNav.find("> li:not(.dropdown)").each(function(i, li){
                liWidth[i] = $(li).width();
            });
            var dropDownWidth = $cloneNav.find("> li.dropdown").width();
            function controlItems(){
                var allWidth = $topbar.find(".container-fluid").width();
                var spaceMargin = 0;
                if (Math.abs(parseInt($rightbar.css("margin-right")))>0) spaceMargin = Math.abs(parseInt($rightbar.css("margin-right")));
                var spaceWidth = parseInt(allWidth - rightBarWidth) + spaceMargin;
                var fillW = 0;
                var breakIndex = 0;
                for(var i=0; i < liWidth.length; i++) {
                    if(fillW + liWidth[i] <= spaceWidth) {
                        fillW += liWidth[i];
                        breakIndex = i + 1;
                    } else {
                        breakIndex = i;
                        break;
                    }
                }
                if(spaceWidth < fillW + dropDownWidth && liWidth.length != breakIndex){
                    breakIndex -= 1;
                }
                $navItems.detach();
                $dropdown.detach();
                if(liWidth.length == breakIndex) {
                    $navItems.appendTo($nav);
                }
                else {
                    $navItems.slice(0, breakIndex).appendTo($nav);
                    $navItems.slice(breakIndex).appendTo($dropdown.find(".dropdown-menu"));
                    $dropdown.appendTo($nav);
                }
            }
            $(window).resize(controlItems);
            controlItems();
            $navItems.removeClass("hidden");
            $dropdown.removeClass("hidden");
            $('li.retail.active').removeClass('active');
            $('li.retail strong').wrap('<a href="https://www.ulmart.ru/" rel="nofollow"></a>');
            $('li.cross-border').addClass('active');
        }
    },
    changeLinkAttrReg: function(attr) {
        ACC.common.headerHtml = ACC.common.headerHtml.replace(new RegExp(attr+"=\"/([^\"^/]+)","g"), attr + "=\""+ACC.config.ulmartDomen+"/$1");
        ACC.common.footerHtml = ACC.common.footerHtml.replace(new RegExp(attr+"=\"/([^\"^/]+)","g"), attr + "=\""+ACC.config.ulmartDomen+"/$1");
    },
    showBackupHeaderFooter: function() {
        console.log('Request for backup header footer');
        $.ajax({
            type: 'get',
            url: ACC.config.encodedContextPath + '/ulmart/getuserdata',
            success: function (data) {
                console.log("Backup header-footer is received");
                ACC.common.header = data.header;
                ACC.common.footer = data.footerCommon;
                if (ACC.common.cssAvailability || ACC.common.localStylesDownloaded) {
                    ACC.common.insertHeaderFooterHtml();
                }
            },
            error: function(e) {
                console.log("ERROR");
            }
        });
    },
    checkUlmartCssJsAvailability: function() {
        $.ajax({
            type: "GET",
            url: ACC.config.ulmartCssUrl,
            success: function() {
                ACC.common.cssAvailability = true;
                if (!ACC.common.headerFooterIsInserted) {
	                ACC.common.insertHeaderFooterHtml();
                }
            },
            error: function(xhr, status, error) {
                console.log("Ulmart css unavailable. Link local styles");
                var link = document.createElement("link");
                link.rel = "stylesheet";
                link.href = ACC.config.commonResourcePath + "/css/common_cb.css";
                link.type = "text/css";
                link.onload = function(){
                    ACC.common.localStylesDownloaded = true;
                    console.log("Local styles has been downloaded");
                    ACC.common.insertHeaderFooterHtml();
                };
                $("head").prepend(link);
            }
        });
    },
    insertHeaderFooterHtml: function() {
        $('.dummy_header').html(ACC.common.header);
        $('.dummy_footer').html(ACC.common.footer);
        ACC.common.showCrossboardTab();
        ACC.common.headerFooterIsInserted = true;
    }
};

$(document).ready(function ()
{
    //ACC.common.setCurrentCurrency();
    //ACC.common.bindAll();
    ACC.common.checkUlmartCssJsAvailability();
    //ACC.common.getUlmartData();
    console.info('document ready');
});

$('#ulmart-frame').ready(function() {
    console.info('iframe ready');
	ACC.common.checkUlmartCssJsAvailability();
    ACC.common.getUlmartData();
});

/* Extend jquery with a postJSON method */
jQuery.extend({
    postJSON: function (url, data, callback)
    {
        return jQuery.post(url, data, callback, "json");
    }
});

// add a CSRF request token to POST ajax request if its not available
$.ajaxPrefilter(function (options, originalOptions, jqXHR)
{
	// Modify options, control originalOptions, store jqXHR, etc
	if (options.type === "post" || options.type === "POST")
	{
		var noData = (typeof options.data === "undefined");
		if (noData || options.data.indexOf("CSRFToken") === -1)
		{
			options.data = (!noData ? options.data + "&" : "") + "CSRFToken=" + ACC.config.CSRFToken;
		}
	}
});
