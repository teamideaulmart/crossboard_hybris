"use strict";

var tempStorageForDataLayer = {};

$(document).ready(function() {
	//------------------------------------------------------------------------GTM
	// push adblocker param to temp storage
	if( window.useAdBlocker === undefined ) {
		// adblocker detected
		tempStorageForDataLayer.isUseAdblocker = 'on';
	} else {
		// adblocker not used
		tempStorageForDataLayer.isUseAdblocker = 'off';
	}

	// push promotions to temp storage
	if (typeof dataLayer != "undefined" && typeof dataLayerBanners != "undefined") {
		tempStorageForDataLayer.promotions = dataLayerBanners;
	}

	// push tempStorageForDataLayer to dataLayer
	dataLayer.push(tempStorageForDataLayer);

	//gtm events
	// клик по городу в popup выберите город
	$(document).on("click", ".js-gtm-city-choose", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'choose',
			'eventLabel': 'visitorCity',
			'eventContext': $(this).data('gtm-event-context')
		});
	});

	// запуск поиска по сайту
	$(".mainSearch_button").on("click", function () {
		var searchRequest = $('#input_SearchBox').val();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'search',
			'eventContext': searchRequest
		})
	});

	// клик по корзине (открытие корзины)
	$(document).on("click", "#bottom-cart", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'open',
			'eventLabel': 'cart',
			'eventLocation':'footer'
		});
	});

	// клик по кнопке "Узнать" в popup "что с моим заказом"
	$(document).on("click", "#checkMyOrderPopup .gtm-check-order-status", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'checkOrderStatus',
			'eventContext':document.querySelector("#order-number").value
		});
	});

	// клик по кнопке "Узнать", в popup "обратная связь"
	$(document).on("click", "#feedback_modal .gtm-check-order-status", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'checkOrderStatus',
			'eventContext':document.querySelector("#order-number2").value
		});
	});

	// клик по кнопке "Отправить" сообщение в popup "Обратная связь"
	$(document).on("click", "#feedback_modal #buttonNext", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'sendMessage',
			'eventLocation':'sendMessage'
		});
	});

	// клик по кнопке "оформить заказ" в bottom-panel
	$(document).on("click", "#bottomButton", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'open',
			'eventLabel': 'checkout',
			'eventLocation':'footer'
		});
	});

	// клик по пункту меню (каталог товаров)
	$('.js-gtm-catalog-item').on("click", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'mainCatalog',
			'eventContent':$(this).data('gtm-name')
		});
	});

	// клик по кнопкам из разделов меню
	// работает только на локальном хедере
	$(document).on('click','.js-gtm-main-menu-item',function() {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'changeProject',
			'eventContent':$(this).data('gtm-name')
		});
	});

	// запуск видео: "Смотри видео, которое все объясняет"
	$('.js-gtm-video-btn').on("click", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'videoAboutCb'
		});
	});

	// клик по поставщику из блока "Наши поставщики"
	$('.js-gtm-supplier').on("click", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'open',
			'eventLabel': 'sellerSite',
			'eventContent':$(this).data('gtm-name')
		});
	});

	// клик по псевдо ссылкам из блока "Остались вопросы"
	$('.js-gtm-question-link').on("click", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'open',
			'eventLabel': 'help',
			'eventContent': $(this).data('gtm-name')
		});
	});

	// клик по баннеру
	$('.js-gtm-banner').on("click", function () {
		var currentData = $(this).data();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'banner',
			'eventLocation': currentData.gtmType,
			'eventContext': currentData.gtmId,
			'eventContent':currentData.gtmName,
			'eventPosition':currentData.gtmNum
		});
	});

	// клик по товару - открытие страницы товара
	$(".js-gtm-product-item").on("click", function() {
		var currentData = $(this).data();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'open',
			'eventLabel':'productPage',
			'eventLocation':currentData.gtmLocation,
			'eventPosition':currentData.gtmNum,
			'eventProductId': currentData.gtmId,
			'eventProductName': currentData.gtmName,
			'eventCategoryName':currentData.gtmCategoryName,
			'eventCategoryId':currentData.gtmCategoryId,
			'eventVendorName':currentData.gtmVendorName,
			'eventProductPrice':currentData.gtmPrice
		});
	});

	// изменение типа сортировки
	$(".gtm-sort-select").on("change", function() {
		var newValue = $(this).find("option:selected").text();
		var productItemsArray = [];
		$('.js-gtm-product-item').each(function() {
			var data = $(this).data();
			productItemsArray.push({
				'eventPosition':data.gtmNum,
				'eventProductId':data.gtmId,
				'eventProductName':data.gtmName,
				'eventProductTag':'none',
				'eventCategoryId':data.gtmCategoryId,
				'eventCategoryName':data.gtmCategoryName,
				'eventVendorName':data.gtmVendorName,
				'eventProductPrice':data.gtmPrice
			});
		});
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel':'sort',
			'eventLocation':$(this).data('gtm-location'),
			'eventContent': newValue,
			'eventProducts':productItemsArray
		});
	});

	$(document).on("click", ".js-gtm-facet-reset", function () {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'resetAllFilters'
		});
	});

	// клик по кнопке "показать"
	$(document).on("click", ".js-gtm-facet-show-btn", function () {
		var productItemsArray = [],
			container = $(this).closest(".facet"),
			gtmLocation = container.data("gtm-location"),
			gtmName = container.data("gtm-name");
		$('.js-gtm-product-item').each(function() {
			var data = $(this).data();
			productItemsArray.push({
				'eventPosition':data.gtmNum,
				'eventProductId':data.gtmId,
				'eventProductName':data.gtmName,
				'eventProductTag':'none',
				'eventCategoryId':data.gtmCategoryId,
				'eventCategoryName':data.gtmCategoryName,
				'eventVendorName':data.gtmVendorName,
				'eventProductPrice':data.gtmPrice
			});
		});
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'choose',
			'eventLabel': 'useFilter',
			'eventContent':gtmName,
			"eventLocation":gtmLocation,
			'eventProducts':productItemsArray
		});
	});

	// включение/выключение фильтров по товарам
	$(".gtm-facet-checkbox").on('change',function() {
		var state;
		if (this.checked) {
			state='filterOn';
		} else {
			state='filterOff'
		}
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel':state,
			'eventContext':$(this).data('gtm-value'),
			'eventContent':$(this).data('gtm-name')
		});
	});

	// клик по кнопке "Перейти"
	$(".js-gtm-item-go-btn").on("click", function() {
		var productData = $(this).closest('.productItem').find('.js-gtm-product-item').data();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Conversions',
			'eventAction': 'open',
			'eventLabel': 'sellerProductPage',
			'eventLocation':$(this).data("gtm-location"),
			'eventPosition':productData.gtmNum,
			'eventProductId':productData.gtmId,
			'eventProductName':productData.gtmName,
			'eventProductTag':"none",
			'eventCategoryId':productData.gtmCategoryId,
			'eventCategoryName':productData.gtmCategoryName,
			'eventVendorName':productData.gtmVendorName,
			'eventProductPrice':productData.gtmPrice
		});
	});

	// изменение типа отображения товаров в каталоге
	$(".js-gtm-view-type").on("click", function() {
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'open',
			'eventLabel': 'changeProductPhotos',
			'eventContent':$(this).data('gtm-view-type')
		});
	});

	// клик по псевдо-сылке "Из ..." - открытие условий доставки
	$(".js-gtm-item-from").on("click", function() {
		var productdData = $(this).closest('.productItem').find('.js-gtm-product-item').data();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Conversions',
			'eventAction': 'open',
			'eventLabel': 'deliveryFromConditions',
			'eventProductId':productdData.gtmId,
			'eventProductName':productdData.gtmName,
			'eventContent':$(this).data('gtm-country')
		});
	});

	// клик по кнопке "Перейти"
	$(".js-gtm-product-go-btn").on("click", function() {
		var data = $(this).data();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Conversions',
			'eventAction': 'open',
			'eventLabel': 'sellerProductPage',
			'eventLocation':'ProductPage',
			'eventProductId':data.gtmId,
			'eventProductName':data.gtmName,
			'eventProductTag':"none",
			'eventCategoryId':data.gtmCategoryId,
			'eventCategoryName':data.gtmCategoryName,
			'eventVendorName':data.gtmVendorName,
			'eventProductPrice':data.gtmPrice
		});
	});

	// клик по фото товара
	$(".js-gtm-product-image").on("click", function() {
		var data = $(this).data();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'click',
			'eventLabel': 'photo',
			'eventProductId':data.gtmId,
			'eventProductName':data.gtmName
		});
	});

	// клик по кнопке "Подробнее о товарах из-за границы"
	$(".js-gtm-delivery-btn").on("click", function() {
		var data = $(this).data();
		dataLayer.push({
			'event': 'OWOX',
			'eventCategory': 'Interactions',
			'eventAction': 'open',
			'eventLabel': 'help',
			'eventContent':'Подробнее о товарах из-за границы'
		});
	});

	//gtm events###

	//------------------------------------------------------------------------GTM###
});

function pushProductsToGA() {
	if (typeof dataLayer != "undefined" && typeof dataLayerProducts != "undefined") {
		// if we have more than 30 products, split them to chunks by 30 items and push these sub-arrays to dataLayer
		var i, j, subArray, subArrayLength = 30;
		for (i = 0, j = dataLayerProducts.length; i < j; i += subArrayLength) {
			subArray = dataLayerProducts.slice(i, i + subArrayLength);
			dataLayer.push({
				'event': 'OWOX',
				'eventCategory': 'Non-Interactions',
				'eventAction': 'show',
				'eventLabel': 'product',
				'eventProducts': subArray
			});
		}
	}
}