ACC.productDetail = {
    removeImgInDescription: function() {
        $('.productCard_descFull img').remove();
    },
	
	initPageEvents: function ()
	{
		
		$('.productImageGallery .jcarousel-skin').jcarousel({
			vertical: true
		});

		// По клику на картинку товара вызывается фоторама в режиме fullscreen
		// Изначально она скрыта через класс is_hidden
		$('.productImageZoomLink').add('.productImagePrimaryLink').on("click", function(e){
			$('.fotorama').removeClass('is_hidden');
			$('.fotorama').fotorama({
				width:500
			});
			var fotorama = $(".fotorama").data('fotorama');
			fotorama.requestFullScreen();
            fotorama.show($('.productImagePrimary img').attr('data-galleryposition'));
		});
		// Скрываем фотораму при выходе из фулскрина
		$('.fotorama').on('fotorama:fullscreenexit', function (e, fotorama) {
			$(this).addClass('is_hidden');
		});
		// Ширина навигации приравнивается к ширине картинки
		$('.fotorama').on('fotorama:fullscreenenter', function (e, fotorama) {
			setTimeout(function() {
				var currentWidth = $(".fotorama__stage__shaft img").width();
				$(".fotorama__nav").width(currentWidth);
			},100);
		});
		// Ширина навигации приравнивается к ширине картинки при ресайзе
		$(window).on("resize",function() {
			setTimeout(function() {
				var currentWidth = $(".fotorama__stage__shaft img").width();
				$(".fotorama__nav").width(currentWidth);
			},100);
		});
		
		$(".productImageGallery img").click(function(e) {
			$(".productImagePrimary img").attr("src", $(this).attr("data-primaryimagesrc"));
			$(".productImagePrimary img").attr("data-galleryposition", $(this).attr("data-galleryposition"));
			$(".productImageGallery .thumb").removeClass("active");
			$(this).parent(".thumb").addClass("active");
		});


		$(document).on("click","#colorbox .productImageGallery img",function(e) {
			$("#colorbox  .productImagePrimary img").attr("src", $(this).attr("data-zoomurl"));
			$("#colorbox .productImageGallery .thumb").removeClass("active");
			$(this).parent(".thumb").addClass("active");
		});
		
		
		
		$("body").on("keyup", "input[name=qtyInput]", function(event) {
  			var input = $(event.target);
		  	var value = input.val();
		  	var qty_css = 'input[name=qty]';
  			while(input.parent()[0] != document) {
 				input = input.parent();
 				if(input.find(qty_css).length > 0) {
  					input.find(qty_css).val(value);
  					return;
 				}
  			}
		});
		
	


		$("#Size").change(function () {
			var url = "";
			var selectedIndex = 0;
			$("#Size option:selected").each(function () {
				url = $(this).attr('value');
				selectedIndex = $(this).attr("index");
			});
			if (selectedIndex != 0) {
				window.location.href=url;
			}
		});

		$("#variant").change(function () {
			var url = "";
			var selectedIndex = 0;

			$("#variant option:selected").each(function () {
				url = $(this).attr('value');
				selectedIndex = $(this).attr("index");
			});
			if (selectedIndex != 0) {
				window.location.href=url;
			}
		});

		$(".selectPriority").change(function () {
			var url = "";
			var selectedIndex = 0;

			url = $(this).attr('value');
			selectedIndex = $(this).attr("index");

			if (selectedIndex != 0) {
				window.location.href=url;
			}
		});

	}

};

$(document).ready(function ()
{
	with(ACC.productDetail)
	{
		initPageEvents();
        removeImgInDescription();
	}
});

