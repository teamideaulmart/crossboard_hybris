"use strict";

var gulp = require("gulp");
var browserSync = require("browser-sync");
var less = require('gulp-less');
var path = require('path');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var tagsPath = "webroot/WEB-INF/tags/desktop/";
var jspPath = "webroot/WEB-INF/views/desktop/";
var cssPath = "webroot/_ui/desktop/common/css";
var lessPath = "webroot/_ui/desktop/common/less";

gulp.task('less', function () {
	return gulp.src(lessPath + '/compile.less')
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(autoprefixer('last 3 versions'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(cssPath))
		.pipe(browserSync.stream());
});

gulp.task('server', function () {
	browserSync({
		proxy: 'https://localhost:9002/',
		port: 8080
	});
});

gulp.task('reload', function () {
	return gulp.src('')
		.pipe(browserSync.stream());
});

gulp.task('watch', function () {
	gulp.watch(tagsPath + '**/*.tag', ['reload']);
	gulp.watch(jspPath + '**/*.jsp', ['reload']);
	gulp.watch(lessPath + "**/*.less", ['less']);
});

gulp.task('default', ['watch', 'server', 'less']);