# Компиляция less на лету
В web.xml есть фильтр на url = /wro/*
В конфиге wro.xml ( в WEB-INF ) прописаны группы.
По аналогии можно добавлять свои группы.
Например:
    
    <group name="crbd">
       <css>/_ui/desktop/common/css/crbd-less/variables.less</css>
       <css>/_ui/desktop/common/css/crbd-less/compile.less</css>
       <css>/_ui/desktop/common/css/colors.css</css>
       <css>/_ui/desktop/common/css/for-footer.css</css>
       
       <js minimize="false">/_ui/desktop/common/js/jquery.min.js</js>
              <!-- j carousel -->
       <js minimize="false">/_ui/desktop/common/js/jquery.jcarousel-full-source.js</js>
    </group>
    
После того, как данная группа прописана, можно запросить все css одним файлом и все js одним файлом
При это содержимое файлов будет минимизировано, если не указано иное, как для jquery.min.js
Запрашивать нужно по шаблону /wro/ИМЯ_ГРУППЫ.(js или css)
    
    <link rel="stylesheet" href="/wro/crbd.css"/>
    <script language="javascript" src="/wro/crbd.js"><script>