package ru.teamidea.crossboard.b2c.integration.utils.holder;

/**
 * Created by Marina on 23.09.2016.
 */
public class Holder<E> {

    private E E;

    public E getE() {
        return E;
    }

    public void setE(E e) {
        E = e;
    }
}
