package ru.teamidea.crossboard.b2c.integration.handlers;

/**
 * Created by Marina on 13.09.2016.
 */

import ru.ulmart.cb.catalog.ProductDescription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class ProductDescriptionsHandler extends AbstractSingleElementHandler<ProductDescription> {
    private List<String> outerTags;
    private List<String> outerClosedTags;

    public ProductDescriptionsHandler(String fName, int collectionSize) {
        super(fName, collectionSize);

        // outer tags in natural order
        outerTags = Arrays.asList("productDescriptions");
    }

    @Override
    protected String getElementTag() {
        return "productDescription";
    }

    @Override
    protected String getRootTagName() {
        return "uploadProductDescriptionsRequest";
    }

    @Override
    protected List<String> getOuterElementTagList() {
        return outerTags;
    }

    @Override
    protected List<String> getOuterElementClosedTagList() {
        if (null == this.outerClosedTags) {
            outerClosedTags = new ArrayList<>(this.outerTags);

            Collections.reverse(outerClosedTags);
        }

        return this.outerClosedTags;
    }
}