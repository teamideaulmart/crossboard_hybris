package ru.teamidea.crossboard.b2c.integration.transformers;

import org.apache.log4j.Logger;
import org.springframework.messaging.Message;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Timofey Klyubin on 21.09.16.
 */
public class DeleteItemsTransformer {

    private static final Logger LOGGER = Logger.getLogger(DeleteItemsTransformer.class);

    public String transform(final Message<File> message) throws IOException {
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("[%1$s] Calling transformer", message.getPayload().getName()));
        }

        BufferedReader reader = new BufferedReader(new FileReader(message.getPayload()));
        final StringBuilder sb = new StringBuilder();
        reader.lines().forEachOrdered(sb::append);
        return sb.toString();
    }
}
