package ru.teamidea.crossboard.b2c.integration.event.listener;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import org.apache.log4j.Logger;
import ru.teamidea.crossboard.b2c.integration.event.AbstractProductProcessingEvent;

/**
 * Created by Timofey Klyubin on 20.09.16.
 */
public abstract class AbstractProductProcessingEventListener<EVENT extends AbstractProductProcessingEvent>
        extends AbstractEventListener<EVENT> {

    private static final Logger LOG = Logger.getLogger(AbstractProductProcessingEventListener.class);

    @Override
    protected void onEvent(EVENT productProcessingEvent) {
        if (productProcessingEvent == null) {
            throw new NullPointerException("productProcessingEvent is null!");
        }
    }
}
