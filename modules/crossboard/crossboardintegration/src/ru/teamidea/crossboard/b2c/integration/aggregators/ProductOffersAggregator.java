package ru.teamidea.crossboard.b2c.integration.aggregators;

import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.cb.common.CallbackType;

/**
 * Created by Marina on 13.09.2016.
 */
public class ProductOffersAggregator extends AbstractCallbackRequestAggregator {
    private static final long serialVersionUID = 1L;

    @Override
    protected void setCallbackType(CallbackRequest callbackRequest) {
        callbackRequest.setCalbackType(CallbackType.PRODUCTOFFERS);
    }
}