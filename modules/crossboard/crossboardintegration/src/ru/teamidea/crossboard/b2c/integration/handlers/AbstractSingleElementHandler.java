package ru.teamidea.crossboard.b2c.integration.handlers;

/**
 * Created by Marina on 13.09.2016.
 */

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractSingleElementHandler<E> extends AbstractElementHandler {

    public AbstractSingleElementHandler(String fName, int collectionSize) {
        super(fName, collectionSize);
    }

    @Override
    public void doStartElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        // outer tags first
        if (equalTag(qName, getElementTag()) && !getOuterElementTagList().isEmpty()) {
            for (String tagName : getOuterElementTagList()) {
                String prefix = qName.split(":").length > 1 ? qName.split(":")[0] + ":" : "";
                concatToElement("<" + prefix + tagName + ">");
            }
        }

        // other tags
        if (!equalTag(qName, getRootTagName())) {
            if (!inOuterTags(qName)) {
                concatToElement("<" + getTag(qName) + ">");
            }
        }
    }

    @Override
    public void doEndElement(String uri, String localName, String qName) throws SAXException {
        concatToElement(getContent());
        concatToElement("</" + getTag(qName) + ">");
        if (equalTag(qName, getElementTag())) {
            // outer tags
            if (!getOuterElementClosedTagList().isEmpty()) {
                String prefix = qName.split(":").length > 1 ? qName.split(":")[0] + ":" : "";
                for (String tagName : getOuterElementClosedTagList()) {
                    concatToElement("</" + prefix + tagName + ">");
                }
            }

            getElementPartList().add(doFinalize(getElement()));

            if (getElementPartList().size() > getCollectionSize()) {
                fillResult();
                getElementPartList().clear();
            }

            clearElement();
        }

        clearContent();
    }

    private boolean inOuterTags(String qName) {
        qName = qName.split(":").length > 1 ? qName.split(":")[1] : qName;
        return getOuterElementTagList().contains(qName);
    }

    private void fillResult() {
        getElementXMLList().add(new ArrayList<>(getElementPartList()));
    }

    protected abstract String getElementTag();

    protected List<String> getOuterElementTagList() {
        return Collections.emptyList();
    }

    protected List<String> getOuterElementClosedTagList() {
        return Collections.emptyList();
    }
}