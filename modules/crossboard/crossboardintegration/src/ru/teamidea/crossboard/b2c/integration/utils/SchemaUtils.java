package ru.teamidea.crossboard.b2c.integration.utils;

import org.apache.log4j.Logger;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.utils.util.HybrisPathUtils;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class SchemaUtils {
    private static final Logger LOGGER = Logger.getLogger(SchemaUtils.class);
    
    private static final String XSD_CHARSET = "utf-8";
    private static final String SCHEMA_REGEXP = "xmlns:.*?\".*?\"";
    private static final String JAR_SOURCE_PREFIX = "jar:file:";
    private static final String EXTENSION_PATH = HybrisPathUtils.getProjectPath(CrossboardintegrationConstants.EXTENSIONNAME).getPath();
    private static final String JAR_ARTEFACT_NAME = "crossboardapi-1.0-SNAPSHOT.jar";
    private static final String XSD_PATH = JAR_SOURCE_PREFIX + EXTENSION_PATH + File.separator + "lib" + File.separator + JAR_ARTEFACT_NAME + "!/xsd/";
    private static Map<XSDSchema, XSDSchemaData> schemaMap = new HashMap<XSDSchema, XSDSchemaData>();

    static{
        final String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        final SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
        try{
            final XSDSchema[] xsdSchemas = XSDSchema.values();
            for (final XSDSchema xsdSchema: xsdSchemas){
                final String xsd = XSD_PATH + xsdSchema.getValue() + ".xsd";
                final URL schemaURL = new URL(xsd);
                final Schema schema = schemaFactory.newSchema(schemaURL);
                final List<String> namespaces = extractNamespaces(schemaURL);
                final XSDSchemaData data = new XSDSchemaData(schema, namespaces);
                schemaMap.put(xsdSchema, data);
            }
        }catch (final Exception e){
            LOGGER.error("Error creating parser : ", e);
        }
    }

    private static List<String> extractNamespaces(final URL schemaURL) throws IOException {
        final InputStream is = schemaURL.openStream();
        final byte[] xsdBts = new byte[is.available()];
        is.read(xsdBts);
        is.close();
        final String xsdStr = new String(xsdBts, XSD_CHARSET);
        final Pattern pattern = Pattern.compile(SCHEMA_REGEXP);
        final java.util.regex.Matcher matcher = pattern.matcher(xsdStr);
        int end = 0;
        final List<String> resultList = new ArrayList<String>();
        while (matcher.find(end)){
            final MatchResult result = matcher.toMatchResult();
            resultList.add(result.group());
            end = matcher.end();
        }
        return resultList;
    }

    public static List<String> getNamespaces(final XSDSchema schema) {
        final XSDSchemaData data = schemaMap.get(schema);
        if (data == null){
            return null;
        }
        return data.getNamespaces();
    }

    public static Schema getSchema(final XSDSchema schema) {
        final XSDSchemaData data = schemaMap.get(schema);
        if (data == null){
            return null;
        }
        return data.getSchema();
    }

    public static enum XSDSchema {
        catalog("catalog"),
        common("common");

        private String value;

        XSDSchema(String path) {
            this.value = path;
        }

        public String getValue() {
            return this.value;
        }
    }

    public static class XSDSchemaData {
        private final Schema schema;
        private final List<String> namespaces;

        public XSDSchemaData(final Schema schema, final List<String> namespaces) {
            this.schema = schema;
            this.namespaces = namespaces;
        }

        public Schema getSchema() {
            return schema;
        }

        public List<String> getNamespaces() {
            return namespaces;
        }
    }
}
