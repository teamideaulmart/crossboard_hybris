package ru.teamidea.crossboard.b2c.integration.result;

/**
 * Created by Marina on 14.09.2016.
 */

import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.utils.result.ResultHelper;
import ru.ulmart.cb.common.ResponseResult;
import ru.teamidea.crossboard.b2c.integration.utils.result.Result;

import java.io.File;
import java.io.Serializable;

public abstract class AbstractResultMaker<E> implements Serializable {
    private static final long serialVersionUID = 7603428869047791680L;

    public Message<?> make(final Message<File> message,
                         @Header(CrossboardintegrationConstants.MESSAGE_HEADER_VALIDATION_RESULT_CODE) Result validationResult,
                         @Header(CrossboardintegrationConstants.MESSAGE_HEADER_VALIDATION_ERROR_MESSAGE) String errorMessage){
        final String fName = message.getPayload().getName();
        final ResponseResult result = ResultHelper.mkResult(validationResult, errorMessage);
        return MessageBuilder.withPayload(newResponse(result, fName))
                .copyHeaders(message.getHeaders())
                .build();
    }

    protected abstract E newResponse(final ResponseResult result, final String fName);
}