package ru.teamidea.crossboard.b2c.integration.exceptions;

import ru.ulmart.cb.common.ItemUid;

import java.util.List;

/**
 * Created by Timofey Klyubin on 13.09.16.
 */
public class CrossboardValidationException extends Exception {

    private List<String> validationResult;
    private ItemUid uid;

    private CrossboardValidationException() {

    }

    public CrossboardValidationException(List<String> validationResult, ItemUid uid) {
        this.validationResult = validationResult;
        this.uid = uid;
    }

    public List<String> getValidationResult() {
        return this.validationResult;
    }

    public ItemUid getUid() {
        return uid;
    }
}
