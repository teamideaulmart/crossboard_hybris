package ru.teamidea.crossboard.b2c.integration.processors.file;

/**
 * Created by Marina on 13.09.2016.
 */
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.MediaUtil;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * Created by Marina on 29.01.2016.
 */
public class ArchiveFileProcessor implements Serializable
{

    protected final static Logger LOGGER = Logger.getLogger(ArchiveFileProcessor.class);

    private static final String DATE_PATTERN_KEY = "integration.archive.filename.dateformat";
    private static final String ZIP_FILE_SUFFIX = ".zip";
    private final String archiveDirectory;

    private ConfigurationService configurationService;

    /**
     * Instantiates a new archive file processor.
     *
     * @param archiveDirectory
     *           the archive directory
     */
    public ArchiveFileProcessor(final String archiveDirectory)
    {
        this.archiveDirectory = archiveDirectory;
    }

    /**
     * Form a zip-archive based on processed file in the folder. Save the zip-archive in archive directory folder. Remove
     * the processed file from processing directory.
     *
     * @param file
     *           processed file
     */
    public void archive(final Path file) throws IOException
    {
        ServicesUtil.validateParameterNotNull(file, "Parameter file must not be null");
        final String fileName = file.getFileName().toString();
        final Path archivePath = Paths.get(getArchiveDirectory());
        Files.createDirectories(archivePath);
        boolean archived = false;
        try (final ZipOutputStream zos = new ZipOutputStream(Files.newOutputStream(generateFile(fileName, archivePath),
                StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING));
             final InputStream fis = Files.newInputStream(file, StandardOpenOption.READ))
        {
            final ZipEntry zipEntry = new ZipEntry(fileName);
            zos.putNextEntry(zipEntry);
            MediaUtil.copy(fis, zos, false);
            LOGGER.info(String.format("[%1$s] Move to archive", fileName));
            archived = true;
        }
        catch (final IOException e)
        {
            LOGGER.error("Processed file backup error", e);
        }
        if (archived)
        {
            Files.deleteIfExists(file);
            LOGGER.info(String.format("[%1$s] Was deleted", fileName));
        }
    }

    /**
     * Gets the archive directory.
     *
     * @return the archive directory
     */
    public String getArchiveDirectory()
    {
        return archiveDirectory;
    }

    /**
     * @return the configurationService
     */
    public ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    /**
     * @param configurationService
     *           the configurationService to set
     */
    public void setConfigurationService(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    private Path generateFile(final String prefix, final Path archiveDirectory) throws IOException
    {
        final StringBuilder filename = new StringBuilder().append(prefix).append(
                DateFormatUtils.format(new java.util.Date(), configurationService.getConfiguration().getString(DATE_PATTERN_KEY)))
                .append(ZIP_FILE_SUFFIX);
        return archiveDirectory.resolve(filename.toString());
    }
}