package ru.teamidea.crossboard.b2c.integration.utils;

import javax.xml.bind.JAXBElement;


public final class JAXBUtils
{
	public static <E> E getValue(final JAXBElement<E> element) throws JAXBElementIsNullException
	{
		if (element == null)
		{
			throw new JAXBElementIsNullException();
		}
		return element.getValue();
	}

	public static class JAXBElementIsNullException extends Exception
	{
		private static final long serialVersionUID = 1L;

		public JAXBElementIsNullException()
		{
			super("Element is null");
		}
	}
}
