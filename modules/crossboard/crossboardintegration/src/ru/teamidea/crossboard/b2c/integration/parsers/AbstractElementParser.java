package ru.teamidea.crossboard.b2c.integration.parsers;

import ru.teamidea.crossboard.b2c.integration.handlers.AbstractSingleElementHandler;

/**
 * Created by Marina on 13.09.2016.
 */
public abstract class AbstractElementParser<T, E extends AbstractSingleElementHandler<T>> extends AbstractParser<E> {
    private static final long serialVersionUID = 1L;
}