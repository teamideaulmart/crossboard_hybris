package ru.teamidea.crossboard.b2c.integration.validators.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.util.localization.Localization;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import ru.teamidea.crossboard.b2c.integration.validators.ValidatorService;
import ru.ulmart.cb.catalog.ProductAttribute;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Timofey Klyubin on 12.09.16.
 */
public class ValidatorServiceImpl implements ValidatorService {

    @Override
    public List<String> validateCrossboardProductModel(ProductModel productModel, Locale locale) {
        List<String> errorsList = new ArrayList<>();

        if (StringUtils.isEmpty(productModel.getMerchantSku())) {
            errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.merchant_is_empty"));
        }

        if (StringUtils.isEmpty(productModel.getVendorSku())) {
            errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.vendor_is_empty"));
        }

        if (productModel instanceof GenericVariantProductModel) {
            if (productModel.getSupercategories() == null || productModel.getSupercategories().stream()
                    .filter(category -> category instanceof VariantValueCategoryModel).collect(Collectors.toList()).size() < 1) {
                errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.variant_product_has_no_categories"));
            }
        }
        else {
            if (productModel.getSupercategories() == null || productModel.getSupercategories().size() < 1) {
                errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.product_has_no_categories"));
            }
        }
        
        if (StringUtils.isEmpty(productModel.getName(locale))) {
            errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.name_is_empty"));
        }

        if (StringUtils.isEmpty(productModel.getDescription(locale))) {
            errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.description_is_empty"));
        }

        if (StringUtils.isEmpty(productModel.getBrand(locale))) {
            errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.brand_is_empty"));
        }

        if (StringUtils.isEmpty(productModel.getModel(locale))) {
            errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.model_is_empty"));
        }

        if (StringUtils.isEmpty(productModel.getManufacturerName())) {
            errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.manufacturer_is_empty"));
        }

        return errorsList.size() > 0 ? errorsList : Collections.EMPTY_LIST;
    }

    @Override
    public List<String> validateAttributesUniqueness(List<ProductAttribute> attributes) {
        List<String> errorsList = new ArrayList<>();

        Supplier<Stream<ProductAttribute>> variantStream = () -> attributes.stream().filter((attr) -> BooleanUtils.isTrue(attr.isVariant()));
        Supplier<Stream<ProductAttribute>> nonVariantStream = () -> attributes.stream().filter((attr) -> BooleanUtils.isNotTrue(attr.isVariant()));

        errorsList.addAll(validateDuplicatesAgainstStream(variantStream));
        errorsList.addAll(validateDuplicatesAgainstStream(nonVariantStream));

        return errorsList.size() > 0 ? errorsList : Collections.EMPTY_LIST;
    }

    private List<String> validateDuplicatesAgainstStream(final Supplier<Stream<ProductAttribute>> streamSupplier) {
        List<String> errorsList = new ArrayList<>();

        Set<String> skip = new HashSet<>();

        streamSupplier.get().forEach((attr) -> {
            if (streamSupplier.get().filter(a -> a.getCode().equals(attr.getCode())).count() > 1) {
                if (!skip.contains(attr.getCode())) {
                    errorsList.add(Localization.getLocalizedString("crossboard.soap.ws.validator.non_unique_attributes", new Object[]{attr.getCode()}));
                    skip.add(attr.getCode());
                }
            }
        });

        return errorsList.size() > 0 ? errorsList : Collections.EMPTY_LIST;
    }
}
