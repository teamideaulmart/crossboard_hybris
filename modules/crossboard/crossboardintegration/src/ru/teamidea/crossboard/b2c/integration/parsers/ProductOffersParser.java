package ru.teamidea.crossboard.b2c.integration.parsers;

import ru.teamidea.crossboard.b2c.integration.handlers.ProductOffersHandler;
import ru.teamidea.crossboard.b2c.integration.utils.SchemaUtils;
import ru.ulmart.cb.catalog.ProductOffer;

/**
 * Created by Marina on 13.09.2016.
 */
public class ProductOffersParser extends AbstractElementParser<ProductOffer, ProductOffersHandler> {
    private static final long serialVersionUID = 1L;

    @Override
    protected SchemaUtils.XSDSchema getXSDSchema() {
        return SchemaUtils.XSDSchema.catalog;
    }

    @Override
    protected ProductOffersHandler makeHandler(String fileName, int collectionSize) {
        return new ProductOffersHandler(fileName, collectionSize);
    }
}
