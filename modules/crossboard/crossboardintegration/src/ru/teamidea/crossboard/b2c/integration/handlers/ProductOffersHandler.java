package ru.teamidea.crossboard.b2c.integration.handlers;

import ru.ulmart.cb.catalog.ProductOffer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Marina on 16.09.2016.
 */
public class ProductOffersHandler extends AbstractSingleElementHandler<ProductOffer> {
    private List<String> outerTags;
    private List<String> outerClosedTags;

    public ProductOffersHandler(String fName, int collectionSize) {
        super(fName, collectionSize);

        // outer tags in natural order
        outerTags = Arrays.asList("productOffers");
    }

    @Override
    protected String getElementTag() {
        return "productOffer";
    }

    @Override
    protected String getRootTagName() {
        return "uploadProductOffersRequest";
    }

    @Override
    protected List<String> getOuterElementTagList() {
        return outerTags;
    }

    @Override
    protected List<String> getOuterElementClosedTagList() {
        if (null == this.outerClosedTags) {
            outerClosedTags = new ArrayList<>(this.outerTags);

            Collections.reverse(outerClosedTags);
        }

        return this.outerClosedTags;
    }
}