package ru.teamidea.crossboard.b2c.integration.processors.catalog;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.messaging.handler.annotation.Headers;
import ru.teamidea.crossboard.b2c.integration.aspect.ExecuteInSession;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.exceptions.IntegrationInternalException;
import ru.teamidea.crossboard.b2c.integration.handlers.CatalogServiceBusinessLogicRequestHandler;
import ru.teamidea.crossboard.b2c.integration.processors.AbstractElementSessionDependentProcessor;
import ru.teamidea.crossboard.b2c.integration.utils.result.Result;
import ru.teamidea.crossboard.b2c.integration.utils.result.ResultHelper;
import ru.teamidea.utils.misc.Pair;
import ru.ulmart.cb.catalog.DeleteItemsRequest;
import ru.ulmart.cb.catalog.ObjectFactory;
import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.cb.common.CallbackType;
import ru.ulmart.cb.common.ItemUid;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductService;
import ru.ulmart.crossboard.core.services.user.MerchantService;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.List;
import java.util.Map;

/**
 * Created by Marina on 16.09.2016.
 */
public class DeleteItemsProcessor extends AbstractElementSessionDependentProcessor<CallbackRequest, Object> {

    private final static Logger LOGGER = Logger.getLogger(DeleteItemsProcessor.class);

    private MerchantService merchantService;
    private CrossboardProductService crossboardProductService;
    private ModelService modelService;

    @ExecuteInSession
    public CallbackRequest processCallback(final String element,
                                           @Headers Map<Object, Object> headers)
            throws IntegrationInternalException, JAXBException {
        return processElement(element, headers).getFirst();
    }

    @Override
    protected Pair<CallbackRequest,Object> processElement(String element, final Map<Object, Object> headers) throws IntegrationInternalException, JAXBException {
        final DeleteItemsRequest deleteItemsRequest = unmarshallDeleteItemsRequest(element);

        final CallbackRequest callbackRequest = new CallbackRequest();
        callbackRequest.setResponseResults(new CallbackRequest.ResponseResults());
        callbackRequest.setCalbackType(CallbackType.DELETEITEMS);

        final String merchantId = (String) headers.get(CrossboardintegrationConstants.MESSAGE_HEADER_MERCHANT_ID);
        final MerchantModel merchant = merchantService.findMerchantForExternalCode(merchantId);
        if (merchant == null) {
            LOGGER.error(String.format("Stop processing delete items request due to... merchant for id %1$s not found", merchantId));
            callbackRequest.setResult(ResultHelper.mkResult(Result.UNKNOWN_MERCHANT, Result.UNKNOWN_MERCHANT.getMessage()));
            return new Pair<>(callbackRequest, null);
        }
        else {
            execute(deleteItemsRequest, callbackRequest);
        }

        ResultHelper.setCommonResult(callbackRequest);
        return new Pair<>(callbackRequest, null);
    }

    @Override
    protected void fireEvent(List<Object> eventData) {
        LOGGER.info("No event has been fired for DeleteItemProcessor");
    }

    private DeleteItemsRequest unmarshallDeleteItemsRequest(final String element) throws IntegrationInternalException, JAXBException {
        final Object result = unmarshall(element);
        return ((JAXBElement<DeleteItemsRequest>) result).getValue();
    }

    private void execute(final DeleteItemsRequest deleteItemsRequest, final CallbackRequest callbackRequest) {

        switch (CallbackType.fromValue(deleteItemsRequest.getDeleteItemType().value())) {
            case PRODUCTDESCRIPTIONS: {
                for (final ItemUid itemUid : deleteItemsRequest.getItems().getItem()) {

                    callbackRequest.getResponseResults().getResponseResult().addAll(CatalogServiceBusinessLogicRequestHandler
                            .handleRequest((successResponse) -> {

                        final ProductModel productModel = crossboardProductService
                                .findProductByMerchantSku(itemUid.getUid1(), deleteItemsRequest.getHeader().getMerchantId());
                        if (productModel == null) {
                            successResponse.getErrorMessage().add(String.format("Product '%1$s' does not exist, no need to delete.",
                                    itemUid.getUid1()));
                        } else if (productModel.getDeleted()) {
                            LOGGER.info(String.format("Product '%1$s' has already been marked as deleted.",
                                    itemUid.getUid1()));
                            successResponse.setUid(itemUid);
                        } else {
                            productModel.setDeleted(true);
                            modelService.save(productModel);
                            successResponse.setUid(itemUid);
                        }

                    }));
                }
            }
            break;
            case PRODUCTOFFERS: {
                LOGGER.info("Deleting product offers is not supported.");
            }
            break;
            default: {
                IllegalArgumentException iae = new IllegalArgumentException(String.format("'%1$s' is wrong item type to delete!",
                        deleteItemsRequest.getDeleteItemType().value()));
                LOGGER.error(iae.getMessage(), iae);
                throw iae;
            }
        }
    }

    @Override
    protected Class<?> getObjectFactoryClass() {
        return ObjectFactory.class;
    }

    public void setMerchantService(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    public void setCrossboardProductService(CrossboardProductService crossboardProductService) {
        this.crossboardProductService = crossboardProductService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}