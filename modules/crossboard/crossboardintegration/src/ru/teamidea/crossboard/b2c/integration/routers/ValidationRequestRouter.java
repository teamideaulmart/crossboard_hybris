package ru.teamidea.crossboard.b2c.integration.routers;

/**
 * Created by Marina on 13.09.2016.
 */

import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.utils.result.Result;
import ru.teamidea.crossboard.b2c.integration.utils.result.ResultHelper;

import java.io.File;
import java.io.Serializable;

public class ValidationRequestRouter implements Serializable {
    private static final long serialVersionUID = 1L;

    private final static Logger LOG = Logger.getLogger(ValidationRequestRouter.class);

    private String success;
    private String error;

    public String route(@Header(CrossboardintegrationConstants.MESSAGE_HEADER_VALIDATION_RESULT_CODE) Result validationResult,
                        final Message<File> message) {
        final boolean result = ResultHelper.ifSuccessResult(String.valueOf(validationResult.getValue()));
        final String fileName = message.getPayload().getName();

        if(LOG.isDebugEnabled()){
            LOG.debug(String.format("[%1$s] validation result %2$s", fileName, result));
        }

        return result ? success : error;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
