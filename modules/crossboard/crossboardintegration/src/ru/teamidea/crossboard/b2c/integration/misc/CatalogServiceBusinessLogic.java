package ru.teamidea.crossboard.b2c.integration.misc;

import ru.ulmart.cb.common.CallbackResponseResult;

/**
 * Created by Timofey Klyubin on 14.09.16.
 */
@FunctionalInterface
public interface CatalogServiceBusinessLogic {
    void doLogic(final CallbackResponseResult successResult) throws Exception;
}
