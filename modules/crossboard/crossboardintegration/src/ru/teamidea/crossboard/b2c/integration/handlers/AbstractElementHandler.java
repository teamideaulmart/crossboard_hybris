package ru.teamidea.crossboard.b2c.integration.handlers;

/**
 * Created by Marina on 13.09.2016.
 */

import org.apache.commons.lang.StringEscapeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

public abstract class AbstractElementHandler extends DefaultHandler {
    private final static String XML_TAG = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";

    private final String fName;
    private final int collectionSize;
    private String content;
    private String element;
    private final List<List<String>> elementXMLList = new ArrayList<List<String>>();
    private final List<String> elementPartList = new ArrayList<String>();
    private final List<String> escape = Arrays.asList(new String[]{"&", "<", ">"});
    private String schema = null;
    private final Map<String, String> tagMap = new HashMap<String, String>();

    public AbstractElementHandler(final String fName, final int collectionSize) {
        super();
        this.fName = fName;
        this.collectionSize = collectionSize;
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        clearElement();
        clearContent();
        elementXMLList.clear();
        elementPartList.clear();
        tagMap.clear();
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        fillSchema(attributes);
        doStartElement(uri, localName, qName, attributes);
    }

    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        doEndElement(uri, localName, qName);
    }

    private void fillSchema(final Attributes attributes) {
        if (schema == null) {
            final StringBuilder builder = new StringBuilder();
            for (int i = 0, len = attributes.getLength(); i < len; i++) {
                builder.append(" ");
                builder.append(attributes.getQName(i));
                builder.append("=\"");
                builder.append(attributes.getValue(i));
                builder.append("\"");
            }
            schema = builder.toString();
        }
    }

    private void fillResult() {
        elementXMLList.add(new ArrayList<String>(elementPartList));
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        if (elementPartList.size() > 0) {
            fillResult();
        }
    }

    protected String doFinalize(final String content) {
        final String rootTagName = getTag(getRootTagName());
        return XML_TAG + String.format("<%1$s %2$s>%3$s</%1$s>", rootTagName, schema, content);
    }

    protected boolean equalTag(final String tag, final String sampleTag) {
        final String newTag = tagMap.get(sampleTag);
        boolean result = false;
        if (newTag != null) {
            result = tag != null && (tag.equals(newTag) || tag.endsWith(":" + newTag));
            if (result) {
                return true;
            }
        }
        result = tag != null && sampleTag != null && (tag.equals(sampleTag) || tag.endsWith(":" + sampleTag));
        if (result) {
            tagMap.put(sampleTag, tag);
        }
        return result;
    }

    protected String getTag(final String sampleTag) {
        String tag = tagMap.get(sampleTag);
        if (tag == null) {
            tag = sampleTag;
        }
        return tag;
    }

    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        String result = String.copyValueOf(ch, start, length).trim();
        if (escape.contains(result)) {
            result = StringEscapeUtils.escapeXml(result);
        }
        content += result;
    }

    public List<List<String>> getElementXMLList() {
        return elementXMLList;
    }

    protected int getCollectionSize() {
        return collectionSize;
    }

    protected String getfName() {
        return fName;
    }

    protected String getContent() {
        return content;
    }

    protected void concatToContent(final String content) {
        this.content += content;
    }

    protected String getElement() {
        return element;
    }

    protected void concatToElement(final String element) {
        this.element += element;
    }

    protected List<String> getElementPartList() {
        return elementPartList;
    }

    protected void clearElement() {
        this.element = "";
    }

    protected void clearContent() {
        this.content = "";
    }

    protected abstract String getRootTagName();

    protected abstract void doStartElement(String uri, String localName, String qName, Attributes attributes) throws SAXException;

    protected abstract void doEndElement(String uri, String localName, String qName) throws SAXException;
}