package ru.teamidea.crossboard.b2c.integration.processors.catalog;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.exceptions.CrossboardValidationException;
import ru.teamidea.crossboard.b2c.integration.exceptions.IntegrationInternalException;
import ru.teamidea.crossboard.b2c.integration.handlers.CatalogServiceBusinessLogicRequestHandler;
import ru.teamidea.crossboard.b2c.integration.processors.AbstractElementSessionDependentProcessor;
import ru.teamidea.crossboard.b2c.integration.utils.holder.Holder;
import ru.teamidea.solrsearch.events.ProductListPartialChangesTxEvent;
import ru.teamidea.utils.misc.Pair;
import ru.teamidea.utils.util.TimestampSequenceProvider;
import ru.teamidea.utils.util.Transliterator;
import ru.ulmart.cb.catalog.ObjectFactory;
import ru.ulmart.cb.catalog.ProductOffer;
import ru.ulmart.cb.catalog.UploadProductOffersRequest;
import ru.ulmart.cb.common.CallbackResponseResult;
import ru.ulmart.cb.common.ItemUid;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductService;
import ru.ulmart.crossboard.core.services.catalog.CrossboardUnitService;
import ru.ulmart.crossboard.core.services.user.MerchantService;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Created by Marina on 14.09.2016.
 */
public class ProductOffersProcessor extends AbstractElementSessionDependentProcessor<CallbackResponseResult, PK> {

    private MerchantService merchantService;
    private CrossboardUnitService crossboardUnitService;
    private CrossboardProductService crossboardProductService;
    private ModelService modelService;
    private CommonI18NService commonI18NService;
    private EventService eventService;
    private StockService stockService;

    static final public Date MINTIME = Date.from(LocalDate.now(ZoneId.systemDefault()).minus(1, ChronoUnit.DAYS).atStartOfDay(ZoneId.systemDefault()).toInstant());
    static final public DateTime MAXTIME = new DateTime(9999, 1, 1, 0, 0, 0, DateTimeZone.getDefault());

    @Override
    protected void fireEvent(List<PK> eventData) {
        eventService.publishEvent(
                new ProductListPartialChangesTxEvent(TimestampSequenceProvider.getSequenceNumber(), true, eventData, ProductModel._TYPECODE)
        );
    }

    @Override
    protected Pair<CallbackResponseResult, PK> processElement(String element, final Map<Object, Object> headers) throws IntegrationInternalException, JAXBException {

        final Holder<PK> holder = new Holder<>();
        holder.setE(null);

        final CallbackResponseResult callbackResponseResult =
                CatalogServiceBusinessLogicRequestHandler.handleRequest((successResult) -> {
                    final CrossboardValidationException[] priceException = {null};

                    Transaction.current().execute(new TransactionBody() {
                        @Override
                        public <T> T execute() throws Exception {
                            final ProductOffer productOffer = unmarshallProductOffer(element);

                            final String merchantId = (String) headers.get(CrossboardintegrationConstants.MESSAGE_HEADER_MERCHANT_ID);
                            final MerchantModel merchant = merchantService.findMerchantForExternalCode(merchantId);
                            if (merchant == null) {
                                throw new IntegrationInternalException("merchant with id='" + merchantId + "' not found!");
                            }

                            final ProductModel productModel = crossboardProductService
                                    .findProductByMerchantSku(productOffer.getMerchantSku(), merchantId);
                            if (productModel == null) {
                                throw new IntegrationInternalException("Product with merchantSku='" + productOffer.getMerchantSku() + "' not found!");
                            }

                            final PK productPk = productModel.getPk();

                            final ItemUid productUid = new ItemUid();
                            productUid.setUid1(productModel.getCode());
                            successResult.setUid(productUid);

                            //TODO: set deleted... maybe later
                            //productModel.setDeleted(false);

                            final Locale locale = getLocale(headers);
                            try {
                                setPriceRowAndUnit(productModel, productOffer, locale, productUid);
                            } catch (CrossboardValidationException e) {
                                priceException[0] = e;
                            }
                            setStockLevel(merchant, productModel, productOffer);

                            holder.setE(productPk);
                            return null;
                        }
                    });

                    if (priceException[0] != null) {
                        throw priceException[0];
                    }
                }).get(0);
        return new Pair<>(callbackResponseResult, holder.getE());
    }

    private void setPriceRowAndUnit(final ProductModel productModel, final ProductOffer productOffer, final Locale locale,
                                    final ItemUid itemUid) throws CrossboardValidationException {
        final String salesUnit = Transliterator.transliterate(productOffer.getSalesUnit());
        final UnitModel priceUnit = crossboardUnitService.getOrCreateUnitModel(salesUnit, locale, productModel.getCatalogVersion());
        productModel.setUnit(priceUnit);
        modelService.save(productModel);

        PriceRowModel priceRow = productModel.getEurope1Prices().stream().findAny().orElse(null);
        if (priceRow == null) {
            priceRow = modelService.create(PriceRowModel.class);
            priceRow.setCatalogVersion(productModel.getCatalogVersion());
            priceRow.setProductId(productModel.getCode());
            priceRow.setProductMatchQualifier(Long.valueOf(-1));
        }

        productModel.setEurope1Prices(Collections.singleton(priceRow));
        priceRow.setUnit(priceUnit);
        priceRow.setCurrency(commonI18NService.getCurrency(productOffer.getUnitPrice().getCurrency().value()));

        if (BigDecimal.ZERO.compareTo(productOffer.getUnitPrice().getPriceValue()) == 0) {
            throw new CrossboardValidationException(
                    Collections.singletonList("Product price value cannot be zero"),
                    itemUid
            );
        }

        priceRow.setPrice(productOffer.getUnitPrice().getPriceValue().doubleValue());
        priceRow.setNet(Boolean.TRUE);

        if (productOffer.getStartDate() != null) {
            final Calendar startDateCalendar = productOffer.getStartDate().toGregorianCalendar();
            final LocalDate startLocalDate = LocalDateTime.ofInstant(startDateCalendar.toInstant(), ZoneId.systemDefault()).toLocalDate();
            priceRow.setStartTime(Date.from(startLocalDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        }

        if (productOffer.getEndDate() != null) {
            final Calendar endDateCalendar = productOffer.getEndDate().toGregorianCalendar();
            final LocalDateTime endLocalDate = LocalDateTime.ofInstant(endDateCalendar.toInstant(), ZoneId.systemDefault());
            final LocalDateTime currentTime = LocalDateTime.now(ZoneId.systemDefault());

            priceRow.setEndTime(Date.from(endLocalDate.atZone(ZoneId.systemDefault())
                    .withHour(23).withMinute(59).withSecond(59).withNano(999999999).toInstant()));

            if (currentTime.isAfter(endLocalDate)) {
                throw new CrossboardValidationException(
                        Collections.singletonList("End date must be after current date!"),
                        itemUid
                );
            } else if (priceRow.getStartTime() != null && priceRow.getStartTime().after(priceRow.getEndTime())) {
                throw new CrossboardValidationException(
                        Collections.singletonList("End date must be after start date!"),
                        itemUid
                );
            }
        }

        if (productOffer.getStartDate() == null) {
            priceRow.setStartTime(MINTIME);
        }
        if (productOffer.getEndDate() == null) {
            priceRow.setEndTime(MAXTIME.toDate());
        }

        modelService.save(priceRow);
    }

    private void setStockLevel(final MerchantModel merchant, final ProductModel productModel, final ProductOffer productOffer) throws IntegrationInternalException {
        final WarehouseModel merchantWarehouse = merchant.getWarehouses().stream().findAny().orElse(null);
        if (merchantWarehouse == null) {
            throw new IntegrationInternalException("Merchant '" + merchant.getCode() + "' does not have a warehouse!");
        }

        StockLevelModel productStockLevel = stockService.getStockLevel(productModel, merchantWarehouse);
        if (productStockLevel == null) {
            productStockLevel = modelService.create(StockLevelModel.class);
            productStockLevel.setWarehouse(merchantWarehouse);
            productStockLevel.setProductCode(productModel.getCode());
        }
        if (productOffer.getAvailableStock().intValue() > 0) {
            productStockLevel.setInStockStatus(InStockStatus.FORCEINSTOCK);
        } else {
            productStockLevel.setInStockStatus(InStockStatus.FORCEOUTOFSTOCK);
        }
        modelService.save(productStockLevel);
        productModel.setStockLevels(Collections.singleton(productStockLevel));
        stockService.updateActualStockLevel(productModel, merchantWarehouse, productOffer.getAvailableStock().intValue(), StringUtils.EMPTY);
    }

    private Locale getLocale(final Map<Object, Object> headers) {
        final Object langHeader = headers.get(CrossboardintegrationConstants.MESSAGE_HEADER_LANGUAGE);
        final Locale localeHeader = langHeader != null ? Locale.forLanguageTag((String) langHeader) : null;
        return localeHeader != null ? localeHeader : CrossboardintegrationConstants.DEFAULT_LOCALE;
    }

    private ProductOffer unmarshallProductOffer(final String element) throws IntegrationInternalException, JAXBException {
        final Object result = unmarshall(element);
        final UploadProductOffersRequest.ProductOffers productOffers = ((JAXBElement<UploadProductOffersRequest>) result).getValue().getProductOffers();
        final List<ProductOffer> productOfferList = productOffers.getProductOffer();

        if (productOfferList.size() == 0) {
            throw new IntegrationInternalException(String.format("Couldn't find any product offers in generated string [%1$s]", element));
        }
        if (productOfferList.size() > 1) {
            throw new IntegrationInternalException(String.format("Too many product offers in generated string [%1$s]", element));
        }

        return productOfferList.get(0);
    }

    @Override
    protected Class<?> getObjectFactoryClass() {
        return ObjectFactory.class;
    }

    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    public void setCrossboardUnitService(CrossboardUnitService crossboardUnitService) {
        this.crossboardUnitService = crossboardUnitService;
    }

    public void setMerchantService(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    public void setCrossboardProductService(CrossboardProductService crossboardProductService) {
        this.crossboardProductService = crossboardProductService;
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setStockService(StockService stockService) {
        this.stockService = stockService;
    }

}