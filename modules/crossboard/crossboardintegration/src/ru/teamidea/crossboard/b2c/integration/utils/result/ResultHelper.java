package ru.teamidea.crossboard.b2c.integration.utils.result;

import org.apache.commons.lang.StringUtils;
import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.cb.common.ResponseResult;

public class ResultHelper {
    private static final String SPLITTER = ". ";

    public static boolean ifSuccessResult(final String resultCode){
        return StringUtils.equals(resultCode, String.valueOf(Result.SUCCESS.getValue()));
    }

    public static void setCommonResult(final CallbackRequest callbackRequest) {
        if (callbackRequest.getResponseResults().getResponseResult().stream()
                .filter(result -> !ResultHelper.ifSuccessResult(result.getResultCode()))
                .count() == 0) {
            callbackRequest.setResult(ResultHelper.mkResult(Result.SUCCESS));
        } else {
            callbackRequest.setResult(ResultHelper.mkResult(Result.PROCESSING_ERRORS));
        }
    }

    public static ResponseResult mkResult(final Result result) {
        return mkResult(result, (String) null);
    }

    public static <E extends ResponseResult> void fillResult(final Result result, E error) {
        error.setResultCode(String.valueOf(result.getValue()));
        error.getErrorMessage().add(result.getMessage());
    }

    public static <E extends ResponseResult> void fillResult(final Result result, String msg, E error) {
        error.setResultCode(String.valueOf(result.getValue()));
        error.getErrorMessage().add(msg);
    }

    public static <E extends ResponseResult> E mkResult(final Result result, Class<E> clazz) {
        return mkResult(result, (String) null, clazz);
    }

    public static ResponseResult mkResult(final Result result, String errorMsg) {
        return mkResult(result, errorMsg, ResponseResult.class);
    }

    public static <E extends ResponseResult> E mkResult(final Result result, String errorMsg, Class<E> clazz) {
        E instance;

        try {
            instance = clazz.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        if (errorMsg == null || errorMsg.trim().isEmpty()) {
            errorMsg = result.getMessage();
        } else {
            errorMsg = result.getMessage() + SPLITTER + errorMsg;
        }
        instance.setResultCode(String.valueOf(result.getValue()));
        instance.getErrorMessage().add(errorMsg);
        return instance;
    }
}
