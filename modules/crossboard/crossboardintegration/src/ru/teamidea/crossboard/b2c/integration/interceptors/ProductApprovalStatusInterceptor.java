package ru.teamidea.crossboard.b2c.integration.interceptors;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import ru.ulmart.crossboard.core.enums.ProductDescriptionStatus;
import ru.ulmart.crossboard.core.enums.ProductMediaStatus;

/**
 * Created by Timofey Klyubin on 23.09.16.
 */
public class ProductApprovalStatusInterceptor implements PrepareInterceptor<ProductModel> {

    @Override
    public void onPrepare(ProductModel productModel, InterceptorContext interceptorContext) throws InterceptorException {
        if (productModel.getMediaStatus() == ProductMediaStatus.PROCESSED &&
                productModel.getDescriptionStatus() == ProductDescriptionStatus.PROCESSED) {
            productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        }
    }
}
