package ru.teamidea.crossboard.b2c.integration.exceptions;

/**
 * Created by Marina on 14.09.2016.
 */
public class IntegrationInternalException extends Exception {

    public IntegrationInternalException() {
        super();
    }

    public IntegrationInternalException(String msg) {
        super(msg);
    }

}
