package ru.teamidea.crossboard.b2c.integration.handlers;

import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import org.apache.log4j.Logger;
import ru.teamidea.crossboard.b2c.client.ws.CatalogCallbackClient;
import ru.teamidea.crossboard.b2c.integration.exceptions.CrossboardValidationException;
import ru.teamidea.crossboard.b2c.integration.exceptions.IntegrationInternalException;
import ru.teamidea.crossboard.b2c.integration.misc.CatalogServiceBusinessLogic;
import ru.ulmart.cb.common.CallbackResponseResult;
import ru.ulmart.cb.common.ItemUid;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marina on 19.09.2016.
 */
public class CatalogServiceBusinessLogicRequestHandler {

    private static final Logger LOGGER = Logger.getLogger(CatalogServiceBusinessLogicRequestHandler.class);

    public static List<CallbackResponseResult> handleRequest(CatalogServiceBusinessLogic businessLogic) {
        List<CallbackResponseResult> results = new ArrayList<>();
        CallbackResponseResult successResult = new CallbackResponseResult();
        try {
            businessLogic.doLogic(successResult);

            successResult.setResultCode(CatalogCallbackClient.SUCCESS);
            results.add(successResult);
        }  catch (CrossboardValidationException cve) {
            final CallbackResponseResult validationError = getCallbackResponseResultForException(CatalogCallbackClient.VALIDATION_ERROR, cve, cve.getUid());
            validationError.setUid(cve.getUid());
            for (String error : cve.getValidationResult()) {
                validationError.getErrorMessage().add(error);
            }
            results.add(validationError);
        } catch (ModelSavingException mse) {
            results.add(getCallbackResponseResultForException(CatalogCallbackClient.PERSISTENCE_ERROR, mse,successResult.getUid()));
        } catch (RuntimeException | IntegrationInternalException re) {
            results.add(getCallbackResponseResultForException(CatalogCallbackClient.INTERNAL_SERVER_ERROR, re, successResult.getUid()));
        } catch (Exception e) {
            results.add(getCallbackResponseResultForException(CatalogCallbackClient.UNKNOWN_ERROR, e, successResult.getUid()));
        }
        return results;
    }

    private static CallbackResponseResult getCallbackResponseResultForException(final String resultCode, final Exception e,
                                                                                final ItemUid uid){
        final CallbackResponseResult result = new CallbackResponseResult();
        result.setResultCode(resultCode);
        result.setUid(uid);

        final String errorMessage = e.getLocalizedMessage() != null ? e.getLocalizedMessage()
                : (e.getCause() != null ? e.getCause().getLocalizedMessage() : null);
        result.getErrorMessage().add(errorMessage);

        LOGGER.warn("<" + e.getClass().getName() + "> " + e.getMessage(), e);
        if(e instanceof CrossboardValidationException) {
            for (String validationResult: ((CrossboardValidationException) e).getValidationResult()) {
                LOGGER.warn("<" + validationResult + "> ");
            }
        }

        return result;
    }
}
