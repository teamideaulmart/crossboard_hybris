package ru.teamidea.crossboard.b2c.integration.cronjobs.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.commons.collections.MapUtils;
import ru.teamidea.crossboard.b2c.integration.model.cronjob.CleanUpOldFilesCronJobModel;

/**
 * Created by Timofey Klyubin on 20.09.16.
 */
public class CleanupOldFilesCronJobValidateInterceptor implements ValidateInterceptor<CleanUpOldFilesCronJobModel> {
    @Override
    public void onValidate(CleanUpOldFilesCronJobModel cronJobModel,
                           InterceptorContext interceptorContext) throws InterceptorException {
        if (cronJobModel.getDaysOld() < 0) {
            throw new InterceptorException("CleanUpOldFilesCronJobModel.daysOld must be positive integer!");
        }
        if (MapUtils.isEmpty(cronJobModel.getCleanupFolders())) {
            throw new InterceptorException("No folders was specified for CleanupCronJob: Please specify at least one folder to clean.");
        }
    }
}
