package ru.teamidea.crossboard.b2c.integration.processors;

/**
 * Created by Marina on 14.09.2016.
 */

import org.apache.log4j.Logger;
import org.springframework.messaging.handler.annotation.Headers;
import ru.teamidea.crossboard.b2c.integration.aspect.ExecuteInSession;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.exceptions.IntegrationInternalException;
import ru.teamidea.utils.misc.Pair;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractElementSessionDependentProcessor<E, EVENT_DATA> extends AbstractXMLProcessor {

    private static final Logger LOGGER = Logger.getLogger(AbstractElementSessionDependentProcessor.class);

    @ExecuteInSession
    public List<E> process(final List<String> elementList, @Headers Map<Object, Object> headers) throws IntegrationInternalException, JAXBException {
        final String fileName = (String) headers.get(CrossboardintegrationConstants.MESSAGE_HEADER_FILE_NAME);

        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("[%1$s] Start element list processing", fileName));
        }

        final List<E> resultList = new ArrayList<>();
        final List<EVENT_DATA> eventData = new ArrayList<>();

        for (final String element : elementList) {
            final Pair<E, EVENT_DATA> pair = processElement(element, headers);
            resultList.add(pair.getFirst());
            if (pair.getSecond() != null) {
                eventData.add(pair.getSecond());
            }
        }

        LOGGER.info("Trying to publish "+this.getClass().getSimpleName());
        fireEvent(eventData);
        LOGGER.info(this.getClass().getSimpleName()+"UploadProductDescriptionsEvent has been published");


        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("[%1$s] Finish element list processing", fileName));
        }

        return resultList;
    }

    protected abstract Pair<E, EVENT_DATA> processElement(final String element, final Map<Object, Object> headers) throws IntegrationInternalException, JAXBException;

    protected abstract void fireEvent(final List<EVENT_DATA> eventData);
}