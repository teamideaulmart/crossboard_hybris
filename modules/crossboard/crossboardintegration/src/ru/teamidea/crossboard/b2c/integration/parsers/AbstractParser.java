package ru.teamidea.crossboard.b2c.integration.parsers;

/**
 * Created by Marina on 13.09.2016.
 */

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.xml.sax.SAXException;
import ru.teamidea.crossboard.b2c.integration.beans.ServiceBean;
import ru.teamidea.crossboard.b2c.integration.handlers.AbstractElementHandler;
import ru.teamidea.crossboard.b2c.integration.utils.SchemaUtils;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.List;

public abstract class AbstractParser<E extends AbstractElementHandler> implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(AbstractParser.class);

    @Autowired
    private ServiceBean serviceBean;
    private SAXParser parser;

    public AbstractParser() {
        try {
            final SAXParserFactory parserFactor = SAXParserFactory.newInstance();
            parserFactor.setSchema(SchemaUtils.getSchema(getXSDSchema()));
            parser = parserFactor.newSAXParser();
        } catch (final Exception e) {
            LOGGER.error("Error while parsing XML file", e);
        }
    }

    public Message<List<List<String>>> parse(final Message<File> message) throws ParserConfigurationException, SAXException, IOException {
        final File file = message.getPayload();
        final String fileName = file.getName();

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("[%1$s] Start parsing", fileName));
        }

        final E handler = makeHandler(fileName, serviceBean.getCollectionSize());
        final InputStream is = new FileInputStream(file);
        parser.parse(is, handler);

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("[%1$s] End parsing", fileName));
        }

        return MessageBuilder.withPayload(handler.getElementXMLList()).copyHeaders(
                message.getHeaders()).build();
    }

    protected abstract SchemaUtils.XSDSchema getXSDSchema();

    protected abstract E makeHandler(String fileName, int collectionSize);
}