package ru.teamidea.crossboard.b2c.integration.processors.catalog;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import de.hybris.platform.util.localization.Localization;
import de.hybris.platform.variants.model.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import ru.teamidea.crossboard.b2c.integration.beans.ServiceBean;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.event.UploadProductDescriptionsEvent;
import ru.teamidea.crossboard.b2c.integration.exceptions.CrossboardValidationException;
import ru.teamidea.crossboard.b2c.integration.exceptions.IntegrationInternalException;
import ru.teamidea.crossboard.b2c.integration.handlers.CatalogServiceBusinessLogicRequestHandler;
import ru.teamidea.crossboard.b2c.integration.processors.AbstractElementSessionDependentProcessor;
import ru.teamidea.crossboard.b2c.integration.utils.holder.Holder;
import ru.teamidea.crossboard.b2c.integration.validators.ValidatorService;
import ru.teamidea.utils.misc.Pair;
import ru.teamidea.utils.util.TimestampSequenceProvider;
import ru.ulmart.cb.catalog.*;
import ru.ulmart.cb.common.CallbackResponseResult;
import ru.ulmart.cb.common.ItemUid;
import ru.ulmart.crossboard.core.constants.CrossboardCoreConstants;
import ru.ulmart.crossboard.core.enums.ProductDescriptionStatus;
import ru.ulmart.crossboard.core.event.TranslateVariantValueCategoryEvent;
import ru.ulmart.crossboard.core.model.CrossboardProductAttributeModel;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.catalog.CrossboardCategoryService;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductService;
import ru.ulmart.crossboard.core.services.user.MerchantService;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants.RUSSIAN_LOCALE;

/**
 * Created by Marina on 14.09.2016.
 */
public class ProductDescriptionsProcessor extends AbstractElementSessionDependentProcessor<CallbackResponseResult, String> {

    private static final Logger LOG = Logger.getLogger(ProductDescriptionsProcessor.class);

    private static final String VARIANT_CATEGORY_PREFIX = "var_";
    private static final String BASE_PRODUCT_VARIANT_TYPE = GenericVariantProductModel._TYPECODE;
    private static final String DEFAULT_MEDIA_FORMAT = "1000Wx1000H";

    public static final int DEFAULT_MEDIA_PRIORITY = 1;

    private CrossboardCategoryService crossboardCategoryService;
    private MerchantService merchantService;
    private ModelService modelService;
    private TypeService typeService;
    private CategoryService categoryService;
    private ValidatorService validatorService;
    private MediaService mediaService;
    private CrossboardProductService crossboardProductService;
    private CatalogVersionService catalogVersionService;
    private EventService eventService;

    private CatalogVersionModel catalogVersion;

    private ServiceBean serviceBean;

    private final static Pattern httpsPattern = Pattern.compile("^https://(.+)");
    private static String https2http(final String url) {
        final Matcher matcher = httpsPattern.matcher(url);
        if (matcher.matches()) {
            LOG.warn("Replacing https to http in media: " + url);
            return matcher.replaceFirst("http://$1");
        }
        return url;
    }

    @Override
    protected void fireEvent(List<String> eventData) {
        eventService.publishEvent(new UploadProductDescriptionsEvent(eventData));
    }

    @Override
    protected Pair<CallbackResponseResult, String> processElement(String element, final Map<Object, Object> headers) throws IntegrationInternalException, JAXBException {
        final Holder<String> holder = new Holder<>();
        holder.setE(null);

        final CallbackResponseResult callbackResponseResult =
                CatalogServiceBusinessLogicRequestHandler.handleRequest((successResult) -> {

                    Transaction.current().execute(new TransactionBody() {
                        @Override
                        public <T> T execute() throws Exception {
                            final ProductDescription productDescription = unmarshallProductDescription(element);

                            final String merchantId = (String) headers.get(CrossboardintegrationConstants.MESSAGE_HEADER_MERCHANT_ID);
                            final String generatedProductCode = merchantId + "_" + productDescription.getMerchantSku();

                            final MerchantModel merchant = merchantService.findMerchantForExternalCode(merchantId);
                            if (merchant == null) {
                                throw new IntegrationInternalException("merchant with id='" + merchantId + "' not found!");
                            }

                            final boolean createVariant = !StringUtils.isEmpty(productDescription.getVariantGroup());
                            final ProductModel crossboardProduct = crossboardProductService
                                    .getOrCreateProductModel(generatedProductCode, createVariant);

                            final ItemUid productUid = new ItemUid();
                            productUid.setUid1(crossboardProduct.getCode());
                            successResult.setUid(productUid);

                            crossboardProduct.setXmlcontent(element);

                            if ((crossboardProduct instanceof VariantProductModel) && !createVariant) {
                                throw new CrossboardValidationException(
                                        Collections.singletonList(Localization
                                                .getLocalizedString("crossboard.soap.ws.validator.type_change_not_allowed", new Object[]{productUid.getUid1()})),
                                        productUid
                                );
                            }

                            if (productDescription.getAttributes() != null) { // merchant may send soap request without <attributes> tag
                                List<String> attributesValidationResult = validatorService
                                        .validateAttributesUniqueness(productDescription.getAttributes().getAttribute());

                                if (!attributesValidationResult.isEmpty()) {
                                    throw new CrossboardValidationException(attributesValidationResult, productUid);
                                }
                            }

                            final Locale locale = getLocale(headers);

                            populateCrossboardProduct(merchant, productDescription, crossboardProduct, locale);

                            if (locale.getLanguage().equals(new Locale("ru").getLanguage())) {
                                crossboardProduct.setDescriptionStatus(ProductDescriptionStatus.PROCESSED);
                            }

                            List<String> validationResult = validatorService.validateCrossboardProductModel(crossboardProduct, locale);

                            if (!validationResult.isEmpty()) {
                                throw new CrossboardValidationException(validationResult, productUid);
                            }
                            modelService.save(crossboardProduct);

                            holder.setE(generatedProductCode);
                            return null;
                        }
                    });

                }).get(0);

        return new Pair<>(callbackResponseResult, holder.getE());
    }

    private void populateCrossboardProduct(MerchantModel merchant, ProductDescription productDescription, ProductModel crossboardProduct, Locale locale) throws IntegrationInternalException {
        if (merchant == null) {
            throw new IntegrationInternalException("Merchant must not be null!");
        }

        final Holder<Boolean> changed = new Holder<>();
        changed.setE(false);

        crossboardProduct.setMerchant(merchant);

        crossboardProduct.setMerchantSku(productDescription.getMerchantSku());
        crossboardProduct.setVendorSku(productDescription.getVendorSku());
        crossboardProduct.setMerchantCategoryId(productDescription.getMerchantCategoryId());
        if (locale.equals(Locale.ENGLISH)) {
            if (!StringUtils.equals(crossboardProduct.getShortName(locale), productDescription.getShortName())) {
                changed.setE(true);
                crossboardProduct.setShortName("", RUSSIAN_LOCALE);
                crossboardProduct.setShortName(productDescription.getShortName(), Locale.ENGLISH);
            }
        } else {
            crossboardProduct.setShortName(productDescription.getShortName(), locale);
        }
        if (locale.equals(Locale.ENGLISH)) {
            if (!StringUtils.equals(crossboardProduct.getName(locale), productDescription.getName())) {
                changed.setE(true);
                crossboardProduct.setName("", RUSSIAN_LOCALE);
                crossboardProduct.setName(productDescription.getName(), Locale.ENGLISH);
            }
        } else {
            crossboardProduct.setName(productDescription.getName(), locale);
        }
        if (locale.equals(Locale.ENGLISH)) {
            if (!StringUtils.equals(crossboardProduct.getShortDescription(locale), productDescription.getShortDescription())) {
                changed.setE(true);
                crossboardProduct.setShortDescription("", RUSSIAN_LOCALE);
                crossboardProduct.setShortDescription(productDescription.getShortDescription(), Locale.ENGLISH);
            }
        } else {
            crossboardProduct.setShortDescription(productDescription.getShortDescription(), locale);
        }
        if (locale.equals(Locale.ENGLISH)) {
            if (!StringUtils.equals(crossboardProduct.getDescription(locale), productDescription.getFullDescription())) {
                changed.setE(true);
                crossboardProduct.setDescription("", RUSSIAN_LOCALE);
                crossboardProduct.setDescription(productDescription.getFullDescription(), Locale.ENGLISH);
            }
        } else {
            crossboardProduct.setDescription(productDescription.getFullDescription(), locale);
        }

        crossboardProduct.setManufacturerName(productDescription.getManufacturer());
        crossboardProduct.setMerchantUrl(productDescription.getUrl());

        if (locale.equals(Locale.ENGLISH)) {
            if (!StringUtils.equals(crossboardProduct.getBrand(locale), productDescription.getBrand())) {
                changed.setE(true);
                crossboardProduct.setBrand("", RUSSIAN_LOCALE);
                crossboardProduct.setBrand(productDescription.getBrand(), Locale.ENGLISH);
            }
        } else {
            crossboardProduct.setBrand(productDescription.getBrand(), locale);
        }

        if (locale.equals(Locale.ENGLISH)) {
            if (!StringUtils.equals(crossboardProduct.getModel(locale), productDescription.getModel())) {
                changed.setE(true);
                crossboardProduct.setModel("", RUSSIAN_LOCALE);
                crossboardProduct.setModel(productDescription.getModel(), Locale.ENGLISH);
            }
        } else {
            crossboardProduct.setModel(productDescription.getModel(), locale);
        }

        if (productDescription.getWarrantyPeriod() != null && productDescription.getWarrantyPeriod().compareTo(BigInteger.ZERO) != 0) {
            crossboardProduct.setWarrantyPeriod(productDescription.getWarrantyPeriod().intValue());
        }
        if (productDescription.getLength() != null) {
            crossboardProduct.setLength(productDescription.getLength().doubleValue());
        }
        if (productDescription.getWidth() != null) {
            crossboardProduct.setWidth(productDescription.getWidth().doubleValue());
        }
        if (productDescription.getHeight() != null) {
            crossboardProduct.setHeight(productDescription.getHeight().doubleValue());
        }
        if (productDescription.getVolume() != null) {
            crossboardProduct.setVolume(productDescription.getVolume().doubleValue());
        }
        crossboardProduct.setCatalogVersion(getCatalogVersion());
        crossboardProduct.setApprovalStatus(ArticleApprovalStatus.CHECK);
        crossboardProduct.setDeleted(Boolean.FALSE);

        if (productDescription.getAttributes() != null) {
            populateAttributes(productDescription, crossboardProduct, locale, changed);
        }

        LOG.trace("About to populate categories");
        populateCategories(productDescription, crossboardProduct, merchant);
        LOG.trace(
                "Got the following categories on product: " + crossboardProduct.getSupercategories()
                        .stream()
                        .map(CategoryModel::getCode)
                        .reduce((l, r) -> l + ", " + r).orElse("")
        );

        if (crossboardProduct instanceof GenericVariantProductModel) {
            populateVariants(productDescription, (GenericVariantProductModel) crossboardProduct,
                    crossboardProduct.getSupercategories(), locale, changed);
        }

        if (productDescription.getImages() != null) {
            populateImages(productDescription, crossboardProduct);
        } else {
            crossboardProduct.setImages(Collections.emptyList());
        }

        if (changed.getE()) {
            crossboardProduct.setDescriptionStatus(ProductDescriptionStatus.NOT_PROCESSED);
        }

    }

    private void populateCategories(ProductDescription productDescription, ProductModel crossboardProduct, final MerchantModel merchant) {
        Set<CategoryModel> categories = new HashSet<>();
        LOG.trace(
                "Got the following marketplace categories in request: " +
                String.join(", ", productDescription.getMarketplaceCategories().getMarketplaceCategoryId())
        );
        for (String categoryId : productDescription.getMarketplaceCategories().getMarketplaceCategoryId()) {
            final CategoryModel category = categoryService.getCategoryForCode(categoryId);
            boolean allowed = category.getMerchantConfig()
                    .stream()
                    .anyMatch((cat) -> cat.getMerchant().getExternalCode().equals(merchant.getExternalCode())
                            && cat.getImportAllowed() != null
                            && cat.getImportAllowed());
            if (allowed) {
                categories.add(category);
            } else {
                LOG.warn(String.format("Importing into %1$s is not allowed for merchant [%2$s].", category.getCode(), merchant.getCode()));
            }
        }

        if (categories.size() == 0) {
            throw new ModelSavingException("You're not allowed to import into any of specified categories.");
        }

        crossboardProduct.setSupercategories(categories);
    }

    private void populateImages(final ProductDescription productDescription, final ProductModel crossboardProduct){
        final Collection<MediaModel> modelsToRemove = new HashSet<>();
        if (crossboardProduct.getImages() != null) {
            crossboardProduct.getImages()
                    .stream()
                    .filter(media -> !productDescription.getImages().getImage()
                            .stream()
                            .anyMatch(img -> (media.getOriginalUrl() != null && media.getOriginalUrl().equals(img.getUrl()))))
                    .forEach(modelsToRemove::add);
        } else {
            crossboardProduct.setImages(new ArrayList<>());
        }

        final AtomicInteger imgIndex = new AtomicInteger(TimestampSequenceProvider.getSequenceNumber());
        productDescription.getImages().getImage()
                .stream()
                .filter(img -> !crossboardProduct.getImages()
                        .stream()
                        .anyMatch((media) -> img.getUrl() != null && img.getUrl().equals(media.getOriginalUrl())))
                .forEach((image) -> {
                    final MediaModel media = modelService.create(MediaModel.class);
                    final MediaFormatModel mediaFormat = mediaService.getFormat(DEFAULT_MEDIA_FORMAT);
                    media.setCode(crossboardProduct.getCode() + "_" + String.valueOf(imgIndex.getAndIncrement())); // TODO: code generation
                    media.setCatalogVersion(getCatalogVersion());
                    media.setAltText(image.getDescription());
                    media.setMediaFormat(mediaFormat);
                    media.setProduct(crossboardProduct);
                    media.setOriginalUrl(image.getUrl());
                    media.setFolder(mediaService.getFolder("images"));
                    media.setPriority(DEFAULT_MEDIA_PRIORITY);
                    modelService.save(media);
                });


        final Collection<MediaModel> modelsToSave = new HashSet<>();

        modelService.refresh(crossboardProduct);

        crossboardProduct.getImages()
                .stream()
                .filter(media -> media.getPriority() == null || media.getPriority().equals(CrossboardCoreConstants.BASE_IMAGE_MEDIA_PRIORITY))
                .forEach((media) -> {
                    media.setPriority(DEFAULT_MEDIA_PRIORITY);
                    modelsToSave.add(media);
                });

        Supplier<Stream<Image>> baseImagesStream = () -> productDescription.getImages().getImage()
                .stream().filter(Image::isBaseImage);

        if (baseImagesStream.get().count() > 0) {
            crossboardProduct.getImages()
                    .stream()
                    .filter(media -> baseImagesStream.get().anyMatch(image -> image.getUrl().equals(media.getOriginalUrl())))
                    .forEach((media) -> {
                        media.setPriority(CrossboardCoreConstants.BASE_IMAGE_MEDIA_PRIORITY);
                        modelsToSave.add(media);
                    });
        } else if (crossboardProduct.getImages().size() > 0) {
            crossboardProduct.getImages().get(0).setPriority(CrossboardCoreConstants.BASE_IMAGE_MEDIA_PRIORITY);
            modelsToSave.add(crossboardProduct.getImages().get(0));
        }

        modelService.saveAll(modelsToSave);
        modelService.removeAll(modelsToRemove);
    }

    private void populateAttributes(ProductDescription productDescription, ProductModel crossboardProduct, Locale locale, Holder<Boolean> changed) {
        // remove old
        if (crossboardProduct.getAttributes() != null) {
            crossboardProduct.getAttributes().forEach(modelService::remove);
        }

        List<CrossboardProductAttributeModel> attributeList = new ArrayList<>();
        for (ProductAttribute attribute : productDescription.getAttributes().getAttribute()) {
            if (attribute.isVariant() != null && attribute.isVariant()) {
                continue;
            }

            final CrossboardProductAttributeModel productAttribute = modelService
                    .create(CrossboardProductAttributeModel.class);
            productAttribute.setCode(crossboardProduct.getCode() + "_" + attribute.getCode());

            if (locale.equals(Locale.ENGLISH)) {
                if (!StringUtils.equals(productAttribute.getName(locale), attribute.getName())) {
                    changed.setE(true);
                    productAttribute.setName("", RUSSIAN_LOCALE);
                    productAttribute.setName(attribute.getName(), Locale.ENGLISH);
                }
            } else {
                productAttribute.setName(attribute.getName(), locale);
            }
            if (locale.equals(Locale.ENGLISH)) {
                if (!StringUtils.equals(productAttribute.getValue(locale), attribute.getValue())) {
                    changed.setE(true);
                    productAttribute.setValue("", RUSSIAN_LOCALE);
                    productAttribute.setValue(attribute.getValue(), Locale.ENGLISH);
                }
            } else {
                productAttribute.setValue(attribute.getValue(), locale);
            }
            if (locale.equals(Locale.ENGLISH)) {
                if (!StringUtils.equals(productAttribute.getUnit(locale), attribute.getUnit())) {
                    changed.setE(true);
                    productAttribute.setUnit("", RUSSIAN_LOCALE);
                    productAttribute.setUnit(attribute.getUnit(), Locale.ENGLISH);
                }
            } else {
                productAttribute.setUnit(attribute.getUnit(), locale);
            }
            modelService.save(productAttribute);

            attributeList.add(productAttribute);
        }

        crossboardProduct.setAttributes(attributeList);
    }

    private void populateVariants(ProductDescription productDescription, GenericVariantProductModel crossboardProduct,
                                  Collection<CategoryModel> categories, Locale locale, Holder<Boolean> changed) {
        final ProductModel baseProduct = crossboardProductService
                .getOrCreateProductModel(productDescription.getVariantGroup(), false);
        baseProduct.setVariantType((VariantTypeModel) typeService.getComposedTypeForCode(BASE_PRODUCT_VARIANT_TYPE));
        baseProduct.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        baseProduct.setMerchant(crossboardProduct.getMerchant());

        Set<CategoryModel> newCategories = new HashSet<>(categories);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Base product categories before: " + String.join(", ", newCategories
            .stream()
            .map(CategoryModel::getCode)
            .reduce((l, r) -> l + ", " + r).orElse("")));
        }
        if (baseProduct.getSupercategories() != null) {
            baseProduct.getSupercategories()
                    .stream()
                    .filter(category -> !(category instanceof VariantValueCategoryModel))
                    .forEach(newCategories::add);
        }
        if (LOG.isTraceEnabled()) {
            LOG.trace("Base product categories after: " + String.join(", ", newCategories
                    .stream()
                    .map(CategoryModel::getCode)
                    .reduce((l, r) -> l + ", " + r).orElse("")));
        }
        baseProduct.setSupercategories(newCategories);
        baseProduct.setCatalogVersion(getCatalogVersion());
        crossboardProduct.setBaseProduct(baseProduct);

        crossboardProduct.setSupercategories(new HashSet<>());

        for (ProductAttribute productAttribute : productDescription.getAttributes().getAttribute()) {
            if (productAttribute.isVariant() == null || !productAttribute.isVariant()) {
                continue;
            }

            if (productAttribute.getCode().equals("")) {
                throw new ModelSavingException("You must specify valid variant attribute code");
            }

            VariantCategoryModel variantCategory= null;

            try {
                variantCategory = crossboardCategoryService
                        .getVariantCategoryByCode(getCatalogVersion(), VARIANT_CATEGORY_PREFIX + productAttribute.getCode());
                baseProduct.getSupercategories().add(variantCategory);
            } catch (UnknownIdentifierException uie) {
                throw new UnknownIdentifierException(uie.getMessage().replace(VARIANT_CATEGORY_PREFIX, ""));
            }

            VariantValueCategoryModel variantValueCategory = crossboardCategoryService
                    .getOrCreateVariantValueCategory(
                            getCatalogVersion(),
                            VARIANT_CATEGORY_PREFIX + productAttribute.getCode() + "_" + productAttribute.getValue().toLowerCase()
                    );
            if (variantValueCategory.getSupercategories() == null || variantValueCategory.getSupercategories().isEmpty()) {
                variantValueCategory.setSupercategories(Collections.singletonList(variantCategory));
            }

            variantValueCategory.setSequence(TimestampSequenceProvider.getSequenceNumber());

            if (locale.equals(Locale.ENGLISH)) {
                if (!StringUtils.equals(variantValueCategory.getName(locale), productAttribute.getValue())
                        || variantValueCategory.getName(RUSSIAN_LOCALE).equals("")) {
                    changed.setE(true);
                    variantValueCategory.setName("", RUSSIAN_LOCALE);
                    variantValueCategory.setName(productAttribute.getValue(), Locale.ENGLISH);
                    eventService.publishEvent(new TranslateVariantValueCategoryEvent(getCatalogVersion()));
                }
            } else {
                variantValueCategory.setName(productAttribute.getValue(), locale);
            }

            modelService.save(variantValueCategory);
            crossboardProduct.getSupercategories().add(variantValueCategory);
        }

        modelService.save(baseProduct);
    }

    private ProductDescription unmarshallProductDescription(final String element) throws IntegrationInternalException, JAXBException {
        final Object result = unmarshall(element);
        final UploadProductDescriptionsRequest.ProductDescriptions productDescriptions = ((JAXBElement<UploadProductDescriptionsRequest>) result).getValue().getProductDescriptions();
        final List<ProductDescription> productDescriptionList = productDescriptions.getProductDescription();

        if (productDescriptionList.size() == 0) {
            throw new IntegrationInternalException(String.format("Couldn't find any product descriptions in generated string [%1$s]", element));
        }
        if (productDescriptionList.size() > 1) {
            throw new IntegrationInternalException(String.format("Too many product descriptions in generated string [%1$s]", element));
        }

        return productDescriptionList.get(0);
    }

    private Locale getLocale(final Map<Object, Object> headers){
        final Object langHeader = headers.get(CrossboardintegrationConstants.MESSAGE_HEADER_LANGUAGE);
        final Locale localeHeader = langHeader != null ? Locale.forLanguageTag((String) langHeader) : null;
        return localeHeader != null ? localeHeader : CrossboardintegrationConstants.DEFAULT_LOCALE;
    }

    @Override
    protected Class<?> getObjectFactoryClass() {
        return ObjectFactory.class;
    }

    public void setCrossboardCategoryService(CrossboardCategoryService crossboardCategoryService) {
        this.crossboardCategoryService = crossboardCategoryService;
    }

    public void setMerchantService(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setTypeService(TypeService typeService) {
        this.typeService = typeService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setValidatorService(ValidatorService validatorService) {
        this.validatorService = validatorService;
    }

    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }

    public void setCrossboardProductService(CrossboardProductService crossboardProductService) {
        this.crossboardProductService = crossboardProductService;
    }

    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public void setServiceBean(ServiceBean serviceBean) {
        this.serviceBean = serviceBean;
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    private CatalogVersionModel getCatalogVersion() {
        if (catalogVersion == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Fetching new catalogVersion bean...");
            }
            catalogVersion = catalogVersionService.getCatalogVersion(serviceBean.getProductCatalogName(),
                    serviceBean.getProductCatalogVersion());
            LOG.debug(String.format("Was trying to fetch catalog version with catalog name '%1$s' and catalog version '%2$s'",
                    serviceBean.getProductCatalogName(), serviceBean.getProductCatalogVersion()));
        }
        if (catalogVersion == null) {
            LOG.warn("Got null catalog version!");
        } else {
            LOG.debug(String.format("Got: %1$s(%2$s)", catalogVersion.getCatalog().getName(), catalogVersion.getVersion()));
        }
        return catalogVersion;
    }
}