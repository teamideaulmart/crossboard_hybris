package ru.teamidea.crossboard.b2c.integration.routers;

/**
 * Created by Marina on 12.09.2016.
 */
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


public class FileNameRouter implements Serializable
{
    private static final long serialVersionUID = 1L;

    private final static Logger LOG = Logger.getLogger(FileNameRouter.class);

    private final Map<String, String> fileNamesChannelMapping;

    public FileNameRouter(Map<String, String> fileNamesChannelMapping){
        this.fileNamesChannelMapping = fileNamesChannelMapping;
    }

    /**
     * Returns channel name for file processing
     * Based on name of the file
     * @param file file
     * @return channel name
     */
    public String route(File file)
    {
        if(MapUtils.isEmpty(fileNamesChannelMapping)){
            LOG.warn(String.format("[%1$s] Incorrect configuration: empty file names - channels mapping", file.getName()));
            return null;
        }

        Set<String> channels = fileNamesChannelMapping.keySet().stream().filter(s -> file.getName().contains(s)).collect(Collectors.toSet());

        if (channels.size() == 0)
        {
            LOG.warn(String.format("[%1$s] Incorrect configuration: no channels found for file name", file.getName()));
            return null;
        }

        if (channels.size() > 1)
        {
            LOG.warn(String.format("[%2$s] Incorrect configuration: found channels %1$s for file name", channels.stream().collect(Collectors.joining(", ")), file.getName()));
            return null;
        }

        String channelId = fileNamesChannelMapping.get(channels.iterator().next());
        if (LOG.isDebugEnabled())
        {
            LOG.debug(String.format("[%1$s] Routing file to the channel with id %2$s", file.getName(), channelId));
        }

        return channelId;
    }

}