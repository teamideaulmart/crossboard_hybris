package ru.teamidea.crossboard.b2c.integration.utils.result;

import java.io.Serializable;

public enum Result implements Serializable {
    SUCCESS(0, "Success"),
    INVALID_XML(150, "Invalid XML"),
    PROCESSING_ERRORS(120, "Errors during items processing"),
    INTERNAL_ERROR(100, "Internal server error"),
    UNKNOWN_MERCHANT(170, "Unlnown merchant");

    private final int value;
    private final String message;

    Result(int value, String message) {
        this.value = value;
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

}
