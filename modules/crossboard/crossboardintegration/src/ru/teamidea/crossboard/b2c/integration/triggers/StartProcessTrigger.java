package ru.teamidea.crossboard.b2c.integration.triggers;

import de.hybris.platform.util.Config;
import org.apache.log4j.Logger;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;

import java.util.Date;

/**
 * Created by Marina on 22.01.2016.
 */
public class StartProcessTrigger implements Trigger
{
    protected final static Logger LOG = Logger.getLogger(StartProcessTrigger.class);

    private final boolean enabled;
    private final long period;
    private final long initialDelay;

    private final String parameterName;

    public StartProcessTrigger(long period, long initialDelay, String parameterName){
        this.period = period;
        this.initialDelay = initialDelay;
        this.parameterName = parameterName;
        this.enabled = Config.getBoolean(parameterName, false);
    }

    /**
     * Calculates date and time of the next process execution
     * Based on set time period
     * @param triggerContext trigger context
     * @return date and time of the next execution
     */
    @Override
    public Date nextExecutionTime(TriggerContext triggerContext)
    {
        if (enabled)
        {
            return triggerContext.lastScheduledExecutionTime() == null ? new Date(System.currentTimeMillis() + this.initialDelay) :
                    new Date(triggerContext.lastCompletionTime().getTime() + this.period);
        }

        LOG.info(String.format("Integration process disabled. Check system parameter %1$s.", parameterName));
        return null;
    }
}