package ru.teamidea.crossboard.b2c.integration.processors.callback;

import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import ru.teamidea.crossboard.b2c.client.ws.CatalogCallbackClient;
import ru.teamidea.crossboard.b2c.integration.aspect.ExecuteInSession;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.processors.file.ArchiveFileProcessor;
import ru.teamidea.crossboard.b2c.integration.processors.file.ErrorFileProcessor;
import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.user.MerchantService;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Marina on 14.09.2016.
 */
public class CallbackProcessor implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(CallbackProcessor.class);

    private CatalogCallbackClient catalogCallbackClient;
    private MerchantService merchantService;

    private ArchiveFileProcessor archiveFileProcessor;
    private ErrorFileProcessor errorFileProcessor;
    private String requestPath;

    @ExecuteInSession
    public void process(final Message<CallbackRequest> callbackRequest,
                        @Header(CrossboardintegrationConstants.MESSAGE_HEADER_MERCHANT_ID) String merchantId,
                        @Header(CrossboardintegrationConstants.MESSAGE_HEADER_FILE_NAME) String fileName){

        boolean error = false;

        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("[%1$s] Start callback processing for merchant id [%2$s]", fileName, merchantId));
        }

        try {
            error = !callbackRequest.getPayload().getResult().getResultCode().equals(CatalogCallbackClient.SUCCESS);
            processCallback(callbackRequest.getPayload(), merchantId);
        }
        catch(RuntimeException ex){
            LOGGER.error(String.format("[%1$s] Error when trying to process callback request", fileName), ex);
        }

        try {
            if (error) {
                errorRequestFile(fileName, callbackRequest.getPayload().getResult().getErrorMessage());
            } else {
                archiveRequestFile(fileName);
            }
        }
        catch(IOException ex){
            LOGGER.error(String.format("[%1$s] Error when trying to archive file", fileName), ex);
        }
        catch(RuntimeException ex){
            LOGGER.error(String.format("[%1$s] Error when trying to archive file", fileName), ex);
        }

        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("[%1$s] End callback processing for merchant id [%2$s]", fileName, merchantId));
        }
    }

    protected void processCallback(final CallbackRequest callbackRequest, final String merchantId){
        final MerchantModel merchant = merchantService.findMerchantForExternalCode(merchantId);
        if(merchant == null){
            LOGGER.error(String.format("Couldn't send %1$s callback due to... merchant for id %2$s not found",
                    callbackRequest.getCalbackType().value(),  merchantId));
        }
        else{
            catalogCallbackClient.sendCallback(callbackRequest, merchant);
        }
    }

    private void archiveRequestFile(final String fName) throws IOException {
        final Path path = Paths.get(requestPath + File.separator + fName);
        archiveFileProcessor.archive(path);
    }

    private void errorRequestFile(final String fName, final Object error) throws IOException {
        final Path path = Paths.get(requestPath + File.separator + fName);
        errorFileProcessor.moveToError(path, error);
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public void setArchiveFileProcessor(ArchiveFileProcessor archiveFileProcessor) {
        this.archiveFileProcessor = archiveFileProcessor;
    }

    public void setErrorFileProcessor(ErrorFileProcessor errorFileProcessor) {
        this.errorFileProcessor = errorFileProcessor;
    }

    public void setCatalogCallbackClient(CatalogCallbackClient catalogCallbackClient) {
        this.catalogCallbackClient = catalogCallbackClient;
    }

    public void setMerchantService(MerchantService merchantService) {
        this.merchantService = merchantService;
    }
}