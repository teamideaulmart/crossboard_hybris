package ru.teamidea.crossboard.b2c.integration.validators.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import ru.teamidea.crossboard.b2c.integration.aspect.ExecuteInSession;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.utils.result.Result;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.user.MerchantService;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Marina on 19.09.2016.
 */
public class MerchantValidator implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(MerchantValidator.class);

    @Autowired
    private MerchantService merchantService;

    @ExecuteInSession
    public Message<File> validate(final Message<File> message, @Header(CrossboardintegrationConstants.MESSAGE_HEADER_MERCHANT_ID) String merchantId) {
        final MerchantModel merchantModel = merchantService.findMerchantForExternalCode(merchantId);

        Result result = Result.SUCCESS;
        if(merchantModel == null){
            LOGGER.error(String.format("Couldn't proceed with file %1$s processing due to... merchant for id %2$s not found",
                    message.getPayload().getName(),  merchantId));
            result = Result.UNKNOWN_MERCHANT;
        }

        return MessageBuilder.withPayload(message.getPayload())
                .copyHeadersIfAbsent(message.getHeaders())
                .setHeaderIfAbsent(CrossboardintegrationConstants.MESSAGE_HEADER_VALIDATION_RESULT_CODE, result)
                .build();
    }

}
