package ru.teamidea.crossboard.b2c.integration.event;

import java.util.List;

/**
 * Created by Timofey Klyubin on 20.09.16.
 */
public final class UploadProductDescriptionsEvent extends AbstractProductProcessingEvent {

    public UploadProductDescriptionsEvent(List<String> productCodes) {
        super(productCodes);
    }
}
