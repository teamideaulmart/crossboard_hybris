package ru.teamidea.crossboard.b2c.integration.validators.impl;

import ru.teamidea.crossboard.b2c.integration.utils.SchemaUtils;
import ru.teamidea.crossboard.b2c.integration.validators.AbstractXMLValidator;

/**
 * Created by Marina on 13.09.2016.
 */
public class XMLCatalogValidator extends AbstractXMLValidator {
    private static final long serialVersionUID = 1L;

    @Override
    protected SchemaUtils.XSDSchema getXSDSchema() {
        return SchemaUtils.XSDSchema.catalog;
    }
}
