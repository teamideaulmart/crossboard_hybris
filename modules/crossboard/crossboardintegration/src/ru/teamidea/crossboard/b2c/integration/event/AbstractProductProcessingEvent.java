package ru.teamidea.crossboard.b2c.integration.event;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.List;

/**
 * Created by Timofey Klyubin on 20.09.16.
 */

public abstract class AbstractProductProcessingEvent extends AbstractEvent {

    private List<String> productCodes;

    public AbstractProductProcessingEvent(List<String> productCodes) {
        this.productCodes = productCodes;
    }

    public List<String> getProductCodes() {
        return productCodes;
    }
}
