package ru.teamidea.crossboard.b2c.integration.validators;

/**
 * Created by Marina on 13.09.2016.
 */

import org.apache.log4j.Logger;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.xml.sax.SAXException;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.utils.SchemaUtils;
import ru.teamidea.crossboard.b2c.integration.utils.result.Result;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public abstract class AbstractXMLValidator<E> implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(AbstractXMLValidator.class);

    public Message<File> validate(Message<File> message) {
        final File file = message.getPayload();

        Result result = Result.SUCCESS;
        String errorMessage = "";
        try {
            validate(SchemaUtils.getSchema(getXSDSchema()), file);
        } catch (SAXException e) {
            LOGGER.error("SAX Error while validate XML file : ", e);
            result = Result.INVALID_XML;
            errorMessage = e.getMessage();
        } catch (Exception e) {
            LOGGER.error("Error while validate XML file : ", e);
            result = Result.INTERNAL_ERROR;
            errorMessage = e.getMessage();
        }

        return MessageBuilder.withPayload(message.getPayload())
                .copyHeadersIfAbsent(message.getHeaders())
                .setHeader(CrossboardintegrationConstants.MESSAGE_HEADER_VALIDATION_RESULT_CODE, result)
                .setHeader(CrossboardintegrationConstants.MESSAGE_HEADER_VALIDATION_ERROR_MESSAGE, errorMessage)
                .build();
    }

    private static void validate(final Schema schema, final File xml) throws SAXException, IOException {
        final Validator validator = schema.newValidator();
        final Source source = new StreamSource(xml);
        validator.validate(source);
    }

    protected abstract SchemaUtils.XSDSchema getXSDSchema();
}