package ru.teamidea.crossboard.b2c.integration.aggregators;

import org.apache.log4j.Logger;
import org.springframework.messaging.handler.annotation.Header;
import ru.teamidea.crossboard.b2c.integration.constants.CrossboardintegrationConstants;
import ru.teamidea.crossboard.b2c.integration.utils.result.ResultHelper;
import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.cb.common.CallbackResponseResult;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Marina on 14.09.2016.
 */
public abstract class AbstractCallbackRequestAggregator implements Serializable {
    private static final long serialVersionUID = 1L;

    protected static final Logger LOG = Logger.getLogger(AbstractCallbackRequestAggregator.class);

    public CallbackRequest aggregate(List<CallbackResponseResult[]> processingResults, @Header(CrossboardintegrationConstants.MESSAGE_HEADER_FILE_NAME) String fileName) {
        if(LOG.isDebugEnabled()){
            LOG.debug(String.format("[%1$s] Starting to aggregate processing results", fileName));
        }

        final CallbackRequest callbackRequest = new CallbackRequest();
        setCallbackType(callbackRequest);
        callbackRequest.setResponseResults(new CallbackRequest.ResponseResults());

        for (CallbackResponseResult[] p : processingResults) {
            callbackRequest.getResponseResults().getResponseResult().addAll(Arrays.asList(p));
        }
        ResultHelper.setCommonResult(callbackRequest);

        if(LOG.isDebugEnabled()){
            LOG.debug(String.format("[%1$s] Finishing to aggregate processing results", fileName));
        }
        return callbackRequest;
    }

    protected abstract void setCallbackType(CallbackRequest callbackRequest);
}