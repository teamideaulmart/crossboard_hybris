package ru.teamidea.crossboard.b2c.integration.result;

import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.cb.common.CallbackType;
import ru.ulmart.cb.common.ResponseResult;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Marina on 19.09.2016.
 */
public class CallbackResultMaker extends AbstractResultMaker<CallbackRequest> {

    private static final long serialVersionUID = 1L;

    private final static Logger LOG = Logger.getLogger(CallbackResultMaker.class);

    private Map<String, String> fileNamesCallbackTypeMapping;

    public CallbackResultMaker(Map<String, String> fileNamesCallbackTypeMapping){
        this.fileNamesCallbackTypeMapping = fileNamesCallbackTypeMapping;
    }

    protected CallbackRequest newResponse(final ResponseResult result, final String fName) {
        final CallbackRequest callback = new CallbackRequest();
        setCallbackType(callback, fName);
        callback.setResult(result);
        return callback;
    }

    protected void setCallbackType(final CallbackRequest callback, final String fName) {
        if(MapUtils.isEmpty(fileNamesCallbackTypeMapping)){
            LOG.warn(String.format("[%1$s] Incorrect configuration: empty file names - callback result type mapping", fName));
        }

        Set<String> callbackResultTypes = fileNamesCallbackTypeMapping.keySet().stream().filter(s -> fName.contains(s)).collect(Collectors.toSet());

        if (callbackResultTypes.size() == 0)
        {
            LOG.warn(String.format("[%1$s] Incorrect configuration: no callback result type found for file name", fName));
        }

        if (callbackResultTypes.size() > 1)
        {
            LOG.warn(String.format("[%2$s] Incorrect configuration: found callback result type %1$s for file name", callbackResultTypes.stream().collect(Collectors.joining(", ")), fName));
        }

        final String callbackResultType = fileNamesCallbackTypeMapping.get(callbackResultTypes.iterator().next());

        try {
            callback.setCalbackType(CallbackType.valueOf(callbackResultType));
        }
        catch(RuntimeException ex){
            LOG.error(String.format("Couldn't find callback result type for code %1$s", callbackResultType), ex);
        }
    }

}
