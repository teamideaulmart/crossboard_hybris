package ru.teamidea.crossboard.b2c.integration.cronjobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.teamidea.crossboard.b2c.integration.model.cronjob.CleanUpOldFilesCronJobModel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

/**
 * Created by Timofey Klyubin on 19.09.16.
 */
public class CleanUpOldFilesJobPerformable extends AbstractJobPerformable<CleanUpOldFilesCronJobModel> {

    private static final Logger LOG = Logger.getLogger(CleanUpOldFilesJobPerformable.class);

    private static final String INTEGRATION_FOLDER_SUFFIX = ".dir";
 	private static final String INTEGRATION_FOLDER_PREFIX = "integration.local.";

    private ConfigurationService configurationService;

    @Autowired
    @Qualifier("configurationService")
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Override
    public PerformResult perform(final CleanUpOldFilesCronJobModel cronJob) {
        if (cronJob == null) {
            LOG.warn("Provided CleanUpOldFilesCronJobModel is null");
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }

        final LocalDate deleteDate = getDeleteDate(cronJob.getDaysOld());
        final Map<String, String> cleanupFolders = cronJob.getCleanupFolders();

        if (MapUtils.isNotEmpty(cleanupFolders)) {
            boolean success = true;
            for (String folderCode : cleanupFolders.keySet()) {
                if (clearAbortRequestedIfNeeded(cronJob)) {
                    return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
                }

                final Path folder = getFolder(folderCode);
                if (folder != null && Files.exists(folder)) {
                    try {
                        Files.walk(folder)
                                .filter(Files::isRegularFile)
                                .forEach((pathToFile) -> {
                                    File file = new File(pathToFile.toUri());
                                    LocalDate lastModified = getLastModifiedDate(file);
                                    if (lastModified.isBefore(deleteDate)) {
                                        if (file.delete()) {
                                            LOG.info(String.format("'%1$s' has been deleted.", file.getName()));
                                        } else {
                                            LOG.warn(String.format("Could not delete file: '%1$s'", file.getAbsolutePath()));
                                        }
                                    }
                                });
                    } catch (IOException e) {
                        LOG.error(String.format("IOException occurred during cleanup process: \"%1$s\"", e.getMessage()));
                    }
                } else {
                    LOG.warn(String.format("Directory with code '%1$s' does not exist.", folder));
                    success = false;
                }
            }
            return new PerformResult(success ? CronJobResult.SUCCESS : CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }
        LOG.error("Unexpected error occurred.");
        return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
    }

    @Override
    public boolean isAbortable() {
        return true;
    }

    private Path getFolder(String folder) {
        String path = configurationService.getConfiguration()
                .getString(INTEGRATION_FOLDER_PREFIX + folder + INTEGRATION_FOLDER_SUFFIX);
        return path.equals("") ? null : Paths.get(path);
    }

    private LocalDate getDeleteDate(int daysOld) {
        return LocalDate.now().minus(daysOld, ChronoUnit.DAYS);
    }

    private LocalDate getLastModifiedDate(File file) {
        final Instant lastModified = new Date(file.lastModified()).toInstant();
        return LocalDateTime.ofInstant(lastModified, ZoneId.systemDefault()).toLocalDate();
    }
}
