package ru.teamidea.crossboard.b2c.integration.event.listener;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.util.CollectionUtils;
import ru.teamidea.crossboard.b2c.integration.beans.ServiceBean;
import ru.teamidea.crossboard.b2c.integration.event.UploadProductDescriptionsEvent;
import ru.ulmart.crossboard.core.services.media.MediaDownloadService;
import ru.ulmart.crossboard.core.services.translate.DescriptionTranslateService;

/**
 * Created by Timofey Klyubin on 20.09.16.
 */
public class UploadProductDescriptionsEventListener extends AbstractProductProcessingEventListener<UploadProductDescriptionsEvent> {

    private static final Logger LOG = Logger.getLogger(UploadProductDescriptionsEventListener.class);

    private MediaDownloadService mediaDownloadService;
    private ServiceBean serviceBean;
    private CatalogVersionService catalogVersionService;
    private UserService userService;

    @Autowired
    private DescriptionTranslateService descriptionTranslateService;

    @Override
    protected void onEvent(UploadProductDescriptionsEvent uploadProductDescriptionsEvent) {
        if(LOG.isDebugEnabled()) {
            LOG.debug("UploadProductDescriptionsEvent fired");
        }

        super.onEvent(uploadProductDescriptionsEvent);

        if (CollectionUtils.isEmpty(uploadProductDescriptionsEvent.getProductCodes())) {
            LOG.warn("Uploading descriptions didn't finish successfully, skipping media downloading.");
            return;
        }

        setupSession();

        final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(
                serviceBean.getProductCatalogName(),
                serviceBean.getProductCatalogVersion()
        );

        mediaDownloadService.downloadMediasForProducts(
                uploadProductDescriptionsEvent.getProductCodes(),
                catalogVersion
        );

        descriptionTranslateService.translateProductDescriptions(
                uploadProductDescriptionsEvent.getProductCodes(),
                catalogVersion
        );

        if(LOG.isDebugEnabled()) {
            LOG.debug("UploadProductDescriptionsEvent processed");
        }
    }

    private void setupSession() {
        userService.setCurrentUser(userService.getUserForUID(serviceBean.getUser()));

        catalogVersionService.setSessionCatalogVersion(
                serviceBean.getProductCatalogName(),
                serviceBean.getProductCatalogVersion()
        );
    }

    // -- Spring autowiring

    @Autowired
    public void setMediaDownloadService(MediaDownloadService mediaDownloadService) {
        this.mediaDownloadService = mediaDownloadService;
    }

    @Autowired
    public void setServiceBean(ServiceBean serviceBean) {
        this.serviceBean = serviceBean;
    }

    @Autowired
    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
