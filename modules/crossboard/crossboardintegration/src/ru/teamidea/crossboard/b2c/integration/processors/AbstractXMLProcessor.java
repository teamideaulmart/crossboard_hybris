package ru.teamidea.crossboard.b2c.integration.processors;

/**
 * Created by Marina on 14.09.2016.
 */
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

public abstract class AbstractXMLProcessor {

    private static final String UTF_8 = "UTF-8";
    private Unmarshaller unmarshaller;

    private Unmarshaller getUnmarshaller() throws JAXBException {
        if (unmarshaller == null){
            final JAXBContext jbc = JAXBContext.newInstance(getObjectFactoryClass());
            unmarshaller = jbc.createUnmarshaller();
        }
        return unmarshaller;
    }

    protected Object unmarshall(final String element) throws JAXBException {
        try {
            final InputStream is = new ByteArrayInputStream(element.getBytes(Charset.forName(UTF_8)));
            return getUnmarshaller().unmarshal(is);
        } catch (Exception e) {
            String el = element;

            throw e;
        }
    }

    protected abstract Class<?> getObjectFactoryClass();
}
