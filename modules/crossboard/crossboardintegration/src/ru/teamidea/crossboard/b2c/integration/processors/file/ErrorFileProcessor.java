package ru.teamidea.crossboard.b2c.integration.processors.file;

import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.*;

/**
 * Created by Timofey Klyubin on 07.10.16.
 */
public class ErrorFileProcessor implements Serializable {

    private static final Logger LOG = Logger.getLogger(ErrorFileProcessor.class);

    private String errorDirectory;

    public ErrorFileProcessor(String errorDirectory) {
        this.errorDirectory = errorDirectory;
    }

    public void moveToError(final Path file, final Object error) throws IOException {
        ServicesUtil.validateParameterNotNull(file, "file must non be null");
        LOG.info(String.format("moving [%1$s] to error directory due to %2$s", file, error));

        final Path errorPath = Paths.get(errorDirectory);
        Files.createDirectories(errorPath);

        try {
            Files.move(file, Paths.get(errorDirectory, file.getFileName().toString()));
        } catch (IOException e) {
            LOG.error(String.format("failed to move %1$s to error directory (%2$s)", file, errorDirectory), e);
        }
    }
}
