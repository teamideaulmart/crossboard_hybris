package ru.teamidea.crossboard.b2c.integration.parsers;

import ru.teamidea.crossboard.b2c.integration.handlers.ProductDescriptionsHandler;
import ru.teamidea.crossboard.b2c.integration.utils.SchemaUtils;
import ru.ulmart.cb.catalog.ProductDescription;

/**
 * Created by Marina on 13.09.2016.
 */
public class ProductDescriptionsParser extends AbstractElementParser<ProductDescription, ProductDescriptionsHandler> {
    private static final long serialVersionUID = 1L;

    @Override
    protected SchemaUtils.XSDSchema getXSDSchema() {
        return SchemaUtils.XSDSchema.catalog;
    }

    @Override
    protected ProductDescriptionsHandler makeHandler(String fileName, int collectionSize) {
        return new ProductDescriptionsHandler(fileName, collectionSize);
    }
}
