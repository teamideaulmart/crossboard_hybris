/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package ru.teamidea.crossboard.b2c.integration.constants;

import java.util.Locale;

/**
 * Global class for all Crossboardintegration constants. You can add global constants for your extension into this class.
 */
public final class CrossboardintegrationConstants extends GeneratedCrossboardintegrationConstants
{
	public static final String EXTENSIONNAME = "crossboardintegration";

	private CrossboardintegrationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "crossboardintegrationPlatformLogo";

	public static final String MESSAGE_HEADER_VALIDATION_RESULT_CODE = "validationResult";
	public static final String MESSAGE_HEADER_MERCHANT_ID = "merchantId";
	public static final String MESSAGE_HEADER_VALIDATION_ERROR_MESSAGE = "errorMessage";
	public static final String MESSAGE_HEADER_FILE_NAME = "fileName";
	public static final String MESSAGE_HEADER_LANGUAGE = "language";

	public static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
	public static final Locale RUSSIAN_LOCALE = Locale.forLanguageTag("ru");
}
