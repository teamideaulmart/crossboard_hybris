package ru.teamidea.crossboard.b2c.integration.validators;


import de.hybris.platform.core.model.product.ProductModel;
import ru.ulmart.cb.catalog.ProductAttribute;

import java.util.List;
import java.util.Locale;

/**
 * Created by Timofey Klyubin on 12.09.16.
 */
public interface ValidatorService {
    /**
     * Takes ProductModel as input, validates it against set of rules listed here (Section 2.2):
     * https://docs.google.com/document/d/1BpW-9uw4E9KTNrq3kjzyG37pH6YRCBnG9NC_qsM3sqg/edit
     *
     * @param productModel model to be validated
     * @param locale
     * @return List of error messages. The list is empty if the product is valid.
     */
    List<String> validateCrossboardProductModel(ProductModel productModel, Locale locale);
    List<String> validateAttributesUniqueness(List<ProductAttribute> attributes);
}
