package ru.teamidea.crossboard.b2c.ftp.service.impl;

import ru.teamidea.crossboard.b2c.ftp.service.AbstractCSVConversionStrategy;
import ru.teamidea.crossboard.b2c.ftp.service.CSVConversionStrategy;

import java.util.Map;

/**
 * Created by Timofey Klyubin on 27.09.16.
 */
public final class ProductDescriptionsCSVConversionStrategy extends AbstractCSVConversionStrategy implements CSVConversionStrategy {

    private static final String REQUEST_ROOT_TAG = "uploadProductDescriptionsRequest";
    private static final String LIST_ROOT_TAG = "productDescriptions";
    private static final String ROOT_TAG = "productDescription";

    public ProductDescriptionsCSVConversionStrategy() {
        super(REQUEST_ROOT_TAG, LIST_ROOT_TAG);
    }

    @Override
    protected void buildOne(final Map<Integer, String> data, final StringBuilder builder) {
        builder.append("<" + CATALOG_NAMESPACE + ":" + ROOT_TAG + ">");

        builder.append("<" + CATALOG_NAMESPACE + ":merchantSku>")
                .append(getByName("merchantSku", data))
                .append("</" + CATALOG_NAMESPACE + ":merchantSku>");

        builder.append("<" + CATALOG_NAMESPACE + ":vendorSku>")
                .append(getByName("vendorSku", data))
                .append("</" + CATALOG_NAMESPACE + ":vendorSku>");

        builder.append("<" + CATALOG_NAMESPACE + ":marketplaceCategories>");
        headers.entrySet()
                .stream()
                .filter(entry -> entry.getKey().startsWith("marketplaceCategoryId"))
                .forEach((entry) -> {
                    if (!data.get(entry.getValue()).equals("")) {
                        builder.append("<" + CATALOG_NAMESPACE + ":marketplaceCategoryId>")
                                .append(data.get(entry.getValue()))
                                .append("</" + CATALOG_NAMESPACE + ":marketplaceCategoryId>");
                    }
                });
        builder.append("</" + CATALOG_NAMESPACE + ":marketplaceCategories>");

        if (!getByName("merchantCategoryId", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":merchantCategoryId>")
                    .append(getByName("merchantCategoryId", data))
                    .append("</" + CATALOG_NAMESPACE + ":merchantCategoryId>");
        }

        if (!getByName("shortName", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":shortName>")
                    .append(getByName("shortName", data))
                    .append("</" + CATALOG_NAMESPACE + ":shortName>");
        }

        builder.append("<" + CATALOG_NAMESPACE + ":name>")
                .append(getByName("name", data))
                .append("</" + CATALOG_NAMESPACE + ":name>");

        if (!getByName("shortDescription", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":shortDescription>")
                    .append(getByName("shortDescription", data))
                    .append("</" + CATALOG_NAMESPACE + ":shortDescription>");
        }

        builder.append("<" + CATALOG_NAMESPACE + ":fullDescription>")
                .append(getByName("fullDescription", data))
                .append("</" + CATALOG_NAMESPACE + ":fullDescription>");

        builder.append("<" + CATALOG_NAMESPACE + ":url>")
                .append(getByName("url", data))
                .append("</" + CATALOG_NAMESPACE + ":url>");

        builder.append("<" + CATALOG_NAMESPACE + ":brand>")
                .append(getByName("brand", data))
                .append("</" + CATALOG_NAMESPACE + ":brand>");

        builder.append("<" + CATALOG_NAMESPACE + ":manufacturer>")
                .append(getByName("manufacturer", data))
                .append("</" + CATALOG_NAMESPACE + ":manufacturer>");

        builder.append("<" + CATALOG_NAMESPACE + ":model>")
                .append(getByName("model", data))
                .append("</" + CATALOG_NAMESPACE + ":model>");

        if (!getByName("warrantyPeriod", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":warrantyPeriod>")
                    .append(getByName("warrantyPeriod", data))
                    .append("</" + CATALOG_NAMESPACE + ":warrantyPeriod>");
        }

        if (!getByName("length", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":length>")
                    .append(getByName("length", data))
                    .append("</" + CATALOG_NAMESPACE + ":length>");
        }

        if (!getByName("width", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":width>")
                    .append(getByName("width", data))
                    .append("</" + CATALOG_NAMESPACE + ":width>");
        }

        if (!getByName("height", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":height>")
                    .append(getByName("height", data))
                    .append("</" + CATALOG_NAMESPACE + ":height>");
        }

        if (!getByName("volume", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":volume>")
                    .append(getByName("volume", data))
                    .append("</" + CATALOG_NAMESPACE + ":volume>");
        }

        if (!getByName("weight", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":weight>")
                    .append(getByName("weight", data))
                    .append("</" + CATALOG_NAMESPACE + ":weight>");
        }

        builder.append("<" + CATALOG_NAMESPACE + ":salesUnit>")
                .append(getByName("salesUnit", data))
                .append("</" + CATALOG_NAMESPACE + ":salesUnit>");



        builder.append("<" + CATALOG_NAMESPACE + ":images>");
        headers.entrySet()
                .stream()
                .filter(entry -> entry.getKey().startsWith("imageUrl"))
                .forEach((entry) -> {
                    builder.append("<" + CATALOG_NAMESPACE + ":image>")
                            .append("<" + CATALOG_NAMESPACE + ":url>")
                            .append(data.get(entry.getValue()))
                            .append("</" + CATALOG_NAMESPACE + ":url>")
                            .append("<" + CATALOG_NAMESPACE + ":baseImage>false</" + CATALOG_NAMESPACE + ":baseImage>")
                            .append("</" + CATALOG_NAMESPACE + ":image>");
                });
        builder.append("</" + CATALOG_NAMESPACE + ":images>");

        if (!getByName("variantGroup", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":variantGroup>")
                    .append(getByName("variantGroup", data))
                    .append("</" + CATALOG_NAMESPACE + ":variantGroup>");
        }

        builder.append("<" + CATALOG_NAMESPACE + ":attributes>");
        headers.entrySet()
                .stream()
                .filter(entry -> entry.getKey().startsWith("attributeCode"))
                .forEach((entry) -> {
                    String ind = entry.getKey().replace("attributeCode", "");
                    builder.append("<" + CATALOG_NAMESPACE + ":attribute>");
                    builder.append("<" + CATALOG_NAMESPACE + ":code>")
                            .append(data.get(entry.getValue()))
                            .append("</" + CATALOG_NAMESPACE + ":code>")
                            .append("<" + CATALOG_NAMESPACE + ":name>")
                            .append(getByName("attributeName"+ind, data))
                            .append("</" + CATALOG_NAMESPACE + ":name>");

                    builder.append("<" + CATALOG_NAMESPACE + ":value>")
                            .append(getByName("attributeValue"+ind, data))
                            .append("</" + CATALOG_NAMESPACE + ":value>");

                    if (!getByName("attributeVariant", data).equals("")) {
                        builder.append("<" + CATALOG_NAMESPACE + ":variant>")
                                .append(getByName("attributeVariant" + ind, data).equals("1"))
                                .append("</" + CATALOG_NAMESPACE + ":variant>");
                    }
                    if (!getByName("attributeUnit", data).equals("")) {
                        builder.append("<" + CATALOG_NAMESPACE + ":unit>")
                                .append(getByName("attributeUnit" + ind, data))
                                .append("</" + CATALOG_NAMESPACE + ":unit>");
                    }
                    builder.append("</" + CATALOG_NAMESPACE + ":attribute>");
                });
        builder.append("</" + CATALOG_NAMESPACE + ":attributes>");

        builder.append("</" + CATALOG_NAMESPACE + ":" + ROOT_TAG + ">");
    }

}
