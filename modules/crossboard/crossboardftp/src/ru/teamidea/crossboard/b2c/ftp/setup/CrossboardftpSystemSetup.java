/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package ru.teamidea.crossboard.b2c.ftp.setup;

import static ru.teamidea.crossboard.b2c.ftp.constants.CrossboardftpConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import ru.teamidea.crossboard.b2c.ftp.constants.CrossboardftpConstants;
import ru.teamidea.crossboard.b2c.ftp.service.CrossboardftpService;


@SystemSetup(extension = CrossboardftpConstants.EXTENSIONNAME)
public class CrossboardftpSystemSetup
{
	private final CrossboardftpService crossboardftpService;

	public CrossboardftpSystemSetup(final CrossboardftpService crossboardftpService)
	{
		this.crossboardftpService = crossboardftpService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		crossboardftpService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return CrossboardftpSystemSetup.class.getResourceAsStream("/crossboardftp/sap-hybris-platform.png");
	}
}
