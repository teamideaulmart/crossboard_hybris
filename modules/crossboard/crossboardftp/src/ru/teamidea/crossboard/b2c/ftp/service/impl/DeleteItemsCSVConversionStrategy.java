package ru.teamidea.crossboard.b2c.ftp.service.impl;

import ru.teamidea.crossboard.b2c.ftp.service.AbstractCSVConversionStrategy;
import ru.teamidea.crossboard.b2c.ftp.service.CSVConversionStrategy;

import java.util.Map;

/**
 * Created by Timofey Klyubin on 28.09.16.
 */
public final class DeleteItemsCSVConversionStrategy extends AbstractCSVConversionStrategy implements CSVConversionStrategy {

    private static final String REQUEST_ROOT_TAG = "deleteItemsRequest";
    private static final String LIST_ROOT_TAG = "items";
    private static final String ROOT_TAG = "item";

    public DeleteItemsCSVConversionStrategy() {
        super(REQUEST_ROOT_TAG, LIST_ROOT_TAG);
    }

    @Override
    protected void afterHeader(StringBuilder builder, Map<Integer, String> data) {
        builder.append("<" + CATALOG_NAMESPACE + ":deleteItemType>")
                .append(getByName("deleteItemType", data))
                .append("</" + CATALOG_NAMESPACE + ":deleteItemType>");
    }

    @Override
    protected void buildOne(Map<Integer, String> data, StringBuilder builder) {
        builder.append("<" + CATALOG_NAMESPACE + ":" + ROOT_TAG + ">");

        builder.append("<" + COMMON_NAMESPACE + ":uid1>")
                .append(getByName("uid1", data))
                .append("</" + COMMON_NAMESPACE + ":uid1>");

        if (!getByName("uid2", data).equals("")) {
            builder.append("<" + COMMON_NAMESPACE + ":uid2>")
                    .append(getByName("uid2", data))
                    .append("</" + COMMON_NAMESPACE + ":uid2>");
        }

        if (!getByName("uid3", data).equals("")) {
            builder.append("<" + COMMON_NAMESPACE + ":uid3>")
                    .append(getByName("uid3", data))
                    .append("</" + COMMON_NAMESPACE + ":uid3>");
        }

        builder.append("</" + CATALOG_NAMESPACE + ":" + ROOT_TAG + ">");

    }
}
