package ru.teamidea.crossboard.b2c.ftp.facade;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.teamidea.crossboard.b2c.ftp.service.CSVConversionService;
import ru.teamidea.crossboard.b2c.ftp.service.CSVConversionStrategy;
import ru.teamidea.utils.util.TimestampSequenceProvider;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by Timofey Klyubin on 28.09.16.
 */
public class CSVConversionFacade {

    private static final Logger LOG = Logger.getLogger(CSVConversionFacade.class);

    private final CSVConversionStrategy conversionStrategy;
    private final String targetFileName;

    @Value("${crossboardftp.csv.encoding}")
    private String encoding;

    @Value("${integration.local.processing.dir}")
    private String processingDirectory;

    private CSVConversionService conversionService;

    public CSVConversionFacade(CSVConversionStrategy conversionStrategy, String targetFileName) {
        this.conversionStrategy = conversionStrategy;
        this.targetFileName = targetFileName;
    }

    public Path convert(File csvData) throws IOException {
        final InputStream is = new FileInputStream(csvData);
        LOG.info(String.format("Started conversion of CSV file %1$s", csvData.getAbsolutePath()));
        ByteArrayOutputStream os = conversionService.convertCSV2XML(is, conversionStrategy, encoding);
        String fileName = targetFileName + "_" + String.valueOf(TimestampSequenceProvider.getSequenceNumber()) + ".xml";
        LOG.info("Conversion has finished, writing result to " + fileName);
        Files.write(Paths.get(processingDirectory, fileName), os.toByteArray(),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
        LOG.info("Successfully wrote to " + fileName);
        return Paths.get(csvData.getAbsolutePath());
    }

    @Autowired
    public void setConversionService(CSVConversionService conversionService) {
        this.conversionService = conversionService;
    }
}
