package ru.teamidea.crossboard.b2c.ftp.service.impl;

import ru.teamidea.crossboard.b2c.ftp.service.AbstractCSVConversionStrategy;
import ru.teamidea.crossboard.b2c.ftp.service.CSVConversionStrategy;

import java.util.Map;

/**
 * Created by Timofey Klyubin on 28.09.16.
 */
public final class ProductOffersCSVConversionStrategy extends AbstractCSVConversionStrategy implements CSVConversionStrategy {

    private static final String REQUEST_ROOT_TAG = "uploadProductOffersRequest";
    private static final String LIST_ROOT_TAG = "productOffers";
    private static final String ROOT_TAG = "productOffer";

    public ProductOffersCSVConversionStrategy() {
        super(REQUEST_ROOT_TAG, LIST_ROOT_TAG);
    }

    @Override
    protected void buildOne(final Map<Integer, String> data, final StringBuilder builder) {
        builder.append("<" + CATALOG_NAMESPACE + ":" + ROOT_TAG + ">");

        builder.append("<" + CATALOG_NAMESPACE + ":merchantSku>")
                .append(getByName("merchantSku", data))
                .append("</" + CATALOG_NAMESPACE + ":merchantSku>");

        builder.append("<" + CATALOG_NAMESPACE + ":availableStock>")
                .append(getByName("availableStock", data))
                .append("</" + CATALOG_NAMESPACE + ":availableStock>");

        builder.append("<" + CATALOG_NAMESPACE + ":salesUnit>")
                .append(getByName("salesUnit", data))
                .append("</" + CATALOG_NAMESPACE + ":salesUnit>");

        builder.append("<" + CATALOG_NAMESPACE + ":unitPrice>")
                .append("<" + CATALOG_NAMESPACE + ":priceValue>")
                .append(getByName("priceValue", data))
                .append("</" + CATALOG_NAMESPACE + ":priceValue>")
                .append("<" + CATALOG_NAMESPACE + ":currency>")
                .append(getByName("currency", data))
                .append("</" + CATALOG_NAMESPACE + ":currency>")
                .append("</" + CATALOG_NAMESPACE + ":unitPrice>");

        if (!getByName("startDate", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":startDate>")
                    .append(getByName("startDate", data))
                    .append("</" + CATALOG_NAMESPACE + ":startDate>");
        }

        if (!getByName("endDate", data).equals("")) {
            builder.append("<" + CATALOG_NAMESPACE + ":endDate>")
                    .append(getByName("endDate", data))
                    .append("</" + CATALOG_NAMESPACE + ":endDate>");
        }

        builder.append("</" + CATALOG_NAMESPACE + ":" + ROOT_TAG + ">");
    }
}
