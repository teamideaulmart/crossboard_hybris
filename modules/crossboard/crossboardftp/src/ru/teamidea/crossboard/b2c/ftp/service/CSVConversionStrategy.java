package ru.teamidea.crossboard.b2c.ftp.service;

import de.hybris.platform.util.CSVReader;

import java.util.Map;

/**
 * Created by Timofey Klyubin on 27.09.16.
 */
public interface CSVConversionStrategy {
    void combineCSVData(final CSVReader csvReader, final StringBuilder builder);
    void setHeaders(final Map<String, Integer> headers);
}
