package ru.teamidea.crossboard.b2c.ftp.service;

import de.hybris.platform.util.CSVReader;

import java.util.Map;

/**
 * Created by Timofey Klyubin on 28.09.16.
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractCSVConversionStrategy implements CSVConversionStrategy {

    protected static final String CATALOG_NAMESPACE = "cat";
    protected static final String COMMON_NAMESPACE = "com";

    protected final String REQUEST_ROOT_TAG;
    protected final String LIST_ROOT_TAG;

    private static final String VERSION = "1.0";
    private static final String XMLNS = "xmlns:" + CATALOG_NAMESPACE + "=\"http://www.cb.ulmart.ru/catalog/\" xmlns:" +
            COMMON_NAMESPACE + "=\"http://www.cb.ulmart.ru/common/\" " + COMMON_NAMESPACE +
            ":version=\"" + VERSION + "\"";

    protected Map<String, Integer> headers;

    public AbstractCSVConversionStrategy(String requestRootTag, String listRootTag) {
        this.REQUEST_ROOT_TAG = requestRootTag;
        this.LIST_ROOT_TAG = listRootTag;
    }

    protected String getByName(final String attributeName, final Map<Integer, String> data) {
        Integer ind = headers.get(attributeName);
        if (ind == null) {
            return "";
        }
        return data.get(ind);
    }

    protected void buildHeader(StringBuilder builder, Map<Integer, String> data) {
        builder.append("<" + CATALOG_NAMESPACE + ":header>");

        builder.append("<" + COMMON_NAMESPACE + ":language>")
                .append(data.get(headers.get("language")))
                .append("</" + COMMON_NAMESPACE + ":language>");

        builder.append("<" + COMMON_NAMESPACE + ":merchantId>")
                .append(data.get(headers.get("merchantId")))
                .append("</" + COMMON_NAMESPACE + ":merchantId>");

        builder.append("</" + CATALOG_NAMESPACE + ":header>");
    }

    protected void afterHeader(StringBuilder builder, Map<Integer, String> data) {

    }

    protected abstract void buildOne(final Map<Integer, String> data, final StringBuilder builder);

    @Override
    public void combineCSVData(CSVReader csvReader, StringBuilder builder) {
        builder.append("<" + CATALOG_NAMESPACE + ":").append(REQUEST_ROOT_TAG).append(" ").append(XMLNS).append(">");

        Map<Integer, String> firstLine = csvReader.getLine();

        // build header
        buildHeader(builder, firstLine);
        afterHeader(builder, firstLine);

        //build body
        builder.append("<" + CATALOG_NAMESPACE + ":").append(LIST_ROOT_TAG).append(">");

        buildOne(firstLine, builder);
        while (csvReader.readNextLine()) {
            buildOne(csvReader.getLine(), builder);
        }

        builder.append("</" + CATALOG_NAMESPACE + ":").append(LIST_ROOT_TAG).append(">");


        builder.append("</" + CATALOG_NAMESPACE + ":").append(REQUEST_ROOT_TAG).append(">");
    }

    @Override
    public void setHeaders(Map<String, Integer> headers) {
        this.headers = headers;
    }
}
