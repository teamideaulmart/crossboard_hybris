package ru.teamidea.crossboard.b2c.ftp.service;

import java.io.*;

/**
 * Created by Timofey Klyubin on 27.09.16.
 */
public interface CSVConversionService {

    ByteArrayOutputStream convertCSV2XML(final InputStream xsv, final CSVConversionStrategy conversionStrategy, final String encoding) throws IOException;
}
