package ru.teamidea.crossboard.b2c.ftp.service.impl;

import de.hybris.platform.util.CSVReader;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import ru.teamidea.crossboard.b2c.ftp.service.CSVConversionService;
import ru.teamidea.crossboard.b2c.ftp.service.CSVConversionStrategy;
import ru.teamidea.crossboard.b2c.integration.aspect.ExecuteInSession;

import java.io.*;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Timofey Klyubin on 27.09.16.
 */
public class CSVConversionServiceImpl implements CSVConversionService {

    private static final Logger LOG = Logger.getLogger(CSVConversionServiceImpl.class);

    @Value("${crossboardftp.csv.field_separator}")
    private char[] fieldSeparator;

    @Value("${crossboardftp.csv.text_separator}")
    private char textSeparator;

    @Override
    @ExecuteInSession
    public ByteArrayOutputStream convertCSV2XML(InputStream xsv, CSVConversionStrategy conversionStrategy, String encoding) {
        final CSVReader csvReader;
        try {
            csvReader = new CSVReader(xsv, encoding);
            csvReader.setFieldSeparator(fieldSeparator);
            csvReader.setTextSeparator(textSeparator);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }

        if (csvReader.readNextLine()) {
            conversionStrategy.setHeaders(
                    csvReader.getLine().entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                           Map.Entry::getValue,
                            Map.Entry::getKey
                    ))
            );
        }
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        if (csvReader.readNextLine()) {
            final StringBuilder xmlBuilder = new StringBuilder();
            conversionStrategy.combineCSVData(csvReader, xmlBuilder);
            try {
                os.write(xmlBuilder.toString().getBytes(encoding));
                os.flush();
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
        return os;
    }



}
