/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package ru.ulmart.crossboard.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import ru.ulmart.crossboard.fulfilmentprocess.constants.CrossboardFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class CrossboardFulfilmentProcessManager extends GeneratedCrossboardFulfilmentProcessManager
{
	public static final CrossboardFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CrossboardFulfilmentProcessManager) em.getExtension(CrossboardFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
