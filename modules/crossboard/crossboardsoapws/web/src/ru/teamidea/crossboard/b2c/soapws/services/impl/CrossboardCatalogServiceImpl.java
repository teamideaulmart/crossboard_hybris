package ru.teamidea.crossboard.b2c.soapws.services.impl;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import ru.teamidea.crossboard.b2c.integration.marshal.StreamedMarshalWriter;
import ru.teamidea.crossboard.b2c.integration.marshal.StreamedWriterBody;
import ru.teamidea.crossboard.b2c.integration.marshal.StreamingMarshal;
import ru.teamidea.crossboard.b2c.soapws.constants.CrossboardsoapwsConstants;
import ru.teamidea.crossboard.b2c.integration.storage.LocalFileSoapRequestStorageStrategy;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.ulmart.cb.catalog.*;
import ru.ulmart.cb.hybris.catalog.ws.CrossboardCatalogService;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.catalog.CrossboardCategoryService;
import ru.ulmart.crossboard.core.services.user.MerchantService;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;


@WebService(name = "CrossboardCatalogService", serviceName = "CrossboardCatalogService", portName = "CrossboardCatalogServiceSOAP",
        targetNamespace = "http://www.cb.ulmart.ru/hybris/catalog/ws",
        endpointInterface = "ru.ulmart.cb.hybris.catalog.ws.CrossboardCatalogService")
@HandlerChain(file = "../handler-chain/crossboard-catalog-service-handler-chain.xml")
@Transactional
public class CrossboardCatalogServiceImpl implements CrossboardCatalogService {
    private static final Logger LOGGER = Logger.getLogger(CrossboardCatalogServiceImpl.class);

    private CrossboardCategoryService crossboardCategoryService;
    private CatalogService catalogService;
    private MerchantService merchantService;
    private TIConfigUtils tiConfigUtils;
    private CatalogVersionModel catalogVersion;

    @Override
    public DownloadCategoriesResponse downloadCategories(DownloadCategoriesRequest downloadCategoriesRequest) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("CALL downloadCategories");
        }

        final String categoriesType = downloadCategoriesRequest.getCategoriesType().value();
        final CategoryModel rootCategory = getCrossboardCategoryService().getRootCategoryForCatalogVersionAndCategoriesType(getCatalogVersion(), categoriesType);

        if (rootCategory == null) {
            LOGGER.warn(String.format("Root category for catalog version %1$s and categories type %2$s not found", getCatalogVersion().getVersion(), categoriesType));
            return new DownloadCategoriesResponse();
        }

        final Collection<CategoryModel> categories = getCrossboardCategoryService().getAllSubcategoriesForCategory(rootCategory);
        if (CollectionUtils.isEmpty(categories)) {
            LOGGER.warn(String.format("Subcategories for root category code %1$s and categories type %2$s not found", rootCategory.getCode(), categoriesType));
            return new DownloadCategoriesResponse();
        }

        final String externalCode = downloadCategoriesRequest.getHeader().getMerchantId();
        final MerchantModel merchant = getMerchantService().findMerchantForExternalCode(externalCode);

        final DownloadCategoriesResponse downloadCategoriesResponse = new DownloadCategoriesResponse();
        downloadCategoriesResponse.setVersion(downloadCategoriesResponse.getVersion());
        final DownloadCategoriesResponse.Categories crossboardCategories = new DownloadCategoriesResponse.Categories();
        for (CategoryModel categoryModel : categories) {
            crossboardCategories.getCategory().add(convertCategoryModelToCategory(categoryModel, merchant, rootCategory));
        }
        downloadCategoriesResponse.setCategories(crossboardCategories);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("FINISH downloadCategories");
        }

        return downloadCategoriesResponse;
    }

    private Category convertCategoryModelToCategory(final CategoryModel categoryModel, final MerchantModel merchant, final CategoryModel rootCategory) {
        final Category category = new Category();
        category.setCategoryId(categoryModel.getCode());
        category.setName(categoryModel.getName(Locale.ENGLISH));
        category.setImportAllowed(getCrossboardCategoryService().isImportAllowedForCategoryAndMerchant(categoryModel, merchant));

        // set parent category
        final List<CategoryModel> supercategories = categoryModel.getSupercategories().stream()
                .filter(cat -> cat.getAllSupercategories().contains(rootCategory))
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(supercategories)) {
            category.setParentCategoryId(supercategories.get(0).getCode());
        } else {
            category.setParentCategoryId(StringUtils.EMPTY);
        }

        return category;
    }

    @Override
    public void uploadProductDescriptions(UploadProductDescriptionsRequest uploadProductDescriptionsRequest) {
        LOGGER.debug("Call EMPTY uploadProductDescriptions");
    }

    @Override
    public void uploadProductOffers(UploadProductOffersRequest uploadProductOffersRequest) {
        LOGGER.debug("Call EMPTY uploadProductOffers");
    }

    @Override
    public void deleteItems(DeleteItemsRequest deleteItemsRequest) {
        LOGGER.debug("Call EMPTY deleteItems");
    }

    private TIConfigUtils getTiConfigUtils() {
        if (tiConfigUtils == null) {
            tiConfigUtils = Registry.getApplicationContext().getBean("tiConfigUtils", TIConfigUtils.class);
        }
        return tiConfigUtils;
    }

    private CrossboardCategoryService getCrossboardCategoryService() {
        if (crossboardCategoryService == null) {
            crossboardCategoryService = Registry.getApplicationContext().getBean("crossboardCategoryService", CrossboardCategoryService.class);
        }
        return crossboardCategoryService;
    }

    private CatalogService getCatalogService() {
        if (catalogService == null) {
            catalogService = Registry.getApplicationContext().getBean("catalogService", CatalogService.class);
        }
        return catalogService;
    }

    private CatalogVersionModel getCatalogVersion() {
        if (catalogVersion == null) {
            final String catalogNameParameter = getTiConfigUtils().getPropertyValue(CrossboardsoapwsConstants.INTEGRATION_PRODUCT_CATALOG_NAME_PARAM, CrossboardsoapwsConstants.INTEGRATION_PRODUCT_CATALOG_NAME_DEFAULT);
            final String catalogVersionParameter = getTiConfigUtils().getPropertyValue(CrossboardsoapwsConstants.INTEGRATION_PRODUCT_CATALOG_VERSION_PARAM, CrossboardsoapwsConstants.INTEGRATION_PRODUCT_CATALOG_VERSION_DEFAULT);

            catalogVersion = getCatalogService().getCatalogVersion(catalogNameParameter, catalogVersionParameter);
        }

        return catalogVersion;
    }

    private MerchantService getMerchantService() {
        if (merchantService == null) {
            merchantService = Registry.getApplicationContext().getBean("merchantService", MerchantService.class);
        }
        return merchantService;
    }

}
