package ru.teamidea.crossboard.b2c.soapws.handlers;

import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.StringWriter;
import java.util.Set;

/**
 * Created by Denis Zevakhin <zevakhin@teamidea.ru> on 21.04.16.
 */
public class LogSOAPHandler implements SOAPHandler<SOAPMessageContext> {
    private static final Logger LOG = Logger.getLogger(LogSOAPHandler.class);

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        logging(context);
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        logging(context);
        return true;
    }

    private void logging(SOAPMessageContext context) {
        try {
            Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

            if (LOG.isDebugEnabled()) {
                if (outboundProperty) {
                    LOG.debug("\nOutgoing message:\n");
                } else {
                    LOG.debug("\nIncoming message:\n");
                }

                if (!outboundProperty) {
                    if (LOG.isDebugEnabled()) {
                        final String requestHeaders = context.get(MessageContext.HTTP_REQUEST_HEADERS).toString();
                        LOG.debug("Request headers:");
                        LOG.debug(requestHeaders);
                        LOG.debug("===================================================");
                    }
                }

                SOAPMessage message = context.getMessage();

                try {
                    Transformer transformer = TransformerFactory.newInstance().newTransformer();
                    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    StreamResult result = new StreamResult(new StringWriter());
                    transformer.transform(message.getSOAPPart().getContent(), result);
                    String xml = result.getWriter().toString();
                    if(LOG.isDebugEnabled()) {
                        LOG.debug(xml);
                    }
                } catch (SOAPException | TransformerException e) {
                    LOG.error(e.getMessage());
                }
            }
        } catch (Throwable e) {
            LOG.error("Error while handling soap message:", e);
        }
    }

    @Override
    public void close(MessageContext context) {
    }
}
