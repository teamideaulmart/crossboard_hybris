package ru.teamidea.crossboard.b2c.soapws.handlers;

import ru.teamidea.crossboard.b2c.integration.utils.SchemaUtils;

import javax.xml.validation.Schema;

/**
 * Created by Marina on 05.09.2016.
 */
public class CrossboardCatalogServiceHandler extends AbstractErrorHandler
{
    @Override
    protected Schema getSchema() {
        return SchemaUtils.getSchema(SchemaUtils.XSDSchema.catalog);
    }
}
