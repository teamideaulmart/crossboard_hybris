package ru.teamidea.crossboard.b2c.soapws.handlers;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import ru.teamidea.crossboard.b2c.integration.storage.LocalFileSoapRequestStorageStrategy;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.LogicalMessage;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.LogicalHandler;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Map;

/**
 * Created by Alexey on 21.09.2016.
 */
public class StorePayloadHandler implements LogicalHandler<LogicalMessageContext> {

    private final static Logger LOG = Logger.getLogger(StorePayloadHandler.class);

    private LocalFileSoapRequestStorageStrategy localFileSoapRequestStorageStrategy;

    private static final String SOAP_ACTION_KEY = "javax.xml.ws.soap.http.soapaction.uri";

    private String getSoapAction(LogicalMessageContext context) {
        String soapAction = (String) context.get(SOAP_ACTION_KEY);

        if (StringUtils.isEmpty(soapAction)) {
            return "undefined";
        }

        return soapAction.replaceAll("\"", "");
    }

    @Override
    public boolean handleMessage(LogicalMessageContext context) {
        Boolean isOutBound = (Boolean) context
                .get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        if (isOutBound || !storeRequestAsFile(getSoapAction(context))) {
            return true;
        }

        LogicalMessage lm = context.getMessage();
        Source payload = lm.getPayload();

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Result result = new StreamResult(os);

        try {
            Transformer trans;
            trans = TransformerFactory.newInstance().newTransformer();
            trans.transform(payload, result);

            ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
            getLocalFileSoapRequestStorageStrategy().storeInputStream(getSoapAction(context), is);

            LOG.info("Successfully write payload to file!");

        } catch (Exception e) {
            // When XSD-schema validation fails.
            LOG.info("XSD-schema validation fails: \n" + e.getMessage());
            throw new WebServiceException(e);
        }

        return true;
    }

    private boolean storeRequestAsFile(final String soapAction) {
        final Map<String, String> fileNames = Config.getParametersByPattern("integration.merchant");
        if (MapUtils.isNotEmpty(fileNames) && fileNames.values().contains(soapAction)) {
            return true;
        }
        return false;
    }

    private LocalFileSoapRequestStorageStrategy getLocalFileSoapRequestStorageStrategy() {
        if (localFileSoapRequestStorageStrategy == null) {
            localFileSoapRequestStorageStrategy = Registry.getApplicationContext().getBean("localFileSoapRequestStorageStrategy", LocalFileSoapRequestStorageStrategy.class);
        }
        return localFileSoapRequestStorageStrategy;
    }

    @Override
    public boolean handleFault(LogicalMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {

    }
}
