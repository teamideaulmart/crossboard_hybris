package ru.teamidea.crossboard.b2c.soapws.handlers;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.ws.LogicalMessage;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.LogicalHandler;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;

/**
 * Created by Denis Zevakhin <zevakhin@teamidea.ru> on 21.04.16.
 */
public abstract class AbstractErrorHandler implements LogicalHandler<LogicalMessageContext> {

    private final static Logger LOG = Logger.getLogger(AbstractErrorHandler.class);

    @Override
    public boolean handleMessage(LogicalMessageContext context) {
        Boolean isOutBound = (Boolean) context
                .get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        if (isOutBound) {
            return true;
        }

        LogicalMessage lm = context.getMessage();
        Source payload = lm.getPayload();
        StreamResult res = new StreamResult(new StringWriter());
        String message = "";

        try {
            Transformer trans;
            trans = TransformerFactory.newInstance().newTransformer();
            trans.transform(payload, res);
            message = res.getWriter().toString();
            // Validate
            validate(message);
        } catch (Exception e) {
            // When XSD-schema validation fails.
            LOG.info("XSD-schema validation fails: \n" + e.getMessage());
            throw new WebServiceException(e);
        }

        return true;
    }

    private void validate(String xml)
            throws ParserConfigurationException, IOException,
            MalformedURLException, SAXException {

        DocumentBuilder parser = null;
        org.w3c.dom.Document document = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        parser = dbf.newDocumentBuilder();

        byte bytes[] = xml.getBytes();
        document = parser.parse(new ByteArrayInputStream(bytes));

        javax.xml.validation.Validator validator = getSchema().newValidator();
        validator.validate(new DOMSource(document));
    }

    protected abstract Schema getSchema();

    @Override
    public boolean handleFault(LogicalMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
    }
}
