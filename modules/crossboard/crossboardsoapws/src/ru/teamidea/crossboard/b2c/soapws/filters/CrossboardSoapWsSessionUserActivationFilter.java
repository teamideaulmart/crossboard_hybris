package ru.teamidea.crossboard.b2c.soapws.filters;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Alexey on 27.05.2016.
 */
public class CrossboardSoapWsSessionUserActivationFilter extends GenericFilterBean {
    private static final Logger LOGGER = Logger.getLogger(CrossboardSoapWsSessionUserActivationFilter.class);

    @Autowired
    private UserService userService;

    @Autowired
    private CommonI18NService commonI18NService;

    @Autowired
    private SessionService sessionService;

    @Value("${integration.session.language}")
    private String sessionLocale;

    private String userUid;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        LOGGER.info("Initialize session user [" + userUid + "] for SOAP request");

        if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
            throw new ServletException("CrossboardSoapWsSessionUserActivationFilter just supports HTTP requests");
        }

        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final HttpServletResponse httpResponse = (HttpServletResponse) response;

        UserModel user = userService.getUserForUID(userUid);
        sessionService.getCurrentSession().setAttribute("language", commonI18NService.getLanguage(sessionLocale));

        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("Activate session user [%s]", user.getUid() ));
        }
        userService.setCurrentUser(userService.getUserForUID(userUid));

        filterChain.doFilter(httpRequest, httpResponse);
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

}
