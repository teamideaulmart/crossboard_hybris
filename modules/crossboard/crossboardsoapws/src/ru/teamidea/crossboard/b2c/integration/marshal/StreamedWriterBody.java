package ru.teamidea.crossboard.b2c.integration.marshal;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.Map;


/**
 * Created by Alexey on 18.03.2016.
 */
public interface StreamedWriterBody
{
	void writeBody(final XMLStreamWriter xmlOut) throws XMLStreamException, JAXBException;
}
