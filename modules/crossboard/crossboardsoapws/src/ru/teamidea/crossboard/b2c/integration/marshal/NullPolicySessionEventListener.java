package ru.teamidea.crossboard.b2c.integration.marshal;

import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.oxm.mappings.XMLDirectMapping;
import org.eclipse.persistence.oxm.mappings.nullpolicy.XMLNullRepresentationType;
import org.eclipse.persistence.sessions.Project;
import org.eclipse.persistence.sessions.SessionEvent;
import org.eclipse.persistence.sessions.SessionEventAdapter;

/**
 * Created by Marina on 02.04.2016.
 */
public class NullPolicySessionEventListener extends SessionEventAdapter
{

	@Override
	public void preLogin(SessionEvent event)
	{
		Project project = event.getSession().getProject();
		for (ClassDescriptor descriptor : project.getOrderedDescriptors())
		{
			descriptor.getMappings().stream().filter(mapping -> mapping.isAbstractDirectMapping()).forEach(mapping -> {
				XMLDirectMapping xmlDirectMapping = (XMLDirectMapping) mapping;
				xmlDirectMapping.getNullPolicy().setMarshalNullRepresentation(XMLNullRepresentationType.EMPTY_NODE);
				xmlDirectMapping.getNullPolicy().setNullRepresentedByEmptyNode(true);
			});
		}
	}

}
