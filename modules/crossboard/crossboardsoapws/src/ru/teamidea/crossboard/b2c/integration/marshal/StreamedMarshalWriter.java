package ru.teamidea.crossboard.b2c.integration.marshal;

import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Map;


/**
 * Created by Alexey on 18.03.2016.
 */
public class StreamedMarshalWriter
{
	private final static Logger LOGGER = Logger.getLogger(StreamedMarshalWriter.class);

	private Path filePath;
	private StreamedWriterBody body;
	private XMLStreamWriter xmlOut;
	private boolean indent;

	private OutputStream outStream;
	//private Map<?, StreamingMarshal> marshalers;

	public StreamedMarshalWriter(final Path filePath, StreamedWriterBody body)
	{
		this(filePath, body, false);
	}

	public StreamedMarshalWriter(final Path filePath, StreamedWriterBody body, boolean indent)
	{
		this.filePath = filePath;
		this.body = body;
		this.indent = indent;
	}

	public void execute() throws IOException, XMLStreamException, JAXBException
	{
		openStream();
		try
		{
			//marshalers = body.createMarshalers(xmlOut);

			xmlOut.writeStartDocument();
			body.writeBody(xmlOut);
			xmlOut.writeEndDocument();

		} catch (Exception e){
			LOGGER.error("Failed to write to XML stream", e);
			throw e;
		}
		finally
		{
			closeStream();
		}
	}

	private void openStream() throws IOException, XMLStreamException
	{
		outStream = new FileOutputStream(filePath.toFile());
		xmlOut = XMLOutputFactory.newFactory().createXMLStreamWriter(outStream);
		if(indent) {
			xmlOut = new IndentingXMLStreamWriter(xmlOut);
		}
	}

	private void closeStream() throws XMLStreamException, IOException
	{
		try
		{
			xmlOut.flush();
			xmlOut.close();
			LOGGER.debug("XMLStreamWriter closed");
		} finally
		{
			outStream.flush();
			outStream.close();
			LOGGER.debug("OutputStream closed");
		}


	}
}
