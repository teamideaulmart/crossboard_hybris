package ru.teamidea.crossboard.b2c.integration.storage;

import com.google.common.base.Preconditions;
import de.hybris.platform.media.exceptions.MediaStoreException;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.MediaUtil;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import ru.teamidea.crossboard.b2c.integration.marshal.StreamedMarshalWriter;
import ru.teamidea.crossboard.b2c.integration.marshal.StreamedWriterBody;
import ru.teamidea.crossboard.b2c.integration.marshal.StreamingMarshal;
import ru.teamidea.crossboard.b2c.soapws.constants.CrossboardsoapwsConstants;
import ru.ulmart.cb.catalog.DeleteItemsRequest;
import ru.ulmart.cb.catalog.UploadProductDescriptionsRequest;
import ru.ulmart.cb.catalog.UploadProductOffersRequest;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

/**
 * Created by Alexey on 21.09.2016.
 */
public class LocalFileSoapRequestStorageStrategy {
    private static final Logger LOG = Logger.getLogger(LocalFileSoapRequestStorageStrategy.class);

    private static final String DELIMITER = "_";
    private static final String FILE_EXT = ".xml";

    private String mainDataDirPath;

    public void storeInputStream(String soapAction, InputStream dataStream) {
        Preconditions.checkNotNull(dataStream);

        String fileName = getFileName(soapAction);
        File media = null;
        FileOutputStream fos = null;

        try {
            File localStorageDirPath = getLocalStorageDataDir();
            media = new File(localStorageDirPath, fileName);
            boolean location = media.createNewFile();
            if (!location) {
                throw new MediaStoreException("New request file already exists! (file:" + media + ", dir: " + localStorageDirPath + ")");
            }

            fos = new FileOutputStream(media);
            long size = MediaUtil.copy(dataStream, fos, true);

        } catch (IOException ioe) {
            throw new MediaStoreException("Error writing request file (file:" + media + ", dir:" + mainDataDirPath + ")", ioe);
        } finally {
            closeInputStream(dataStream);
            closeOutputStreams(fos);
        }
    }

    /**
     * Prepare file name
     * @param soapAction
     * @return
     */
    private String getFileName(String soapAction) {
        StringBuffer fileName = new StringBuffer(soapAction).append(DELIMITER).append(System.currentTimeMillis()).append(FILE_EXT);
        return fileName.toString();
    }

    private File getLocalStorageDataDir() {
        String dataDir = mainDataDirPath;
        if (dataDir == null) {
            LOG.warn("Property [mainDataDirPath] not set. Using temp directory.");
            dataDir = (new File(System.getProperty("java.io.tmpdir"))).getAbsolutePath();
        }

        File dir = new File(dataDir.trim());
        if (!dir.exists()) {
            dir.mkdirs();
        } else if (dir.exists() && !dir.isDirectory()) {
            throw new IllegalArgumentException(dir + " is no valid directory. Please create it or adjust [mainDataDirPath] property");
        }

        if (!dir.isDirectory()) {
            throw new IllegalArgumentException(dir + " is no valid directory");
        }

        return dir;
    }

    private Path getLocalStoragePath(){
        String dataDir = mainDataDirPath;
        if (dataDir == null) {
            LOG.warn("Property [mainDataDirPath] not set. Using temp directory.");
            dataDir = (new File(System.getProperty("java.io.tmpdir"))).getAbsolutePath();
        }

        Path storagePath = Paths.get(mainDataDirPath);

        // TODO: check if exists
        return storagePath;
    }

    private void closeInputStream(InputStream dataStream) {
        try {
            dataStream.close();
        } catch (IOException e) {
            LOG.error("Cannot close stream", e);
        }

    }

    private void closeOutputStreams(OutputStream outputStream) {
        try {
            outputStream.close();
        } catch (IOException e) {
            LOG.error("Cannot close stream", e);
        }
    }

    public void setMainDataDirPath(String mainDataDirPath) {
        this.mainDataDirPath = mainDataDirPath;
    }
}
