package ru.teamidea.crossboard.b2c.integration.marshal;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamWriter;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.sessions.SessionEventListener;


/**
 * Created by Alexey on 18.03.2016.
 */
public class StreamingMarshal<T>
{

	private XMLStreamWriter xmlOut;
	private Marshaller marshaller;
	private final Class<T> type;

	public StreamingMarshal(Class<T> type, XMLStreamWriter xmlOut) throws JAXBException
	{
		this.type = type;

		Map<String, Object> properties = new HashMap<>(1);
		SessionEventListener sessionEventListener = new NullPolicySessionEventListener();
		properties.put(JAXBContextProperties.SESSION_EVENT_LISTENER, sessionEventListener);

		JAXBContext context = JAXBContextFactory.createContext(new Class[]{type}, properties);
		marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
		CharacterEscapeHandler escapeHandler = NoEscapeHandler.theInstance;
		marshaller.setProperty("com.sun.xml.bind.characterEscapeHandler", escapeHandler);

		this.xmlOut = xmlOut;
	}

	public void write(T t) throws JAXBException
	{
		JAXBElement<T> element = new JAXBElement<>(QName.valueOf(type.getSimpleName()), type, t);
		marshaller.marshal(element, xmlOut);
	}
}
