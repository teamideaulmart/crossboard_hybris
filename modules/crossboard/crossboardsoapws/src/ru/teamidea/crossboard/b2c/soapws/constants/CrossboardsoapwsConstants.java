/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package ru.teamidea.crossboard.b2c.soapws.constants;

/**
 * Global class for all Crossboardsoapws constants. You can add global constants for your extension into this class.
 */
public final class CrossboardsoapwsConstants extends GeneratedCrossboardsoapwsConstants
{
	public static final String EXTENSIONNAME = "crossboardsoapws";

	private CrossboardsoapwsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "crossboardsoapwsPlatformLogo";

	public static final String INTEGRATION_PRODUCT_CATALOG_NAME_PARAM = "integration.catalog.name";
	public static final String INTEGRATION_PRODUCT_CATALOG_VERSION_PARAM = "integration.catalog.version";

	public static final String INTEGRATION_PRODUCT_CATALOG_NAME_DEFAULT = "crossboardProductCatalog";
	public static final String INTEGRATION_PRODUCT_CATALOG_VERSION_DEFAULT = "Online";

	public static final String INTEGRATION_MERCHANT_PRODUCTS_DESCRIPTIONS_FILE_NAME  = "integration.merchant.products.descriptions.file.name";
	public static final String INTEGRATION_MERCHANT_PRODUCTS_OFFERS_FILE_NAME  = "integration.merchant.products.offers.file.name";
	public static final String INTEGRATION_MERCHANT_DELETE_ITEMS_FILE_NAME  = "integration.merchant.delete.items.file.name";
}
