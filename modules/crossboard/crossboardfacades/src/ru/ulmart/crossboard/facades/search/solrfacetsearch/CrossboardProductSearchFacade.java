package ru.ulmart.crossboard.facades.search.solrfacetsearch;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * Created by Oksana Kurushina on 10.10.2016.
 */
public interface CrossboardProductSearchFacade<ITEM extends ProductData> extends ProductSearchFacade<ITEM> {

    Integer getAutocompleteGoodsAmountUlmart(String term, HttpServletRequest request, HttpServletResponse servletResponse);
    Integer getAutocompleteGoodsAmountDiscount(String term, HttpServletRequest request, HttpServletResponse servletResponse);
    void putCity(String city, HttpServletRequest request, HttpServletResponse servletResponse);


    List<CategoryData> categoryTextSearch(String text, int maxResultsCount);

}
