package ru.ulmart.crossboard.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.variantoptions.VariantOptionDataMediaPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import ru.teamidea.utils.util.TIConfigUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
* Created by Timofey Klyubin on 04.10.16.
*/
public class CrossboardVariantOptionDataMediaPopulator<SOURCE extends VariantProductModel, TARGET extends VariantOptionData>
        extends VariantOptionDataMediaPopulator<SOURCE, TARGET> {

    private static final String RESIZE_THUMBNAIL = "resize=64x64";
    private static final String DEFAULT_IMAGE = "product.catalog.default.image.url";

    private TIConfigUtils tiConfigUtils;
    private Converter<MediaModel, ImageData> imageConverter;

    private String addResizeParam(final String url) {
        if (StringUtils.isEmpty(url)) {
            return url;//FIXME
        }
        try {
            final URI imageUri = new URI(url);
            if (StringUtils.isEmpty(imageUri.getQuery())) {
                return  url + "?" + RESIZE_THUMBNAIL;
            } else {
                return url + "&" + RESIZE_THUMBNAIL;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return url;
    }

    @Override
    public void populate(VariantProductModel variantProductModel, VariantOptionData variantOptionData) throws ConversionException {
        final List<VariantOptionQualifierData> qualifierDataList = new ArrayList<>();

        String imgAltText = "";

        for (final CategoryModel category : variantProductModel.getSupercategories()) {
            if (category instanceof VariantValueCategoryModel) {
                final VariantValueCategoryModel valueCategory = (VariantValueCategoryModel) category;

                final VariantCategoryModel vc = (VariantCategoryModel) valueCategory.getSupercategories().get(0);
                if (imgAltText.equals("") && BooleanUtils.isTrue(vc.getHasImage())) {
                    imgAltText = valueCategory.getName();
                }

                final VariantOptionQualifierData qualifierData = new VariantOptionQualifierData();
                qualifierData.setName(valueCategory.getName());
                final MediaModel baseImage = variantProductModel.getImages()
                        .stream()
                        .sorted((l, r) -> Integer.compare(l.getPriority(), r.getPriority()))
                        .findFirst().orElse(null);
                if (baseImage != null) {
                    qualifierData.setImage(imageConverter.convert(baseImage));
                    qualifierData.getImage().setUrl(addResizeParam(qualifierData.getImage().getUrl()));
                } else {
                    final ImageData imageData = new ImageData();
                    imageData.setFormat("1000Wx1000H");
                    String defaultUrl = tiConfigUtils.getPropertyValue(DEFAULT_IMAGE);
                    imageData.setUrl(addResizeParam(defaultUrl));
                    qualifierData.setImage(imageData);
                }
                qualifierDataList.add(qualifierData);
            }
        }
        for (VariantOptionQualifierData qd : qualifierDataList) {
            qd.getImage().setAltText(imgAltText);
        }
        variantOptionData.setVariantOptionQualifiers(qualifierDataList);
    }

    @Required
    public void setTiConfigUtils(TIConfigUtils tiConfigUtils) {
        this.tiConfigUtils = tiConfigUtils;
    }

    @Required
    public void setImageConverter(Converter<MediaModel, ImageData> imageConverter) {
        this.imageConverter = imageConverter;
    }
}
