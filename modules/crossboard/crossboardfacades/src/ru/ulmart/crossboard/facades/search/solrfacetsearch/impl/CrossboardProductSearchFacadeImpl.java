package ru.ulmart.crossboard.facades.search.solrfacetsearch.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.impl.DefaultSolrProductSearchFacade;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import ru.ulmart.crossboard.core.services.catalog.CrossboardCategoryService;
import ru.ulmart.crossboard.core.services.search.UlmartElasticSearchService;
import ru.ulmart.crossboard.facades.search.solrfacetsearch.CrossboardProductSearchFacade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Oksana Kurushina on 10.10.2016.
 */
public class CrossboardProductSearchFacadeImpl extends DefaultSolrProductSearchFacade implements CrossboardProductSearchFacade {

    @Autowired
    private CrossboardCategoryService crossboardCategoryService;

    @Autowired
    private UrlResolver categoryDataUrlResolver;

    private Converter<CategoryModel, CategoryData> categoryConverter;

    private static final Logger LOG = Logger.getLogger(CrossboardProductSearchFacadeImpl.class);

    @Autowired
    private UlmartElasticSearchService ulmartElasticSearchService;

    @Override
    public Integer getAutocompleteGoodsAmountUlmart(String term, HttpServletRequest request, HttpServletResponse servletResponse) {
        return ulmartElasticSearchService.getAutocompleteGoodsAmount(ulmartElasticSearchService.ULMART_ELASTIC_SEARCH_URL, term, request, servletResponse);
    }

    @Override
    public Integer getAutocompleteGoodsAmountDiscount(String term, HttpServletRequest request, HttpServletResponse servletResponse) {
        return ulmartElasticSearchService.getAutocompleteGoodsAmount(ulmartElasticSearchService.ULMART_DISCOUNT_ELASTIC_SEARCH_URL, term, request, servletResponse);
    }

    @Override
    public void putCity(String city, HttpServletRequest request, HttpServletResponse servletResponse) {
        ulmartElasticSearchService.putCity(city, request, servletResponse);
    }

    @Override
    public List<CategoryData> categoryTextSearch(String text, int maxResultsCount) {
        final Collection<CategoryModel> categoryModels = crossboardCategoryService.getRootCategoriesForCatalogVersionsAndNameTerm(text, maxResultsCount);
        final List<CategoryData> categories = new ArrayList<CategoryData>();
        for (final CategoryModel categoryModel : categoryModels)
        {
            categories.add(resolveCategory(categoryModel));
        }
        return categories;
    }

    private CategoryData resolveCategory(final CategoryModel categoryModel)
    {
        final CategoryData categoryData = new CategoryData();
        categoryConverter.convert(categoryModel, categoryData);
        categoryData.setUrl(categoryDataUrlResolver.resolve(categoryData));
        return categoryData;
    }

    @Required
    public void setCategoryConverter(Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }
}
