package ru.ulmart.crossboard.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.CategoryPopulator;
import de.hybris.platform.commercefacades.product.data.CategoryData;

public class CrossboardCategoryPopulator extends CategoryPopulator{

    @Override
    public void populate(CategoryModel source, CategoryData target) {
        super.populate(source, target);
        if (source.getThumbnail()!=null){
             target.setThumbnail(super.getImageConverter().convert(source.getThumbnail()));
        }
        if (source.getPicture()!=null){
            target.setPicture(super.getImageConverter().convert(source.getPicture()));
        }
    }
}