package ru.ulmart.crossboard.facades.search.solrfacetsearch.converters.populator;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.user.MerchantService;
import ru.ulmart.crossboard.facades.product.data.MerchantData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marina on 04.10.2016.
 */
public class CrossboardSearchResultProductPopulator extends SearchResultProductPopulator {

    private static final Logger LOG = Logger.getLogger(CrossboardSearchResultProductPopulator.class);

    private MerchantService merchantService;
    private CategoryService categoryService;
    private Converter<MerchantModel, MerchantData> merchantConverter;
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Override
    public void populate(final SearchResultValueData source, final ProductData target) {

        super.populate(source, target);

        target.setWarrantyPeriod(this.<Integer>getValue(source, "warrantyPeriod"));
        target.setBrand(this.<String>getValue(source, "brand"));

        target.setMerchantUrl(this.<String>getValue(source, "merchantUrl"));
        final String externalCode = getValue(source, "merchantCode");
        if (externalCode != null) {
            final MerchantModel merchant = merchantService.findMerchantForExternalCode(externalCode);
            target.setMerchant(merchantConverter.convert(merchant));
        } else {
            LOG.warn(String.format("Product with code %1$s does not have merchant!", target.getCode()));
        }
        final List<String> categories = getValue(source, "allCategories");
        target.setCategories(new ArrayList<>());
        for (String category : categories) {
            target.getCategories().add(categoryConverter.convert(categoryService.getCategoryForCode(category)));
        }
    }

    @Override
    protected void populatePrices(final SearchResultValueData source, final ProductData target)
    {
        // Pull the volume prices flag
        final Boolean volumePrices = this.<Boolean> getValue(source, "volumePrices");
        target.setVolumePricesFlag(volumePrices == null ? Boolean.FALSE : volumePrices);

        final Double priceFrom = this.<Double> getValue(source, "priceFrom");
        if (priceFrom != null)
        {
            final PriceData priceData = getPriceDataFactory().create(PriceDataType.FROM,
                    BigDecimal.valueOf(priceFrom.doubleValue()), getCommonI18NService().getCurrentCurrency());
            target.setPrice(priceData);
        }
        else {
            final Double priceValue = this.<Double>getValue(source, "priceValue");
            if (priceValue != null) {
                final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY,
                        BigDecimal.valueOf(priceValue.doubleValue()), getCommonI18NService().getCurrentCurrency());
                target.setPrice(priceData);
            }
        }
    }

    public void setMerchantService(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    public void setMerchantConverter(Converter<MerchantModel, MerchantData> merchantConverter) {
        this.merchantConverter = merchantConverter;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryConverter(Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }
}
