package ru.ulmart.crossboard.facades.search.converters.populator;

import de.hybris.platform.commercefacades.search.converters.populator.FacetPopulator;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.converters.Converters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oksana Kurushina on 03.10.2016.
 */
public class CrossboardFacetPopulator<QUERY, STATE> extends FacetPopulator<QUERY, STATE> {

    @Override
    public void populate(final FacetData<QUERY> source, final FacetData<STATE> target)
    {
        target.setCode(source.getCode());
        target.setCategory(source.isCategory());
        target.setMultiSelect(source.isMultiSelect());
        target.setName(source.getName());
        target.setPriority(source.getPriority());
        target.setVisible(source.isVisible());

        List<FacetValueData<QUERY>> values = source.getValues();

        if (source.getTopValues() != null)
        {
            target.setTopValues(Converters.convertAll(source.getTopValues(), getFacetValueConverter()));
            values = excludeTopValues(values, source.getTopValues());
        }

        if (values != null)
        {
            target.setValues(Converters.convertAll(values, getFacetValueConverter()));
        }
    }
    private List<FacetValueData<QUERY>> excludeTopValues(List<FacetValueData<QUERY>> values, List<FacetValueData<QUERY>> topValues) {
        List<FacetValueData<QUERY>> result = new ArrayList<>();
        if (values != null) {
            for (FacetValueData<QUERY> value : values) {
                boolean find = false;
                for (FacetValueData<QUERY> topValue : topValues) {
                    if (value.getCode().equals(topValue.getCode())) {
                        find = true;
                        break;
                    }
                }
                if (!find) {
                    result.add(value);
                }
            }
        }
        return result;
    }
}
