package ru.ulmart.crossboard.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import ru.ulmart.crossboard.core.model.CrossboardProductAttributeModel;
import ru.ulmart.crossboard.facades.product.data.CrossboardProductAttributeData;

/**
 * Created by Timofey Klyubin on 26.09.16.
 */
public class CrossboardProductAttributePopulator implements Populator<CrossboardProductAttributeModel, CrossboardProductAttributeData> {
    @Override
    public void populate(CrossboardProductAttributeModel crossboardProductAttributeModel,
                         CrossboardProductAttributeData crossboardProductAttributeData) throws ConversionException {

        crossboardProductAttributeData.setCode(crossboardProductAttributeModel.getCode());
        crossboardProductAttributeData.setName(crossboardProductAttributeModel.getName());
        crossboardProductAttributeData.setUnit(crossboardProductAttributeModel.getUnit());
        crossboardProductAttributeData.setValue(crossboardProductAttributeModel.getValue());
    }
}
