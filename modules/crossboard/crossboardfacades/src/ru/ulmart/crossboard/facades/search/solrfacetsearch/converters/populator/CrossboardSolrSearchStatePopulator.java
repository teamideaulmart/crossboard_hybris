package ru.ulmart.crossboard.facades.search.solrfacetsearch.converters.populator;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.converters.populator.SolrSearchStatePopulator;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Oksana Kurushina on 26.09.2016.
 */
public class CrossboardSolrSearchStatePopulator extends SolrSearchStatePopulator {

    private static final Logger LOG = Logger.getLogger(CrossboardSolrSearchStatePopulator.class);

    @Override
    protected String buildUrlQueryString(final SolrSearchQueryData source, final SearchStateData target)
    {
        final String searchQueryParam = target.getQuery().getValue();
        if (StringUtils.isNotBlank(searchQueryParam))
        {
            try
            {
                return "?filters=" + URLEncoder.encode(searchQueryParam, "UTF-8");
            }
            catch (final UnsupportedEncodingException e)
            {
                LOG.error("Unsupported encoding (UTF-8). Fallback to html escaping.", e);
                return "?filters=" + StringEscapeUtils.escapeHtml(searchQueryParam);
            }
        }
        return "";
    }
}
