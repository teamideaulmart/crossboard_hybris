package ru.ulmart.crossboard.facades.email;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.apache.log4j.Logger;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.ulmart.crossboard.core.model.email.DescriptionErrorNotificationProcessModel;

import java.util.Arrays;

import static ru.teamidea.utils.constants.TeamideaUtilsConstants.RUSSIAN_LOCALE;
import static ru.ulmart.crossboard.core.services.email.CrossboardEmailGenerationService.MULTIPLE_EMAILS;
import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;


/**
 * Created by Timofey Klyubin on 11.10.16.
 */
public class DescriptionErrorEmailContext extends AbstractEmailContext<DescriptionErrorNotificationProcessModel> {

    private static final String CURRENT_URL = "currentUrl";
    private static final String USER_MESSAGE = "userMessage";
    private static final String DESCRIPTION_ERROR = "descriptionError";
    private static final String DEFAULT_FROM_EMAIL = "marketplace@ulmart.ru";
    private static final String DEFAULT_TO_EMAIL = "belyakov.a.u@ulmart.ru";
    private static final String DEFAULT_TITLE = "Ошибка описания товара";

    private static final String TICONFIG_FROM_DISPLAY_NAME = "ulmart.crossboard.notification.email.from.name";
    private static final String TICONFIG_FROM_EMAIL = "ulmart.crossboard.notification.email.from";
    private static final String TICONFIG_RECIPIENTS = "ulmart.crossboard.notification.email.recipients";

    private static final Logger LOG = Logger.getLogger(DescriptionErrorEmailContext.class);

    private TIConfigUtils tiConfigUtils;

    @Override
    public void init(DescriptionErrorNotificationProcessModel businessProcessModel, EmailPageModel emailPageModel) {
        if (businessProcessModel != null) {
            setUrlEncodingAttributes(getUrlEncoderService().getUrlEncodingPatternForEmail(businessProcessModel));

            put(CURRENT_URL, businessProcessModel.getUrl());
            put(USER_MESSAGE, escapeHtml(businessProcessModel.getUserMessage()));
            put(DESCRIPTION_ERROR, escapeHtml(businessProcessModel.getDescriptionError()));

            put(FROM_DISPLAY_NAME, tiConfigUtils.getPropertyValue(TICONFIG_FROM_DISPLAY_NAME, ""));
            put(FROM_EMAIL, tiConfigUtils.getPropertyValue(TICONFIG_FROM_EMAIL, DEFAULT_FROM_EMAIL));
            String[] emails = tiConfigUtils.getPropertyValue(TICONFIG_RECIPIENTS, DEFAULT_TO_EMAIL).split(",");
            put(MULTIPLE_EMAILS, Arrays.asList(emails));
            put(TITLE, DEFAULT_TITLE);
        } else {
            LOG.error("DescriptionErrorNotificationProcessModel is null!");
        }
    }

    @Override
    protected BaseSiteModel getSite(DescriptionErrorNotificationProcessModel businessProcessModel) {
        return null;
    }

    @Override
    protected CustomerModel getCustomer(DescriptionErrorNotificationProcessModel businessProcessModel) {
        return null;
    }

    @Override
    protected LanguageModel getEmailLanguage(DescriptionErrorNotificationProcessModel businessProcessModel) {
        return businessProcessModel.getEmailLanguage();
    }

    public void setTiConfigUtils(TIConfigUtils tiConfigUtils) {
        this.tiConfigUtils = tiConfigUtils;
    }
}
