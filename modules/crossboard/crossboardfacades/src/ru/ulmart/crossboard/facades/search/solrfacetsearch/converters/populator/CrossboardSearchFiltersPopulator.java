package ru.ulmart.crossboard.facades.search.solrfacetsearch.converters.populator;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchFiltersPopulator;
import de.hybris.platform.solrfacetsearch.search.FacetValueField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Timofey Klyubin on 25.10.16.
 */
public class CrossboardSearchFiltersPopulator extends SearchFiltersPopulator {

    @Override
    public void populate(SearchQueryPageableData source, SolrSearchRequest target) {
        super.populate(source, target);
        SearchQuery searchQuery = (SearchQuery)target.getSearchQuery();
        List<FacetValueField> fields = searchQuery.getFacetValues();
        for (FacetValueField field : fields) {
            if (field.getField().equals("priceValue")) {
                String userQuery = "";
                Set<String> values = field.getValues();
                Set<String> newValues = new HashSet<>();
                for(String value : values){
                    if(value != null) {
                        userQuery = StringUtils.remove(value, '\\');
                        newValues.add(userQuery);
                    } else {
                        newValues.add(null);
                    }
                }
                field.setValues(newValues);
                searchQuery.addQuery("priceValue", userQuery);
            }
        }
        fields.remove(fields
                .stream().filter(f -> f.getField().equals("priceValue")).findAny().orElse(null));
    }
}
