package ru.ulmart.crossboard.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.facades.product.data.MerchantData;

/**
 * Created by Timofey Klyubin on 26.09.16.
 */
public class MerchantPopulator implements Populator<MerchantModel, MerchantData> {

    private static final Logger LOG = Logger.getLogger(MerchantPopulator.class);

    private Converter<MediaModel, ImageData> imageConverter;

    @Override
    public void populate(MerchantModel merchantModel, MerchantData merchantData) throws ConversionException {
        if (merchantModel == null) {
            LOG.error("MerchantModel is null! Can't populate ModelData!");
            return;
        }
        merchantData.setCode(merchantModel.getCode());
        merchantData.setName(merchantModel.getName());
        merchantData.setDescription(merchantModel.getDescription());
        merchantData.setStatistics(merchantModel.getStatistics());
        merchantData.setUrl(merchantModel.getUrl());
        merchantData.setSiteUrlPattern(merchantModel.getSiteUrlPattern());
        if(merchantModel.getPicture() != null) {
            merchantData.setPicture(imageConverter.convert(merchantModel.getPicture()));
        }
    }

    public void setImageConverter(Converter<MediaModel, ImageData> imageConverter) {
        this.imageConverter = imageConverter;
    }
}
