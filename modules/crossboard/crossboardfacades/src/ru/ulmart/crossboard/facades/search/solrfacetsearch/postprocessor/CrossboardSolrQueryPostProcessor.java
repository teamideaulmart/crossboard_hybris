package ru.ulmart.crossboard.facades.search.solrfacetsearch.postprocessor;

import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SolrQueryPostProcessor;
import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;


/**
 * Created by Timofey Klyubin on 25.10.16.
 */
public class CrossboardSolrQueryPostProcessor implements SolrQueryPostProcessor {

    private static final String MINIMUM_SHOULD_MATCH = "mm";
    private static final String MINIMUM_SHOULD_MATCH_VALUE_KEY = "crossboard.solr.minimum.should.match.value";
    private static final String MINIMUM_SHOULD_MATCH_ENABLED_KEY = "crossboard.solr.minimum.should.match.enabled";

    @Override
    public SolrQuery process(SolrQuery solrQuery, SearchQuery searchQuery) {
        String query = solrQuery.getQuery();
        if (Config.getBoolean(MINIMUM_SHOULD_MATCH_ENABLED_KEY, false)) {
            solrQuery.set(MINIMUM_SHOULD_MATCH, Config.getParameter(MINIMUM_SHOULD_MATCH_VALUE_KEY));
        }
        solrQuery.setQuery(StringUtils.remove(query, '\\'));
        return solrQuery;
    }
}
