package ru.ulmart.crossboard.facades.search.converters;

import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultSolrQueryConverter;

import static java.util.Arrays.stream;

/**
 * Created by Timofey Klyubin on 02.11.16.
 */
public class CrossboardSolrQueryConverter extends DefaultSolrQueryConverter {

    private static final String DEFAULT_QUERY = "*:*";
    private static final String PRICE_FIELD = "priceValue";
    private static final String PRICE_FIELD_OPERATOR = " AND ";

    @Override
    protected String buildQuery(String[] queries, SearchQuery searchQuery) {
        if (queries.length == 0) {
            return DEFAULT_QUERY;
        }
        final SearchQuery.Operator fieldOperator = this.resolveOperator(searchQuery);
        String fullText = stream(queries)
                .filter(q -> !q.contains(PRICE_FIELD))
                .reduce((l, r) -> l.concat(fieldOperator.getName()).concat(r)).orElse("");

        String priceQuery = stream(queries)
                .filter(q -> q.contains(PRICE_FIELD))
                .reduce((l, r) -> l.concat(PRICE_FIELD_OPERATOR).concat(r)).orElse(DEFAULT_QUERY);

        if (fullText.equals("")) {
            return priceQuery;
        } else {
            return "(".concat(fullText).concat(")").concat(PRICE_FIELD_OPERATOR).concat(priceQuery);
        }
    }
}
