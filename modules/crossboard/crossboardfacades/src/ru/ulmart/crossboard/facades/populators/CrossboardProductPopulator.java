package ru.ulmart.crossboard.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPopulator;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;
import ru.ulmart.crossboard.core.enums.MediaDownloadStatus;
import ru.ulmart.crossboard.core.model.CrossboardProductAttributeModel;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.facades.product.data.CrossboardProductAttributeData;
import ru.ulmart.crossboard.facades.product.data.MerchantData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static ru.teamidea.utils.constants.TeamideaUtilsConstants.RUSSIAN_LOCALE;

/**
 * Created by Timofey Klyubin on 26.09.16.
 */
public class CrossboardProductPopulator extends ProductPopulator {

    private Converter<MediaModel, ImageData> imageConverter;
    private Converter<CrossboardProductAttributeModel, CrossboardProductAttributeData> attributeConverter;
    private Converter<MerchantModel, MerchantData> merchantConverter;
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {
        super.populate(productModel, productData);

        productData.setCode(productModel.getCode());
        productData.setMerchantSku(productModel.getMerchantSku());
        productData.setVendorSku(productModel.getVendorSku());

        final I18NService i18NService = Registry.getApplicationContext().getBean("i18nService", I18NService.class);
        i18NService.setLocalizationFallbackEnabled(false);
        productData.setShortName(productModel.getShortName(RUSSIAN_LOCALE));
        i18NService.setLocalizationFallbackEnabled(true);

        productData.setDescription(productModel.getDescription());
        productData.setShortDescription(productModel.getShortDescription());

        productData.setMerchantUrl(productModel.getMerchantUrl());

        productData.setModel(productModel.getModel());
        productData.setBrand(productModel.getBrand());
        productData.setWarrantyPeriod(productModel.getWarrantyPeriod());
        productData.setLength(productModel.getLength());
        productData.setWidth(productModel.getWidth());
        productData.setHeight(productModel.getHeight());
        productData.setVolume(productModel.getVolume());
        productData.setTranslated(productModel.getTranslated());

        if(productModel.getMerchant() != null) {
            productData.setMerchant(merchantConverter.convert(productModel.getMerchant()));
        }

        Collection<VariantProductModel> variants = productModel.getVariants();
        productData.setImages(new LinkedList<>());
        List<MediaModel> images = productModel.getImages();
        if (CollectionUtils.isNotEmpty(variants)) {
            images = ((VariantProductModel)variants.toArray()[0]).getImages();
        }
        images.stream()
              .sorted((l, r) -> Integer.compare(l.getPriority(), r.getPriority()))
              .filter(media -> media.getDownloadStatus() != MediaDownloadStatus.NEW
                            && media.getDownloadStatus() != MediaDownloadStatus.ERROR
                            && media.getDownloadStatus() != MediaDownloadStatus.PROCESSING)
              .forEach((media) -> productData.getImages().add(imageConverter.convert(media)));

        productData.setAttributes(new ArrayList<>(productModel.getAttributes().size()));
        for (CrossboardProductAttributeModel attributeModel : productModel.getAttributes()) {
            productData.getAttributes().add(attributeConverter.convert(attributeModel));
        }

        productData.setCategories(new ArrayList<>(productModel.getSupercategories().size()));
        for (CategoryModel categoryModel : productModel.getSupercategories()) {
            productData.getCategories().add(categoryConverter.convert(categoryModel));
        }

        if (productData.getName() == null && CollectionUtils.isNotEmpty(variants)) {
            productData.setName(((VariantProductModel)variants.toArray()[0]).getName());
        }
    }

    public void setImageConverter(Converter<MediaModel, ImageData> imageConverter) {
        this.imageConverter = imageConverter;
    }

    public void setAttributeConverter(Converter<CrossboardProductAttributeModel, CrossboardProductAttributeData> attributeConverter) {
        this.attributeConverter = attributeConverter;
    }

    public void setMerchantConverter(Converter<MerchantModel, MerchantData> merchantConverter) {
        this.merchantConverter = merchantConverter;
    }

    public void setCategoryConverter(Converter<CategoryModel, CategoryData> categoryConverter) {
        this.categoryConverter = categoryConverter;
    }
}
