package ru.ulmart.crossboard.core.daos.user.impl;

import com.google.common.collect.ImmutableMap;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.daos.user.MerchantDao;
import ru.ulmart.crossboard.core.model.MerchantModel;

import java.util.List;

/**
 * Created by Marina on 08.09.2016.
 */
public class MerchantDaoImpl extends DefaultGenericDao<MerchantModel> implements MerchantDao {

    private static final Logger LOGGER = Logger.getLogger(MerchantDaoImpl.class);

    private static final String FQ_FIND_MERCHANT_FOR_EXTERNAL_CODE =
            "SELECT {m." + MerchantModel.PK + "} FROM {" + MerchantModel._TYPECODE + " as m} WHERE " +
                    "{m." + MerchantModel.EXTERNALCODE + "} = ?externalCode";

    private static final String FQ_FIND_ALL_MERCHANTS =
            "SELECT {m." + MerchantModel.PK + "} FROM {" + MerchantModel._TYPECODE + " as m}";

    public MerchantDaoImpl() {
        super(MerchantModel._TYPECODE);
    }

    @Override
    public MerchantModel findMerchantForExternalCode(final String externalCode) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(
                    String.format("Find merchant by externalCode = %1$s", externalCode));
        }

        List<MerchantModel> result = getFlexibleSearchService().<MerchantModel>search(FQ_FIND_MERCHANT_FOR_EXTERNAL_CODE,
                ImmutableMap.<String, Object>builder().put("externalCode", externalCode).build()).getResult();


        if (result.size() == 0) {
            LOGGER.warn(String.format("Not found merchant for external code %1$s.", externalCode));
            return null;
        }

        if (result.size() > 1) {
            LOGGER.warn(String.format("Found more than one merchant for external code %1$s. Returning the last one.", externalCode));
        }
        return result.get(0);
    }

    @Override
    public List<MerchantModel> findAllMerchants() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Find all merchants");
        }

        return getFlexibleSearchService().<MerchantModel>search(FQ_FIND_ALL_MERCHANTS).getResult();
    }


}
