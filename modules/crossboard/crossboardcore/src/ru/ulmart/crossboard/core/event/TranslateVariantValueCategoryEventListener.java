package ru.ulmart.crossboard.core.event;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.services.translate.DescriptionTranslateService;

import java.util.Collections;

/**
 * Created by Timofey Klyubin on 10.10.16.
 */
public class TranslateVariantValueCategoryEventListener extends AbstractEventListener<TranslateVariantValueCategoryEvent> {

    private static final Logger LOG = Logger.getLogger(TranslateVariantValueCategoryEventListener.class);

    private DescriptionTranslateService descriptionTranslateService;
    private FlexibleSearchService flexibleSearchService;

    @Override
    protected void onEvent(TranslateVariantValueCategoryEvent translateVariantValueCategoryEvent) {
        LOG.info("TranslateVariantValueCategoryEvent fired");

        String query = "SELECT {c."+ VariantValueCategoryModel.CODE +
                "} FROM {" + VariantValueCategoryModel._TYPECODE +
                " as c} WHERE {c." + VariantValueCategoryModel.NAME + "[ru]}=''";
        FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.setResultClassList(Collections.singletonList(String.class));
        searchQuery.setCatalogVersions(translateVariantValueCategoryEvent.getCatalogVersion());
        SearchResult<String> result = flexibleSearchService.search(searchQuery);
        descriptionTranslateService.translateVariantValueCategories(result.getResult(), translateVariantValueCategoryEvent.getCatalogVersion());
    }

    public void setDescriptionTranslateService(DescriptionTranslateService descriptionTranslateService) {
        this.descriptionTranslateService = descriptionTranslateService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
