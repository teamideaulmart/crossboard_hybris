package ru.ulmart.crossboard.core.services.catalog;

import de.hybris.platform.category.model.CategoryModel;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by vitalii on 13.10.16.
 */
public interface CrossboardViewTypeService {

    Cookie getViewTypeCookie(HttpServletRequest request, CategoryModel category, Integer viewTypeCode);

    Cookie getViewTypeCookie(HttpServletRequest request, Integer viewTypeCode);
}