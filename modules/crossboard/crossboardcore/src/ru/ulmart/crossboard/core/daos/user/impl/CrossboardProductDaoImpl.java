package ru.ulmart.crossboard.core.daos.user.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import ru.ulmart.crossboard.core.daos.user.CrossboardProductDao;
import ru.ulmart.crossboard.core.model.MerchantModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by Timofey Klyubin on 14.09.16.
 */
public class CrossboardProductDaoImpl extends DefaultGenericDao<ProductModel> implements CrossboardProductDao {

    private static final String BY_MERCHANT_SKU_QUERY = "SELECT {p." +
            ProductModel.PK + "} FROM {" +
            ProductModel._TYPECODE + " as p JOIN " +
            MerchantModel._TYPECODE + " as m " +
            "ON ({m." + MerchantModel.CODE +
            "} = ?" + MerchantModel.CODE +
            " OR {m." + MerchantModel.EXTERNALCODE + "} = ?" +
            MerchantModel.EXTERNALCODE + " )" +
            "} WHERE {p." + ProductModel.MERCHANTSKU +
            "}=?" + ProductModel.MERCHANTSKU +
            " AND {p." + ProductModel.MERCHANT + "} = {m." +
            MerchantModel.PK + "}";

    private static final String BY_VENDOR_SKU_QUERY = "SELECT {cp." +
            ProductModel.PK + "} FROM {" +
            ProductModel._TYPECODE + " as cp} WHERE {cp." +
            ProductModel.VENDORSKU + "} = ?" + ProductModel.VENDORSKU;

    private static final String BY_CODE_QUERY = "SELECT {cp." +
            ProductModel.PK + "} FROM {" +
            ProductModel._TYPECODE + " as cp} WHERE {cp." +
            ProductModel.CODE + "} = ?" + ProductModel.CODE;

    private static final String ALL_QUERY = "SELECT {cp." +
            ProductModel.PK + "} FROM {" +
            ProductModel._TYPECODE + " as cp}";

    public CrossboardProductDaoImpl() {
        super(ProductModel._TYPECODE);
    }

    @Override
    public ProductModel findProductByMerchantSku(String merchantSku, String merchantId) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(BY_MERCHANT_SKU_QUERY);
        query.addQueryParameter(MerchantModel.CODE, merchantId);
        query.addQueryParameter(MerchantModel.EXTERNALCODE, merchantId);
        query.addQueryParameter(ProductModel.MERCHANTSKU, merchantSku);
        final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
        return result.getResult().stream().findAny().orElse(null);
    }

    @Override
    public ProductModel findProductByVendorSku(String vendorSku) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(BY_VENDOR_SKU_QUERY);
        query.addQueryParameter(ProductModel.VENDORSKU, vendorSku);
        final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
        return result.getResult().stream().findAny().orElse(null);
    }

    @Override
    public ProductModel findProductByCode(String code) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(BY_CODE_QUERY);
        query.addQueryParameter(ProductModel.CODE, code);
        final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
        return result.getResult().stream().findAny().orElse(null);
    }

    @Override
    public ProductModel findProductByCode(CatalogVersionModel catalogVersion, String code) {
        FlexibleSearchQuery query = new FlexibleSearchQuery(BY_CODE_QUERY);
        query.addQueryParameter(ProductModel.CODE, code);
        query.setCatalogVersions(Collections.singletonList(catalogVersion));
        final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
        return result.getResult().stream().findAny().orElse(null);
    }

    @Override
    public List<ProductModel> findAllProducts() {
        FlexibleSearchQuery query = new FlexibleSearchQuery(ALL_QUERY);
        final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
        return result.getResult();
    }
}
