package ru.ulmart.crossboard.core.daos.user;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

/**
 * Created by Timofey Klyubin on 14.09.16.
 */
public interface CrossboardProductDao {
    ProductModel findProductByMerchantSku(String merchantSku, String merchantId);
    ProductModel findProductByVendorSku(String vendorSku);
    ProductModel findProductByCode(String code);
    ProductModel findProductByCode(CatalogVersionModel catalogVersion, String code);
    List<ProductModel> findAllProducts();
}
