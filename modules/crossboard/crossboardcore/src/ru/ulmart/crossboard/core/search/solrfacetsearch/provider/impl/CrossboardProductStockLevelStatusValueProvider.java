package ru.ulmart.crossboard.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductStockLevelStatusValueProvider;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;

/**
 * Created by Timofey Klyubin on 10.11.16.
 */
public class CrossboardProductStockLevelStatusValueProvider extends ProductStockLevelStatusValueProvider {
    @Override
    protected StockLevelStatus getProductStockLevelStatus(ProductModel product, BaseStoreModel baseStore) {
        if (product.getVariantType() == null) {
            return super.getProductStockLevelStatus(product, baseStore);
        }

        boolean inStock = false;
        if (CollectionUtils.isNotEmpty(product.getVariants())) {
            for (VariantProductModel variantProduct : product.getVariants()) {
                StockLevelStatus sls = super.getProductStockLevelStatus(variantProduct, baseStore);
                if (sls != null && !sls.equals(StockLevelStatus.OUTOFSTOCK)) {
                    inStock = true;
                    break;
                }
            }
        }
        return inStock ? StockLevelStatus.INSTOCK : StockLevelStatus.OUTOFSTOCK;
    }
}
