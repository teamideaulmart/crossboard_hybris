package ru.ulmart.crossboard.core.services.catalog;

import de.hybris.platform.core.model.product.ProductModel;
import ru.ulmart.crossboard.core.model.MerchantModel;

import java.util.List;

/**
 * Created by Timofey Klyubin on 14.09.16.
 */
public interface CrossboardProductService {
    ProductModel findProductByMerchantSku(final String merchantSku, final String merchantId);
    ProductModel findProductByVendorSku(final String vendorSku);
    ProductModel findProductByCode(final String code);
    ProductModel getOrCreateProductModel(String code, boolean createVariant);

    List<ProductModel> findAllProducts();
}
