package ru.ulmart.crossboard.core.services.email;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Timofey Klyubin on 11.10.16.
 */
public class CrossboardEmailGenerationService extends DefaultEmailGenerationService {

    private static final Logger LOG = Logger.getLogger(CrossboardEmailGenerationService.class);

    public static final String MULTIPLE_EMAILS = "emailsList";

    private static final String TO_DISPLAY_NAME = "Crossboard Administrator";

    @Override
    public EmailMessageModel generate(BusinessProcessModel businessProcessModel, EmailPageModel emailPageModel) {
        LOG.debug("Generating email ...");
        return super.generate(businessProcessModel, emailPageModel);
    }

    @Override
    protected boolean validate(AbstractEmailContext<BusinessProcessModel> emailContext) {
        LOG.debug("Validating email context ...");
        if (StringUtils.isEmpty(emailContext.getFromEmail())) {
            LOG.warn("Missing FromEmail in " + emailContext.getClass().getName());
            return false;
        }
        if (!(emailContext.get(MULTIPLE_EMAILS) instanceof List) || CollectionUtils.isEmpty((List) emailContext.get(MULTIPLE_EMAILS))) {
            LOG.warn("Missing recipient emails in " + emailContext.getClass().getName());
            return false;
        }
        return true;
    }

    @Override
    protected EmailMessageModel createEmailMessage(String emailSubject, String emailBody, AbstractEmailContext<BusinessProcessModel> emailContext) {
        LOG.debug("Creating email message ...");

        final List<EmailAddressModel> toEmails = new ArrayList<>();
        if (!(emailContext.get(MULTIPLE_EMAILS) instanceof List)) {
            final RuntimeException iae = new IllegalArgumentException("Invalid type for recipient email list!");
            LOG.error("Invalid type for recipient email list!", iae);
            throw iae;
        }
        List<String> recipientList = (List<String>) emailContext.get(MULTIPLE_EMAILS);
        for (String recipient : recipientList) {
            toEmails.add(getEmailService().getOrCreateEmailAddressForEmail(recipient, TO_DISPLAY_NAME));
        }

        final EmailAddressModel fromAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getFromEmail(),
                emailContext.getFromDisplayName());
        return getEmailService().createEmailMessage(toEmails, Collections.emptyList(),
                Collections.emptyList(), fromAddress, emailContext.getFromEmail(), emailSubject, emailBody, null);
    }
}
