package ru.ulmart.crossboard.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ProductMerchantUrlProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {

    private FieldNameProvider fieldNameProvider;

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
                                                 final Object model) throws FieldValueProviderException
    {
        if (model == null)
        {
            throw new IllegalArgumentException("No model given");
        }

        ProductModel product = (ProductModel) model;
        final List<FieldValue> fieldValues = new ArrayList<FieldValue>();
        final Object value = getMerchantUrl(product);

        if (value != null) {
            final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);
            fieldValues.addAll(fieldNames.stream().map(fieldName -> new FieldValue(fieldName, value)).collect(Collectors.toList()));
        }

        return fieldValues;
    }

    private Object getMerchantUrl(ProductModel product) {
        String merchantUrl = product.getMerchantUrl();

        if(!StringUtils.isEmpty(merchantUrl)){
            return merchantUrl;
        }

        if (product instanceof VariantProductModel){
            return ((VariantProductModel) product).getBaseProduct().getMerchantUrl();
        }

        if(product.getVariants() != null){
            for(VariantProductModel variant: product.getVariants()){
                merchantUrl = variant.getMerchantUrl();
                if(!StringUtils.isEmpty(merchantUrl)){
                    return merchantUrl;
                }
            }
        }

        return null;
    }

    protected FieldNameProvider getFieldNameProvider()
    {
        return fieldNameProvider;
    }

    /**
     * @param fieldNameProvider
     *           the fieldNameProvider to set
     */
    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
    {
        this.fieldNameProvider = fieldNameProvider;
    }
}
