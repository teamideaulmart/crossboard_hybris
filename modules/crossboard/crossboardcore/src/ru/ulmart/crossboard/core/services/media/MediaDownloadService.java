package ru.ulmart.crossboard.core.services.media;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

/**
 * Created by max on 19.09.16.
 */
public interface MediaDownloadService {
    void downloadMediasForProducts(List<String> productCodes, CatalogVersionModel catalogVersion);
}
