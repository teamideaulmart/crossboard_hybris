package ru.ulmart.crossboard.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * Created by Alexey on 13.10.2016.
 */
public class ProductMerchantNameProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {
    private FieldNameProvider fieldNameProvider;
    private CommonI18NService commonI18NService;

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
                                                 final Object model) throws FieldValueProviderException
    {
        if (model == null)
        {
            throw new IllegalArgumentException("No model given");
        }

        ProductModel product = (ProductModel) model;
        final List<FieldValue> fieldValues = new ArrayList<FieldValue>();


        if (indexedProperty.isLocalized())
        {
            final Collection<LanguageModel> languages = indexConfig.getLanguages();
            for (final LanguageModel language : languages)
            {
                Object value = null;
                final Locale locale = i18nService.getCurrentLocale();
                try
                {
                    i18nService.setCurrentLocale(commonI18NService.getLocaleForLanguage(language));
                    value = getMerchantName(product);
                }
                finally
                {
                    i18nService.setCurrentLocale(locale);
                }

                if (value != null)
                {
                    final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, language.getIsocode());
                    for (final String fieldName : fieldNames)
                    {
                        fieldValues.add(new FieldValue(fieldName, value));
                    }
                }
            }
        }
        else
        {
            final Object value = getMerchantName(product);

            if (value != null)
            {
                final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);

                for (final String fieldName : fieldNames)
                {
                     fieldValues.add(new FieldValue(fieldName, value));
                }
            }
        }
        return fieldValues;
    }

    private Object getMerchantName(ProductModel product) {
        if(product.getMerchant() != null){
            return product.getMerchant().getName();
        }
        return null;
    }

    protected FieldNameProvider getFieldNameProvider()
    {
        return fieldNameProvider;
    }

    /**
     * @param fieldNameProvider
     *           the fieldNameProvider to set
     */
    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
    {
        this.fieldNameProvider = fieldNameProvider;
    }


    protected CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }
}
