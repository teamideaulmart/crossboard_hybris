package ru.ulmart.crossboard.core.services.catalog;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.UnitService;

import java.util.Locale;

/**
 * Created by Marina on 17.09.2016.
 */
public interface CrossboardUnitService extends UnitService {

    UnitModel getOrCreateUnitModel(String unitCode, Locale locale, CatalogVersionModel catalogVersion);
}
