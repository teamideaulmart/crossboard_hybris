package ru.ulmart.crossboard.core.services.search;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Oksana Kurushina on 12.10.2016.
 */
public interface UlmartElasticSearchService {

    String ULMART_ELASTIC_SEARCH_URL = "ulmart.elastic.search.url";
    String ULMART_DISCOUNT_ELASTIC_SEARCH_URL = "ulmart.discount.elastic.search.url";

    Integer getAutocompleteGoodsAmount(String url, String term, HttpServletRequest request, HttpServletResponse servletResponse);

    void putCity(String city, HttpServletRequest request, HttpServletResponse servletResponse);

}
