package ru.ulmart.crossboard.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.util.*;

public class ProductFromPriceProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {

    private FieldNameProvider fieldNameProvider;
    private PriceService priceService;

    protected void addFieldValues(final IndexedProperty indexedProperty, final ProductModel product,
                                  final Collection<FieldValue> fieldValues, final Collection<Object> values, final String currencyIsoCode) {
        final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, currencyIsoCode);
        for (final String fieldName : fieldNames) {
            values.stream().forEach(value -> {
                fieldValues.add(new FieldValue(fieldName, value));
            });
        }
    }

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
                                                 final Object model) throws FieldValueProviderException {
        try {

            final ProductModel product = (ProductModel) model; // this provider should only be used with products
            final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

            if (indexConfig.getCurrencies().isEmpty()) {
                final List<PriceInformation> prices = getProductPrices(product);
                if (CollectionUtils.isNotEmpty(prices)) {
                    createFieldValue(indexedProperty, product, fieldValues, prices);
                }

            } else {

                final CurrencyModel sessionCurrency = i18nService.getCurrentCurrency();
                try {
                    for (final CurrencyModel currency : indexConfig.getCurrencies()) {
                        i18nService.setCurrentCurrency(currency);
                        final List<PriceInformation> prices = getProductPrices(product);
                        if (prices != null && !prices.isEmpty()) {
                            createFieldValue(indexedProperty, product, fieldValues, prices);
                        }
                    }
                } finally {
                    i18nService.setCurrentCurrency(sessionCurrency);
                }
            }

            return fieldValues;
        } catch (Exception e) {
            LOG.error("Cannot evaluate " + indexedProperty.getName() + " using " + this.getClass().getName(), e);
            throw new FieldValueProviderException("Cannot evaluate " + indexedProperty.getName() + " using " + this.getClass().getName(), e);
        }
    }

    private void createFieldValue(IndexedProperty indexedProperty, ProductModel product, Collection<FieldValue> fieldValues, List<PriceInformation> prices) throws FieldValueProviderException {
        PriceInformation currentPriceInfo = prices.get(0);
        Double currentPrice = Double.valueOf(currentPriceInfo.getPriceValue().getValue());
        List<String> ranges = getRangeNameList(indexedProperty, currentPrice);

        if (CollectionUtils.isNotEmpty(ranges)) {
            addFieldValues(indexedProperty, product, fieldValues, Collections.unmodifiableCollection(ranges), currentPriceInfo.getPriceValue().getCurrencyIso().toLowerCase());
        } else {
            addFieldValues(indexedProperty, product, fieldValues, Collections.singleton(currentPrice), currentPriceInfo.getPriceValue().getCurrencyIso().toLowerCase());
        }
    }

    private List<PriceInformation> getProductPrices(final ProductModel product) {
        if (CollectionUtils.isNotEmpty(product.getVariants())) {
            List<PriceInformation> prices = new ArrayList<>();
            for (VariantProductModel variant : product.getVariants()) {
                prices.addAll(getPriceService().getPriceInformationsForProduct(variant));
            }

            if (CollectionUtils.isNotEmpty(prices)) {
                PriceInformation minPrice = prices.stream()
                        .sorted((l, r) -> Double.compare(l.getPriceValue().getValue(), r.getPriceValue().getValue()))
                        .findFirst().orElse(null);
                if (minPrice != null) {
                    return Arrays.asList(minPrice);
                }
            }
        }

        return null;
    }

    protected FieldNameProvider getFieldNameProvider() {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    public PriceService getPriceService() {
        return priceService;
    }

    @Required
    public void setPriceService(PriceService priceService) {
        this.priceService = priceService;
    }
}
