package ru.ulmart.crossboard.core.services.stock;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.stock.impl.DefaultCommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;

/**
 * Created by Timofey Klyubin on 11.11.16.
 */
public class CrossboardCommerceStockService extends DefaultCommerceStockService {

    @Override
    public StockLevelStatus getStockLevelStatusForProductAndBaseStore(ProductModel product, BaseStoreModel baseStore) {
        return getProductStockLevelStatus(product, baseStore);
    }

    protected StockLevelStatus getProductStockLevelStatus(ProductModel product, BaseStoreModel baseStore) {
        if (product.getVariantType() == null) {
            return super.getStockLevelStatusForProductAndBaseStore(product, baseStore);
        }

        boolean inStock = false;
        if (CollectionUtils.isNotEmpty(product.getVariants())) {
            for (VariantProductModel variantProduct : product.getVariants()) {
                StockLevelStatus sls = super.getStockLevelStatusForProductAndBaseStore(variantProduct, baseStore);
                if (sls != null && !sls.equals(StockLevelStatus.OUTOFSTOCK)) {
                    inStock = true;
                    break;
                }
            }
        }
        return inStock ? StockLevelStatus.INSTOCK : StockLevelStatus.OUTOFSTOCK;
    }
}
