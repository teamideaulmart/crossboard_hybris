package ru.ulmart.crossboard.core.search;

import de.hybris.platform.commerceservices.search.solrfacetsearch.querybuilder.impl.DefaultFreeTextQueryBuilder;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.solr.client.solrj.util.ClientUtils;

/**
 * Created by Timofey Klyubin on 21.11.16.
 */
public class CrossboardFreeTextQueryBuilder extends DefaultFreeTextQueryBuilder {

    private static final String DESCRIPTION_FIELD = "description";
    private static final String DESCRIPTION_FUZZY_ERROR = "1";
    private static final String NAME_FIELD = "name";
    private static final String NAME_FUZZY_ERROR = "2";

    @SuppressWarnings("deprecation")
    @Override
    protected void addFreeTextQuery(SearchQuery searchQuery, String field, String value, String suffixOp, double boost) {
        SearchQuery.Operator operator = (field.equals(DESCRIPTION_FIELD) && suffixOp.equals("~")) ? SearchQuery.Operator.AND : SearchQuery.Operator.OR;
        if (suffixOp.equals("~")) {
            if (field.equals(DESCRIPTION_FIELD)) {
                suffixOp = "~".concat(DESCRIPTION_FUZZY_ERROR);
            }
            if (field.equals(NAME_FIELD)) {
                suffixOp = "~".concat(NAME_FUZZY_ERROR);
            }
        }
        final RawQuery rawQuery = new RawQuery(field, ClientUtils.escapeQueryChars(value) + suffixOp + "^" + boost, operator);
        searchQuery.addRawQuery(rawQuery);
    }
}
