package ru.ulmart.crossboard.core.jobs;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import ru.ulmart.crossboard.core.enums.MediaDownloadStatus;
import ru.ulmart.crossboard.core.jobs.model.cronjob.DownloadMediasCronJobModel;
import ru.ulmart.crossboard.core.services.media.MediaDownloadService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Marina on 04.11.2016.
 */
public class DownloadMediasJobPerformable extends AbstractJobPerformable<DownloadMediasCronJobModel> {
    private static final Logger LOG = Logger.getLogger(DownloadMediasJobPerformable.class.getName());

    private MediaDownloadService mediaDownloadService;
    private CatalogVersionService catalogVersionService;

    @Override
    public PerformResult perform(final DownloadMediasCronJobModel cronJobModel) {
        LOG.info("Starting DownloadMediasJobPerformable");

        Set<String> productCodes = new HashSet<>();
        List<CategoryModel> categories = cronJobModel.getCategories();
        for(CategoryModel cat:categories){
            List<ProductModel> allProducts = new ArrayList<>(cat.getProducts());
            List<ProductModel> products = cat.getProducts();
            allProducts.addAll(products);
            for (CategoryModel subcategory : cat.getAllSubcategories()) {
                subcategory.getProducts().stream().filter(productModel -> CollectionUtils.isNotEmpty(productModel.getVariants())).forEach(productModel -> {
                    allProducts.addAll(productModel.getVariants());
                });
                allProducts.addAll(subcategory.getProducts());
            }
            products.stream().filter(productModel -> CollectionUtils.isNotEmpty(productModel.getVariants())).forEach(productModel -> {
                allProducts.addAll(productModel.getVariants());
            });
            productCodes.addAll(allProducts.stream().filter(p -> BooleanUtils.isTrue(cronJobModel.getIgnoreMediaStatus())
                    || checkProductMedias(p)).map(ProductModel::getCode).collect(Collectors.toList()));
        }

        if(productCodes.size() < 1){
            LOG.warn("All products already have downloaded medias (SUCCESS)");
        }else {
            List<String> codes = new LinkedList<>();
            codes.addAll(productCodes);
            LOG.info(String.format("Call mediaDownloadService with %d products", productCodes.size()));
            CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("crossboardProductCatalog", "Online");
            mediaDownloadService.downloadMediasForProducts(codes,catalogVersion);
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private boolean checkProductMedias(final ProductModel p) {
        for (MediaModel mediaModel : p.getImages()) {
            if (mediaModel.getDownloadStatus() != MediaDownloadStatus.SUCCESS) {
                return true;
            }
        }
        return false;
    }

    @Required
    public void setMediaDownloadService(MediaDownloadService mediaDownloadService) {
        this.mediaDownloadService = mediaDownloadService;
    }

    @Required
    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

}
