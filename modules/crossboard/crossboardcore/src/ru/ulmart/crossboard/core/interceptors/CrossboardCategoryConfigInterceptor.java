package ru.ulmart.crossboard.core.interceptors;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.springframework.beans.factory.annotation.Value;
import ru.ulmart.crossboard.core.model.MerchantConfigModel;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.user.MerchantService;

public class CrossboardCategoryConfigInterceptor implements PrepareInterceptor<CategoryModel> {

    @Resource(name = "merchantService")
    private MerchantService merchantService;

    @Override
    public void onPrepare(CategoryModel category, InterceptorContext ctx) throws InterceptorException {
        List<MerchantModel> merchants = merchantService.findAllMerchants();

        for (MerchantModel merchant : merchants) {
            if (!hasMerchant(category, merchant)) {
                MerchantConfigModel configModel = ctx.getModelService().create(MerchantConfigModel.class);
                configModel.setCategory(category);
                configModel.setMerchant(merchant);
                ctx.registerElementFor(configModel, PersistenceOperation.SAVE);
            }
        }
    }

    private static boolean hasMerchant(CategoryModel category, MerchantModel merchant) {
        Collection<MerchantConfigModel> configModels = category.getMerchantConfig() == null ? Collections.emptyList() : category.getMerchantConfig();
        for (MerchantConfigModel configModel : configModels) {
            if (configModel.getMerchant().getCode().equals(merchant.getCode())) return true;
        }
        return false;
    }
}