package ru.ulmart.crossboard.core.strategies;

import de.hybris.platform.acceleratorservices.process.strategies.impl.DefaultProcessContextResolutionStrategy;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

/**
 * Created by Timofey Klyubin on 13.10.16.
 */
public class CrossboardProcessContextResolutionStrategy extends DefaultProcessContextResolutionStrategy {

    private String emailCatalogVersion;
    private String emailCatalogName;

    @Override
    public void initializeContext(BusinessProcessModel businessProcess) {
        ServicesUtil.validateParameterNotNull(businessProcess, "businessProcess must not be null");
        getBaseSiteService().setCurrentBaseSite(getBaseSiteService().getAllBaseSites().iterator().next(), true);
        getCommonI18NService().setCurrentLanguage(getCommonI18NService().getLanguage("ru"));
    }

    @Override
    public CatalogVersionModel getContentCatalogVersion(BusinessProcessModel businessProcess) {
        return getCatalogVersionService().getCatalogVersion(
                emailCatalogName,
                emailCatalogVersion
        );
    }

    public void setEmailCatalogVersion(String emailCatalogVersion) {
        this.emailCatalogVersion = emailCatalogVersion;
    }

    public void setEmailCatalogName(String emailCatalogName) {
        this.emailCatalogName = emailCatalogName;
    }
}
