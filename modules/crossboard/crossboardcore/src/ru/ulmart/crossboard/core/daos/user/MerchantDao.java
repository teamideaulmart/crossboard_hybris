package ru.ulmart.crossboard.core.daos.user;

import ru.ulmart.crossboard.core.model.MerchantModel;

import java.util.List;

/**
 * Created by Marina on 08.09.2016.
 */
public interface MerchantDao {

   MerchantModel findMerchantForExternalCode(final String externalCode);

   List<MerchantModel> findAllMerchants();
}
