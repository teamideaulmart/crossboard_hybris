package ru.ulmart.crossboard.core.event;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

/**
 * Created by Timofey Klyubin on 10.10.16.
 */
public class TranslateUnitEvent extends AbstractEvent {

    private final CatalogVersionModel catalogVersion;

    public TranslateUnitEvent(CatalogVersionModel catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogVersion;
    }
}
