package ru.ulmart.crossboard.core.services.catalog;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import ru.ulmart.crossboard.core.model.MerchantConfigModel;
import ru.ulmart.crossboard.core.model.MerchantModel;

import java.util.Collection;

/**
 * Created by Marina on 06.09.2016.
 */
public interface CrossboardCategoryService extends CategoryService {

    CategoryModel getRootCategoryForCatalogVersionAndCategoriesType(final CatalogVersionModel catalogVersion, final String categoriesType);

    MerchantConfigModel getMerchantConfigForCategoryAndMerchant(final CategoryModel categoryModel, final MerchantModel merchant);

    VariantCategoryModel getVariantCategoryByCode(CatalogVersionModel catalogVersion, String code);

    VariantValueCategoryModel getOrCreateVariantValueCategory(final CatalogVersionModel catalogVersion, final String code);

    boolean isImportAllowedForCategoryAndMerchant(final CategoryModel categoryModel, final MerchantModel merchant);

    CategoryPageModel getCategoryPageByViewType(final Integer viewType);

    Collection<CategoryModel> getRootCategoriesForCatalogVersionsAndNameTerm(final String categoryName, int maxResultsCount);
}
