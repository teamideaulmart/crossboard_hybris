package ru.ulmart.crossboard.core.services.catalog.impl;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ulmart.crossboard.core.services.catalog.CrossboardCategoryService;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductReportService;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * Created by max on 08.11.16.
 */
public class CrossboardProductReportServiceImpl implements CrossboardProductReportService {


    private static final Logger LOG = LoggerFactory.getLogger(CrossboardProductReportServiceImpl.class);


    public CrossboardProductService getProductService() {
        return productService;
    }

    @Autowired
    public void setProductService(CrossboardProductService productService) {
        this.productService = productService;
    }

    private CrossboardProductService productService;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private UserService userService;


    public CrossboardCategoryService getCategoryService() {
        return categoryService;
    }

    @Autowired
    public void setCategoryService(CrossboardCategoryService categoryService) {
        this.categoryService = categoryService;
    }

    private CrossboardCategoryService categoryService;


    public CatalogService getCatalogService() {
        return catalogService;
    }

    @Autowired
    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    private CatalogService catalogService;


    public CommercePriceService getPriceService() {
        return priceService;
    }

    @Autowired
    public void setPriceService(CommercePriceService priceService) {
        this.priceService = priceService;
    }

    private CommercePriceService priceService;


    public String getCSVData() {

        userService.setCurrentUser(userService.getUserForUID("admin"));

        StrBuilder result = new StrBuilder();
        result.append("категория;код;название;name;бренд;цена").appendNewLine();

        CategoryModel rootCategory = categoryService.getRootCategoryForCatalogVersionAndCategoriesType(getCatalogVersion(), "hyb_categories");
        printCategoryContent(rootCategory, result, 1, "КОРЕНЬ ");

        String filePath = Config.getParameter("HYBRIS_TEMP_DIR") + "/reportProducts.csv";
        LOG.info("writing file: "+filePath);

        try {
            FileUtils.writeStringToFile(new File(filePath),result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    String buildCategoryPathPart(String initial, CategoryModel cat){
        StrBuilder sb = new StrBuilder();
        sb.append(initial).append("/").append(cat.getCode()).append(" (").append(cat.getName(Locale.forLanguageTag("RU"))).append(") ");
        return sb.toString();
    }
    private void printCategoryContent(CategoryModel rootCategory, StrBuilder result, int counter, String prefixString) {


        List<CategoryModel> categories = rootCategory.getCategories();
        for (CategoryModel cat : categories) {
            SearchResult<ProductModel> results = productDao.findProductsByCategory(cat, 0, 99999);
            List<ProductModel> products = new ArrayList<>(results != null ? results.getResult() : new ArrayList<>());
            for (ProductModel prod : products) {
                printProduct(result, prod,prefixString);
            }
            printCategoryContent(cat, result, counter + 1,buildCategoryPathPart(prefixString,cat));
        }

    }

    private void printGenericProduct(StrBuilder result, ProductModel prod,String categoryPath) {
        if(StringUtils.equals(categoryPath, "КОРЕНЬ ")) {
            return;
        }

        result.append(categoryPath).append(";");
        result.append(prod.getCode()).append(";");
        result.append(prod.getName(Locale.forLanguageTag("RU"))).append(";");
        result.append(prod.getName(Locale.forLanguageTag("EN"))).append(";");
        result.append(prod.getBrand()).append(";");

        PriceInformation priceInformation = priceService.getWebPriceForProduct(prod);
        result.append(priceInformation != null ? (priceInformation.getPriceValue() != null ? priceInformation.getPriceValue().getValue() : "")
                : "").append(";");
        result.appendNewLine();
    }


    private void printProduct(StrBuilder result, ProductModel prod,String prefix) {
        Collection<VariantProductModel> variants = prod.getVariants();

        if (variants.isEmpty()) {
            printGenericProduct(result, prod,prefix);
            return;
        }

        for (ProductModel pr : variants) {
            printGenericProduct(result, pr,prefix);
        }


    }



    private CatalogVersionModel getCatalogVersion() {
        CatalogVersionModel catalogVersion = null;
        final String catalogNameParameter = "crossboardProductCatalog";
        final String catalogVersionParameter = "Online";

        catalogVersion = getCatalogService().getCatalogVersion(catalogNameParameter, catalogVersionParameter);

        return catalogVersion;
    }

}
