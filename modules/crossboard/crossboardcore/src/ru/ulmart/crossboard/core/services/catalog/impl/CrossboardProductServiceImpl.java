package ru.ulmart.crossboard.core.services.catalog.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ulmart.crossboard.core.daos.user.CrossboardProductDao;
import ru.ulmart.crossboard.core.model.CrossboardProductAttributeModel;
import ru.ulmart.crossboard.core.services.catalog.CrossboardProductService;

import java.util.List;

/**
 * Created by Timofey Klyubin on 14.09.16.
 */
public class CrossboardProductServiceImpl implements CrossboardProductService {

    private CrossboardProductDao crossboardProductDao;
    private ModelService modelService;

    @Autowired
    public void setCrossboardProductDao(CrossboardProductDao crossboardProductDao) {
        this.crossboardProductDao = crossboardProductDao;
    }

    @Autowired
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * Finds product using merchant identifier or external code
     *
     * @param merchantSku product's merchant SKU
     * @param merchantId identifier or external code
     * @return found product
     */
    @Override
    public ProductModel findProductByMerchantSku(String merchantSku, String merchantId) {
        return crossboardProductDao.findProductByMerchantSku(merchantSku, merchantId);
    }

    @Override
    public ProductModel findProductByVendorSku(String vendorSku) {
        return crossboardProductDao.findProductByVendorSku(vendorSku);
    }

    @Override
    public ProductModel findProductByCode(String code) {
        return crossboardProductDao.findProductByCode(code);
    }

    @Override
    public ProductModel getOrCreateProductModel(String code, boolean createVariant) {
        Class<? extends ProductModel> clazz = createVariant ? GenericVariantProductModel.class : ProductModel.class;
        ProductModel crossboardProduct = findProductByCode(code);
        if (crossboardProduct == null) {
            crossboardProduct = modelService.create(clazz);
            crossboardProduct.setCode(code);
        } else if (createVariant && !(crossboardProduct instanceof VariantProductModel)) {
            VariantProductModel variantCrossboardProduct = modelService.create(clazz);
            variantCrossboardProduct.setCode(code);
            variantCrossboardProduct.setImages(crossboardProduct.getImages());
            crossboardProduct.getAttributes().forEach(modelService::remove);
            modelService.remove(crossboardProduct);
            return variantCrossboardProduct;
        }
        return crossboardProduct;
    }

    @Override
    public List<ProductModel> findAllProducts() {
        return crossboardProductDao.findAllProducts();
    }


}
