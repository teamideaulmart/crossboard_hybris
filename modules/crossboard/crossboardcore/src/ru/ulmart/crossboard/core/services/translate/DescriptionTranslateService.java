package ru.ulmart.crossboard.core.services.translate;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

/**
 * Created by max on 21.09.16.
 */
public interface DescriptionTranslateService {
    void translateProductDescriptions(List<String> productCodes, CatalogVersionModel catalogVersion);
    void translateVariantValueCategories(List<String> categoryCodes, CatalogVersionModel catalogVersion);
    void translateSalesUnits(List<String> unitCodes, CatalogVersionModel catalogVersion);
}
