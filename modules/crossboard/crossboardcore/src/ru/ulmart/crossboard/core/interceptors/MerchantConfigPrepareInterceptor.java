package ru.ulmart.crossboard.core.interceptors;

import java.util.Collection;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.model.MerchantConfigModel;
import ru.ulmart.crossboard.core.model.MerchantModel;

public class MerchantConfigPrepareInterceptor implements PrepareInterceptor<MerchantConfigModel> {

    private static final Logger LOG = Logger.getLogger(MerchantConfigPrepareInterceptor.class);

    @Override
    public void onPrepare(MerchantConfigModel model, InterceptorContext ctx) throws InterceptorException {

        final MerchantModel merchant = model.getMerchant();
        final Collection<CategoryModel> subcategories = model.getCategory().getCategories();

        if (subcategories == null) {
            return;
        }

        LOG.info(String.format("Adding merchant configs to %1$s subcategories", subcategories.size()));

        if (CollectionUtils.isNotEmpty(subcategories)) {
            subcategories.forEach((sub) -> {
                boolean hasConfig = sub.getMerchantConfig() != null
                        && sub.getMerchantConfig().size() > 0
                        && sub.getMerchantConfig().stream().anyMatch(config -> config.getMerchant().getCode().equals(merchant.getCode()));
                if (hasConfig) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug(String.format("Category %1$s already has needed merchant config", sub.getCode()));
                    }
                    if (BooleanUtils.isTrue(model.getImportAllowed()) || BooleanUtils.isTrue(model.getDisplayAllowed())) {
                        @SuppressWarnings("OptionalGetWithoutIsPresent")
                        final MerchantConfigModel currentConfig = sub.getMerchantConfig()
                                .stream()
                                .filter(config -> config.getMerchant().getCode().equals(merchant.getCode()))
                                .findAny().get();
                        if (BooleanUtils.isTrue(model.getImportAllowed())) {
                            currentConfig.setImportAllowed(Boolean.TRUE);
                        }
                        if (BooleanUtils.isTrue(model.getDisplayAllowed())) {
                            currentConfig.setDisplayAllowed(Boolean.TRUE);
                        }
                        ctx.registerElementFor(currentConfig, PersistenceOperation.SAVE);
                    }
                } else {
                    boolean alreadyInSaving = ctx.getElementsRegisteredFor(PersistenceOperation.SAVE)
                            .stream()
                            .anyMatch((obj) -> (obj instanceof MerchantConfigModel)
                                    && ((MerchantConfigModel) obj).getMerchant().getCode().equals(model.getMerchant().getCode())
                                    && ((MerchantConfigModel) obj).getCategory().getCode().equals(sub.getCode()));

                    if (alreadyInSaving) {
                        LOG.debug("Merchant config is already being saved!");
                    } else {
                        MerchantConfigModel newConfig = ctx.getModelService().create(MerchantConfigModel.class);
                        newConfig.setImportAllowed(model.getImportAllowed());
                        if (BooleanUtils.isTrue(model.getDisplayAllowed())) {
                            newConfig.setDisplayAllowed(Boolean.TRUE);
                        }
                        newConfig.setMerchant(merchant);
                        newConfig.setCategory(sub);
                        ctx.registerElementFor(newConfig, PersistenceOperation.SAVE);
                    }
                }

            });
        }
    }
}