package ru.ulmart.crossboard.core.services.translate.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.TenantAwareThreadFactory;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import ru.teamidea.crossboard.b2c.integration.aspect.ExecuteInSession;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.ulmart.crossboard.core.daos.user.CrossboardProductDao;
import ru.ulmart.crossboard.core.enums.ProductDescriptionStatus;
import ru.ulmart.crossboard.core.model.CrossboardProductAttributeModel;
import ru.ulmart.crossboard.core.services.translate.DescriptionTranslateService;

import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static ru.teamidea.utils.constants.TeamideaUtilsConstants.RUSSIAN_LOCALE;

/**
 * Created by max on 21.09.16.
 */
public class DescriptionTranslateServiceImpl implements DescriptionTranslateService {

    private static final Logger LOGGER = Logger.getLogger(DescriptionTranslateService.class);

    @Autowired
    private ModelService modelService;

    @Autowired
    private CrossboardProductDao productDao;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private I18NService i18NService;

    @Autowired
    private TIConfigUtils tiConfigUtils;

    public static final String HOST = "teamidea.hybris.dev.yandex.translate.host";
    public static final String URI = "teamidea.hybris.dev.yandex.translate.uri";
    public static final String APIKEY = "teamidea.hybris.dev.yandex.translate.api.key";

    private static final String options = "lang=en-ru&format=html";
    private static final String scheme = "https://";

    private static final String TRANSLATE_HOST_DEFAULT = "translate.yandex.net";
    private static final String TRANSLATE_URI_DEFAULT = "/api/v1.5/tr.json/translate?";

    private String translateHost;
    private String translateUri;
    private String apiKey;
    private String urlString;

    private static final String THREADPOOLSIZE = "teamidea.translate.threadpoolsize";
    private static final int DEFAULT_THREADPULLSIZE = 4;
    private static final String BATCHSIZE = "teamidea.translate.batchsize";
    private static final int DEFAULT_BATCHSIZE = 4;

    private int threadPoolSize = 4;
    private int maxBatchSize = 3;

    @PostConstruct
    public void init() throws RuntimeException {
        try {
            threadPoolSize = Config.getInt(BATCHSIZE, 3);
        } catch (NumberFormatException e) {
            threadPoolSize = DEFAULT_THREADPULLSIZE;
            LOGGER.error("property " + THREADPOOLSIZE + " has wrong value, using default: " + DEFAULT_THREADPULLSIZE);
        }
        try {
            maxBatchSize = Config.getInt(BATCHSIZE, 4);
        } catch (NumberFormatException e) {
            maxBatchSize = DEFAULT_BATCHSIZE;
            LOGGER.error("property " + BATCHSIZE + " has wrong value, using default: " + DEFAULT_BATCHSIZE);
        }

        translateHost = Config.getString(HOST, TRANSLATE_HOST_DEFAULT);
        translateUri = Config.getString(URI, TRANSLATE_URI_DEFAULT);
        apiKey = Config.getParameter(APIKEY);

        StringBuilder sb = new StringBuilder();
        sb.append(scheme).append(translateHost).append(translateUri).append("key=").append(apiKey).append("&").append(options);
        urlString = sb.toString();
    }

    private void rebuildUrlString() {

        translateHost = tiConfigUtils.getPropertyValue("translate.host", translateHost);
        translateUri = tiConfigUtils.getPropertyValue("translate.uri", translateUri);
        apiKey = tiConfigUtils.getPropertyValue("traslate.apikey", apiKey);
        StringBuilder sb = new StringBuilder();
        sb.append(scheme).append(translateHost).append(translateUri).append("key=").append(apiKey).append("&").append(options);
        urlString = sb.toString();

    }

    @Override
    public void translateProductDescriptions(List<String> productCodes, CatalogVersionModel catalogVersion) {
       translateWithTranslator(
               productCodes,
               ((modelService1, modelsToTranslate, receivedUrlString) -> new ProductTranslator(modelService1, (List<ProductModel>) modelsToTranslate, receivedUrlString)),
               ((code, catalogVersion1) -> {
                   ProductModel product = productDao.findProductByCode(catalogVersion1, code);
                   if (product == null) {
                       LOGGER.warn("product with code <" + code + "> not found");
                       throw new NullPointerException(String.format("Product with code '%1$s' not found!", code));
                   }
                   return product;
               }),
               catalogVersion
       );
    }

    @Override
    @ExecuteInSession
    public void translateVariantValueCategories(List<String> categoryCodes, CatalogVersionModel catalogVersion) {
        translateWithTranslator(
                categoryCodes,
                ((modelService1, modelsToTranslate, receivedUrlString) -> new ValueCategoryTranslator(modelService1, (List<VariantValueCategoryModel>) modelsToTranslate, receivedUrlString)),
                (code, catalogVersion1) -> {
                    CategoryModel category = null;
                    try {
                        category = categoryService.getCategoryForCode(catalogVersion1, code);
                    } catch (UnknownIdentifierException e) {
                        LOGGER.warn("VariantValueCategory with code <" + code + "> not found");
                        throw new NullPointerException(String.format("VariantValueCategory with code '%1$s' not found!", code));
                    }
                    if (category instanceof VariantValueCategoryModel) {
                        return (VariantValueCategoryModel) category;
                    } else {
                        LOGGER.warn("Category with code <" + code + "> is not a VariantValueCategory!");
                        throw new IllegalArgumentException("Category with code <" + code + "> is not a VariantValueCategory!");
                    }
                },
                catalogVersion
        );
    }

    @Override
    public void translateSalesUnits(List<String> unitCodes, CatalogVersionModel catalogVersion) {
        translateWithTranslator(
                unitCodes,
                (modelService1, modelsToTranslate, receivedUrlString) -> new SalesUnitTranslator(modelService1, (List<UnitModel>) modelsToTranslate, receivedUrlString),
                (code, catalogVersion1) -> {
                    UnitModel unit = null;
                    try {
                        unit = unitService.getUnitForCode(code);
                    } catch (UnknownIdentifierException e) {
                        LOGGER.warn("unit with code <" + code + "> not found");
                        throw new NullPointerException(String.format("unit with code '%1$s' not found!", code));
                    }
                    return unit;
                },
                catalogVersion
        );
    }

    private <T extends ItemModel> void translateWithTranslator(List<String> codes,
                                                               TranslatorSupplier<AbstractTranslator<T>> translatorSupplier,
                                                               ModelRetriever<T> modelRetriever,
                                                               CatalogVersionModel catalogVersion) {
        if (!checkTranslateEnabled()) {
            return;
        }

        LOGGER.info(String.format("Starting to translate content of %1$s models",
                String.valueOf(codes.stream().collect(Collectors.joining(", ")))));

        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        final ExecutorService executorService = getExecutorService(threadPoolSize);

        List<T> modelsToTranslate = new ArrayList<>();
        for (String code : codes) {

            try {
                T model = modelRetriever.retrieve(code, catalogVersion);
                modelsToTranslate.add(model);
            } catch (RuntimeException ex) {
                LOGGER.error(ex.getMessage(), ex);
            }

            if (modelsToTranslate.size() >= maxBatchSize) {
                AbstractTranslator<T> runner = translatorSupplier.get(modelService, modelsToTranslate, urlString);
                modelsToTranslate = new ArrayList<>();
                executorService.submit(runner);
            }

        }
        if (modelsToTranslate.size() > 0) {
            AbstractTranslator<T> runner = translatorSupplier.get(modelService, modelsToTranslate, urlString);
            executorService.submit(runner);
        }

        executorService.shutdown();

        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Error during translate models executor await termination", e);
        }

        if (executorService.isTerminated()) {
            stopWatch.stop();
            LOGGER.info(String.format("Finished to translate for %1$s models in %2$s ms",
                    codes.stream().collect(Collectors.joining(", ")), stopWatch.getTime()));
        }
    }

    private boolean checkTranslateEnabled() {
        if (BooleanUtils.isFalse(tiConfigUtils.getBooleanPropertyValue("translate.enabled"))) {
            LOGGER.warn("translation is disabled in TIConfig");
            return false;
        }
        rebuildUrlString();
        return true;
    }


    private ExecutorService getExecutorService(int threadPoolSize) {

        JaloSession jaloSession = JaloSession.getCurrentSession();
        jaloSession.activate();
        i18NService.setLocalizationFallbackEnabled(false);

        return Executors.newFixedThreadPool(threadPoolSize,
                new TenantAwareThreadFactory(Registry.getCurrentTenant(), jaloSession));
    }

/////////////////////////////////////////////////////////

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public CrossboardProductDao getProductDao() {
        return productDao;
    }

    public void setProductDao(CrossboardProductDao productDao) {
        this.productDao = productDao;
    }


    public TIConfigUtils getTiConfigUtils() {
        return tiConfigUtils;
    }

    public void setTiConfigUtils(TIConfigUtils tiConfigUtils) {
        this.tiConfigUtils = tiConfigUtils;
    }

    public I18NService getI18NService() {
        return i18NService;
    }

    public void setI18NService(I18NService i18NService) {
        this.i18NService = i18NService;
    }
}

@FunctionalInterface
interface TranslatorSupplier<S extends AbstractTranslator<? extends ItemModel>> {
    S get(ModelService modelService, List<? extends ItemModel> modelsToTranslate, String urlString);
}

@FunctionalInterface
interface ModelRetriever<T extends ItemModel> {
    T retrieve(String code, CatalogVersionModel catalogVersion);
}

abstract class AbstractTranslator<T extends ItemModel> implements Runnable {
    private final String REQUEST_METHOD = "POST";

    protected ModelService modelService;
    protected List<T> modelList;
    private Logger LOGGER = Logger.getLogger(AbstractTranslator.class);

    private String UrlString;

    public AbstractTranslator(ModelService modelService, List<T> modelList, String UrlString) {
        this.modelService = modelService;
        this.modelList = modelList;
        this.UrlString = UrlString;
    }

    @Override
    public abstract void run();

    protected List<String> doTranslate(List<String> translations) {
        URL requestUrl;
        LOGGER.warn(UrlString);
        try {
            requestUrl = new URL(UrlString);
        } catch (MalformedURLException e) {
            LOGGER.error("Malformed URL = " + UrlString);
            return null;
        }


        try {
            HttpsURLConnection conn = (HttpsURLConnection) requestUrl.openConnection();


            try {
                conn.setRequestMethod(REQUEST_METHOD); // POST, т.к. текст может быть длинным
            } catch (ProtocolException e) {
                LOGGER.error("PROTOCOL EXCEPTION");
                return null;
            }

            prepareRequest(translations, conn); //trows IOException
            checkResponseIsOk(conn); //trows IOException
            String httpResonse = getResponseAsString(conn); //trows IOException
            LOGGER.debug(httpResonse);

            List<String> result = parseJsonToStringList(httpResonse);

            LOGGER.warn(result);
            return result;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }

    private List<String> parseJsonToStringList(String jsonString) {
        //{"code":200,"lang":"en-ru","text":["Автомобильный адаптер постоянного тока батареи","","","","",""]}
        List<String> result = new LinkedList<>();
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray texts = jsonObject.getJSONArray("text");
        for (int i = 0; i < texts.length(); i++) {
            result.add(texts.getString(i));
        }

        return result;
    }

    private String getResponseAsString(HttpsURLConnection conn) throws IOException {
        StringBuilder responseSB = new StringBuilder();
        BufferedReader br =
                new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));

        String input;

        while ((input = br.readLine()) != null) {
            responseSB.append(input);
        }
        br.close();

        return responseSB.toString();
    }

    private void checkResponseIsOk(HttpsURLConnection conn) throws IOException {
        int httpResponseCode = conn.getResponseCode();

        if (httpResponseCode != 200) {
            String msg = String.format("Invalid HTTP response code: %d", httpResponseCode);
            throw new IOException(msg);
        }

        LOGGER.warn("Response Code : " + httpResponseCode);
    }

    private void prepareRequest(List<String> translations, HttpsURLConnection conn) throws IOException {
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("charset", "utf-8");

        String query = createQuery(translations);

        LOGGER.warn("POST SIZE IS: " + query.length());

        conn.setRequestProperty("Content-length", String.valueOf(query.length()));
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setInstanceFollowRedirects(false);

        DataOutputStream output = new DataOutputStream(conn.getOutputStream());

        output.writeBytes(query);

        output.close();
    }

    private String createQuery(List<String> descriptions) {
        List<String> queries = new LinkedList<>();
        for (String dsc : descriptions) {
            queries.add("text=" + encode(dsc));
        }

        return String.join("&", queries);

    }

    private String encode(String str) {
        if (StringUtils.isEmpty(str)) return "";
        return URLEncoder.encode(str);
    }
}

class ProductTranslator extends AbstractTranslator<ProductModel> implements Runnable {
    private final Locale SOURCE_LANG = Locale.ENGLISH;
    private final Locale TARGET_LANG = RUSSIAN_LOCALE;

    private Logger LOGGER = Logger.getLogger(ProductTranslator.class);

    private LinkedList<CrossboardProductAttributeModel> savedAttributesList;


    public ProductTranslator(ModelService modelService, List<ProductModel> products, String UrlString) {
        super(modelService, products, UrlString);
    }

    @Override
    public void run() {

        try {

            for (ProductModel p: modelList) {
                modelService.refresh(p);
                LOGGER.info("Processing product: " + p.getCode());
                if (p.getDescriptionStatus() == ProductDescriptionStatus.PROCESSED) {
                    LOGGER.debug(String.format("Skip product %s, cause description status is PROCESSED", p.getCode()));
                } else {
                    translate(p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void translate(ProductModel product) {
        List<String> toTranslate = collectStrings(product);
        if (toTranslate.size() < 1) {
            LOGGER.debug(String.format("All strings are already translated for product %s", product.getCode()));
            product.setDescriptionStatus(ProductDescriptionStatus.PROCESSED);
            LOGGER.debug("Change DescriptionStatus to PROCESSED");
            modelService.save(product);
            return;
        }
        List<String> translatedStrings = doTranslate(toTranslate);
        pasteTranslations(product, translatedStrings);

    }

    private void pasteTranslations(ProductModel product, List<String> translatedStrings) {

        String code = product.getCode();


        if (translatedStrings == null || translatedStrings.isEmpty()) {
            LOGGER.error(String.format("Product with code %s was not translated. Reason printed above.", code));
            return;
        }


        LOGGER.info(String.format("Going to paste %d translated strings to product with code = %s", translatedStrings.size(), code));
        Iterator<String> itr = translatedStrings.iterator();
        try {

            if (isEmptyString(product.getName(TARGET_LANG))) product.setName(itr.next(), TARGET_LANG);
            if (isEmptyString(product.getShortName(TARGET_LANG))) product.setShortName(itr.next(), TARGET_LANG);
            if (isEmptyString(product.getDescription(TARGET_LANG))){
                product.setDescription(itr.next(), TARGET_LANG);
                product.setTranslated(Boolean.TRUE);
            }
            if (isEmptyString(product.getShortDescription(TARGET_LANG)))
                product.setShortDescription(itr.next(), TARGET_LANG);
            if (isEmptyString(product.getBrand(TARGET_LANG))) product.setBrand(itr.next(), TARGET_LANG);
            if (isEmptyString(product.getModel(TARGET_LANG))) product.setModel(itr.next(), TARGET_LANG);


            for (CrossboardProductAttributeModel attr : savedAttributesList) {
                if (isEmptyString(attr.getName(TARGET_LANG))) attr.setName(itr.next(), TARGET_LANG);
                if (isEmptyString(attr.getValue(TARGET_LANG))) attr.setValue(itr.next(), TARGET_LANG);
                if (isEmptyString(attr.getUnit(TARGET_LANG))) attr.setUnit(itr.next(), TARGET_LANG);
            }
        } catch (NoSuchElementException e) {
            LOGGER.error(String.format("We need more translated strings for product < %s >", code));
            LOGGER.error(e.getMessage());
            modelService.refresh(product);
            product.setDescriptionStatus(ProductDescriptionStatus.ERROR);
            modelService.save(product);
            return;
        }


        if (itr.hasNext()) {
            LOGGER.error("Too many strings were translated. No more fields in product to paste translated strings");
            LOGGER.error(String.format("Can not save product with code = %s", code));
            modelService.refresh(product);
            product.setDescriptionStatus(ProductDescriptionStatus.ERROR);
            modelService.save(product);
        } else {
            LOGGER.info("Looks good. Iterator is empty. Saving product with code " + code);
            modelService.saveAll(savedAttributesList);
            product.setDescriptionStatus(ProductDescriptionStatus.PROCESSED);
            modelService.save(product);
        }
    }


    private List<String> collectStrings(ProductModel product) {
        String code = product.getCode();
        LOGGER.info("translating product with code = " + code);
        List<String> translations = new LinkedList<>();
        if (isEmptyString(product.getName(TARGET_LANG))) translations.add(product.getName(SOURCE_LANG));
        if (isEmptyString(product.getShortName(TARGET_LANG))) translations.add(product.getShortName(SOURCE_LANG));
        if (isEmptyString(product.getDescription(TARGET_LANG))) translations.add(product.getDescription(SOURCE_LANG));
        if (isEmptyString(product.getShortDescription(TARGET_LANG)))
            translations.add(product.getShortDescription(SOURCE_LANG));
        if (isEmptyString(product.getBrand(TARGET_LANG))) translations.add(product.getBrand(SOURCE_LANG));
        if (isEmptyString(product.getModel(TARGET_LANG))) translations.add(product.getModel(SOURCE_LANG));

        List<CrossboardProductAttributeModel> productAttributes = product.getAttributes();
        savedAttributesList = new LinkedList<>();
        for (CrossboardProductAttributeModel attr : productAttributes) {
            savedAttributesList.add(attr);
        }

        for (CrossboardProductAttributeModel attr : savedAttributesList) {
            if (isEmptyString(attr.getName(TARGET_LANG))) translations.add(attr.getName(SOURCE_LANG));
            if (isEmptyString(attr.getValue(TARGET_LANG))) translations.add(attr.getValue(SOURCE_LANG));
            if (isEmptyString(attr.getUnit(TARGET_LANG))) translations.add(attr.getUnit(SOURCE_LANG));
        }

        LOGGER.info(String.format("Going to translate %d strings", translations.size()));

        return translations;
    }

    private boolean isEmptyString(String str) {
        return StringUtils.isEmpty(str);
    }

}

class ValueCategoryTranslator extends AbstractTranslator<VariantValueCategoryModel> implements Runnable {
    private final Locale SOURCE_LANG = Locale.ENGLISH;
    private final Locale TARGET_LANG = RUSSIAN_LOCALE;

    private Logger LOGGER = Logger.getLogger(ValueCategoryTranslator.class);


    public ValueCategoryTranslator(ModelService modelService, List<VariantValueCategoryModel> categories, String UrlString) {
        super(modelService, categories, UrlString);
    }

    @Override
    public void run() {

        try {

            for (VariantValueCategoryModel category: modelList) {
                modelService.refresh(category);
                LOGGER.info("Processing variant value category: " + category.getCode());
                translate(category);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void translate(VariantValueCategoryModel category) {
        List<String> toTranslate = collectStrings(category);
        if (toTranslate.size() < 1) {
            LOGGER.debug(String.format("All strings are already translated for VariantValueCategory %s", category.getCode()));
            modelService.save(category);
            return;
        }
        List<String> translatedStrings = doTranslate(toTranslate);
        pasteTranslations(category, translatedStrings);
    }

    private void pasteTranslations(VariantValueCategoryModel category, List<String> translatedStrings) {

        String code = category.getCode();

        if (translatedStrings == null || translatedStrings.isEmpty()) {
            LOGGER.error(String.format("VariantValueCategory with code %s was not translated. Reason printed above.", code));
            return;
        }


        LOGGER.info(String.format("Going to paste %d translated strings to VariantValueCategory with code = %s", translatedStrings.size(), code));
        Iterator<String> itr = translatedStrings.iterator();
        try {

            if (isEmptyString(category.getName(TARGET_LANG))) category.setName(itr.next(), TARGET_LANG);

        } catch (NoSuchElementException e) {
            LOGGER.error(String.format("We need more translated strings for VariantValueCategory < %s >", code));
            LOGGER.error(e.getMessage());
            modelService.refresh(category);
            modelService.save(category);
            return;
        }


        if (itr.hasNext()) {
            LOGGER.error("Too many strings were translated. No more fields in VariantValueCategory to paste translated strings");
            LOGGER.error(String.format("Can not save VariantValueCategory with code = %s", code));
            modelService.refresh(category);
            modelService.save(category);
        } else {
            LOGGER.info("Looks good. Iterator is empty. Saving VariantValueCategory with code " + code);
            modelService.save(category);
        }
    }


    private List<String> collectStrings(VariantValueCategoryModel category) {
        String code = category.getCode();
        LOGGER.info("translating VariantValueCategory with code = " + code);
        List<String> translations = Collections.emptyList();

        if (isEmptyString(category.getName(TARGET_LANG))) {
            translations = Collections.singletonList(category.getName(SOURCE_LANG));
        }
        LOGGER.info(String.format("Going to translate %d strings", translations.size()));

        return translations;
    }

    private boolean isEmptyString(String str) {
        return StringUtils.isEmpty(str);
    }

}

class SalesUnitTranslator extends AbstractTranslator<UnitModel> implements Runnable {
    private final Locale SOURCE_LANG = Locale.ENGLISH;
    private final Locale TARGET_LANG = RUSSIAN_LOCALE;

    private Logger LOGGER = Logger.getLogger(ValueCategoryTranslator.class);


    public SalesUnitTranslator(ModelService modelService, List<UnitModel> units, String UrlString) {
        super(modelService, units, UrlString);
    }

    @Override
    public void run() {

        try {

            for (UnitModel unit: modelList) {
                modelService.refresh(unit);
                LOGGER.info("Processing unit: " + unit.getCode());
                translate(unit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void translate(UnitModel unit) {
        List<String> toTranslate = collectStrings(unit);
        if (toTranslate.size() < 1) {
            LOGGER.debug(String.format("All strings are already translated for unit %s", unit.getCode()));
            modelService.save(unit);
            return;
        }
        List<String> translatedStrings = doTranslate(toTranslate);
        pasteTranslations(unit, translatedStrings);
    }

    private void pasteTranslations(UnitModel unit, List<String> translatedStrings) {

        String code = unit.getCode();

        if (translatedStrings == null || translatedStrings.isEmpty()) {
            LOGGER.error(String.format("unit with code %s was not translated. Reason printed above.", code));
            return;
        }


        LOGGER.info(String.format("Going to paste %d translated strings to unit with code = %s", translatedStrings.size(), code));
        Iterator<String> itr = translatedStrings.iterator();
        try {

            if (isEmptyString(unit.getName(TARGET_LANG))) unit.setName(itr.next(), TARGET_LANG);

        } catch (NoSuchElementException e) {
            LOGGER.error(String.format("We need more translated strings for unit < %s >", code));
            LOGGER.error(e.getMessage());
            modelService.refresh(unit);
            modelService.save(unit);
            return;
        }


        if (itr.hasNext()) {
            LOGGER.error("Too many strings were translated. No more fields in unit to paste translated strings");
            LOGGER.error(String.format("Can not save unit with code = %s", code));
            modelService.refresh(unit);
            modelService.save(unit);
        } else {
            LOGGER.info("Looks good. Iterator is empty. Saving unit with code " + code);
            modelService.save(unit);
        }
    }


    private List<String> collectStrings(UnitModel unit) {
        String code = unit.getCode();
        LOGGER.info("translating unit with code = " + code);
        List<String> translations = Collections.emptyList();

        if (isEmptyString(unit.getName(TARGET_LANG))) {
            translations = Collections.singletonList(unit.getName(SOURCE_LANG));
        }
        LOGGER.info(String.format("Going to translate %d strings", translations.size()));

        return translations;
    }

    private boolean isEmptyString(String str) {
        return StringUtils.isEmpty(str);
    }

}
