package ru.ulmart.crossboard.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.CategorySource;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class CrossboardVariantsProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider, Serializable {

    private CategorySource categorySource;
    private FieldNameProvider fieldNameProvider;
    private CommonI18NService commonI18NService;
    private CategoryService categoryService;

    protected CategorySource getCategorySource()
    {
        return categorySource;
    }

    @Required
    public void setCategorySource(final CategorySource categorySource)
    {
        this.categorySource = categorySource;
    }

    protected FieldNameProvider getFieldNameProvider()
    {
        return fieldNameProvider;
    }

    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
    {
        this.fieldNameProvider = fieldNameProvider;
    }

    protected CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }

    private void processVariantProduct(VariantProductModel variantProduct, final Set<CategoryModel> variantValues,
                                       final CategoryModel categoryModel) {

        final ProductModel baseProduct = variantProduct.getBaseProduct();

        if (baseProduct.getSupercategories()
                .stream().anyMatch(sc -> sc.getCode().equalsIgnoreCase(categoryModel.getCode()))) {

            final Set<String> unfoldedCategories = new HashSet<>();
            baseProduct.getSupercategories()
                    .stream()
                    .filter(c -> !(c instanceof VariantCategoryModel))
                    .forEach(c -> unfoldedCategories.addAll(c.getUnfoldedVariantCategories()
                            .stream()
                            .map(CategoryModel::getCode)
                            .collect(Collectors.toSet())));

            baseProduct.getVariants().forEach(v -> {
                Boolean equalsToVariant = null;
                for (CategoryModel c : v.getSupercategories()) {
                    if ((c instanceof VariantValueCategoryModel) && unfoldedCategories.contains(c.getSupercategories().iterator().next().getCode())) {
                        VariantValueCategoryModel vvc = (VariantValueCategoryModel) c;
                        if (equalsToVariant == null) {
                            equalsToVariant = true;
                        }
                        equalsToVariant = equalsToVariant && variantProduct.getSupercategories()
                                .stream()
                                .filter(sc -> sc.getCode().equals(vvc.getCode()))
                                .count() > 0;
                    }
                }
                if (BooleanUtils.isTrue(equalsToVariant)) {
                    v.getSupercategories()
                            .stream()
                            .filter(vpc -> vpc.getSupercategories().stream().anyMatch(vpcs -> vpcs.getCode().equalsIgnoreCase(categoryModel.getCode())))
                            .forEach(variantValues::add);
                }
            });
        }

        Collection<CategoryModel> variantValueCategories = variantProduct.getSupercategories().stream()
                .filter(variantValue -> variantValue.getSupercategories().stream()
                        .anyMatch(variantCategory -> variantCategory.getCode().equalsIgnoreCase(categoryModel.getCode()))).collect(Collectors.toList());
        variantValues.addAll(variantValueCategories);
    }

    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model) throws FieldValueProviderException {
        final Collection<LanguageModel> languages = indexConfig.getLanguages();
        String propertyName = indexedProperty.getName();
        final CategoryModel categoryModel = categoryService.getCategoryForCode(propertyName);
        final Set<CategoryModel> variantValues = new HashSet<>();

        if (categoryModel != null) {
            if (model instanceof VariantProductModel) {
                VariantProductModel variant = (VariantProductModel) model;
                processVariantProduct(variant, variantValues, categoryModel);
            }
        }

        if (categoryModel != null && model instanceof ProductModel) {
            ProductModel product = (ProductModel) model;
            Collection<VariantProductModel> variants = product.getVariants();
            for (VariantProductModel variant : variants) {
                processVariantProduct(variant, variantValues, categoryModel);
            }
        }

        final Collection<FieldValue> fieldValues = new ArrayList<>();

        for (CategoryModel category :variantValues) {
            if (!languages.isEmpty()) {
               for (LanguageModel language : languages) {
                   fieldValues.addAll(createFieldValue(category, language, indexedProperty));
                  }
               } else{
                   fieldValues.addAll(createFieldValue(category, null, indexedProperty));
                }
            }

        if (fieldValues.isEmpty()) {
            return Collections.emptyList();
        }

        return fieldValues;
    }

    protected Object getPropertyValue(final Object model)
    {
        return getPropertyValue(model, "name");
    }

    protected Object getPropertyValue(final Object model, final String propertyName)
    {
        return modelService.getAttributeValue(model, propertyName);
    }

    protected List<FieldValue> createFieldValue(final CategoryModel category, final LanguageModel language,
                                                final IndexedProperty indexedProperty)
    {
        final List<FieldValue> fieldValues = new ArrayList<FieldValue>();

        if (language != null)
        {
            final Locale locale = i18nService.getCurrentLocale();
            Object value = null;
            try
            {
                i18nService.setCurrentLocale(getCommonI18NService().getLocaleForLanguage(language));
                value = getPropertyValue(category);
            }
            finally
            {
                i18nService.setCurrentLocale(locale);
            }

            final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, language.getIsocode());
            for (final String fieldName : fieldNames)
            {
                fieldValues.add(new FieldValue(fieldName, value));
            }
        }
        else
        {
            final Object value = getPropertyValue(category);
            final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
            for (final String fieldName : fieldNames)
            {
                fieldValues.add(new FieldValue(fieldName, value));
            }
        }

        return fieldValues;
    }


    public CategoryService getCategoryService() {
        return categoryService;
    }

    @Required
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}