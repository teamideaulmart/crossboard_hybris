package ru.ulmart.crossboard.core.daos.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.daos.CategoryDao;
import de.hybris.platform.category.model.CategoryModel;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Created by Alexey on 25.10.2016.
 */
public interface CrossboardCategoryDao extends CategoryDao {

    Collection<CategoryModel> getRootCategoriesForCatalogVersionAndNameTerm(@Nonnull final String categoryName,
                                                                            @Nonnull final CatalogVersionModel catalogVersion,
                                                                            int maxResultsCount);
}
