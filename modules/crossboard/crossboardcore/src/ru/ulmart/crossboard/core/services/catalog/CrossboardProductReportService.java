package ru.ulmart.crossboard.core.services.catalog;

/**
 * Created by max on 08.11.16.
 */
public interface CrossboardProductReportService {
     String getCSVData();
}
