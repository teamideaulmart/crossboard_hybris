package ru.ulmart.crossboard.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ImageValueProvider;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.enums.MediaDownloadStatus;

/**
 * Created by Marina on 28.09.2016.
 */
public class CrossboardImageValueProvider extends ImageValueProvider {

    private static final Logger LOG = Logger.getLogger(ImageValueProvider.class);

    @Override
    protected MediaModel findMedia(final ProductModel product, final MediaFormatModel mediaFormat)
    {
        if (product != null && mediaFormat != null)
        {
            final MediaModel mediaModel = product.getImages()
                    .stream()
                    .filter(media -> StringUtils.equals(media.getMediaFormat().getQualifier(), mediaFormat.getQualifier()))
                    .filter(media -> media.getDownloadStatus() != MediaDownloadStatus.NEW
                            && media.getDownloadStatus() != MediaDownloadStatus.ERROR
                            && media.getDownloadStatus() != MediaDownloadStatus.PROCESSING)
                    .sorted((l, r) -> Integer.compare(l.getPriority(), r.getPriority()))
                    .findFirst().orElse(null);

            if(mediaModel != null){
                return mediaModel;
            }

            // Failed to find media in product
            if (product instanceof VariantProductModel)
            {
                // Look in the base product
                return findMedia(((VariantProductModel) product).getBaseProduct(), mediaFormat);
            }

            // Failed to find media for base product get it from first variant
            if(product.getVariants() != null){
                for(VariantProductModel variant: product.getVariants()){
                    final MediaModel mediaModelVar = variant.getImages()
                            .stream()
                            .filter(media -> StringUtils.equals(media.getMediaFormat().getQualifier(), mediaFormat.getQualifier()))
                            .filter(media -> media.getDownloadStatus() != MediaDownloadStatus.NEW
                                    && media.getDownloadStatus() != MediaDownloadStatus.ERROR
                                    && media.getDownloadStatus() != MediaDownloadStatus.PROCESSING)
                            .sorted((l, r) -> Integer.compare(l.getPriority(), r.getPriority()))
                            .findFirst().orElse(null);

                    if(mediaModelVar != null){
                        return mediaModelVar;
                    }
                }
            }
        }
        return null;
    }
}
