package ru.ulmart.crossboard.core.services.catalog.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.impl.DefaultUnitService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.event.TranslateUnitEvent;
import ru.ulmart.crossboard.core.services.catalog.CrossboardUnitService;
import ru.ulmart.crossboard.core.services.translate.DescriptionTranslateService;

import java.util.Locale;

/**
 * Created by Marina on 17.09.2016.
 */
public class CrossboardUnitServiceImpl extends DefaultUnitService implements CrossboardUnitService {

    private final static Logger LOGGER = Logger.getLogger(CrossboardUnitServiceImpl.class);

    private EventService eventService;

    public UnitModel getOrCreateUnitModel(String unitCode, Locale locale, CatalogVersionModel catalogVersion) {
        UnitModel priceUnit = null;
        try {
            priceUnit = getUnitForCode(unitCode);
        } catch (UnknownIdentifierException uie) {
            LOGGER.info("Can't find unit '" + unitCode + "', creating new unit with name '" + unitCode + "'");
        }
        if (priceUnit == null) {
            priceUnit = getModelService().create(UnitModel.class);
            priceUnit.setCode(unitCode);
            priceUnit.setName(unitCode, locale);
            if (locale.equals(Locale.ENGLISH)) {
                priceUnit.setName("", Locale.forLanguageTag("ru"));
            }
            eventService.publishEvent(new TranslateUnitEvent(catalogVersion));
            priceUnit.setUnitType(unitCode);
            getModelService().save(priceUnit);
        }
        return priceUnit;
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
}
