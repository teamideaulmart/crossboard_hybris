package ru.ulmart.crossboard.core.services.search.impl;

import de.hybris.platform.util.Config;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.ulmart.crossboard.core.services.search.UlmartElasticSearchService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Oksana Kurushina on 12.10.2016.
 */
public class UlmartElasticSearchServiceImpl implements UlmartElasticSearchService {
    private static final Logger LOG = Logger.getLogger(UlmartElasticSearchServiceImpl.class);

    private static final String ULMART_PARTNER_LOGIN_URL = "http://api.ulmart.ru/api/u/v1_0/meta/partnerLogin";
    private static final String ULMART_CHANGE_CITY_URL = "http://api.ulmart.ru/api/u/v1_0/app/common/city/";
    private static final String PARTNER_LOGIN = "ulmart.elastic.search.login";
    private static final String PARTNER_PASSWORD = "ulmart.elastic.search.password";
    private static final String PARTNER_REFERRER = "ulmart.elastic.search.referrer";
    private static final String ELASTIC_SEARCH_REQUEST_TIMEOUT_MS = "ulmart.elastic.search.timeout.ms";
    private static final String JSESSIONID_COOKIE = "JSESSIONID";

    @Autowired
    private TIConfigUtils tiConfigUtils;

    public String getCookieByName(String cookieName, HttpServletRequest request) {
        String cookieValue = "";
        javax.servlet.http.Cookie[] cookies = request.getCookies();
        for (javax.servlet.http.Cookie cookie : cookies) {
            if (cookie.getName().equals(cookieName)) {
                cookieValue = cookie.getValue();
            }
        }
        return cookieValue;
    }

    public int sendGoodsAmountRequest(String url, String jsessionId, String term) {
        HttpClient client;
        Integer timeout = tiConfigUtils.getIntegerPropertyValue(ELASTIC_SEARCH_REQUEST_TIMEOUT_MS);
        if (timeout != null) {
            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(timeout)
                    .setSocketTimeout(timeout)
                    .build();
            client = HttpClientBuilder.create().setDefaultRequestConfig(config).useSystemProperties().build();
        } else {
            client = HttpClientBuilder.create().useSystemProperties().build();
        }

        List<NameValuePair> params = new LinkedList<>();
        params.add(new BasicNameValuePair("query", term));
        String paramString = URLEncodedUtils.format(params, "utf-8");
        HttpGet get = new HttpGet(url + "?" + paramString);
        get.setHeader("Cookie", JSESSIONID_COOKIE + "=" + jsessionId);
        HttpResponse response;
        try {
            LOG.info("Send request for goods amount");
            response = client.execute(get);
        } catch (Exception e){
            LOG.error(e.getMessage());
            return 0;
        }
        LOG.info("Response from ulmart api has been received");
        String result;
        try {
            result = getResponseAsString(response);
        } catch(IOException e) {
            LOG.error(e.getMessage());
            return 0;
        }

        JSONObject responseJson = null;
        try{
            responseJson = new JSONObject(result);}
        catch(JSONException ex){
            LOG.error(ex.getMessage());
            return -1;
        }

        if (responseJson == null || responseJson.has("error") && responseJson.getBoolean("error")) { //authenticate problem
            return -1;
        }

        return responseJson.getInt("goodsAmount");
    }

    private String getResponseAsString(HttpResponse response) throws IOException {
        BufferedReader rd;
        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        } catch (IOException ex) {
            LOG.error(ex);
            return "";
        }
        StringBuffer result = new StringBuffer();
        String line;
        try {
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException ex) {
            LOG.error(ex);
            return "";
        }
        return result.toString();
    }

    private JSONObject fillUserJson(String jsessionId) {
        JSONObject json = new JSONObject(), clientInfo = new JSONObject(), cookie = new JSONObject();

        json.put("login", Config.getParameter(PARTNER_LOGIN));
        json.put("password", Config.getParameter(PARTNER_PASSWORD));
        String ipAddress;
        try {
            ipAddress = InetAddress.getLocalHost().getHostAddress();
        } catch(UnknownHostException e) {
            ipAddress = "1.2.3.4";
        }
        clientInfo.put("ip", ipAddress);
        clientInfo.put("agent", "IE 9.0");
        clientInfo.put("referrer", Config.getParameter(PARTNER_REFERRER));
        List<JSONObject> cookies = new ArrayList<>();
        cookie.put(JSESSIONID_COOKIE, jsessionId);
        cookies.add(cookie);
        clientInfo.put("cookies", cookies);
        json.put("clientInfo", clientInfo);
        return json;
    }

    public String loginUlmartSite(String jsessionId, HttpServletResponse servletResponse) {
        JSONObject json = fillUserJson(jsessionId);
        HttpClient client = HttpClientBuilder.create().useSystemProperties().build();
        HttpPost post = new HttpPost(ULMART_PARTNER_LOGIN_URL);
        post.setHeader("Cookie", JSESSIONID_COOKIE + "=" + jsessionId);
        StringEntity requestEntity = new StringEntity(json.toString(), ContentType.APPLICATION_JSON);
        post.setEntity(requestEntity);
        HttpResponse response;
        try {
            LOG.info("Send request for login in ulmart api");
            response = client.execute(post);
        } catch (Exception e){
            LOG.error(e.getMessage());
            return "";
        }
        LOG.info("Response from ulmart api login has been received");
        String result;
        try {
            result = getResponseAsString(response);
        } catch(IOException e) {
            LOG.error(e.getMessage());
            return "";
        }
        JSONObject responseJson = null;
        try{
            responseJson = new JSONObject(result);}
        catch(JSONException ex){
            LOG.error(ex.getMessage());
            return "";
        }

        if (responseJson == null || responseJson.has("error") && responseJson.getBoolean("error")) {
            LOG.error("ElasticSearch: authenticate error");
            return "";
        }
        String newJsessionId = "";
        Header[] responseHeaders = response.getAllHeaders();
        for (Header header : responseHeaders) {
            if (header.getName().equals("Set-Cookie")) {
                HeaderElement headerEl = header.getElements()[0];
                if (headerEl.getName().equals(JSESSIONID_COOKIE)) {
                    javax.servlet.http.Cookie ulmartCookie = new javax.servlet.http.Cookie(headerEl.getName(), headerEl.getValue());
                    ulmartCookie.setPath("/");
                    ulmartCookie.setDomain(".ulmart.ru");
                    servletResponse.addCookie(ulmartCookie);
                    newJsessionId = headerEl.getValue();
                }
            }
        }
        return newJsessionId;
    }

    private int putCityUlmart(HttpServletRequest request, String jSessionId, String cityId) {
        if (cityId.isEmpty()) cityId = "18414";
        HttpClient client = HttpClientBuilder.create().useSystemProperties().build();
        String url = ULMART_CHANGE_CITY_URL + cityId;
        HttpPut put = new HttpPut(url);
        put.setHeader("Cookie", JSESSIONID_COOKIE + "=" + jSessionId);
        HttpResponse response;
        try {
            LOG.info("Send request for change city");
            response = client.execute(put);
        } catch (Exception e){
            LOG.error(e.getMessage());
            return 0;
        }
        LOG.info("Response from ulmart api has been received");
        String result;
        try {
            result = getResponseAsString(response);
        } catch(IOException e) {
            LOG.error(e.getMessage());
            return 0;
        }

        JSONObject responseJson = null;
        try{
            responseJson = new JSONObject(result);}
        catch(JSONException ex){
            LOG.error(ex.getMessage());
            return -1;
        }

        if (responseJson == null || responseJson.has("error") && responseJson.getBoolean("error")) {
            LOG.error("ElasticSearch: authenticate error");
            return -1;
        }
        return 0;
    }

    @Override
    public Integer getAutocompleteGoodsAmount(String urlParameter, String term, HttpServletRequest request, HttpServletResponse servletResponse) {
        String url = Config.getParameter(urlParameter);
        int goodsAmount = sendGoodsAmountRequest(url, getCookieByName(JSESSIONID_COOKIE, request), term);
        if (goodsAmount == -1) {
            String newJsessionId = loginUlmartSite(getCookieByName(JSESSIONID_COOKIE, request), servletResponse);
            if (newJsessionId.isEmpty()) newJsessionId = getCookieByName(JSESSIONID_COOKIE, request);
            putCityUlmart(request, newJsessionId, getCookieByName("city", request));
            goodsAmount = sendGoodsAmountRequest(url, newJsessionId, term);
        }
        return goodsAmount;
    }

    @Override
    public void putCity(String city, HttpServletRequest request, HttpServletResponse servletResponse) {
        if (putCityUlmart(request, getCookieByName(JSESSIONID_COOKIE, request), city) == -1) {
            String newJsessionId = loginUlmartSite(getCookieByName(JSESSIONID_COOKIE, request), servletResponse);
            putCityUlmart(request, newJsessionId, city);
        }
    }
}
