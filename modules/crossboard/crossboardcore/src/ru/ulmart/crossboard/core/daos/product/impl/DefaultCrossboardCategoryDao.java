package ru.ulmart.crossboard.core.daos.product.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.daos.impl.DefaultCategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import ru.ulmart.crossboard.core.daos.product.CrossboardCategoryDao;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexey on 25.10.2016.
 */
public class DefaultCrossboardCategoryDao extends DefaultCategoryDao implements CrossboardCategoryDao {
    /**
     * FIXME: Lookup category by name. This function used to search suggestions.
     * It's BAD practice. Urgently need to rewrite the search for category for
     * suggestions through solr index by categories
     */
    @Deprecated
    @Override
    public Collection<CategoryModel> getRootCategoriesForCatalogVersionAndNameTerm(@Nonnull final String categoryName,
                                                                                   @Nonnull final CatalogVersionModel catalogVersion,
                                                                                   int maxResultsCount)
    {
        final Map<String, Object> queryParameters = new HashMap<String, Object>();
        queryParameters.put("categoryName", "%" + categoryName + "%");
        queryParameters.put("catalogVersion", catalogVersion);

        final StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT {").append(CategoryModel.PK).append("} FROM {").append(CategoryModel._TYPECODE).append("}");
        queryBuilder.append(" WHERE lower({").append(CategoryModel.NAME).append("}) LIKE lower(?categoryName)");
        queryBuilder.append(" AND {").append(CategoryModel.CATALOGVERSION).append("} = (?catalogVersion)");

        final FlexibleSearchQuery fsuery = new FlexibleSearchQuery(queryBuilder.toString());
        fsuery.getQueryParameters().putAll(queryParameters);
        fsuery.setCount(maxResultsCount);
        final SearchResult<CategoryModel> result = getFlexibleSearchService().search(fsuery);
        return result.getResult();
    }
}
