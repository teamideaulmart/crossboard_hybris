package ru.ulmart.crossboard.core.services.media.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.TenantAwareThreadFactory;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import ru.ulmart.crossboard.core.daos.user.CrossboardProductDao;
import ru.ulmart.crossboard.core.enums.MediaDownloadStatus;
import ru.ulmart.crossboard.core.enums.ProductMediaStatus;
import ru.ulmart.crossboard.core.services.media.MediaDownloadService;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by max on 19.09.16.
 */

public class MediaDownloadServiceImpl implements MediaDownloadService {

    private static final String THREADPOOLSIZE = "teamidea.media.download.threadpoolsize";
    private static final int DEFAULT_THREADPULLSIZE = 4;
    private static final String BATCHSIZE = "teamidea.media.download.batchsize";
    private static final int DEFAULT_BATCHSIZE = 4;


    private ModelService modelService;
    private CrossboardProductDao productDao;
    private MediaService mediaService;

    private int threadPoolSize = 4;
    private int maxBatchSize = 3;
    private Logger LOGGER = Logger.getLogger(MediaDownloadService.class);

    public MediaDownloadServiceImpl() {
        try {
            threadPoolSize = Config.getInt(BATCHSIZE, 3);
        } catch (NumberFormatException e) {
            threadPoolSize = DEFAULT_THREADPULLSIZE;
            LOGGER.error("property " + THREADPOOLSIZE + " has wrong value, using default: " + DEFAULT_THREADPULLSIZE);
        }
        try {
            maxBatchSize = Config.getInt(BATCHSIZE, 4);
        } catch (NumberFormatException e) {
            maxBatchSize = DEFAULT_BATCHSIZE;
            LOGGER.error("property " + BATCHSIZE + " has wrong value, using default: " + DEFAULT_BATCHSIZE);
        }

    }

    @Override
    public void downloadMediasForProducts(List<String> productCodes, CatalogVersionModel catalogVersion) {
        LOGGER.info(String.format("Starting to download images for %1$s products",
                String.valueOf(productCodes.stream().collect(Collectors.joining(", ")))));

        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        final ExecutorService executorService = getExecutorService(threadPoolSize);

        List<ProductModel> products = new ArrayList<>();
        for (String code : productCodes) {

            try {
                ProductModel product = getProductModel(catalogVersion, code);
                products.add(product);
            } catch (RuntimeException ex) {
                LOGGER.error(ex.getMessage(), ex);
            }

            if (products.size() >= maxBatchSize) {
                MediaDownloader runner = new MediaDownloader(mediaService, modelService, products);
                products = new ArrayList<>();
                executorService.submit(runner);
            }

        }
        if (products.size() > 0) {
            MediaDownloader runner = new MediaDownloader(mediaService, modelService, products);
            executorService.submit(runner);
        }

        executorService.shutdown();

        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Error during downloading images executor await termination", e);
        }

        if(executorService.isTerminated()) {
            stopWatch.stop();
            LOGGER.info(String.format("Finished to download images for %1$s products in %2$s ms",
                    productCodes.stream().collect(Collectors.joining(", ")), stopWatch.getTime()));
        }
    }

    private ExecutorService getExecutorService(int threadPoolSize) {
        return Executors.newFixedThreadPool(threadPoolSize,
                new TenantAwareThreadFactory(Registry.getCurrentTenant(), JaloSession.getCurrentSession()));
    }

    private ProductModel getProductModel(CatalogVersionModel catalogVersion, String code) throws NullPointerException {
        ProductModel product = getProductDao().findProductByCode(catalogVersion, code);
        if (product == null) {
            LOGGER.warn("product with code <" + code + "> not found");
            throw new NullPointerException(String.format("Product with code '%1$s' not found!", code));
        }
        return product;
    }

///////////////////////////////////////////

    public ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public CrossboardProductDao getProductDao() {
        return productDao;
    }

    @Required
    public void setProductDao(CrossboardProductDao productDao) {
        this.productDao = productDao;
    }

    public MediaService getMediaService() {
        return mediaService;
    }

    @Required
    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }
}


class MediaDownloader implements Runnable {
    private MediaService mediaService;
    private ModelService modelService;
    private List<ProductModel> products;
    private Logger LOGGER = Logger.getLogger(MediaDownloader.class);


    public MediaDownloader(MediaService mediaService, ModelService modelService, List<ProductModel> products) {
        this.mediaService = mediaService;
        this.modelService = modelService;
        this.products = products;
    }

    @Override
    public void run() {
        try {

            for (ProductModel p : products) {
                modelService.refresh(p);
                LOGGER.info("Processing product: " + p.getCode());
                List<MediaModel> images = p.getImages();
                if (images != null && !images.isEmpty()) {
                    downloadImages(images, p);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void downloadImages(List<MediaModel> images, ProductModel p) {
        boolean successFlag = false;
        for (MediaModel img : images) {
            if (downloadSingleImage(img)) {
                LOGGER.info(String.format("Image for code %1$s and url %2$s has been successfully downloaded",
                        img.getCode(), img.getOriginalUrl()));
                successFlag = true;
            }
        }
        if (successFlag) {
            modelService.refresh(p);
            p.setMediaStatus(ProductMediaStatus.PROCESSED);
            modelService.save(p);
        }

    }

    private boolean downloadSingleImage(MediaModel img) {
        modelService.refresh(img);
        String url = img.getOriginalUrl();
        MediaDownloadStatus status = img.getDownloadStatus();
        if (status == MediaDownloadStatus.PROCESSING || status == MediaDownloadStatus.SUCCESS) {
            LOGGER.info(String.format("Image for code %1$s and url %2$s has been already downloaded earlier",
                    img.getCode(), img.getOriginalUrl()));
            return true;
        }

        img.setDownloadStatus(MediaDownloadStatus.PROCESSING);
        modelService.save(img);

        try {
            InputStream input = getDownloadInputStream(url);
            mediaService.setStreamForMedia(img, input);
            img.setDownloadStatus(MediaDownloadStatus.SUCCESS);
            img.setMime(this.receivedContentType);
            modelService.save(img);
            return true;
        } catch (IOException e) {
            img.setDownloadStatus(MediaDownloadStatus.ERROR);
            modelService.save(img);
            LOGGER.error("BAD URL: " + url);
            e.printStackTrace();
            return false;
        }

    }



    private static final int BUFFER_SIZE = 4096;
    private String receivedContentType = "";

    /**
     * Downloads a file from a URL
     * @param fileURL HTTP URL of the file to be downloaded
     * @throws IOException
     */
    public  InputStream getDownloadInputStream(String fileURL)
            throws IOException {
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setReadTimeout(5000);
        httpConn.setConnectTimeout(15000);
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            LOGGER.info("Content-Type = " + contentType);
            LOGGER.info("Content-Disposition = " + disposition);
            LOGGER.info("Content-Length = " + contentLength);
            LOGGER.info("fileName = " + fileName);



            if(!contentType.startsWith("image")) {
                LOGGER.error(" wrong Content-Type: " + contentType);
                throw new IOException(" wrong Content-Type: " + contentType);
            }
            this.receivedContentType = contentType;


            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
           // String saveFilePath = saveDir + File.separator + fileName;
                return inputStream;

        } else {
            LOGGER.error(" Server replied HTTP code: " + responseCode);
            throw new IOException("HTTP status is " + responseCode);
        }
    }


}
