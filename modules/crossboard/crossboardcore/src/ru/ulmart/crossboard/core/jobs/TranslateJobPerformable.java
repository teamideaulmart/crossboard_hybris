package ru.ulmart.crossboard.core.jobs;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.springframework.beans.factory.annotation.Required;
import ru.ulmart.crossboard.core.enums.ProductDescriptionStatus;
import ru.ulmart.crossboard.core.jobs.model.cronjob.TranslateCronJobModel;

import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.services.translate.DescriptionTranslateService;

import java.util.*;

/**
 * Created by max on 30.09.16.
 */
public class TranslateJobPerformable extends AbstractJobPerformable<TranslateCronJobModel> {
    private static final Logger LOG = Logger.getLogger(TranslateJobPerformable.class.getName());


    private DescriptionTranslateService descriptionTranslateService;
    private CatalogVersionService catalogVersionService;

    @Override
    public PerformResult perform(final TranslateCronJobModel cronJobModel) {
        Set<String> productCodes = new HashSet<>();
        LOG.info("Starting TranslateCronJob");
        List<CategoryModel> categories = cronJobModel.getCategories();
        for(CategoryModel cat:categories){
            List<ProductModel> products = cat.getProducts();
            for(ProductModel p:products){
                if(p.getDescriptionStatus() != ProductDescriptionStatus.PROCESSED) {
                    productCodes.add(p.getCode());
                }
            }
        }
        if(productCodes.size() < 1){
            LOG.warn("All product already translated (PROCESSED)");
        }else {
            List<String> codes = new LinkedList<>();
            codes.addAll(productCodes);
            LOG.info(String.format("Call descriptionTraslateService with %d products", productCodes.size()));
            CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("crossboardProductCatalog", "Online");
            descriptionTranslateService.translateProductDescriptions(codes,catalogVersion);
        }


        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }


    @Required
    public void setDescriptionTranslateService(DescriptionTranslateService descriptionTranslateService) {
        this.descriptionTranslateService = descriptionTranslateService;
    }

    @Required
    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

}
