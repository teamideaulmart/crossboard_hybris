package ru.ulmart.crossboard.core.util;

import java.util.regex.Pattern;

/**
 * Created by Timofey Klyubin on 05.10.16.
 */
public class Functions {
    private static final Pattern urlPattern = Pattern.compile("^https?://.+");
    private static final Pattern hostPattern = Pattern.compile("^\\w+\\.+\\w+.+");

    private static final String PRODUCT_URL_PLACEHOLDER = "<PRODUCT_URL>";
    private static final String USER_ID_PLACEHOLDER = "<USER_ID>";

    public static String sanitizeUrl(String url) {
        if (urlPattern.matcher(url.trim()).matches()) {
            return url.trim();
        }
        if (hostPattern.matcher(url.trim()).matches()) {
            return "http://" + url.trim();
        }
        return "";
    }

    public static String patternUrl(String url, String merchantSiteUrlPattern, String userId) {
        return merchantSiteUrlPattern.replaceAll(PRODUCT_URL_PLACEHOLDER, url).replaceAll(USER_ID_PLACEHOLDER,userId);
    }
}
