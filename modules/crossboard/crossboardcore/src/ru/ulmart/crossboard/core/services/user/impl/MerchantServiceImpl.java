package ru.ulmart.crossboard.core.services.user.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import ru.ulmart.crossboard.core.daos.user.MerchantDao;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.user.MerchantService;

import java.util.List;

/**
 * Created by Marina on 08.09.2016.
 */
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    private MerchantDao merchantDao;

    @Override
    public MerchantModel findMerchantForExternalCode(String externalCode) {
        Assert.notNull(externalCode, "Parameter externalCode cannot be null");

        return merchantDao.findMerchantForExternalCode(externalCode);
    }

    @Override
    public List<MerchantModel> findAllMerchants(){
        return merchantDao.findAllMerchants();
    }
}
