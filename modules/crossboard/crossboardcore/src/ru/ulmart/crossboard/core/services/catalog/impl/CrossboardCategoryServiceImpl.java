package ru.ulmart.crossboard.core.services.catalog.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.impl.CatalogUtils;
import de.hybris.platform.catalog.impl.DefaultCatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.servicelayer.daos.CMSPageDao;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.util.Assert;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.ulmart.crossboard.core.constants.CrossboardCoreConstants;
import ru.ulmart.crossboard.core.daos.product.CrossboardCategoryDao;
import ru.ulmart.crossboard.core.model.MerchantConfigModel;
import ru.ulmart.crossboard.core.model.MerchantModel;
import ru.ulmart.crossboard.core.services.catalog.CrossboardCategoryService;
import de.hybris.platform.util.localization.Localization;

import javax.annotation.Resource;
import java.util.Collection;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/**
 * Created by Marina on 06.09.2016.
 */
public class CrossboardCategoryServiceImpl extends DefaultCategoryService implements CrossboardCategoryService {

    private Logger LOGGER = Logger.getLogger(CrossboardCategoryServiceImpl.class);

    private static String PRODUCT_GRID_UID = "productGrid";
    private static String PRODUCT_LIST_UID = "productList";

    @Autowired
    private TIConfigUtils tiConfigUtils;

    @Autowired
    DefaultCatalogVersionService catalogVersionService;

    @Autowired
    TypeService typeService;

    @Autowired
    CMSPageService cmsPageService;

    @Autowired
    private CrossboardCategoryDao crossboardCategoryDao;

    @Override
    public CategoryModel getRootCategoryForCatalogVersionAndCategoriesType(final CatalogVersionModel catalogVersion, final String categoriesType) {
        Assert.notNull(catalogVersion, "Parameter catalogVersion cannot be null");
        Assert.notNull(categoriesType, "Parameter categoriesType cannot be null");

        final String rootCategoryParameter = String.format(CrossboardCoreConstants.CRBD_ROOT_CATEGORY_CONFIG_PARAMETER, categoriesType);
        final String rootCategoryCode = tiConfigUtils.getPropertyValue(rootCategoryParameter, CrossboardCoreConstants.CRBD_ROOT_CATEGORY_CONFIG_PARAMETER_DEFAULT);

        return getCategoryForCode(catalogVersion, rootCategoryCode);
    }

    @Override
    public MerchantConfigModel getMerchantConfigForCategoryAndMerchant(CategoryModel categoryModel, MerchantModel merchant) {
        Assert.notNull(categoryModel, "Parameter categoryModel cannot be null");
        Assert.notNull(merchant, "Parameter merchant cannot be null");

        for (MerchantConfigModel merchantConfigModel : categoryModel.getMerchantConfig()) {
            if (StringUtils.equals(merchantConfigModel.getMerchant().getCode(), merchant.getCode())) {
                return merchantConfigModel;
            }
        }

        LOGGER.warn(String.format("Merchant config for category %1$s and merchant %2$s not found", categoryModel.getCode(), merchant.getCode()));
        return null;
    }

    @Override
    public VariantCategoryModel getVariantCategoryByCode(final CatalogVersionModel catalogVersion, final String code) {
        final CategoryModel category = getCategoryForCode(catalogVersion, code);
        if (category instanceof VariantCategoryModel) {
            return (VariantCategoryModel) category;
        } else {
            LOGGER.error(String.format("Category '%1$s' is not a VariantCategory!", code));
            throw new IllegalArgumentException(String.format("Category '%1$s' is not a VariantCategory!", code));
        }
    }

    @Override
    public VariantValueCategoryModel getOrCreateVariantValueCategory(CatalogVersionModel catalogVersion, String code) {
        CategoryModel category = null;
        try {
            category = getCategoryForCode(catalogVersion, code);
            if (!(category instanceof VariantValueCategoryModel)) {
                LOGGER.error(String.format("Category '%1$s' is not a VariantValueCategoryModel!", code));
                throw new IllegalArgumentException(String.format("Category '%1$s' is not a VariantValueCategoryModel!", code));
            }
        } catch (UnknownIdentifierException e) {
            LOGGER.info(String.format("VariantValueCategory with code '%1$s' not found, creating new VariantValueCategory", code));
        }
        if (category == null) {
            category = getModelService().create(VariantValueCategoryModel.class);
            category.setCatalogVersion(catalogVersion);
            category.setCode(code);
        }
        //noinspection ConstantConditions
        return (VariantValueCategoryModel) category;
    }

    @Override
    public boolean isImportAllowedForCategoryAndMerchant(CategoryModel categoryModel, MerchantModel merchant) {
        Assert.notNull(categoryModel, "Parameter categoryModel cannot be null");
        Assert.notNull(merchant, "Parameter merchant cannot be null");

        final MerchantConfigModel merchantConfigModel = getMerchantConfigForCategoryAndMerchant(categoryModel, merchant);

        if (merchantConfigModel != null) {
            return merchantConfigModel.getImportAllowed() != null
                    ? merchantConfigModel.getImportAllowed().booleanValue()
                    : false;
        }

        return false;
    }

    @Override
    public CategoryPageModel getCategoryPageByViewType(final Integer viewType) {
        String uid = viewType == 1 ? PRODUCT_LIST_UID : PRODUCT_GRID_UID;
        try {
            return (CategoryPageModel)cmsPageService.getPageForId(uid);
        } catch (CMSItemNotFoundException e) {
            LOGGER.error("No category page for viewType: [" + viewType + "] found.");
        }
        return null;
    }

    @Override
    public Collection<CategoryModel> getRootCategoriesForCatalogVersionsAndNameTerm(final String categoryName, int maxResultsCount) {
        final CatalogVersionModel catalogVersion = catalogVersionService.getSessionCatalogVersionForCatalog(tiConfigUtils.getPropertyValue(CrossboardCoreConstants.PRODUCT_DEFAULT_CATALOG));
        /**
         * FIXME: Lookup category by name. This function used to search
         * suggestions. It's BAD practice. Urgently need to rewrite the search
         * for category for suggestions through solr index
         */
        return crossboardCategoryDao.getRootCategoriesForCatalogVersionAndNameTerm(categoryName, catalogVersion, maxResultsCount);
    }

    @Override
    public CategoryModel getCategoryForCode(final String code)
    {
        validateParameterNotNull(code, "Parameter 'code' was null.");
        final Collection<CategoryModel> categories = getCategoriesForCode(code);
        if (categories.isEmpty())
        {
            throw new UnknownIdentifierException(Localization.getLocalizedString("crossboard.error.category_not_found", new Object[]{code}));
        }
        else if (categories.size() > 1)
        {
            throw new AmbiguousIdentifierException("Category with code '" + code + "' is not unique. " + categories.size()
                    + " categories found! (Active session catalogversions: " + getCatalogVersionsString() + ")");
        }
        return categories.iterator().next();
    }

    private String getCatalogVersionsString()
    {
        return CatalogUtils.getCatalogVersionsString(catalogVersionService.getSessionCatalogVersions());
    }
}
