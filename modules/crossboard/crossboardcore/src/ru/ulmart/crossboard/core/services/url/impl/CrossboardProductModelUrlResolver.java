package ru.ulmart.crossboard.core.services.url.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Timofey Klyubin on 15.11.16.
 */
public class CrossboardProductModelUrlResolver extends DefaultProductModelUrlResolver {

    private final static Logger LOG = Logger.getLogger(CrossboardProductModelUrlResolver.class);

    @Override
    protected String resolveInternal(ProductModel source) {
        final ProductModel baseProduct = getBaseProduct(source);

        final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

        String url = getPattern();

        if (currentBaseSite != null && url.contains("{baseSite-uid}"))
        {
            url = url.replace("{baseSite-uid}", currentBaseSite.getUid());
        }
        if (url.contains("{category-path}"))
        {
            url = url.replace("{category-path}", buildPathString(getCategoryPath(baseProduct)));
        }
        if (url.contains("{product-name}"))
        {
            url = url.replace("{product-name}", urlSafe(baseProduct.getName()));
        }
        if (url.contains("{product-code}"))
        {
            url = url.replace("{product-code}", urlSafe(source.getCode()));
        }

        return url;
    }

    @Override
    protected String urlSafe(String text) {
        if (StringUtils.isEmpty(text)) {
            return "";
        }

        String encodedText = null;
        try {
            encodedText = URLEncoder.encode(text, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            LOG.warn("UTF-8 is not supported", e);
        }
        return encodedText == null ? super.urlSafe(text) : encodedText;
    }
}
