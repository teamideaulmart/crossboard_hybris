/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package ru.ulmart.crossboard.core.constants;

/**
 * Global class for all CrossboardCore constants. You can add global constants for your extension into this class.
 */
public final class CrossboardCoreConstants extends GeneratedCrossboardCoreConstants
{
	public static final String EXTENSIONNAME = "crossboardcore";

	private CrossboardCoreConstants()
	{
		//empty
	}

	public static final String CRBD_ROOT_CATEGORY_CONFIG_PARAMETER = "product.catalog.%1$s.root.category";
	public static final String CRBD_ROOT_CATEGORY_CONFIG_PARAMETER_DEFAULT = "hyb_categories";
	public static final int BASE_IMAGE_MEDIA_PRIORITY = 0;

	public static final String PRODUCTS_HISTORY = "products.history"; //Cookie name
	public static final String VIEW_TYPE = "viewType"; //Cookie name

	public static final String PRODUCT_DEFAULT_CATALOG = "integration.catalog.name";
	public static final String LAST_ERROR_MESSAGE_TIME = "lastErrorMessage"; //Cookie name
}
