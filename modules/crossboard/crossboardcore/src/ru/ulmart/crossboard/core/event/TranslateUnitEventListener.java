package ru.ulmart.crossboard.core.event;

import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.log4j.Logger;
import ru.ulmart.crossboard.core.services.translate.DescriptionTranslateService;

import java.util.Collections;

/**
 * Created by Timofey Klyubin on 10.10.16.
 */
public class TranslateUnitEventListener extends AbstractEventListener<TranslateUnitEvent> {

    private static final Logger LOG = Logger.getLogger(TranslateUnitEventListener.class);

    private DescriptionTranslateService descriptionTranslateService;
    private FlexibleSearchService flexibleSearchService;

    @Override
    protected void onEvent(TranslateUnitEvent translateUnitEvent) {
        LOG.info("TranslateUnitEvent fired");

        String query = "SELECT {c."+ UnitModel.CODE +
                "} FROM {" + UnitModel._TYPECODE +
                " as c} WHERE {c." + UnitModel.NAME + "[ru]}=''";
        FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.setResultClassList(Collections.singletonList(String.class));
        searchQuery.setCatalogVersions(translateUnitEvent.getCatalogVersion());
        SearchResult<String> result = flexibleSearchService.search(searchQuery);
        descriptionTranslateService.translateSalesUnits(result.getResult(), translateUnitEvent.getCatalogVersion());
    }

    public void setDescriptionTranslateService(DescriptionTranslateService descriptionTranslateService) {
        this.descriptionTranslateService = descriptionTranslateService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
