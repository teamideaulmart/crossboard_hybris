package ru.ulmart.crossboard.core.services.catalog.impl;

import com.google.common.collect.ImmutableMap;
import de.hybris.platform.category.model.CategoryModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.WebUtils;
import ru.teamidea.utils.util.TIConfigUtils;
import ru.ulmart.crossboard.core.constants.CrossboardCoreConstants;
import ru.ulmart.crossboard.core.enums.CategoryViewType;
import ru.ulmart.crossboard.core.services.catalog.CrossboardViewTypeService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by vitalii on 13.10.16.
 */
public class CrossboardViewTypeServiceImpl implements CrossboardViewTypeService {

    private TIConfigUtils tiConfigUtils;

    private Map<Integer, CategoryViewType> viewTypeMap;

    @Override
    public Cookie getViewTypeCookie(HttpServletRequest request, CategoryModel category, Integer viewTypeCode) {
        Cookie viewTypeCookie = WebUtils.getCookie(request, CrossboardCoreConstants.VIEW_TYPE);
        if (viewTypeCookie == null) {
            viewTypeCookie = new Cookie(CrossboardCoreConstants.VIEW_TYPE, "");
        }

        if (viewTypeCode != null && getViewTypeMap().containsKey(viewTypeCode)) {
            viewTypeCookie.setValue(viewTypeCode.toString());
        } else if (StringUtils.isEmpty(viewTypeCookie.getValue())) {
            if (category.getViewType() != null) {
                viewTypeCookie.setValue(String.valueOf(category.getViewType().ordinal()));
            } else {
                String defaultViewType = tiConfigUtils.getPropertyValue("category.viewtype", "TILE");
                int numValue = CategoryViewType.valueOf(defaultViewType).ordinal();
                viewTypeCookie.setValue(String.valueOf(numValue));
            }
        }

        viewTypeCookie.setMaxAge(60 * 60 * 24 * 365);
        viewTypeCookie.setPath("/");

        return viewTypeCookie;
    }

    @Override
    public Cookie getViewTypeCookie(HttpServletRequest request, Integer viewTypeCode) {
        Cookie viewTypeCookie = WebUtils.getCookie(request, CrossboardCoreConstants.VIEW_TYPE);
        if (viewTypeCookie == null) {
            viewTypeCookie = new Cookie(CrossboardCoreConstants.VIEW_TYPE, "");
        }

        if (viewTypeCode != null && getViewTypeMap().containsKey(viewTypeCode)) {
            viewTypeCookie.setValue(viewTypeCode.toString());
        } else if (StringUtils.isEmpty(viewTypeCookie.getValue())) {
            String defaultViewType = tiConfigUtils.getPropertyValue("category.viewtype", "TILE");
            int numValue = CategoryViewType.valueOf(defaultViewType).ordinal();
            viewTypeCookie.setValue(String.valueOf(numValue));
        }

        viewTypeCookie.setMaxAge(60 * 60 * 24 * 365);
        viewTypeCookie.setPath("/");

        return viewTypeCookie;
    }

    private Map<Integer, CategoryViewType> getViewTypeMap() {
        if (viewTypeMap == null) {
            ImmutableMap.Builder<Integer, CategoryViewType> mapBuilder = ImmutableMap.builder();
            for (CategoryViewType categoryViewType : CategoryViewType.values()) {
                mapBuilder.put(categoryViewType.ordinal(), categoryViewType);
            }
            viewTypeMap = mapBuilder.build();
        }
        return viewTypeMap;
    }

    public void setTiConfigUtils(TIConfigUtils tiConfigUtils) {
        this.tiConfigUtils = tiConfigUtils;
    }
}