package ru.teamidea.crossboard.b2c.integration.beans;

/**
 * Created by Marina on 13.09.2016.
 */
import java.io.Serializable;

public class ServiceBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String user;
    private String productCatalogName;
    private String productCatalogVersion;
    private int collectionSize;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getProductCatalogName() {
        return productCatalogName;
    }

    public void setProductCatalogName(String productCatalogName) {
        this.productCatalogName = productCatalogName;
    }

    public String getProductCatalogVersion() {
        return productCatalogVersion;
    }

    public void setProductCatalogVersion(String productCatalogVersion) {
        this.productCatalogVersion = productCatalogVersion;
    }

    public int getCollectionSize() {
        return collectionSize;
    }

    public void setCollectionSize(int collectionSize) {
        this.collectionSize = collectionSize;
    }

}