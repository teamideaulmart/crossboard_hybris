package ru.teamidea.crossboard.b2c.integration.aspect;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import ru.teamidea.crossboard.b2c.integration.beans.ServiceBean;

/**
 * Created by Marina on 19.09.2016.
 */

@Aspect
public class SessionDependentExecutionAspect {

    protected static final Logger LOG = Logger.getLogger(SessionDependentExecutionAspect.class);

    @Autowired
    private ServiceBean serviceBean;

    @Autowired
    private UserService userService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private CatalogVersionService catalogVersionService;

    private volatile UserModel user;

    @Around("@annotation(ru.teamidea.crossboard.b2c.integration.aspect.ExecuteInSession)")
    public Object executeInSession(ProceedingJoinPoint joinPoint) throws Throwable {

        if(LOG.isDebugEnabled()){
            LOG.debug("Start executing in local session");
        }

        Session localSession = null;
        try
        {
            localSession = getSessionService().createNewSession();
            getUserService().setCurrentUser(getUser());
            getCatalogVersionService().setSessionCatalogVersion(getServiceBean().getProductCatalogName(),
                    getServiceBean().getProductCatalogVersion());

            return joinPoint.proceed();
        }
        finally
        {
            if (localSession != null)
            {
                getSessionService().closeSession(localSession);
            }

            if(LOG.isDebugEnabled()){
                LOG.debug("End executing in local session");
            }
        }
    }

    public CatalogVersionService getCatalogVersionService()
    {
        return catalogVersionService;
    }

    public SessionService getSessionService()
    {
        return sessionService;
    }

    public UserService getUserService()
    {
        return userService;
    }

    public ServiceBean getServiceBean() {
        return serviceBean;
    }

    protected UserModel getUser() {
        if (user == null) {
            user = userService.getUserForUID(serviceBean.getUser());
        }
        return user;
    }
}