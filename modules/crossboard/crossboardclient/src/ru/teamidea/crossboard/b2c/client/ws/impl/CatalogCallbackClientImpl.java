package ru.teamidea.crossboard.b2c.client.ws.impl;

import org.apache.log4j.Logger;
import ru.teamidea.crossboard.b2c.client.constants.CrossboardclientConstants;
import ru.teamidea.crossboard.b2c.client.handlers.LogHandler;
import ru.teamidea.crossboard.b2c.client.ws.CatalogCallbackClient;
import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.cb.merchant.callback.ws.CrossboardCallbackService;
import ru.ulmart.cb.merchant.callback.ws.CrossboardCallbackService_Service;
import ru.ulmart.crossboard.core.model.MerchantModel;

import javax.annotation.PostConstruct;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import java.util.List;
import java.util.Map;

/**
 * Created by Marina on 06.09.2016.
 */
public class CatalogCallbackClientImpl implements CatalogCallbackClient {

    private static final Logger LOGGER = Logger.getLogger(CatalogCallbackClientImpl.class);

    private CrossboardCallbackService crossboardCallbackService;

    private Map<String, String>  merchantEndpointMap;

    @PostConstruct
    public void init() {
        LOGGER.info("Initialize soap client for CrossboardCallbackService");

        final CrossboardCallbackService_Service service = new CrossboardCallbackService_Service();
        crossboardCallbackService = service.getPort(CrossboardCallbackService.class);
        final javax.xml.ws.Binding binding = ((BindingProvider) crossboardCallbackService).getBinding();
        final List<Handler> handlerChain = binding.getHandlerChain();
        handlerChain.add(new LogHandler());
        binding.setHandlerChain(handlerChain);
    }

    @Override
    public void sendCallback(final CallbackRequest callbackRequest, final MerchantModel merchantModel) {
        final String merchantCatalogCallbackEndpoint = merchantEndpointMap.get(String.format(CrossboardclientConstants.MERCHANT_CALLBACK_INTERFACE_CONFIG_PARAM, merchantModel.getUid()));
        final String merchantCatalogCallbackUserName = merchantEndpointMap.get(String.format(CrossboardclientConstants.MERCHANT_CALLBACK_USERNAME_CONFIG_PARAM, merchantModel.getUid()));
        final String merchantCatalogCallbackPassword = merchantEndpointMap.get(String.format(CrossboardclientConstants.MERCHANT_CALLBACK_PASSWORD_CONFIG_PARAM, merchantModel.getUid()));

        ((BindingProvider) crossboardCallbackService).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                merchantCatalogCallbackEndpoint);
        ((BindingProvider) crossboardCallbackService).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, merchantCatalogCallbackUserName);
        ((BindingProvider) crossboardCallbackService).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, merchantCatalogCallbackPassword);

        LOGGER.info(String.format("Send callback request: type = [%1$s], endpoint = [%2$s]", callbackRequest.getCalbackType(), merchantCatalogCallbackEndpoint));

        crossboardCallbackService.processCallback(callbackRequest);
    }

    public void setMerchantEndpointMap(Map<String, String> merchantEndpointMap) {
        this.merchantEndpointMap = merchantEndpointMap;
    }
}
