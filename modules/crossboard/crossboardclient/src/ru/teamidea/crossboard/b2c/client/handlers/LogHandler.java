package ru.teamidea.crossboard.b2c.client.handlers;

import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

/**
 * Created by Denis Zevakhin <zevakhin@teamidea.ru> on 21.04.16.
 */
public class LogHandler implements SOAPHandler<SOAPMessageContext> {
    private final Logger LOGGER = Logger.getLogger(LogHandler.class);
    private static final String ENCODING = "utf-8";

    @Override
    public Set<QName> getHeaders() {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        log(context);
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        log(context);
        return true;
    }

    /*
     * Check the MESSAGE_OUTBOUND_PROPERTY in the context
     * to see if this is an outgoing or incoming message.
     * Write a brief message to the print stream and
     * output the message. The writeTo() method can throw
     * SOAPException or IOException
     */
    private void log(SOAPMessageContext smc) {
        if (LOGGER.isDebugEnabled()) {
            Boolean outboundProperty = (Boolean)
                    smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

            if (outboundProperty.booleanValue()) {
                LOGGER.debug("OUTBOUND MESSAGE:");
            } else {
                LOGGER.debug("INBOUND MESSAGE:");
            }

            LOGGER.debug("### Context values ###");
            smc.keySet().stream().forEach(k -> {
                LOGGER.debug(String.format("%s -> %s", k, smc.get(k)));
            });
            LOGGER.debug("Message:");

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            try {
                SOAPMessage msg = smc.getMessage();
                msg.writeTo(os);
                LOGGER.debug(new String(os.toByteArray(), ENCODING));
            } catch (Exception e) {
                LOGGER.debug("Error while serializing request", e);
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    LOGGER.debug("Error while closing stream", e);
                }
            }
        }
    }

    @Override
    public void close(MessageContext context) {
        // nothing to do
    }
}
