/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package ru.teamidea.crossboard.b2c.client.constants;

/**
 * Global class for all Crossboardclient constants. You can add global constants for your extension into this class.
 */
public final class CrossboardclientConstants extends GeneratedCrossboardclientConstants
{
	public static final String EXTENSIONNAME = "crossboardclient";

	private CrossboardclientConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "crossboardclientPlatformLogo";

	public static final String MERCHANT_CALLBACK_INTERFACE_CONFIG_PARAM = "merchant.%1$s.callback.endpoint";
	public static final String MERCHANT_CALLBACK_USERNAME_CONFIG_PARAM = "merchant.%1$s.callback.username";
	public static final String MERCHANT_CALLBACK_PASSWORD_CONFIG_PARAM = "merchant.%1$s.callback.password";
}
