package ru.teamidea.crossboard.b2c.client.ws;

import ru.ulmart.cb.common.CallbackRequest;
import ru.ulmart.crossboard.core.model.MerchantModel;

/**
 * Created by Marina on 06.09.2016.
 */
public interface CatalogCallbackClient {
    /**
     * Status codes to use in callback responses
     */
    String SUCCESS = "0";
    String VALIDATION_ERROR = "1";
    String PERSISTENCE_ERROR = "2";
    String INTERNAL_SERVER_ERROR = "3";
    String UNKNOWN_ERROR = "4";


    void sendCallback(final CallbackRequest callbackRequest, final MerchantModel merchantModel);
}
